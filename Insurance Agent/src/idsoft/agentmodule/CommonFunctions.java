package idsoft.agentmodule;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.provider.Settings;
import android.text.Html;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

public class CommonFunctions {

	public String NAMESPACE = "http://tempuri.org/";
//	public String URL = "http://72.15.221.151:92/AgencyWeb.asmx";//UAT
	public String URL = "http://72.15.221.153:92/AgencyWeb.asmx";//LIVE

	public static final String LoginPage = "LoginPage";
	public static final String AgentInformation = "AgentInformation";
	public static final String AgentInspection = "agentinspection";
	public static final String DashBoard = "DashBoard";
	public static final String DashBoardWeek = "DashBoardWeek";
	public static final String DashBoardMonth = "DashBoardMonth";
	public static final String DashBoardToday = "DashBoardToday";
	public static final String AgentTable = "AgentTable";
	public static final String OnlineSyncPolicyInfo_Agency = "OnlineSyncPolicyInfo_Agency";
	public static final String LoadInspectionCategory = "LoadInspectionCategory";
	public static final String LoadInspectionType = "LoadInspectionType";
	public static final String LoadInspectionType2 = "LoadInspectionType2";
	public static final String LoadCompany = "LoadCompany";
	public static final String LoadInspectors = "LoadInspectors";
	public static final String LoadInspectionStatus = "LoadInspectionStatus";
	public static final String LoadInspectionSubStatus = "LoadInspectionSubStatus";
	public static final String LoadCaptionValue = "LoadCaptionValue";
	public static final String LoadbuildingType = "LoadbuildingType";
	public static final String LoadInsuranceCarrier = "LoadInsuranceCarrier";
	public static final String AddAImage = "AddAImage";
	public static final String AgentInspection_Pdf = "AgentInspection_Pdf";
	public static final String Property_Count = "Property_Count";
	public static final String Customer_Details = "Customer_Details";
	public static final String Version = "Version";
	public static final String caption = "caption";
	public static final String GetAgencyName = "GetAgencyName";
	public static final String VehicleInspection_Pdf = "VehicleInspection_Pdf";
	public String MY_DATABASE_NAME = "Agent_Database.db";
	SQLiteDatabase db;
	Context con;
	public CharSequence datewithtime;
	public android.text.format.DateFormat df;
	public ProgressDialog pd;
	public String colorname, status, agentid, alerttitle, alertcontent,
			agentname,deviceId,model,manuf,devversion,apiLevel,versionname,agency_name;
	public String inspection_id, inspection_name, inspection_type, awaiting,
			scheduled, inspected, reports_ready, suspended, unsuspended, other,
			total;
	public int maxBarValue = 0; // Maximum value of horizontal progress bar
	public int typeBar = 1, mState, usercheck;
	public static int RUNNING = 1;
	public int delay = 40; // Milliseconds of delay in the update loop
	public int total_bar, noofdatas, show_handler,ipAddress;
	ShowToast toast;
	
	public static Integer[] mThumbIds = {R.drawable.phonenum,R.drawable.inspectiondepotimage};
	
	public String[] arrayid, arraycategory, array_Cat_ID, array_Insp_ID,
			array_InspName, array_Type_ID, arraystateid, arraystatename,
			arraycountyid, arraycountyname, arraycompanyid, arraycompanyname,
			arrayinspectorname, arrayinspectorid, array_statusid,
			array_statusname, array_substatusid, pdfpath;
	
	public String[] array_zipcode;
	public boolean value=true;

	CommonFunctions(Context con) {
		db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		this.con = con;
		df = new android.text.format.DateFormat();
		datewithtime = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date());
		CreateTable(0);
		CreateTable(1);
		CreateTable(2);
		CreateTable(3);
		CreateTable(4);
		CreateTable(5);
		CreateTable(6);
		CreateTable(7);
		CreateTable(8);
		CreateTable(9);
		CreateTable(10);
		CreateTable(11);
		CreateTable(12);
		CreateTable(13);
		CreateTable(14);
		CreateTable(15);
		CreateTable(16);
		CreateTable(17);
		CreateTable(18);
		CreateTable(19);
		CreateTable(20);
		CreateTable(21);
		CreateTable(22);
		
		

	}

	public void CreateTable(int id) {
		switch (id) {
		case 0:
			try {
				db.execSQL("create table if not exists "
						+ LoginPage
						+ " (id integer primary key autoincrement,username varchar2(50),password varchar2(50),agentid varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 1:
			try {
				db.execSQL("create table if not exists "
						+ AgentInformation
						+ " (id integer primary key autoincrement,Agentid varchar2(50),"
						+ "FirstName varchar2(50),LastName varchar2(50),AgencyName varchar2(50),"
						+ "S_address1 varchar2(50),s_address2 varchar2(50),s_city varchar2(50),"
						+ "AgencyState varchar2(50),AgencyCounty varchar2(50),s_zip varchar2(50),"
						+ "License varchar2(50),AgencyWorkPhone varchar2(50),AgencyFaxPhone varchar2(50),"
						+ "Email varchar2(50),CitizenAgencyID varchar2(50),s_username varchar2(50),"
						+ "s_password varchar2(50),AgentStatus varchar2(50),"
						+ "RolesDesc varchar2(50),website varchar2(50),Img_Headshot blob,Img_Agencylogo blob," +
						  "AgencyEmail varchar2(50),Agent_id_2 varchar2(50),Agencyid varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 2:
			try {
				db.execSQL("create table if not exists "
						+ DashBoard
						+ " (id integer primary key autoincrement,inspid varchar2(50),inspname varchar2(50),insptype varchar2(50),awaiting varchar2(50),scheduled varchar2(50),inspected varchar2(50),reportsready varchar2(50),suspended varchar2(50),unsuspended varchar2(50),other varchar2(50),total varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 3:
			try {
				db.execSQL("create table if not exists "
						+ OnlineSyncPolicyInfo_Agency
						+ " (id integer primary key autoincrement,SRID varchar2(50),FirstName varchar2(50),"
						+ "LastName varchar2(50),MiddleName varchar2(50),Address1 varchar2(50),Address2 varchar2(50),"
						+ "City varchar2(50),StateId varchar2(50),State varchar2(50),Country varchar2(50),"
						+ "Zip varchar2(50),HomePhone varchar2(50),CellPhone varchar2(50),WorkPhone varchar2(50),"
						+ "Email varchar2(50),ContactPerson varchar2(50),OwnerPolicyNo varchar2(50),"
						+ "Status varchar2(50),SubStatusID varchar2(50),statefarmcompanyid varchar2(50),"
						+ "InspectorId varchar2(50),wId varchar2(50),CompanyId varchar2(50),"
						+ "InspectorFirstName varchar2(50),InspectorLastName varchar2(50),"
						+ "ScheduledDate varchar2(50),YearBuilt varchar2(50),Nstories varchar2(50),"
						+ "InspectionTypeId varchar2(50),ScheduleCreatedDate varchar2(50),"
						+ "AssignedDate varchar2(50),InspectionStartTime varchar2(50),"
						+ "InspectionEndTime varchar2(50),InspectionComment varchar2(50),"
						+ "IsInspected varchar2(50),InsuranceCompany varchar2(50),s_InspFees varchar2(50),"
						+ "InsuranceAgentName varchar2(50),AgentEmail varchar2(50),InsuranceAgencyName varchar2(50),"
						+ "AgencyEmail varchar2(50),COMMpdf varchar2(50),COMMMergedpdf varchar2(50),RoofPdfPath varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 4:
			try {
				db.execSQL("create table if not exists "
						+ LoadInspectionCategory
						+ " (ID varchar2(50),Category varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 5:
			try {
				db.execSQL("create table if not exists "
						+ LoadInspectionType
						+ " (Cat_ID varchar2(50),Insp_ID varchar2(50),InspName varchar2(50),"
						+ "Type_ID varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 8:
			try {
				db.execSQL("create table if not exists " + LoadCompany
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 9:
			try {
				db.execSQL("create table if not exists " + LoadInspectors
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 10:
			try {
				db.execSQL("create table if not exists "
						+ DashBoardToday
						+ " (id integer primary key autoincrement,inspid varchar2(50),inspname varchar2(50),insptype varchar2(50),awaiting varchar2(50),scheduled varchar2(50),inspected varchar2(50),reportsready varchar2(50),suspended varchar2(50),unsuspended varchar2(50),other varchar2(50),total varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 11:
			try {
				db.execSQL("create table if not exists "
						+ DashBoardWeek
						+ " (id integer primary key autoincrement,inspid varchar2(50),inspname varchar2(50),insptype varchar2(50),awaiting varchar2(50),scheduled varchar2(50),inspected varchar2(50),reportsready varchar2(50),suspended varchar2(50),unsuspended varchar2(50),other varchar2(50),total varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 12:
			try {
				db.execSQL("create table if not exists "
						+ DashBoardMonth
						+ " (id integer primary key autoincrement,inspid varchar2(50),inspname varchar2(50),insptype varchar2(50),awaiting varchar2(50),scheduled varchar2(50),inspected varchar2(50),reportsready varchar2(50),suspended varchar2(50),unsuspended varchar2(50),other varchar2(50),total varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 13:
			try {
				db.execSQL("create table if not exists "
						+ LoadInspectionType2
						+ " (Cat_ID varchar2(50),Insp_ID varchar2(50),InspName varchar2(50),"
						+ "Type_ID varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 14:
			try {
				db.execSQL("create table if not exists " + LoadInspectionStatus
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 15:
			try {
				db.execSQL("create table if not exists "
						+ LoadInspectionSubStatus + " (id varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 16:
			try {
				db.execSQL("create table if not exists " + LoadCaptionValue
						+ " (id varchar2(50),caption varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 17:
			try {
				db.execSQL("create table if not exists "
						+ AddAImage
						+ " (id varchar2(50),imgid INTEGER,agentid varchar(100),caption varchar2(50),elevation varchar2(50),filepath varchar2)");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 18:
			try {
				db.execSQL("create table if not exists " + LoadbuildingType
						+ " (id varchar2(50),buildingtype varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 19:
			try {
				db.execSQL("create table if not exists " + LoadInsuranceCarrier
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 20:
			try {
				db.execSQL("create table if not exists "
						+ AgentInspection_Pdf
//						+ " (id integer primary key autoincrement,filename varchar2)");
				 +
				 " (id integer primary key autoincrement,filename varchar2,FirstName varchar2,LastName varchar2,InspectionAddress1 varchar2,InspectionAddress2 varchar2,InspectionCity varchar2,InspectionState varchar2,InspectionCounty varchar2,PolicyNumber varchar2,InspectionZip varchar2)");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 21:
			try {
				db.execSQL("create table if not exists "
						+ Property_Count
						+ " (id integer primary key autoincrement,propertycount varchar2)");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;

		case 22:
			try {
				db.execSQL("create table if not exists "
						+ Customer_Details
						+ " (id integer primary key autoincrement,s_SRID varchar2,"
						+ "ownersName varchar2,Address varchar2,Address2 varchar2,"
						+ "City varchar2,County varchar2,State varchar2,"
						+ "Zipcode varchar2,PolicyNumber varchar2,Email varchar2,"
						+ "Inspection_Type varchar2,RegisterDate varchar2,AssgDate varchar2,"
						+ "InspDate varchar2,ScheduledDate varchar2,AcceptedDate varchar2,"
						+ "InspectionTypeid varchar2,Status varchar2,COMMpdf varchar2,Phone varchar2,IMC varchar2,InspectorName varchar2)");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 25:
			try {
				db.execSQL("create table if not exists "
						+ Version
						+ " (VersionCode varchar2,VersionName varchar2)");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
			
		case 29:
			try {
				db.execSQL("create table if not exists " + GetAgencyName
						+ " (id varchar2(50),Name varchar2(50))");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 30:
			try {
				db.execSQL("create table if not exists "
						+ VehicleInspection_Pdf
				 + " (id integer primary key autoincrement,filename varchar2," +
				 "FirstName varchar2,LastName varchar2,InspectionAddress1 varchar2,InspectionAddress2 varchar2," +
				 "InspectionCity varchar2,InspectionState varchar2," +
				 "InspectionCounty varchar2,VehicleNumber varchar2,InspectionZip varchar2)");
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		case 31:
			try {
				db.execSQL("create table if not exists "
						+ AgentInspection
				 + " (aid integer primary key autoincrement,AgentId varchar,policynum varchar2,policyname varchar2,squarefootage varchar2,dateofsurvey varchar2," +
				 "timeofsurvey varchar2,typeofstructure varchar2,otherstructure varchar2,signs varchar2,signscomments varchar2,unusual varchar2,"+
				 "unusualcomments varchar2,FirstName varchar2,LastName varchar2,InspectionAddress1 varchar2,InspectionAddress2 varchar2," +
				 "zip varchar2,city varchar2,state varchar2,county varchar2,chkmail varchar2,maddress1 varchar2,maddress2 varchar2,"+
				 "mzip varchar2,mcity varchar2,mstate varchar2,mcounty varchar2,logo varchar2,flag varchar2,additinaltitle varchar2,additonalcomments varchar2,pdfpath varchar2)");
				System.out.println("atanle created");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println("agent tavble ");
			}
			break;

		}
	}

	public final boolean isInternetOn() {
		boolean chk = false;
		ConnectivityManager conMgr = (ConnectivityManager) this.con
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			chk = true;
		} else {
			chk = false;
		}

		return chk;
	}

	public void show_ProgressDialog(String string) {
		// TODO Auto-generated method stub
		String source = "<b><font color=#00FF33>" + string
				+ "</font></b>";
		pd = ProgressDialog.show(this.con, "", Html.fromHtml(source), true);
	}

	public SoapObject Calling_WS_LoginPage(String string, String string2,
			String string3) throws SocketException, IOException,
			NetworkErrorException, TimeoutException, XmlPullParserException,
			Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("UserName", string);
		request.addProperty("Password", string2);
		envelope.setOutputSoapObject(request);
		;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_AgentInformation(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("AgentID", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_DashBoard(String string, String string2,
			String string3) throws SocketException, IOException,
			NetworkErrorException, TimeoutException, XmlPullParserException,
			Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("AgentID", Integer.parseInt(string));
		request.addProperty("Option", string2);
		envelope.setOutputSoapObject(request);
		System.out.println("Import request is" + request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		System.out.println("Import response is" + result);
		return result;

	}

	public String Calling_WS_ChangePassword(String string, String string2,
			String string4, String string3) throws SocketException,
			IOException, NetworkErrorException, TimeoutException,
			XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Username", string);
		request.addProperty("Currentpwd", string2);
		request.addProperty("Newpwd", string4);
		System.out.println("Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}

	public SoapObject Calling_WS_OnlineSyncPolicyInfo_Agency(String string,
			String string1, String string2, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspectionStatus", string);
		request.addProperty("InspectiontypeID", Integer.parseInt(string1));
		request.addProperty("AgentId", Integer.parseInt(string2));
		envelope.setOutputSoapObject(request);
		;
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public SoapObject Calling_WS_OnlineList(String string2, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("AgentId", Integer.parseInt(string2));
		envelope.setOutputSoapObject(request);
		System.out.println("onlinelist request="+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();System.out.println("online resp="+result);
		return result;

	}

	public SoapObject Calling_WS_LoadInspectionCategory(String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		System.out.println("Inspection category request is :"+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		System.out.println("Inspection category result is :"+result);
		return result;

	}

	public SoapObject Calling_WS_LoadInspectionTypes(String string,
			String string2, String string3) throws SocketException,
			IOException, NetworkErrorException, TimeoutException,
			XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("CategoryID", Integer.parseInt(string));
		request.addProperty("CompanyID", Integer.parseInt(string2));
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_LoadBuildingTypes(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("CategoryID", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		System.out.println("Fill building type request is "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_LoadInspectionTypeDescription(String string,
			String string2, String string3) throws SocketException,
			IOException, NetworkErrorException, TimeoutException,
			XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("InspTypeID", Integer.parseInt(string));
		request.addProperty("CompanyID", Integer.parseInt(string2));
		System.out.println("The request for LoadInspectionTypeDetails is "
				+ request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_LoadState(String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_LoadCounty(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("StateID", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public SoapObject Calling_WS_LoadInspectors(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("CompanyID", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_LoadCompany(String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public SoapObject Calling_WS_LoadInspectionType_Discount(String couponcode,
			String inspectionfee, String companyid, String inspectionid,
			String string3) throws SocketException, IOException,
			NetworkErrorException, TimeoutException, XmlPullParserException,
			Exception {

		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("CouponCode", couponcode);
		request.addProperty("InspectionFees", inspectionfee);// Float.parseFloat(inspectionfee)
		request.addProperty("CompanyID", Integer.parseInt(companyid));
		request.addProperty("InspTypeID", Integer.parseInt(inspectionid));
		envelope.setOutputSoapObject(request);
		System.out.println("The request for LoadInspectionType_Discount is "
				+ request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		System.out.println("The result is " + result);
		return result;

	}

	public SoapObject Calling_WS_AGENTINFORMATIONLIST(String string,
			String string3) throws SocketException, IOException,
			NetworkErrorException, TimeoutException, XmlPullParserException,
			Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("AGENTID", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public SoapObject Calling_WS_LOADVEHICLEDETAILS(String string,
			String string3) throws SocketException, IOException,
			NetworkErrorException, TimeoutException, XmlPullParserException,
			Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Usertype", 8);
		request.addProperty("Userid", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		System.out.println("LOADVEHICLEDETAILS request is "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public String Calling_WS_ViewCustomerPDF(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("SRID", string);
		System.out.println("ViewCustomerPDF request is " + request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}
	
	public String Calling_WS_EmailReport(String agentid, String to,
			String subject, String body, String path, String string3) throws SocketException,
			IOException, NetworkErrorException, TimeoutException,
			XmlPullParserException, Exception {
		versionname = getdeviceversionname();
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("UserTypeID", 8);
		request.addProperty("AgentID", agentid);
		request.addProperty("To", to);
		request.addProperty("Subject", subject);
		request.addProperty("Body", body);
		request.addProperty("Pdfpath", path);
		request.addProperty("ApplicationVersion",versionname);
		request.addProperty("DeviceName",model);
		request.addProperty("APILevel",apiLevel);
		System.out.println("Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}
	
	public SoapObject Calling_WS_GETAGENCYNAME(String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public String Calling_WS_CheckCouponcode(String string, String string3) throws SocketException,
			IOException, NetworkErrorException, TimeoutException,
			XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Couponcode", string);
		System.out.println("Coupon code Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}
	
	public SoapObject Calling_WS_GETADDRESSDETAILS(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Zipcode", string);
		System.out.println("Zipcode Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}

	public String encode(String oldstring) {
		if (oldstring == null) {
			oldstring = "";
		}
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return oldstring;

	}

	public String decode(String newstring) {
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newstring;

	}

//	public void hidekeyboard() {
//		System.out.println("hide ");
//		InputMethodManager imm = (InputMethodManager) con
//				.getSystemService(Activity.INPUT_METHOD_SERVICE);
//		imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
//		System.out.println("hide fuly");
//	}
	
	public void hidekeyboard(EditText editText){
        InputMethodManager imm = (InputMethodManager)con.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }

	public boolean eMailValidation(CharSequence stringemailaddress1) {
		// TODO Auto-generated method stub
		// Pattern emailPattern = Pattern
		// .compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
//		Pattern emailPattern = Pattern
//				.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+");
//		Matcher emailMatcher = emailPattern.matcher(stringemailaddress1);
		
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(stringemailaddress1);

		return matcher.matches();

	}
	
	public void Dynamic_Image_Changing(final ImageView iv)
	{
		final Handler handler = new Handler();
	    Runnable runnable = new Runnable() 
	    {
	                int i=0;
	                public void run() 
	                {
	                    iv.setImageResource(mThumbIds[i]);
	                    i++;
	                    if(i>mThumbIds.length-1)
	                    {
	                    i=0;    
	                    }
	                    handler.postDelayed(this, 2000);  //for interval...
	                }

	    };
	            handler.postDelayed(runnable, 1000); //for initial delay..

	}
	
	public Bitmap ShrinkBitmap(String file, int width, int height) {
		Bitmap bitmap = null;
		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			// cf.errorlogmanage("Problme in opening the image in sring  error="
			// + e.getMessage());
			return bitmap;
		}
	}
	
	public void Device_Information()
	{
		deviceId= Settings.System.getString(this.con.getContentResolver(), Settings.System.ANDROID_ID);
		model = android.os.Build.MODEL;
		manuf = android.os.Build.MANUFACTURER;
		devversion = android.os.Build.VERSION.RELEASE;
		apiLevel = android.os.Build.VERSION.SDK;
		WifiManager wifiManager = (WifiManager)(this.con.getSystemService(this.con.WIFI_SERVICE));
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		ipAddress = wifiInfo.getIpAddress();
	}
	
	public boolean checkresponce(Object responce) {
		if (responce != null) {
			if (!"null".equals(responce.toString())
					&& !responce.toString().equals("")) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public String getdeviceversionname() {
		// TODO Auto-generated method stub
		try {
			versionname = this.con.getPackageManager().getPackageInfo(
					this.con.getPackageName(), 0).versionName;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
		}
		return versionname;
	}

		// TODO Auto-generated method stub
		public SoapObject Calling_WS_AGENTINFORMATIONLIST(String string,int s,
				String string3) throws SocketException, IOException,
				NetworkErrorException, TimeoutException, XmlPullParserException,
				Exception {
			SoapObject request = new SoapObject(NAMESPACE, string3);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			request.addProperty("AGENTID", Integer.parseInt(string));
			request.addProperty("ROWID", s);
			envelope.setOutputSoapObject(request);
			System.out.println("request"+request);
			HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
			androidHttpTransport.call(NAMESPACE + string3, envelope);
			SoapObject result = (SoapObject) envelope.getResponse();
			return result;

		
	}

}
