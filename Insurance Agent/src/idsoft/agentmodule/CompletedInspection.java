package idsoft.agentmodule;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class CompletedInspection extends Activity {
	CommonFunctions cf;
	ShowToast toast;
	String agentId="",FirstName="",LastName="",Address1="",Address2="",City="",State="",County="",Zip="",PolicyNumber="",
			PolicyName="",SquareFootage="",Date="",Time="",Structure="",Signs="",Unusual="",Title="",Comments="",path="",
			MAddress1="",MAddress2="",MCity="",MState="",MCounty="",MZip="",Logo="",order_result="",image_result="",getpdf="";
	TextView tvrws;
	int rows=0,i=0,flag=0,show_handler=0,s;
	String[] data,elevation_name,caption_name,file_name,pdfpath,datasend;
	Integer[] rwarr,rwchk;
	Button[] submit,view,download,pter;
	LinearLayout complist;
	MarshalBase64 marshal;
	byte[] bytecoverpagelogo;
	static String reportpath;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.completed);
		cf = new CommonFunctions(this);
		
		ImageView iv=(ImageView)findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);
		
		tvrws = (TextView)findViewById(R.id.rows);
		
		cf.CreateTable(1);
		cf.CreateTable(17);
		cf.CreateTable(31);		
		complist =(LinearLayout)findViewById(R.id.agentinsplist);
		
		try
		{
			
			Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,null);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				agentId = cur.getString(cur.getColumnIndex("Agentid"));
		    }
			cur.close();
			
			complist.removeAllViews();
			ScrollView sv = new ScrollView(this);
			complist.addView(sv);

			final LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			sv.addView(l1);
			
			Cursor cur1=cf.db.rawQuery("select * from "+cf.AgentInspection + " where AgentId='"+agentId+"'", null);
			tvrws.setText("No. of surveys : "+cur1.getCount());
			rows =cur1.getCount();
			if(cur1.getCount()>0)
			{
				cur1.moveToFirst();
				TextView[] tvstatus = new TextView[rows];
				 submit = new Button[rows];			
				 view = new Button[rows];
				 download = new Button[rows];
				 pter = new Button[rows];
				 rwchk = new Integer[rows];
				 pdfpath = new String[rows];
				 datasend = new String[rows];
				 
				RelativeLayout[] rl = new RelativeLayout[rows];
				data = new String[rows];	
				rwarr = new Integer[rows];
				
				final String[] ownersname=new String[rows];
				
					i = 0;
					do {System.out.println("insidedo");
						int rownum = cur1.getInt(cur1.getColumnIndex("aid"));System.out.println("rownum");
						int chkflg = cur1.getInt(cur1.getColumnIndex("flag"));System.out.println("chkflg="+chkflg);
						getpdf = cf.decode(cur1.getString(cur1.getColumnIndex("pdfpath")));
						
						FirstName = cf.decode(cur1.getString(cur1.getColumnIndex("FirstName")));
						if(FirstName.equals(""))
						{
							FirstName = "N/A";
						}
						ownersname[i] =FirstName+" ";
						data[i] = " " + FirstName + " | ";
						System.out.println("FirstName");
						LastName = cf.decode(cur1.getString(cur1.getColumnIndex("LastName")));
						if(LastName.equals(""))
						{
							LastName = "N/A";
						}
						ownersname[i] +=LastName;
						data[i] += LastName + " | ";
						System.out.println("LastName");
						Address1 = cf.decode(cur1.getString(cur1.getColumnIndex("InspectionAddress1")));
						if(Address1.equals(""))
						{
							Address1 = "N/A";
						}
						data[i] += Address1 + " | ";
						System.out.println("Address1");
						Address2 = cf.decode(cur1.getString(cur1.getColumnIndex("InspectionAddress2")));
						if(Address2.equals(""))
						{
							Address2 = "N/A";
						}
						data[i] += Address2 + " | ";
						System.out.println("Address1");
						City = cf.decode(cur1.getString(cur1.getColumnIndex("city")));
						if(City.equals(""))
						{
							City = "N/A";
						}
						data[i] += City + " | ";
						System.out.println("Address1");
						State = cf.decode(cur1.getString(cur1.getColumnIndex("state")));
						if(State.equals("")||State.equals("--Select--"))
						{
							State = "N/A";
						}
						data[i] += State + " | ";
						System.out.println("Address1");
						County = cf.decode(cur1.getString(cur1.getColumnIndex("county")));
						if(County.equals(""))
						{
							County = "N/A";
						}
						data[i] += County + " | ";
						System.out.println("Address1");
						Zip = cf.decode(cur1.getString(cur1.getColumnIndex("zip")));
						if(Zip.equals("")||Zip.equals("0"))
						{
							Zip = "N/A";
						}
						data[i] += Zip + " | ";
						System.out.println("Address1");
						PolicyNumber = cf.decode(cur1.getString(cur1.getColumnIndex("policynum")));
						if(PolicyNumber.equals(""))
						{
							PolicyNumber = "N/A";
						}
						datasend[i] = PolicyNumber;
						
						PolicyName = cf.decode(cur1.getString(cur1.getColumnIndex("policyname")));
						if(PolicyName.equals(""))
						{
							PolicyName = "N/A";
						}
						SquareFootage =cur1.getString(cur1.getColumnIndex("squarefootage"));
						if(SquareFootage.equals(""))
						{
							SquareFootage = "0";
						}
						Date =cur1.getString(cur1.getColumnIndex("dateofsurvey"));
						Time =cur1.getString(cur1.getColumnIndex("timeofsurvey"));
						Structure =cf.decode(cur1.getString(cur1.getColumnIndex("typeofstructure")));
						Signs =cf.decode(cur1.getString(cur1.getColumnIndex("signs")));
						Unusual =cf.decode(cur1.getString(cur1.getColumnIndex("unusual")));
						Title =cf.decode(cur1.getString(cur1.getColumnIndex("additinaltitle")));
						Comments =cf.decode(cur1.getString(cur1.getColumnIndex("additonalcomments")));
						MAddress1 = cf.decode(cur1.getString(cur1.getColumnIndex("maddress1")));
						if(MAddress1.equals(""))
						{
							MAddress1 = "N/A";
						}
						
						MAddress2 = cf.decode(cur1.getString(cur1.getColumnIndex("maddress2")));
						if(MAddress2.equals(""))
						{
							MAddress2 = "N/A";
						}
						
						
						MCity = cf.decode(cur1.getString(cur1.getColumnIndex("mcity")));
						if(MCity.equals(""))
						{
							MCity = "N/A";
						}
						
						MState = cf.decode(cur1.getString(cur1.getColumnIndex("mstate")));
						if(MState.equals("")||MState.equals("--Select--"))
						{
							MState = "N/A";
						}
						
						MCounty = cf.decode(cur1.getString(cur1.getColumnIndex("mcounty")));
						if(MCounty.equals(""))
						{
							MCounty = "N/A";
						}
						
						
						MZip = cf.decode(cur1.getString(cur1.getColumnIndex("mzip")));
						
						
						Logo = cf.decode(cur1.getString(cur1.getColumnIndex("logo")));
						System.out.println("ends");
						
						data[i] += PolicyNumber + " | ";	
						rwarr[i] = rownum;System.out.println("suce");
						rwchk[i] = chkflg;
						pdfpath[i] = getpdf;
						
						System.out.println("chkflg="+chkflg+"sdf"+rwchk[i]);
						LinearLayout.LayoutParams llparams;
						RelativeLayout.LayoutParams tvparams;
						RelativeLayout.LayoutParams btnsubmitparams,downloadparams,pterparams,viewparams,sparams;
						LinearLayout.LayoutParams lltxtparams;
						
						Display display = getWindowManager().getDefaultDisplay();
					    DisplayMetrics displayMetrics = new DisplayMetrics();
					    display.getMetrics(displayMetrics);

					    int width = displayMetrics.widthPixels;
					    int height = displayMetrics.heightPixels;
					    
					    System.out.println("The width is "+width);
					    System.out.println("The height is "+height);
				         
							System.out.println("ends");
							
					    if(width > 1023 || height > 1023)
						{
					    	llparams = new LinearLayout.LayoutParams(
									ViewGroup.LayoutParams.MATCH_PARENT,
									ViewGroup.LayoutParams.MATCH_PARENT);
							llparams.setMargins(0, 0, 0, 0);
							
							tvparams = new RelativeLayout.LayoutParams(
									680,
									ViewGroup.LayoutParams.MATCH_PARENT);
							tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
							tvparams.setMargins(0, 0, 0, 0);
							

							downloadparams = new RelativeLayout.LayoutParams(
									110, ViewGroup.LayoutParams.WRAP_CONTENT);
							downloadparams.addRule(RelativeLayout.CENTER_VERTICAL);
							downloadparams.setMargins(830, 0, 0, 0);
							
							viewparams = new RelativeLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT,
									ViewGroup.LayoutParams.WRAP_CONTENT);
							viewparams.addRule(RelativeLayout.CENTER_VERTICAL);
							viewparams.setMargins(700, 0, 0, 0);
							
							sparams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
									ViewGroup.LayoutParams.WRAP_CONTENT);
							sparams.addRule(RelativeLayout.CENTER_VERTICAL);
							sparams.setMargins(830, 0, 0, 0);
							
							rl[i] = new RelativeLayout(this);
							l1.addView(rl[i], llparams);
							
							LinearLayout lltxt=new LinearLayout(this);
							lltxt.setLayoutParams(tvparams);
							lltxt.setPadding(20, 0, 20, 0);
							rl[i].addView(lltxt);
							
							lltxtparams = new LinearLayout.LayoutParams(
									ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
							lltxtparams.setMargins(0, 30, 0, 30);
			
							tvstatus[i] = new TextView(this);
							tvstatus[i].setLayoutParams(lltxtparams);
							tvstatus[i].setId(1);
							tvstatus[i].setText(data[i]);
							tvstatus[i].setTextColor(Color.WHITE);
							tvstatus[i].setTextSize(14);
							lltxt.addView(tvstatus[i]);
							
							LinearLayout lldownload=new LinearLayout(this);
							lldownload.setLayoutParams(downloadparams);
							rl[i].addView(lldownload);
							
							LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
									110, ViewGroup.LayoutParams.WRAP_CONTENT);
							downloadparams1.gravity=Gravity.CENTER_VERTICAL;
							
							download[i] = new Button(this);
							download[i].setLayoutParams(downloadparams1);
							download[i].setId(2);
							download[i].setText("Download");
							download[i].setBackgroundResource(R.drawable.buttonrepeat);
							download[i].setTextColor(0xffffffff);
							download[i].setTextSize(14);
							download[i].setTypeface(null, Typeface.BOLD);download[i].setVisibility(View.GONE);
							download[i].setTag(i);
							lldownload.addView(download[i]);
							
							LinearLayout llpter=new LinearLayout(this);
							llpter.setLayoutParams(downloadparams);
							rl[i].addView(llpter);
							
							pter[i] = new Button(this);
							pter[i].setLayoutParams(downloadparams1);
							pter[i].setId(2);
							pter[i].setText("Email Report");
							pter[i].setBackgroundResource(R.drawable.buttonrepeat);
							pter[i].setTextColor(0xffffffff);
							pter[i].setTextSize(14);
							pter[i].setTypeface(null, Typeface.BOLD);
							pter[i].setTag(i);
							pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);pter[i].setVisibility(View.GONE);
							llpter.addView(pter[i]);
							
							LinearLayout llview=new LinearLayout(this);
							llview.setLayoutParams(viewparams);
							rl[i].addView(llview);
			
							view[i] = new Button(this);
							view[i].setLayoutParams(downloadparams1);
							view[i].setId(2);
							view[i].setText("View PDF");
							view[i].setBackgroundResource(R.drawable.buttonrepeat);
							view[i].setTextColor(0xffffffff);
							view[i].setTextSize(14);
							view[i].setTypeface(null, Typeface.BOLD);view[i].setVisibility(View.GONE);
							view[i].setTag(i);
							llview.addView(view[i]);
							
							LinearLayout llsub=new LinearLayout(this);
							llsub.setLayoutParams(sparams);
							rl[i].addView(llsub);
							
							submit[i] = new Button(this);
							submit[i].setLayoutParams(downloadparams1);
							submit[i].setId(2);
							submit[i].setText("Submit");
							submit[i].setBackgroundResource(R.drawable.buttonrepeat);
							submit[i].setTextColor(0xffffffff);
							submit[i].setTextSize(14);
							submit[i].setTypeface(null, Typeface.BOLD);
							submit[i].setTag(i);
							llsub.addView(submit[i]);
							
					
					}
					else if((width > 500 && width<1000) || (height > 500 && height<1000))
					{

							llparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
							llparams.setMargins(0, 0, 0, 0);
																				
							tvparams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.MATCH_PARENT);
							tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
							tvparams.setMargins(10, 0, 170, 0);
								
							lltxtparams = new LinearLayout.LayoutParams(
									320, ViewGroup.LayoutParams.MATCH_PARENT);
							lltxtparams.setMargins(0, 0, 0, 0);
												
							btnsubmitparams = new RelativeLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
							btnsubmitparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
							btnsubmitparams.setMargins(0, 0, 20, 0);
							
							downloadparams = new RelativeLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
							downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
							downloadparams.setMargins(0, 0, 20, 0);

							viewparams = new RelativeLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT,
									ViewGroup.LayoutParams.MATCH_PARENT);
							viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
							viewparams.setMargins(20, 0, 150, 0);
							
							rl[i] = new RelativeLayout(this);
							rl[i].setPadding(0, 20, 0, 20);
							l1.addView(rl[i], llparams);
							
							LinearLayout lltxt=new LinearLayout(this);
							lltxt.setLayoutParams(tvparams);
							rl[i].addView(lltxt);						
							

							tvstatus[i] = new TextView(this);
							tvstatus[i].setLayoutParams(lltxtparams);
							tvstatus[i].setId(1);
                        	tvstatus[i].setText(data[i]);
							tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
							tvstatus[i].setTextSize(14);
                            lltxt.addView(tvstatus[i]);
							
							LinearLayout llsubmit=new LinearLayout(this);
							llsubmit.setOrientation(LinearLayout.VERTICAL);
							llsubmit.setLayoutParams(btnsubmitparams);
							llsubmit.setGravity(Gravity.CENTER_HORIZONTAL);
							rl[i].addView(llsubmit);
							
							LinearLayout.LayoutParams submitparams = new LinearLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
							submitparams.topMargin=10;
							submitparams.gravity=Gravity.CENTER_HORIZONTAL;
							
							submit[i] = new Button(this);
							submit[i].setLayoutParams(submitparams);
							submit[i].setId(2);
							submit[i].setText("Submit");
							submit[i].setBackgroundResource(R.drawable.buttonrepeat);
							submit[i].setTextColor(0xffffffff);
							submit[i].setTextSize(14);
							submit[i].setTypeface(null, Typeface.BOLD);
							submit[i].setTag(i);
							
							LinearLayout lldownload=new LinearLayout(this);
							lldownload.setOrientation(LinearLayout.VERTICAL);
							lldownload.setLayoutParams(downloadparams);
							lldownload.setGravity(Gravity.CENTER_HORIZONTAL);
							rl[i].addView(lldownload);
							
							LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
							downloadparams1.topMargin=10;
							downloadparams1.gravity=Gravity.CENTER_HORIZONTAL;
							
							download[i] = new Button(this);
							download[i].setLayoutParams(downloadparams1);
							download[i].setId(2);
							download[i].setText("Download");
							download[i].setBackgroundResource(R.drawable.buttonrepeat);
							download[i].setTextColor(0xffffffff); download[i].setVisibility(View.GONE);
							download[i].setTextSize(14);
							download[i].setTypeface(null, Typeface.BOLD);
							download[i].setTag(i);
							lldownload.addView(download[i]);
					
							pter[i] = new Button(this);
							pter[i].setLayoutParams(downloadparams1);
							pter[i].setId(2);
							pter[i].setText("Email Report");
							pter[i].setBackgroundResource(R.drawable.buttonrepeat);
							pter[i].setTextColor(0xffffffff);
							pter[i].setTextSize(14);
							pter[i].setTypeface(null, Typeface.BOLD);pter[i].setVisibility(View.GONE);
							pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);
							pter[i].setGravity(Gravity.CENTER_VERTICAL);
							lldownload.addView(pter[i]);

							view[i] = new Button(this);
							view[i].setLayoutParams(downloadparams1);
							view[i].setId(2);
							view[i].setText("View PDF");
							view[i].setBackgroundResource(R.drawable.buttonrepeat);
							view[i].setTextColor(0xffffffff);
							view[i].setTextSize(14);view[i].setVisibility(View.GONE);
							view[i].setTypeface(null, Typeface.BOLD);
							view[i].setTag(i);
							lldownload.addView(view[i]);

						}
						
						else
						{
							
							System.out.println("Phoneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
							llparams = new LinearLayout.LayoutParams(
									ViewGroup.LayoutParams.MATCH_PARENT,
									ViewGroup.LayoutParams.WRAP_CONTENT);
							llparams.setMargins(0, 0, 0, 0);
							
							
							
							tvparams = new RelativeLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT,
									ViewGroup.LayoutParams.MATCH_PARENT);
							tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
							tvparams.setMargins(10, 0, 0, 0);
								
							lltxtparams = new LinearLayout.LayoutParams(
									320, ViewGroup.LayoutParams.MATCH_PARENT);
							lltxtparams.setMargins(0, 0, 0, 0);
												
							btnsubmitparams = new RelativeLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
							btnsubmitparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
							btnsubmitparams.setMargins(0, 0, 20, 0);
							
							downloadparams = new RelativeLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
							downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
							downloadparams.setMargins(0, 0, 20, 0);
							
							rl[i] = new RelativeLayout(this);
							rl[i].setPadding(0, 20, 0, 20);
							l1.addView(rl[i], llparams);
							
							LinearLayout lltxt=new LinearLayout(this);
							lltxt.setLayoutParams(tvparams);
							rl[i].addView(lltxt);
							
							

							tvstatus[i] = new TextView(this);
							tvstatus[i].setLayoutParams(lltxtparams);
							tvstatus[i].setId(1);

							tvstatus[i].setText(data[i]);
							tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
							tvstatus[i].setTextSize(14);

							lltxt.addView(tvstatus[i]);
							
							LinearLayout llsubmit=new LinearLayout(this);
							llsubmit.setOrientation(LinearLayout.VERTICAL);
							llsubmit.setLayoutParams(btnsubmitparams);
							llsubmit.setGravity(Gravity.CENTER_HORIZONTAL);
							rl[i].addView(llsubmit);
							
							LinearLayout.LayoutParams submitparams = new LinearLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
							submitparams.topMargin=10;
							submitparams.gravity=Gravity.CENTER_HORIZONTAL;
							
							submit[i] = new Button(this);
							submit[i].setLayoutParams(submitparams);
							submit[i].setId(2);
							submit[i].setText("Submit");
							submit[i].setBackgroundResource(R.drawable.buttonrepeat);
							submit[i].setTextColor(0xffffffff);
							submit[i].setTextSize(14);
							submit[i].setTypeface(null, Typeface.BOLD);
							submit[i].setTag(i);

							llsubmit.addView(submit[i]);
							
							LinearLayout lldownload=new LinearLayout(this);
							lldownload.setOrientation(LinearLayout.VERTICAL);
							lldownload.setLayoutParams(downloadparams);
							lldownload.setGravity(Gravity.CENTER_HORIZONTAL);
							rl[i].addView(lldownload);
							
							LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
									ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
							downloadparams1.topMargin=10;
							downloadparams1.gravity=Gravity.CENTER_HORIZONTAL;
							
							download[i] = new Button(this);
							download[i].setLayoutParams(downloadparams1);
							download[i].setId(2);
							download[i].setText("Download");
							download[i].setBackgroundResource(R.drawable.buttonrepeat);
							download[i].setTextColor(0xffffffff);
							download[i].setTextSize(14);download[i].setVisibility(View.GONE);
							download[i].setTypeface(null, Typeface.BOLD);
							download[i].setTag(i);
							lldownload.addView(download[i]);

							pter[i] = new Button(this);
							pter[i].setLayoutParams(downloadparams1);
							pter[i].setId(2);
							pter[i].setText("Email Report");
							pter[i].setBackgroundResource(R.drawable.buttonrepeat);
							pter[i].setTextColor(0xffffffff);
							pter[i].setTextSize(14);pter[i].setVisibility(View.GONE);
							pter[i].setTypeface(null, Typeface.BOLD);
							pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);
							pter[i].setGravity(Gravity.CENTER_VERTICAL);
							lldownload.addView(pter[i]);
						
							view[i] = new Button(this);
							view[i].setLayoutParams(downloadparams1);
							view[i].setId(2);
							view[i].setText("View PDF");
							view[i].setBackgroundResource(R.drawable.buttonrepeat);
							view[i].setTextColor(0xffffffff);
							view[i].setTextSize(14);view[i].setVisibility(View.GONE);
							view[i].setTypeface(null, Typeface.BOLD);
							view[i].setTag(i);
                  	        lldownload.addView(view[i]);
							

						}

					    if (i % 2 == 0) {
							rl[i].setBackgroundColor(Color.parseColor("#13456d"));
						} else {
							rl[i].setBackgroundColor(Color.parseColor("#386588"));
						}
					    
					   
					       Cursor cur2 = cf.db.rawQuery("select * from " + cf.AgentInspection + " where aid='"+rwarr[s]+"' and AgentId='"+agentId+"'",null);
					       cur2.moveToFirst();
							if (cur2.getCount() >= 1) {
								String path = cf.decode(cur2.getString(cur2.getColumnIndex("pdfpath")));
								
								String[] filenamesplit = path.split("/");
								String pdfname = filenamesplit[filenamesplit.length - 1];
								System.out.println("the file name is" + pdfname);
								File sdDir = new File(Environment.getExternalStorageDirectory()
										.getPath());
								File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
										+ pdfname);
					            System.out.println("ends"+file.exists());
								if (file.exists()) {
									pter[i].setVisibility(View.VISIBLE);
									download[i].setVisibility(View.GONE);
								} else {
									pter[i].setVisibility(View.VISIBLE);
									download[i].setVisibility(View.GONE);
								}System.out.println("asfas");
						    }
							cur2.close();
							
							 if(rwchk[i]==0)
						     {
						    	 submit[i].setVisibility(View.GONE);
						    	 pter[i].setVisibility(View.VISIBLE);
						    	 view[i].setVisibility(View.VISIBLE);
						     }
						     else
						     {
						    	 pter[i].setVisibility(View.GONE);
						    	 view[i].setVisibility(View.GONE);
						    	 submit[i].setVisibility(View.VISIBLE);
						    	
						     }
							 
					    submit[i].setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(final View v) {
								// TODO Auto-generated method stub
								Button b = (Button) v;
								String buttonvalue = v.getTag()	.toString();
								s = Integer.parseInt(buttonvalue);
								System.out.println("s="+rwarr[s]);
								
								cf.CreateTable(17);
								try
								{
									Cursor cur=cf.db.rawQuery("select * from "+cf.AddAImage + " where agentid='"+agentId+"' and imgid='"+rwarr[s]+"'", null);
									//System.out.println("select * from "+cf.AddAImage + " where agentid='"+agentId+"' and imgid='"+s+"'");
									if(cur.getCount()<1)
									{
										flag=1;
									}
									else
									{
										flag=0;
									}
									cur.close();
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								
								try
								{
									Bitmap bitmap = decodeFile(Logo);
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 100, out);
									bytecoverpagelogo = out.toByteArray();
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								
								AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CompletedInspection.this);
						        alertDialogBuilder.setTitle("Internet Connection");
						    	alertDialogBuilder
										.setMessage("You must have an internet connection to package your report!")
										.setCancelable(false)
										.setPositiveButton("OK",new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface dialog,int id) {
												if (cf.isInternetOn() == true) {
													cf.show_ProgressDialog("Submitting Agent Inspection... Please wait.");
													new Thread() {
														public void run() {
															Looper.prepare();
															try {
																SoapObject request = new SoapObject(cf.NAMESPACE,"SaveAgentInspection");
																SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
																envelope.dotNet = true;
																request.addProperty("Id",rows);
																request.addProperty("UserId", Integer.parseInt(agentId));
																request.addProperty("PolicyNumber", PolicyNumber);
																request.addProperty("PolicyName", PolicyName);
																request.addProperty("Firstname", FirstName);
																request.addProperty("Lastname", LastName);
																request.addProperty("SquareFootage", Long.parseLong(SquareFootage));
																request.addProperty("DateofSurvey", Date);
																request.addProperty("TimeofSurvey", Time);
																request.addProperty("TypeofStructure", Structure);
																request.addProperty("SignsofNeglect", Signs);
																request.addProperty("HazardCondition", Unusual);
																//new condition
																request.addProperty("AddiConditions", Title);//"title1~title2~title3~title4~title5~title6~title7~title8~title9~title10~title1~title2~title3~title4~title5~title6~title7~title8~title9~title10~title1~title2~title3~title4~title5~title6~title7~title8~title9~title10"
																request.addProperty("CustomeDefect", Comments);//"dshfgdhksgfdskdgfk~ghfjfdhgldhflghdfl~hfvjdfhgldhfgldhf~jfhdlgfhdlg~hfdshlshflhl~ghgkgkgkgkgkgkjgh~gkgkgkgkgkg~kgkjgkjgjgkg~gkjgkjgkjgkjgkj~gjgkjgkjggjgjgjggj~dshfgdhksgfdskdgfk~ghfjfdhgldhflghdfl~hfvjdfhgldhfgldhf~jfhdlgfhdlg~hfdshlshflhl~ghgkgkgkgkgkgkjgh~gkgkgkgkgkg~kgkjgkjgjgkg~gkjgkjgkjgkjgkj~gjgkjgkjggjgjgjggj~dshfgdhksgfdskdgfk~ghfjfdhgldhflghdfl~hfvjdfhgldhfgldhf~jfhdlgfhdlg~hfdshlshflhl~ghgkgkgkgkgkgkjgh~gkgkgkgkgkg~kgkjgkjgjgkg~gkjgkjgkjgkjgkj~gjgkjgkjggjgjgjggj"
																
																request.addProperty("InspectionAddress1", Address1);
																request.addProperty("InspectionAddress2", Address2);
																request.addProperty("InspectionState", State);
																request.addProperty("InspectionCounty", County);
																request.addProperty("InspectionCity", City);
																
																if(Zip.equals("")||Zip.equals("0")||Zip.equals("N/A"))
																{
																	Zip = "0";
																}
																
																request.addProperty("InspectionZip", Integer.parseInt(Zip));
																request.addProperty("MailingAddress1", MAddress1);
																request.addProperty("MailingAddress2", MAddress2);
																request.addProperty("MailingState", MState);
																request.addProperty("MailingCounty", MCounty);
																request.addProperty("MailingCity", MCity);
																
																if(MZip.equals("")||MZip.equals("0")||MZip.equals("N/A"))
																{
																	MZip = "0";
																}
																
																request.addProperty("MailingZip", Integer.parseInt(MZip));
																request.addProperty("CoverPageLogo", bytecoverpagelogo);

																request.addProperty("Logoname", (rows+1)+"Ag_Coverpagelogo"+agentId+".jpg");
																request.addProperty("Flag", flag);
																
																marshal = new MarshalBase64();
																envelope.setOutputSoapObject(request);
																marshal.register(envelope);

																System.out.println("SaveAgentInspection request is " + request);
																HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
																System.out.println("Before http call");
																androidHttpTransport.call(cf.NAMESPACE+ "SaveAgentInspection", envelope);
																order_result = envelope.getResponse().toString();
																System.out.println("SaveAgentInspection result is"+ order_result);

																show_handler = 5;
																handler.sendEmptyMessage(0);

																
															} catch (IOException e) {
																// TODO Auto-generated catch block
																e.printStackTrace();
																show_handler = 3;
																handler.sendEmptyMessage(0);
															} catch (XmlPullParserException e) {
																// TODO Auto-generated catch block
																e.printStackTrace();
																show_handler = 3;
																handler.sendEmptyMessage(0);
															}
														}

														private Handler handler = new Handler() {
															@Override
															public void handleMessage(Message msg) {
																cf.pd.dismiss();
																if (show_handler == 3) {
																	show_handler = 0;
																	toast = new ShowToast(
																			CompletedInspection.this,
																			"There is a problem on your Network. Please try again later with better Network.");

																} else if (show_handler == 4) {
																	show_handler = 0;
																	toast = new ShowToast(
																			CompletedInspection.this,
																			"There is a problem on your application. Please contact Paperless administrator.");

																} else if (show_handler == 5) {
																	show_handler = 0;
																	
																	Call_SaveImages(order_result);
																	

																}
															}
														};
													}.start();
												} else {
													toast = new ShowToast(CompletedInspection.this,
															"Internet connection not available");
												}
											}
										  })
											.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
												public void onClick(DialogInterface dialog,int id) {
													dialog.cancel();
												}
											});
							 
											
							    	   AlertDialog altdialog = alertDialogBuilder.create();							 
							    	   altdialog.show();
							}
					    });
					    
					    pter[i].setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								String buttonvalue = v.getTag().toString();
								String[] splitvalue=buttonvalue.split("&#40");
								int s = Integer.parseInt(splitvalue[0]);
								String name=splitvalue[1];
								try
								{
									  Cursor cur2 = cf.db.rawQuery("select * from " + cf.AgentInspection + " where aid='"+rwarr[s]+"' and AgentId='"+agentId+"'",null);
									//  System.out.println("select * from " + cf.AgentInspection + " where aid='"+s+"' and AgentId='"+agentId+"'");
								       cur2.moveToFirst();
										if (cur2.getCount() >= 1) {
											 path = cf.decode(cur2.getString(cur2.getColumnIndex("pdfpath")));
											 System.out.println("viewpath="+path);
											
									    }
										cur2.close();
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								 reportpath=path;
								String[] filenamesplit = path.split("/");
								String filename = filenamesplit[filenamesplit.length - 1];
								
								
								String ivalue=splitvalue[2];
								System.out.println("ivalue ="+ivalue);
								int ival=Integer.parseInt(ivalue);
								String ivalsplit=datasend[ival];
								System.out.println("ivaluesplit ="+ivalsplit);
								String pn=datasend[ival];
								String status="";

								Intent intent=new Intent(CompletedInspection.this,EmailReport2.class);
								intent.putExtra("policynumber", "AgentInspection2");
								intent.putExtra("status", status);
								intent.putExtra("mailid", "");
								intent.putExtra("classidentifier", "AgentInspection2");
								intent.putExtra("ownersname", ownersname[s]);
								startActivity(intent);
								finish();

							}
						});
					    
					    view[i].setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Button b = (Button) v;
								String buttonvalue = v.getTag().toString();
								System.out.println("buttonvalue is" + buttonvalue);
								int s = Integer.parseInt(buttonvalue);
								//System.out.println("path="+pdfpath[s]);
								try
								{
									  Cursor cur2 = cf.db.rawQuery("select * from " + cf.AgentInspection + " where aid='"+rwarr[s]+"' and AgentId='"+agentId+"'",null);
									 // System.out.println("select * from " + cf.AgentInspection + " where aid='"+s+"' and AgentId='"+agentId+"'");
								       cur2.moveToFirst();
										if (cur2.getCount() >= 1) {
											 path = cf.decode(cur2.getString(cur2.getColumnIndex("pdfpath")));
											 System.out.println("viewpath="+path);
											
									    }
										cur2.close();
								}
								catch (Exception e) {
									// TODO: handle exception
								}
								System.out.println("path="+path);
								String[] filenamesplit = path.split("/");
								final String filename = filenamesplit[filenamesplit.length - 1];
								System.out
										.println("The File Name is "
												+ filename);
								File sdDir = new File(Environment.getExternalStorageDirectory()
										.getPath());
								File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
										+ filename);
								
								if(file.exists())
								{
									View_Pdf_File(filename,path);
								}
								else
								{
									if (cf.isInternetOn() == true) {
										cf.show_ProgressDialog("Downloading... Please wait.");
										new Thread() {
											public void run() {
												Looper.prepare();
												try {
													String extStorageDirectory = Environment
															.getExternalStorageDirectory()
															.toString();
													File folder = new File(
															extStorageDirectory,
															"DownloadedPdfFile");
													folder.mkdir();
													File file = new File(folder,
															filename);
													try {
														file.createNewFile();
														Downloader.DownloadFile(path,file);
													} catch (IOException e1) {
														e1.printStackTrace();
													}

													show_handler = 2;
													handler.sendEmptyMessage(0);

												} catch (Exception e) {
													// TODO Auto-generated catch block
													System.out.println("The error is "
															+ e.getMessage());
													e.printStackTrace();
													show_handler = 1;
													handler.sendEmptyMessage(0);

												}
											}

											private Handler handler = new Handler() {
												@Override
												public void handleMessage(Message msg) {
													cf.pd.dismiss();
													// dialog1.dismiss();
													if (show_handler == 1) {
														show_handler = 0;
														toast = new ShowToast(
																CompletedInspection.this,
																"There is a problem on your application. Please contact Paperless administrator.");

													} else if (show_handler == 2) {
														show_handler = 0;
														
														View_Pdf_File(filename,path);

													}
												}
											};
										}.start();
									} else {
										toast = new ShowToast(CompletedInspection.this,"Internet connection not available");

									}
								}
							}
						});

					    i++;
					} while (cur1.moveToNext());
					
			}
			else
			{
					
						
			}
			cur1.close();
		}
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("error="+e.getMessage());
		}
	}
	private void Call_SaveImages(String result)
	{
		if(flag==1&&result.toLowerCase().equals("true"))
		{
		
			Call_AgentInformationList();
		}
		else if((flag==1||flag==0)&&result.toLowerCase().equals("false"))
		{
			toast = new ShowToast(
					CompletedInspection.this,
					"There is a problem on your application. Please contact Paperless administrator.");
		}
		else if(flag==0&&result.toLowerCase().equals("true"))
		{
             Save_AgentInspection_Image1();

		}
		
	}
	private void Save_AgentInspection_Image1()
	{
		cf.CreateTable(17);
		Cursor cur=cf.db.rawQuery("select * from "+cf.AddAImage + " where agentid='"+agentId+"' and imgid='"+rwarr[s]+"'", null);
		cur.moveToFirst();
		int cnt=cur.getCount();
		elevation_name=new String[cnt];
		caption_name=new String[cnt];
		file_name=new String[cnt];
		if(cnt>=1)
		{
			int i=0;
			do
			{
				final String elevation=cf.decode(cur.getString(cur.getColumnIndex("elevation")));
				final String caption=cf.decode(cur.getString(cur.getColumnIndex("caption")));
				String filepath=cf.decode(cur.getString(cur.getColumnIndex("filepath")));
				
				elevation_name[i]=elevation;
				caption_name[i]=caption;
				file_name[i]=filepath;
				
				i++;
				
			}while(cur.moveToNext());
		}
		cur.close();
		if(elevation_name.length>=1)
		{
			Save_AgentInspection_Image();
		}
	}
	private void Save_AgentInspection_Image()
	{
			if (cf.isInternetOn() == true) {
				cf.show_ProgressDialog("Submitting Agent Inspection... Please wait.");
				new Thread() {
					public void run() {
						Looper.prepare();
						try {
							Bitmap bitmap=null;
							for(i=0;i<elevation_name.length;i++)
							{
								bitmap= null;
								String[] filenamesplit = file_name[i]
										.split("/");
								final String filename = filenamesplit[filenamesplit.length - 1];
								System.out
										.println("The File Name is "
												+ filename);
								
								 bitmap = cf.ShrinkBitmap(file_name[i], 400, 400);

								System.out.println(" bimap "+bitmap);
								marshal = new MarshalBase64();
								ByteArrayOutputStream out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								final byte[] byteimage = out.toByteArray();
								if(i==elevation_name.length-1)
								{
									flag=1;
								}
								else
								{
									flag=0;
								}
							
							SoapObject request = new SoapObject(cf.NAMESPACE,"SaveAgentElevationImages");
							SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
									SoapEnvelope.VER11);
							envelope.dotNet = true;
							request.addProperty("AgentInfoPk",rows);
							request.addProperty("AgentID", Integer.parseInt(agentId));
							request.addProperty("ImageOrder", i);
							request.addProperty("ElevationType", elevation_name[i]);
							request.addProperty("Caption", caption_name[i]);
							request.addProperty("Image", byteimage);
							request.addProperty("Imagename", "Ag_"+agentId+"i"+(rows)+i+".jpg");
							request.addProperty("Flag", flag);
														
							
							envelope.setOutputSoapObject(request);
							marshal.register(envelope);

							System.out.println("SaveAgentElevationImages request is " + request);
							HttpTransportSE androidHttpTransport = new HttpTransportSE(
									cf.URL);
							System.out.println("Before http call");
							androidHttpTransport.call(cf.NAMESPACE+ "SaveAgentElevationImages", envelope);
							image_result = envelope.getResponse().toString();
							System.out.println("SaveAgentElevationImages result is"+ image_result);
							}
							
							show_handler = 5;
							handler.sendEmptyMessage(0);

							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);
						} catch (XmlPullParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);
						}
					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							cf.pd.dismiss();
							if (show_handler == 3) {
								show_handler = 0;
								toast = new ShowToast(
										CompletedInspection.this,
										"There is a problem on your Network. Please try again later with better Network.");

							} else if (show_handler == 4) {
								show_handler = 0;
								toast = new ShowToast(CompletedInspection.this,
										"There is a problem on your application. Please contact Paperless administrator.");

							} else if (show_handler == 5) {
								show_handler = 0;
								
								if(flag==1&&image_result.toLowerCase().equals("true"))
								{
					
									Call_AgentInformationList();
								}
								else if(flag==1&&image_result.toLowerCase().equals("false"))
								{
									toast = new ShowToast(CompletedInspection.this,
											"There is a problem on your application. Please contact Paperless administrator.");
								}

							}
						}
					};
				}.start();
			} else {
				toast = new ShowToast(CompletedInspection.this,"Internet connection not available");
			}
		
	}
	public void Call_AgentInformationList() {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Retrieving Agent Inspection details... Please wait."
					+ "</font></b>";
			final ProgressDialog pd = ProgressDialog.show(CompletedInspection.this,
					"", Html.fromHtml(source), true);
			
			
			new Thread() {
				SoapObject chklogin;
				public void run() {
					Looper.prepare();
					try {
						System.out.println("btns="+rwarr[s]);
						chklogin = cf
								.Calling_WS_AGENTINFORMATIONLIST(agentId,rwarr[s],"AGENTINFORMATIONLIST_NEW");
						System.out.println("response AGENTINFORMATIONLIST_NEW"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									CompletedInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									CompletedInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							Call_AgentInformationList(chklogin);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(CompletedInspection.this,
					"Internet connection not available");

		}
//	}
	}

	public void Call_AgentInformationList(SoapObject objInsert) {
		cf.CreateTable(20);String filename="",path="";
		cf.db.execSQL("delete from " + cf.AgentInspection_Pdf);
		if(!objInsert.equals(null))
		{
			int propertycount = objInsert.getPropertyCount();
			System.out.println("AgentInspection_Pdf property count" + propertycount);
		if(propertycount>=1)
		{
		for (int i = 0; i < propertycount; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				 filename = String.valueOf(obj.getProperty("PDFPath"));
				 System.out.println("filename="+filename);
				String FirstName = String.valueOf(obj.getProperty("Firstname")); System.out.println("FirstName="+FirstName);
				String LastName = String.valueOf(obj.getProperty("Lastname"));
				String InspectionAddress1 = String.valueOf(obj.getProperty("InspectionAddress1"));
				String InspectionAddress2 = String.valueOf(obj.getProperty("InspectionAddress2"));
				String InspectionCity = String.valueOf(obj.getProperty("InspectionCity"));
				String InspectionState = String.valueOf(obj.getProperty("InspectionState"));
				String InspectionCounty = String.valueOf(obj.getProperty("InspectionCounty"));
				String PolicyNumber = String.valueOf(obj.getProperty("PolicyNumber"));
				String InspectionZip = String.valueOf(obj.getProperty("InspectionZip"));
				
				cf.db.execSQL("insert into " + cf.AgentInspection_Pdf
//						+ " (filename) values('"+ cf.encode(filename)
				+ " (filename,FirstName,LastName,InspectionAddress1,InspectionAddress2,InspectionCity," +
				"InspectionState,InspectionCounty,PolicyNumber,InspectionZip) values('"+ cf.encode(filename) + "','"+ 
				cf.encode(FirstName) + "','"+ cf.encode(LastName) + "','"+ cf.encode(InspectionAddress1)
				+ "','"+ cf.encode(InspectionAddress2) + "','"+ cf.encode(InspectionCity) + "','"+ 
				cf.encode(InspectionState) + "','"+ cf.encode(InspectionCounty)
				+ "','"+ cf.encode(PolicyNumber) + "','"+cf.encode(InspectionZip)+"');");
				

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		toast = new ShowToast(CompletedInspection.this,"Agent Inspection submitted successfully");
		try
		{
			cf.db.execSQL("UPDATE "+ cf.AgentInspection +"  SET flag='0',pdfpath='"+cf.encode(filename)+"' where aid='"+rwarr[s]+"' and AgentId='"+agentId+"'");
			System.out.println("UPDATE "+ cf.AgentInspection +"  SET flag='0',pdfpath='"+cf.encode(filename)+"' where aid='"+rwarr[s]+"' and AgentId='"+agentId+"'");
			 Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInspection + " where aid='"+rwarr[s]+"' and AgentId='"+agentId+"'",null);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				path = cf.decode(cur.getString(cur.getColumnIndex("pdfpath")));
		    }
			cur.close();
			
			
			String[] filenamesplit = path.split("/");
			String pdfname = filenamesplit[filenamesplit.length - 1];
			System.out.println("the file name is" + pdfname);
			File sdDir = new File(Environment.getExternalStorageDirectory()
					.getPath());
			File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
					+ pdfname);
            System.out.println("ends"+file.exists());
			if (file.exists()) {
				pter[s].setVisibility(View.VISIBLE);
				download[s].setVisibility(View.GONE);
			} else {
				pter[s].setVisibility(View.VISIBLE);
				download[s].setVisibility(View.GONE);
			}System.out.println("asfas");

			submit[s].setVisibility(View.GONE);
			view[s].setVisibility(View.VISIBLE);
			System.out.println("rty");
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		}
		else
		{
			
		}
		}
		
	}
	public void clicker(View v)
	{
		switch (v.getId()) {
		 case R.id.agentinsp_home:
			Intent intenthome = new Intent(CompletedInspection.this,HomeScreen.class);
			intenthome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intenthome);
			finish();
			break;
		 case R.id.sync:
			 Intent in = new Intent(CompletedInspection.this,SyncInspection.class);
			 startActivity(in);
			 finish();
			 break;
		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(CompletedInspection.this, HomeScreen.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
		
	}
	private static Bitmap decodeFile(String file) {
	    try {
	    	
	    	File f=new File(file);
	    	
	        // Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

	        // The new size we want to scale to
	        final int REQUIRED_SIZE = 150;

	        // Find the correct scale value. It should be the power of 2.
	        int scale = 1;
	        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
	                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
	            scale *= 2;

	        // Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize = scale;
	        o.inJustDecodeBounds = false;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {
	    }

	    return null;
	}
	private void View_Pdf_File(String filename,String filepath)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {

			Intent intentview = new Intent(CompletedInspection.this,
					ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			// finish();
        }
	}
	
}
