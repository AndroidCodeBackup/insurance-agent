package idsoft.agentmodule;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Signup extends Activity {
	EditText etfirstname, etlastname, etmail, etphone, etaddress1, etcity, etzip, etagencyname, etimage;
//	EditText etaddress2;
	RadioGroup rg;
	String strstate="",strcounty="",strrgvalue="",stragencyname="",agencyid;
	ImageView ivimage,ivupload,ivclose;
	LinearLayout llheadshot,llagencynamespinner,llagencynameedittext;
	TextView tvheadshot;
	CommonFunctions cf;
	ShowToast toast;
	int keyDel,id_len;
	public Uri CapturedImageURI;
	byte[] byteimage;
	String imagefilename,county,state,statevalue,stateid1,countyid1,city,id;
	int show_handler,stateid,countyid;
	String[] arraystateid,arraystatename,arraycountyid,arraycountyname,arrayagencyid,arrayagencyname;
	ArrayAdapter<String> stateadapter, countyadapter,agencyadapter;
	Spinner spinnerstate,spinnercounty,spinneragencyname;
	MarshalBase64 marshal;
	DataBaseHelper dbh;
	boolean countysetselection=false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);
		cf = new CommonFunctions(this);

		etfirstname = (EditText) findViewById(R.id.signup_etfirstname);
		etlastname = (EditText) findViewById(R.id.signup_etlastname);
		etmail = (EditText) findViewById(R.id.signup_etemail);
		etphone = (EditText) findViewById(R.id.signup_etphone);
		etaddress1 = (EditText) findViewById(R.id.signup_etaddress1);
//		etaddress2 = (EditText) findViewById(R.id.signup_etaddress2);
		etcity = (EditText) findViewById(R.id.signup_etcity);
		etzip = (EditText) findViewById(R.id.signup_etzip);
		etagencyname = (EditText) findViewById(R.id.signup_etagencyname);
		etimage = (EditText) findViewById(R.id.signup_etimage);
		ivimage = (ImageView) findViewById(R.id.signup_ivimage);
		ivupload = (ImageView) findViewById(R.id.signup_ivimageselect);
		ivclose = (ImageView) findViewById(R.id.signup_ivimageclose);
		rg = (RadioGroup) findViewById(R.id.signup_radiogroup);
		spinnerstate=(Spinner)findViewById(R.id.signup_spinnerstate);
		spinnercounty=(Spinner)findViewById(R.id.signup_spinnercounty);
		spinneragencyname=(Spinner)findViewById(R.id.signup_spinneragencyname);
		llheadshot=(LinearLayout)findViewById(R.id.signup_llheadshot);
		llagencynamespinner=(LinearLayout)findViewById(R.id.signup_llagencynamespinner);
		llagencynameedittext=(LinearLayout)findViewById(R.id.signup_llagencynameedittext);
		tvheadshot=(TextView)findViewById(R.id.signup_tvheadshot);
		
		etfirstname.addTextChangedListener(new CustomTextWatcher(etfirstname));
		etlastname.addTextChangedListener(new CustomTextWatcher(etlastname));
		etmail.addTextChangedListener(new CustomTextWatcher(etmail));
		etaddress1.addTextChangedListener(new CustomTextWatcher(etaddress1));
//		etaddress2.addTextChangedListener(new CustomTextWatcher(etaddress2));
		etcity.addTextChangedListener(new CustomTextWatcher(etcity));
		etzip.addTextChangedListener(new CustomTextWatcher(etzip));
		etagencyname.addTextChangedListener(new CustomTextWatcher(etagencyname));
		
		etphone.setLongClickable(false);
		
		etphone.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etphone.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etphone.setText("");
		        }
				if (etphone.getText().toString().trim().matches("^0") )
	            {
	                // Not allowed
					etphone.setText("");
	            }

				etphone.setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						// TODO Auto-generated method stub
						if (keyCode == KeyEvent.KEYCODE_DEL)
						{
							keyDel = 1;
							System.out.println("Inside delete");
							
							String str = etphone.getText().toString();
							int len=str.length();
							
							if(len==9)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							if(len==5)
							{
//								str = str.substring(0, str.length()-2);
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							if(len==1)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							
							return false;
						}
						else
						{
							keyDel = 0;
							return false;
						}
					}
				});

				if (keyDel == 0) {
					String a = "";
					String str = etphone.getText().toString();
					String replaced = str.replaceAll(Pattern.quote("("), "");
					replaced = replaced.replaceAll(Pattern.quote("-"), "");
					replaced = replaced.replaceAll(Pattern.quote(")"), "");
//					replaced = replaced.replaceAll(Pattern.quote(" "), "");
					char[] id_char = replaced.toCharArray();
					id_len = replaced.length();
					System.out.println("The length is "+id_len);
					for (int i = 0; i < id_len; i++) {
						if (i == 0) {
							a = "(" + id_char[i];
						} else if (i == 2) {
//							a += id_char[i] + ") ";
							a += id_char[i] + ")";
						} else if (i == 5) {
							a += id_char[i] + "-";
						} else
							a += id_char[i];
							keyDel = 0;
					}
					etphone.removeTextChangedListener(this);
					etphone.setText(a);
					if (before > 0)
						etphone.setSelection(start);
					else
						etphone.setSelection(a.length());
					etphone.addTextChangedListener(this);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// TODO Auto-generated method stub

				switch (checkedId) {
				case R.id.signup_rbagent:
					if(!strrgvalue.equals(""))
					{
						etfirstname.setText("");
						etlastname.setText("");
						etmail.setText("");
						etphone.setText("");
						etaddress1.setText("");
//						etaddress2.setText("");
						etcity.setText("");
						etzip.setText("");
						etagencyname.setText("");
						etimage.setText("");
						spinnerstate.setSelection(0);
					}
					llagencynamespinner.setVisibility(View.VISIBLE);
					llagencynameedittext.setVisibility(View.GONE);
					ivclose.setVisibility(View.GONE);
					ivupload.setVisibility(View.VISIBLE);
					ivimage.setImageBitmap(null);
					ivimage.setVisibility(View.GONE);
					byteimage=null;
					llheadshot.setVisibility(View.VISIBLE);
					tvheadshot.setText("Upload Agent Headshot");
					strrgvalue="Agent";
					System.out.println("rgvalue "+strrgvalue);
					break;

				case R.id.signup_rbagency:
					if(!strrgvalue.equals(""))
					{
						etfirstname.setText("");
						etlastname.setText("");
						etmail.setText("");
						etphone.setText("");
						etaddress1.setText("");
//						etaddress2.setText("");
						etcity.setText("");
						etzip.setText("");
						etagencyname.setText("");
						etimage.setText("");
						spinnerstate.setSelection(0);
					}
					llagencynamespinner.setVisibility(View.GONE);
					llagencynameedittext.setVisibility(View.VISIBLE);
					ivclose.setVisibility(View.GONE);
					ivupload.setVisibility(View.VISIBLE);
					ivimage.setImageBitmap(null);
					ivimage.setVisibility(View.GONE);
					byteimage=null;
					llheadshot.setVisibility(View.VISIBLE);
					tvheadshot.setText("Upload Agency Logo");
					strrgvalue="Agency";
					System.out.println("rgvalue "+strrgvalue);
					break;
				}
			}
		});
		
		statevalue = LoadState();

		if (statevalue == "true") {
			stateadapter = new ArrayAdapter<String>(Signup.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate.setAdapter(stateadapter);
		}

		spinnerstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate = spinnerstate.getSelectedItem().toString();
				stateid = spinnerstate.getSelectedItemPosition();
				id = arraystateid[stateid];
				if (!strstate.equals("--Select--")) {
					LoadCounty(id);
					spinnercounty.setEnabled(true);

				} else {
					System.out.println("inside spinner state else");
					// spinnercounty.setAdapter(null);
					spinnercounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(
							Signup.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercounty.setAdapter(countyadapter);

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				System.out.println("On nothing selected");
			}
		});
		
		spinnercounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty = spinnercounty.getSelectedItem().toString();
				countyid = spinnercounty.getSelectedItemPosition();

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		LoadAgencyName();
		
		spinneragencyname.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				stragencyname = spinneragencyname.getSelectedItem().toString();
				int id = spinneragencyname.getSelectedItemPosition();
				agencyid = arrayagencyid[id];
				if(stragencyname.equals("--Select--"))
				{
					stragencyname="";
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		
		etzip.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(etzip.getText().toString().trim().length()==5)
				{
					spinnerstate.setSelection(0);
					Load_State_County_City(etzip);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	public void LoadAgencyName() {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(Signup.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin1 = cf
								.Calling_WS_GETAGENCYNAME("GETAGENCYNAME");
						
						LoadAgencyName(chklogin1);
						System.out.println("response GETAGENCYNAME" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							agencyadapter = new ArrayAdapter<String>(
									Signup.this,
									android.R.layout.simple_spinner_item,
									arrayagencyname);
							agencyadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinneragencyname.setAdapter(agencyadapter);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(Signup.this,
					"Internet Connection is not Available");

		}
	}

	public void LoadAgencyName(SoapObject objInsert) {
		cf.CreateTable(29);
		cf.db.execSQL("delete from " + cf.GetAgencyName);
		int n = objInsert.getPropertyCount();
		System.out.println("GetAgencyName property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("AgencyID"));
				String Name = String.valueOf(obj.getProperty("Agencyname"));
				cf.db.execSQL("insert into " + cf.GetAgencyName
						+ " (id,Name) values('" + cf.encode(id) + "','"
						+ cf.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		Cursor cur = cf.db.rawQuery("select * from " + cf.GetAgencyName+" order by Name COLLATE NOCASE", null);
		int rows = cur.getCount();
		System.out.println("GetAgencyName count is "+rows);
		arrayagencyid = new String[rows + 1];
		arrayagencyname = new String[rows + 1];
		arrayagencyid[0] = "--Select--";
		arrayagencyname[0] = "--Select--";

		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String id = cf.decode(cur.getString(cur.getColumnIndex("id")));
				String name = cf.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arrayagencyid[i] = id;
				arrayagencyname[i] = name;

				i++;
			} while (cur.moveToNext());

		}

	}
	
	/*public void LoadState() {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(Signup.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin1 = cf
								.Calling_WS_LoadState("LoadState");
						SoapObject obj1 = (SoapObject) chklogin1.getProperty(0);
						// InsertData(obj);
						LoadState(chklogin1);
						System.out.println("response LoadState" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							stateadapter = new ArrayAdapter<String>(
									Signup.this,
									android.R.layout.simple_spinner_item,
									arraystatename);
							stateadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerstate.setAdapter(stateadapter);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(Signup.this,
					"Internet Connection is not Available");

		}
	}

	public void LoadState(SoapObject objInsert) {
		cf.CreateTable(26);
		cf.db.execSQL("delete from " + cf.RegLoadState);
		int n = objInsert.getPropertyCount();
		System.out.println("RegLoadState property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("id"));
				String Name = String.valueOf(obj.getProperty("Name"));
				cf.db.execSQL("insert into " + cf.RegLoadState
						+ " (id,Name) values('" + cf.encode(id) + "','"
						+ cf.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		Cursor cur = cf.db.rawQuery("select * from " + cf.RegLoadState, null);
		int rows = cur.getCount();
		arraystateid = new String[rows + 1];
		arraystatename = new String[rows + 1];
		arraystateid[0] = "--Select--";
		arraystatename[0] = "--Select--";

		// arraystateid2 = new String[rows + 1];
		// arraystatename2 = new String[rows + 1];
		// arraystateid2[0] = "--Select--";
		// arraystatename2[0] = "--Select--";

		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String ID = cf.decode(cur.getString(cur.getColumnIndex("id")));
				String Category = cf.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arraystateid[i] = ID;
				arraystatename[i] = Category;

				// arraystateid2[i] = ID;
				// arraystatename2[i] = Category;

				i++;
			} while (cur.moveToNext());

		}

	}

	public void LoadCounty(final String id) {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(Signup.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = cf.Calling_WS_LoadCounty(id,
								"LoadCounty");
						SoapObject obj = (SoapObject) chklogin.getProperty(0);
						// InsertData(obj);
						LoadCounty(chklogin);
						System.out.println("response LoadCounty" + chklogin);
						// pd.dismiss();
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							countyadapter = new ArrayAdapter<String>(
									Signup.this,
									android.R.layout.simple_spinner_item,
									arraycountyname);
							countyadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnercounty.setAdapter(countyadapter);

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(Signup.this,
					"Internet Connection is not Available");

		}
	}

	public void LoadCounty(SoapObject objInsert) {
		cf.CreateTable(27);
		cf.db.execSQL("delete from " + cf.RegLoadCounty);
		int n = objInsert.getPropertyCount();
		System.out.println("RegLoadCounty property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("id"));
				String Name = String.valueOf(obj.getProperty("Name"));
				cf.db.execSQL("insert into " + cf.RegLoadCounty
						+ " (id,Name) values('" + cf.encode(id) + "','"
						+ cf.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		Cursor cur = cf.db.rawQuery("select * from " + cf.RegLoadCounty, null);
		int rows = cur.getCount();
		arraycountyid = new String[rows + 1];
		arraycountyname = new String[rows + 1];
		arraycountyid[0] = "--Select--";
		arraycountyname[0] = "--Select--";

		System.out.println("RegLoadCounty count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String id = cf.decode(cur.getString(cur.getColumnIndex("id")));
				String Name = cf.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arraycountyid[i] = id;
				arraycountyname[i] = Name;

				i++;
			} while (cur.moveToNext());

		}

	}*/
	
	
	public String LoadState() {
		try {
			dbh = new DataBaseHelper(Signup.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = cf.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = cf.decode(cur.getString(cur
							.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}

	public void LoadCounty(final String stateid) {
		try {
			dbh = new DataBaseHelper(Signup.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(Signup.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty.setAdapter(countyadapter);
		
		if(countysetselection)
		{
			spinnercounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		
	}

	
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.signup_ivimageselect:
			Gallery_Camera_Dialog();
			break;
			
		case R.id.signup_ivimageclose:
			ivupload.setVisibility(View.VISIBLE);
			ivclose.setVisibility(View.GONE);
			etimage.setText("");
			ivimage.setImageBitmap(null);
			ivimage.setVisibility(View.GONE);
			byteimage=null;
			break;
			
		case R.id.signup_submit:
			Check_Validation();
			break;
			
		case R.id.signup_clear:
			Intent intent = getIntent();
			overridePendingTransition(0, 0);
			intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			finish();

			overridePendingTransition(0, 0);
			startActivity(intent);
			break;
			
		case R.id.signup_cancel:
			Intent intent1=new Intent(Signup.this,LoginPage.class);
			startActivity(intent1);
			finish();
			break;
		}
	}
	
	private void Gallery_Camera_Dialog() {
		final Dialog dialog = new Dialog(Signup.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alert);
		dialog.setCancelable(false);
		// dialog.setCanceledOnTouchOutside(true);
		Button btnchoosefromgallery = (Button) dialog
				.findViewById(R.id.alert_choosefromgallery);
		Button btntakeapicturefromcamera = (Button) dialog
				.findViewById(R.id.alert_takeapicturefromcamera);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.alert_helpclose);
		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		btnchoosefromgallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
				i.setType("image/*");
				startActivityForResult(i, 0);

			}
		});
		btntakeapicturefromcamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				String fileName = "temp.jpg";
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				CapturedImageURI = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
				startActivityForResult(intent, 1);
			}
		});
		dialog.show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		String selectedImagePath;
		if (requestCode == 0 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String filePath = cursor.getString(columnIndex);

			File f = new File(filePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			System.out.println("The file length is" + length);
			System.out.println("The file length in kb is" + kb);
			System.out.println("The file length in mb is" + mb);

			if (mb >= 2) {
				toast = new ShowToast(Signup.this,
						"File size exceeds!! Too large to attach");
			} else {
				if(bitmapdb!=null)
				{
					bitmapdb.recycle();
				}
				try
				{
					bitmapdb = BitmapFactory.decodeFile(filePath);
//				}
//				catch (OutOfMemoryError e) {
//					// TODO: handle exception
//					toast=new ShowToast(Signup.this, "You cannot upload this image");
//				}
				
				System.out.println("The bitmap is "+bitmapdb);
				if(bitmapdb==null)
				{
					System.out.println("Inside if");
					toast = new ShowToast(Signup.this,
							"File corrupted!! Cant able to attach");
				}
				else
				{
						Call_UploadPhoto_Dialog(filePath);
				}
			}
			catch (OutOfMemoryError e) {
				// TODO: handle exception
				toast=new ShowToast(Signup.this, "File size exceeds!! Too large to attach");
			}
			}

		} else if (requestCode == 1 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = managedQuery(CapturedImageURI, projection, null,
					null, null);
			int column_index_data = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			String capturedImageFilePath = cursor.getString(column_index_data);
			selectedImagePath = capturedImageFilePath;

			File f = new File(selectedImagePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			System.out.println("The file length is" + length);
			System.out.println("The file length in kb is" + kb);
			System.out.println("The file length in mb is" + mb);

			if (mb >= 2) {
				toast = new ShowToast(Signup.this,
						"File size exceeds!! Too large to attach");
			} else {
				if(bitmapdb!=null)
				{
					bitmapdb.recycle();
				}
				try
				{
					bitmapdb = BitmapFactory.decodeFile(selectedImagePath);
//				}
//				catch (OutOfMemoryError e) {
//					// TODO: handle exception
//					toast=new ShowToast(Signup.this, "You cannot upload this image");
//				}
				if(bitmapdb==null)
				{
					toast = new ShowToast(Signup.this,
							"File corrupted!! Cant able to attach");
				}
				else
				{
						Call_UploadPhoto_Dialog(selectedImagePath);
				}
			}
			catch (OutOfMemoryError e) {
				// TODO: handle exception
				toast=new ShowToast(Signup.this, "File size exceeds!! Too large to attach");
			}
			}

		}
	}
	
	private void Call_UploadPhoto_Dialog(final String filePath) {
		final Bitmap bitmapdb = BitmapFactory.decodeFile(filePath);

		final Dialog dialog = new Dialog(Signup.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.upload_photo_alert);
		dialog.setCancelable(false);

		final ImageView ivimage1 = (ImageView) dialog
				.findViewById(R.id.upload_photo_alertimage);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.upload_photo_alert_close);
		Button btnsave = (Button) dialog
				.findViewById(R.id.upload_photo_alert_save);
		Button btncancel = (Button) dialog
				.findViewById(R.id.upload_photo_alert_cancel);

		ivimage1.setImageBitmap(bitmapdb);

		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				ivimage.setVisibility(View.VISIBLE);
				ivimage.setImageBitmap(bitmapdb);
				Signup.this.ivclose.setVisibility(View.VISIBLE);
				etimage.setText(filePath);
				ivupload.setVisibility(
						View.GONE);
				
//				ByteArrayOutputStream stream = new ByteArrayOutputStream();
//				bitmapdb.compress(Bitmap.CompressFormat.PNG, 0, stream);
//				bytecoverpagelogo = stream.toByteArray();
				System.out.println("The filepath is "+filePath);;
				
				String[] filenamesplit = filePath
						.split("/");
				imagefilename = filenamesplit[filenamesplit.length - 1];
				System.out
						.println("The File Name is "
								+ imagefilename);
				
				Bitmap bitmap = cf.ShrinkBitmap(filePath, 400, 400);

				System.out.println(" bimap "+bitmap);
				MarshalBase64 marshal = new MarshalBase64();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.PNG, 100, out);
				byteimage = out.toByteArray();
				System.out.println("bytecoverpagelogo length is "+byteimage.length);

				if(strrgvalue.equals("Agent"))
				{
					toast=new ShowToast(Signup.this, "Headshot uploaded successfully");
				}
				else if(strrgvalue.equals("Agency"))
				{
					toast=new ShowToast(Signup.this, "Logo uploaded successfully");
				}
				
			}
		});

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();

	}
	
	private void Check_Validation()
	{
		if(llagencynameedittext.getVisibility()==View.VISIBLE)
		{
			stragencyname=etagencyname.getText().toString().trim();
		}
		
		if(!strrgvalue.equals(""))
		{
			if(!etfirstname.getText().toString().trim().equals(""))
			{
				if(!etlastname.getText().toString().trim().equals(""))
				{
					if(!etmail.getText().toString().trim().equals(""))
					{
						if(cf.eMailValidation(etmail.getText().toString().trim()))
						{
							if(!etphone.getText().toString().trim().equals(""))
							{
								if(!(etphone.getText().toString().trim().length()<13))
								{
								if(!etaddress1.getText().toString().trim().equals(""))
								{
									if(!strstate.equals("--Select--"))
									{
										if(!strcounty.equals("--Select--"))
										{
											if(!etcity.getText().toString().trim().equals(""))
											{
												if(!etzip.getText().toString().trim().equals(""))
												{
													if((etzip.getText().toString().length()==5))
													{
														if((!etzip.getText().toString().contains("00000")))
														{
															if(!stragencyname.equals(""))
															{
																if(llagencynameedittext.getVisibility()==View.VISIBLE)
																{
																	if(Arrays.asList(arrayagencyname).contains(etagencyname.getText().toString().trim()))
														            {
																		etagencyname.setText("");
														            	toast=new ShowToast(Signup.this, "This Agencyname already registered please enter a different one");
														            }
																	else
																	{
																		if(!etimage.getText().toString().trim().equals(""))
																		{
																			Register();
																		}
																		else
																		{
																			if(strrgvalue.equals("Agent"))
																			{
																				toast=new ShowToast(Signup.this, "Please Upload Agent Headshot");
																				etimage.requestFocus();
																				etimage.setText("");
																			}
																			else
																			{
																				toast=new ShowToast(Signup.this, "Please Upload Agency Logo");
																				etimage.requestFocus();
																				etimage.setText("");
																			}
																		}
																	}
																}
																else
																{
																	if(!etimage.getText().toString().trim().equals(""))
																	{
																		Register();
																	}
																	else
																	{
																		toast=new ShowToast(Signup.this, "Please upload Agent Headshot");
																		etimage.requestFocus();
																		etimage.setText("");
																	}
																}
															}
															else
															{
																if(llagencynameedittext.getVisibility()==View.VISIBLE)
																{
																	toast=new ShowToast(Signup.this, "Please enter a Agency Name");
																	etagencyname.requestFocus();
																	etagencyname.setText("");
																}
																else
																{
																	toast=new ShowToast(Signup.this, "Please select a Agency Name");
																}
																
															}
														}
														else
														{
															toast=new ShowToast(Signup.this, "Please enter a valid Zip");
															etzip.requestFocus();
															etzip.setText("");
														}
												}
												else
												{
													toast=new ShowToast(Signup.this, "Zip should be 5 characters");
													etzip.requestFocus();
													etzip.setText("");
												}
												}
												else
												{
													toast=new ShowToast(Signup.this, "Please enter Zip");
													etzip.requestFocus();
													etzip.setText("");
												}
											}
											else
											{
												toast=new ShowToast(Signup.this, "Please enter City");
												etcity.requestFocus();
												etcity.setText("");
											}
										}
										else
										{
											toast=new ShowToast(Signup.this, "Please select County");
										}
									}
									else
									{
										toast=new ShowToast(Signup.this, "Please select State");
									}
								}
								else
								{
									toast=new ShowToast(Signup.this, "Please enter Address1");
									etaddress1.requestFocus();
									etaddress1.setText("");
								}
							}
							else
							{
								toast=new ShowToast(Signup.this, "Please enter a valid Phone");
								etphone.requestFocus();
								etphone.setText("");
							}
							}
							else
							{
								toast=new ShowToast(Signup.this, "Please enter Phone");
								etphone.requestFocus();
								etphone.setText("");
							}
						}
						else
						{
							toast=new ShowToast(Signup.this, "Please enter a valid Email");
							etmail.requestFocus();
							etmail.setText("");
						}
					}
					else
					{
						toast=new ShowToast(Signup.this, "Please enter Email");
						etmail.requestFocus();
						etmail.setText("");
					}
				}
				else
				{
					toast=new ShowToast(Signup.this, "Please enter Last Name");
					etlastname.requestFocus();
					etlastname.setText("");
				}
			}
			else
			{
				toast=new ShowToast(Signup.this, "Please enter First Name");
				etfirstname.requestFocus();
				etfirstname.setText("");
			}
		}
		else
		{
			toast=new ShowToast(Signup.this, "Please select the option for Signup as");
		}
	}
	
	private void Register()
	{
		if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Registering... Please wait.");
			new Thread() {
				String register_result;
				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(cf.NAMESPACE,
								"COMMONUSERREGISTRATION");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("Singups",strrgvalue);
						request.addProperty("Firstname", etfirstname.getText().toString());
						request.addProperty("Lastname",etlastname.getText().toString());
						request.addProperty("Email",etmail.getText().toString());
						request.addProperty("Phone",etphone.getText().toString());
						request.addProperty("Address1",etaddress1.getText().toString());
//						request.addProperty("Address2",etaddress2.getText().toString());
						request.addProperty("State",stateid1);
						request.addProperty("County",countyid1);
						request.addProperty("City",etcity.getText().toString());
						request.addProperty("Zip",etzip.getText().toString());
						if(strrgvalue.equals("Agent"))
						{
							request.addProperty("Agencyname",stragencyname);
							request.addProperty("AgencyID",Long.parseLong(agencyid));
						}
						else if(strrgvalue.equals("Agency"))
						{
							request.addProperty("Agencyname",etagencyname.getText().toString().trim());
							request.addProperty("AgencyID",0);
						}
						request.addProperty("Realtoragencyname","");
						request.addProperty("RealtorId",0);
						request.addProperty("Photo",byteimage);
						
						envelope.setOutputSoapObject(request);
						marshal = new MarshalBase64();
						marshal.register(envelope);
						System.out.println("COMMONUSERREGISTRATION request is " + request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL);
						System.out.println("Before http call");
						androidHttpTransport.call(cf.NAMESPACE
								+ "COMMONUSERREGISTRATION", envelope);
						register_result = envelope.getResponse().toString();
						System.out.println("COMMONUSERREGISTRATION result is"
								+ register_result);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);

						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							
							if (register_result.toLowerCase().equals("true")) {

								AlertDialog alertDialog = new AlertDialog.Builder(
										Signup.this).create();

								// Setting Dialog Title
								alertDialog.setTitle("Registration Successful");

								// Setting Dialog Message
								alertDialog
										.setMessage("Please check your e-mail in order to activate your account"+"\n"+"Once again thanks for registering. Have a nice day!");

								// Setting OK Button
								alertDialog.setButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int which) {
												Intent intent=new Intent(Signup.this,LoginPage.class);
												startActivity(intent);
												finish();
											}
										});

								// Showing Alert Message
								alertDialog.show();

							} 
							else
							{
								toast = new ShowToast(
										Signup.this,
										"There is a problem on your application. Please contact Paperless administrator.");
							}

						}
					}
				};
			}.start();
		} else {
			toast = new ShowToast(Signup.this,
					"Internet connection not available");
		}
	}
	
	private void Load_State_County_City(final EditText et)
	{

		if (cf.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(Signup.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = cf
								.Calling_WS_GETADDRESSDETAILS(et.getText().toString(),"GETADDRESSDETAILS");
						System.out.println("response GETADDRESSDETAILS" + chklogin1);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									Signup.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							if (chklogin1.toString().equals("anyType{}"))
							{
								et.setText("");
								toast = new ShowToast(Signup.this,
										"Please enter a valid Zip");
							}
							else
							{
								Load_State_County_City(chklogin1);
							}
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(Signup.this,
					"Internet connection not available");

		}
	
	}
	
	private void Load_State_County_City(SoapObject objInsert)
 {
		SoapObject obj = (SoapObject) objInsert.getProperty(0);
		state = String.valueOf(obj.getProperty("s_state"));
		stateid1 = String.valueOf(obj.getProperty("i_state"));
		county = String.valueOf(obj.getProperty("A_County"));
		countyid1 = String.valueOf(obj.getProperty("i_County"));
		city = String.valueOf(obj.getProperty("city"));

		System.out.println("State :" + state);
		System.out.println("County :" + county);
		System.out.println("City :" + city);

		countysetselection = true;
		spinnerstate.setSelection(stateadapter.getPosition(state));
		etcity.setText(city);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		Intent intent=new Intent(Signup.this,LoginPage.class);
		startActivity(intent);
		finish();
	}
	
	

}
