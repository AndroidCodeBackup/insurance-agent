package idsoft.agentmodule;

import java.io.File;

import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.ksoap2.serialization.SoapObject;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class SearchOrder extends Activity {
	TextView tv_welcomename;
	Button btn_home, btn_search, btn_fromdate, btn_todate;
	Spinner spinner_inspectiontype, spinner_country, spinner_state,
			spinner_status;
	EditText et_city, et_zip, et_fromdate, et_todate;
	AutoCompleteTextView actcustomer;
	LinearLayout dynamic;
	int rws;
	String[] data, datasend, countarr, arraystateid, arraystatename, arraycountyid,
			arraycountyname, array_Cat_ID, array_Insp_ID, array_InspName,
			array_Type_ID, array_statusid, array_statusname, array_substatusid,
			pdfpath, array_email;

	int show_handler, Cnt;
	String customername = "", state = "", country = "",
			inspectiontypename = "", cityname = "", zipcode = "", status = "",
			substatus = "", fromdate = "", todate = "", policynumber = "",
			statusid = "", substatusid = "0", stateid = "0", agentemail,
			agencyemail, inspectiontypeid = "0", stringwelcomename,path,str_query="";
	String dta = "", identifier, srid,statevalue="false",inspectionvalue="false";
	String[] alabama_country = { "Autauga" },ownersname;
	CommonFunctions cf;
	View vv;
	ShowToast toast;
	SoapObject result;
	ArrayAdapter<String> countyadapter;
	DatePickerDialog datePicker;
	Cursor cur1;
	static SearchOrder so;
	DataBaseHelper dbh;
	AlertDialog alertDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.searchorder);
		so=this;
		cf = new CommonFunctions(this);
		ImageView iv=(ImageView)findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);
		statevalue=LoadState();
		inspectionvalue=LoadInspectionType();
//		LoadInspectionStatus();
		tv_welcomename = (TextView) findViewById(R.id.searchorder_welcomename);
		btn_home = (Button) findViewById(R.id.searchorder_buttonhome);
		btn_search = (Button) findViewById(R.id.searchorder_btnsearch);
		btn_fromdate = (Button) findViewById(R.id.searchorder_fromdate);
		btn_todate = (Button) findViewById(R.id.searchorder_todate);
		actcustomer = (AutoCompleteTextView) findViewById(R.id.searchorder_autocompletecustomername);
		spinner_inspectiontype = (Spinner) findViewById(R.id.searchorder_spinnerinspectiontype);
		spinner_country = (Spinner) findViewById(R.id.searchorder_spinnercountry);
		spinner_state = (Spinner) findViewById(R.id.searchorder_spinnerstate);
		spinner_status = (Spinner) findViewById(R.id.searchorder_spinnerstatus);
		// spinner_substatus = (Spinner)
		// findViewById(R.id.searchorder_spinnersubstatus);
		et_city = (EditText) findViewById(R.id.searchorder_etcity);
		et_zip = (EditText) findViewById(R.id.searchorder_etzip);
		et_fromdate = (EditText) findViewById(R.id.searchorder_etfromdate);
		et_todate = (EditText) findViewById(R.id.searchorder_ettodate);
		dynamic = (LinearLayout) findViewById(R.id.searchorder_linearlayoutdynamic);

		// et_fromdate.setText("12/31/2012");
		// et_todate.setText("2/28/2013");
		
		
		//DATABASE get coding
		
		
//		DataBaseHelper dbh = new DataBaseHelper(SearchOrder.this);
//        try {
//				dbh.createDataBase();
//				System.out.println("database created");
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				Log.e(getClass().getSimpleName(), "Could not create or Open the database1");
//			}
//        try
//		{
//		SQLiteDatabase newDB =	dbh.openDataBase();
//		System.out.println("btn clicked");
//		Cursor cursor = newDB.query("zipcode", null, null, null, null, null,
//				null, null);
//		System.out.println("after query");
//		System.out.println("The length is "+cursor.getCount());
//	}
//	catch (SQLiteException e) {
//		// TODO: handle exception
//		System.out.println("Exception "+e.getMessage());
//	}
		
		//DATABASE get coding
		
		if(inspectionvalue=="true")
		{
			ArrayAdapter<String> insptypeadapter = new ArrayAdapter<String>(
					SearchOrder.this,
					android.R.layout.simple_spinner_item,
					array_InspName);
			insptypeadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner_inspectiontype.setAdapter(insptypeadapter);
		}
		
		if(statevalue=="true")
		{
			ArrayAdapter<String> stateadapter = new ArrayAdapter<String>(
					SearchOrder.this,
					android.R.layout.simple_spinner_item,
					arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner_state.setAdapter(stateadapter);
		}

		spinner_state.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				state = spinner_state.getSelectedItem().toString();
				int staid = spinner_state.getSelectedItemPosition();
				stateid = arraystateid[staid];
				if (!state.equals("--Select--")) {
					LoadCounty(stateid);
					spinner_country.setEnabled(true);
				} else {
					System.out.println("inside spinner state else");
					// spinnercounty.setAdapter(null);
					spinner_country.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(SearchOrder.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinner_country.setAdapter(countyadapter);

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		spinner_country.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				country = spinner_country.getSelectedItem().toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		cf.CreateTable(1);
		Cursor cur1 = cf.db.rawQuery("select * from " + cf.AgentInformation,
				null);
		cur1.moveToFirst();
		if (cur1.getCount() >= 1) {
			cf.agentid = cf.decode(cur1.getString(cur1
					.getColumnIndex("Agentid")));
			stringwelcomename = cf.decode(cur1.getString(cur1
					.getColumnIndex("FirstName")));
			stringwelcomename += " "
					+ cf.decode(cur1.getString(cur1.getColumnIndex("LastName")));
		}
		tv_welcomename.setText(stringwelcomename);

		spinner_inspectiontype
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						inspectiontypename = spinner_inspectiontype
								.getSelectedItem().toString();
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});
		
		array_statusname=new String[6];
		array_statusname[0]="All";
		array_statusname[1]="Awaiting Scheduling";
		array_statusname[2]="Scheduled";
		array_statusname[3]="Inspected (In QA Process)";
		array_statusname[4]="Reports Ready";
		array_statusname[5]="Suspended";
		
		
		ArrayAdapter<String> statusadapter = new ArrayAdapter<String>(
				SearchOrder.this,
				android.R.layout.simple_spinner_item,
				array_statusname);
		statusadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_status.setAdapter(statusadapter);

		spinner_status.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				status = spinner_status.getSelectedItem().toString();
//				int position = spinner_status.getSelectedItemPosition();
//				statusid = array_statusid[position];
//				System.out.println("The status id is" + statusid);
				// LoadInspectionSubStatus(statusid);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		// spinner_substatus
		// .setOnItemSelectedListener(new OnItemSelectedListener() {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> arg0, View arg1,
		// int arg2, long arg3) {
		// // TODO Auto-generated method stub
		// substatusid = spinner_substatus.getSelectedItem()
		// .toString();
		// // int position = spinner_substatus
		// // .getSelectedItemPosition();
		// // substatusid = array_substatusid[position];
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> arg0) {
		// // TODO Auto-generated method stub
		//
		// }
		// });

		actcustomer.addTextChangedListener(new Text_Watcher());
		
		et_city.addTextChangedListener(new CustomTextWatcher(et_city));
		et_zip.addTextChangedListener(new CustomTextWatcher(et_zip));

	}

	class Text_Watcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			try {
				cf.CreateTable(22);
				Cursor cur2 = cf.db.rawQuery("select * from "
						+ cf.Customer_Details + " where ownersName like '%"
						+ actcustomer.getText().toString() + "%'", null);
				String[] autofirstname = new String[cur2.getCount()];
				cur2.moveToFirst();
				if (cur2.getCount() != 0) {
					if (cur2 != null) {
						int i = 0;
						do {
							autofirstname[i] = cf.decode(cur2.getString(cur2
									.getColumnIndex("ownersName")));

							if (autofirstname[i].contains("null")) {
								autofirstname[i] = autofirstname[i].replace(
										"null", "");
							}

							i++;
						} while (cur2.moveToNext());
					}
					cur2.close();
				}

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						SearchOrder.this, R.layout.autocompletelist,
						autofirstname);

				actcustomer.setThreshold(1);
				actcustomer.setAdapter(adapter);

			} catch (Exception e) {

			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			if (actcustomer.getText().toString().startsWith(" "))
	        {
	            // Not allowed
	        	actcustomer.setText("");
	        }
		}

	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.searchorder_btnsearch:
//			Search();
			Search1();
			break;

		case R.id.searchorder_fromdate:
			identifier = "0";
			showDialog(0);
			break;

		case R.id.searchorder_todate:
			identifier = "1";
			showDialog(0);
			break;

		case R.id.searchorder_buttonhome:
			Intent back = new Intent(SearchOrder.this, HomeScreen.class);
			startActivity(back);
			finish();
			break;

		case R.id.searchorder_buttonplaceorder:
			Intent ord = new Intent(SearchOrder.this, OrderInspection.class);
			startActivity(ord);
			finish();
			break;
			
		case R.id.searchorder_btnclear:
			Intent intent = getIntent();
		    overridePendingTransition(0, 0);
		    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		    finish();

		    overridePendingTransition(0, 0);
		    startActivity(intent);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			// set date picker as current date
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			datePicker = new DatePickerDialog(this, datePickerListener, year,
					month, day);

			return datePicker;

		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			int year = selectedYear;
			int month = selectedMonth;
			int day = selectedDay;

			// set selected date into textview
			if (identifier.equals("0")) {
				et_fromdate.setText(new StringBuilder().append(month + 1)
						.append("/").append(day).append("/").append(year));
			} else {
				et_todate.setText(new StringBuilder().append(month + 1)
						.append("/").append(day).append("/").append(year));
			}

			// updating date picker to current date
			final Calendar c = Calendar.getInstance();
			datePicker.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
					c.get(Calendar.DATE));

		}
	};
	
	private void Search1()
	{
		
		if(!actcustomer.getText().toString().trim().equals(""))
		{
			str_query +=" ownersName like '%" + cf.encode(actcustomer.getText().toString().trim())+ "%' and";
		}
		if(!et_city.getText().toString().trim().equals(""))
		{
			str_query +=" City like '%" + cf.encode(et_city.getText().toString().trim())+ "%' and";
		}
		if(!state.equals("--Select--"))
		{
			str_query +=" State = '" + cf.encode(state.trim())+ "' and";
		}
		if(!country.equals(""))
		{
			if(!country.equals("--Select--"))
			{
				str_query +=" County = '" + cf.encode(country.trim())+ "' and";
			}
		}
		if(!et_zip.getText().toString().trim().equals(""))
		{
			str_query +=" Zipcode = '" + cf.encode(et_zip.getText().toString().trim())+ "' and";
		}
		if(!inspectiontypename.equals("All"))
		{
			str_query +=" Inspection_Type = '" + inspectiontypename.trim() + "' and";
		}
		else if(inspectiontypename.equals("All"))
		{
			str_query +=" Inspection_Type <> '" + inspectiontypename.trim() + "' and";
		}
		if(!status.equals("All"))
		{
			str_query +=" Status = '" + cf.encode(status.trim())+ "' and";
		}
		else if(status.equals("All"))
		{
			str_query +=" Status <> '" + cf.encode(status.trim())+ "' and";
		}
		if(!et_fromdate.getText().toString().trim().equals(""))
		{
			java.util.Date d1 = null;
			java.util.Date d2 = null;
			String date1 = null,date2 = null;
			if(!et_todate.getText().toString().trim().equals(""))
			{
				SimpleDateFormat format = new SimpleDateFormat(
						"MM/dd/yyyy");
				try {  
				    
					d1=new Date(et_fromdate.getText().toString().trim());
					d2=new Date(et_todate.getText().toString().trim());
					date1=format.format(d1);
					date2=format.format(d2);
					System.out.println(d1);
					System.out.println(d2);  
				      
				} catch (ParseException e) {  
				    // TODO Auto-generated catch block  
				    e.printStackTrace();  
				}
				
				str_query +=" RegisterDate between '" + cf.encode(date1)+ 
						"' and '"+cf.encode(date2)+"' and";
			}
		}
//		if(!et_todate.getText().toString().trim().equals(""))
//		{
//			str_query +=" RegisterDate = '" + et_todate.getText().toString().trim()+ "' and";
//		}
		DB_Query();
		
	}
	
	private void DB_Query()
	{
		cf.CreateTable(22);
		if(str_query.equals(""))
		{
			cur1=cf.db.rawQuery("select * from "+cf.Customer_Details+" order by RegisterDate desc", null);
			System.out.println("select * from "+cf.Customer_Details+" order by RegisterDate desc");
			System.out.println("Data Count is "+cur1.getCount());
			str_query="";
			Get_Data1(cur1);
		}
		else
		{
//			str_query = str_query.substring(0, str_query.length()-4);
//			cur1=cf.db.rawQuery("select * from "+cf.Customer_Details+" where"+str_query, null);
//			System.out.println("select * from "+cf.Customer_Details+" where"+str_query);
//			System.out.println("Data Count is "+cur1.getCount());
//			str_query="";
//
//			Get_Data1(cur1);
			
			if(et_fromdate.getText().toString().equals("")&&et_todate.getText().toString().equals(""))
			{
				str_query = str_query.substring(0, str_query.length()-4);
				cur1=cf.db.rawQuery("select * from "+cf.Customer_Details+" where"+str_query+" order by RegisterDate desc", null);
				System.out.println("select * from "+cf.Customer_Details+" where"+str_query+" order by RegisterDate desc");
				System.out.println("Data Count is "+cur1.getCount());
				str_query="";

				Get_Data1(cur1);
			}
			else
			{
				if(et_fromdate.getText().toString().equals("")&&!et_todate.getText().toString().equals(""))
				{
					toast=new ShowToast(SearchOrder.this, "Please select From Date");
				}
				else if(!et_fromdate.getText().toString().equals("")&&et_todate.getText().toString().equals(""))
				{
					toast=new ShowToast(SearchOrder.this, "Please select To Date");
				}
				else
				{
					java.util.Date d1 = null;
					java.util.Date d2 = null;
					String date1 = null, date2 = null;
					SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
					try {

						d1 = new Date(et_fromdate.getText().toString().trim());
						d2 = new Date(et_todate.getText().toString().trim());
						date1 = format.format(d1);
						date2 = format.format(d2);
						System.out.println(d1);
						System.out.println(d2);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if((d1.before(d2)||(d1.equals(d2))))
					{
						str_query = str_query.substring(0, str_query.length()-4);
						cur1=cf.db.rawQuery("select * from "+cf.Customer_Details+" where"+str_query+" order by RegisterDate desc", null);
						System.out.println("select * from "+cf.Customer_Details+" where"+str_query+" order by RegisterDate desc");
						System.out.println("Data Count is "+cur1.getCount());
						str_query="";
			
						Get_Data1(cur1);
					}
					else
					{
						toast=new ShowToast(SearchOrder.this, "From Date is greater than To Date");
					}
				}
			}
			
		}
		
		
	}
	
	private void Get_Data1(final Cursor cur)
	{
		System.out.println("Inside get data");
		cur.moveToFirst();
		Cnt=cur.getCount();
		System.out.println("The data count 2 is "+Cnt);
		if(cur.getCount()>=1)
		{
			cf.show_ProgressDialog("Loading Data... Please wait");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						
						int j=0;
						pdfpath = new String[Cnt];
						data = new String[Cnt];
						datasend = new String[Cnt];
						array_email = new String[Cnt];
						ownersname=new String[Cnt];
						dta = "";
						do
						{
							String ownersName = cf.decode(cur.getString(cur.getColumnIndex("ownersName")));
							data[j] = " " + ownersName + " | ";
							ownersname[j]=ownersName;
							String PolicyNumber = cf.decode(cur.getString(cur.getColumnIndex("PolicyNumber")));
							data[j] += PolicyNumber + " | ";
							datasend[j] = PolicyNumber + "&#40";
							String county = cf.decode(cur.getString(cur.getColumnIndex("County")));
							data[j] += county + " | ";
							String LocnPhone = cf.decode(cur.getString(cur.getColumnIndex("Phone")));
							data[j] += LocnPhone + " | ";
							String email = cf.decode(cur.getString(cur.getColumnIndex("Email")));
							data[j] += email + " | ";
							array_email[j] = email;
							String IMC = cf.decode(cur.getString(cur.getColumnIndex("IMC")));
							data[j] += IMC + " and ";
							String inspectorname = cf.decode(cur.getString(cur.getColumnIndex("InspectorName")));
							data[j] += inspectorname + " | ";
							String Status = cf.decode(cur.getString(cur.getColumnIndex("Status")));
							data[j] += Status + " | ";
							datasend[j] += Status;
							String registerdate = cf.decode(cur.getString(cur.getColumnIndex("RegisterDate")));
							data[j] += "[Reg: " + registerdate + "]";
							String Assgdate = cf.decode(cur.getString(cur.getColumnIndex("AssgDate")));
							data[j] += " [Assg: " + Assgdate + "]";
							String InspDate = cf.decode(cur.getString(cur.getColumnIndex("InspDate")));
							data[j] += " [Insp: " + InspDate + "]";

							String COMMpdf = cf.decode(cur.getString(cur.getColumnIndex("COMMpdf")));
							pdfpath[j] = COMMpdf;
							
							j++;
							
						}while(cur.moveToNext());
//						display();
						show_handler = 5;
						handler.sendEmptyMessage(0);
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						
					} 
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									SearchOrder.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									SearchOrder.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							display1();

						}
					}
				};
			}.start();
		
			
			
		}
		else
		{
			toast = new ShowToast(SearchOrder.this,
					"Sorry, No record found for this search criteria.");
			dynamic.removeAllViews();
			((RelativeLayout) findViewById(R.id.noterel))
					.setVisibility(vv.GONE);
		}
	}
	
	private void display1() {
		dynamic.removeAllViews();
		((RelativeLayout) findViewById(R.id.noterel)).setVisibility(vv.VISIBLE);
		final Button[] view = new Button[Cnt];
		final Button[] download = new Button[Cnt];
		final Button[] pter = new Button[Cnt];
		final Button[] sendmail = new Button[Cnt];
		final TextView[] tvstatus = new TextView[Cnt];
		LinearLayout ll;
		
		System.out.println("The count value is "+Cnt);
		System.out.println("The data length is "+data.length);

		for (int i = 0; i < data.length; i++) {
			LinearLayout.LayoutParams llparams = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);

//			LinearLayout ll = new LinearLayout(this);
//			ll.setOrientation(LinearLayout.HORIZONTAL);
//			ll.setLayoutParams(llparams);
//			dynamic.addView(ll);

			Display display = getWindowManager().getDefaultDisplay();
			DisplayMetrics displayMetrics = new DisplayMetrics();
			display.getMetrics(displayMetrics);

			int width = displayMetrics.widthPixels;
			int height = displayMetrics.heightPixels;

			System.out.println("The width is " + width);
			System.out.println("The height is " + height);

			if (width > 1023 || height > 1023) {
				
				ll = new LinearLayout(this);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(llparams);
				dynamic.addView(ll);
				
				LinearLayout.LayoutParams lltxtparams = new LinearLayout.LayoutParams(
						740,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lltxtparams.setMargins(20, 20, 0, 20);
				
				LinearLayout.LayoutParams llbtnparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llbtnparams.setMargins(10, 0, 10, 0);
				llbtnparams.gravity=Gravity.CENTER_VERTICAL;
				
				LinearLayout.LayoutParams llbtnparams1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llbtnparams1.setMargins(60, 0, 10, 0);
				llbtnparams1.gravity=Gravity.CENTER;
				
				tvstatus[i]=new TextView(this);
				tvstatus[i].setLayoutParams(lltxtparams);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTag(i);
//				tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
				tvstatus[i].setTextColor(Color.WHITE);
				ll.addView(tvstatus[i]);
				
				view[i]=new Button(this);
				view[i].setLayoutParams(llbtnparams);
				view[i].setTag(i);
				view[i].setText("View PDF");
				view[i].setBackgroundResource(R.drawable.buttonrepeat);
				view[i].setTextColor(0xffffffff);
				view[i].setTextSize(14);
				view[i].setTypeface(null, Typeface.BOLD);
				ll.addView(view[i]);
				
				download[i] = new Button(this);
				download[i].setLayoutParams(llbtnparams);
				download[i].setId(2);
				download[i].setText("Download");
				download[i].setBackgroundResource(R.drawable.buttonrepeat);
				download[i].setTextColor(0xffffffff);
				download[i].setTextSize(14);
				download[i].setTypeface(null, Typeface.BOLD);
				download[i].setTag(i);
				ll.addView(download[i]);
				
				pter[i] = new Button(this);
				pter[i].setLayoutParams(llbtnparams);
				pter[i].setId(2);
				pter[i].setText("Email Report");
				pter[i].setBackgroundResource(R.drawable.buttonrepeat);
				pter[i].setTextColor(0xffffffff);
				pter[i].setTextSize(14);
				pter[i].setTypeface(null, Typeface.BOLD);
				pter[i].setTag(i+"&#40"+array_email[i]+"&#40"+ownersname[i]+"&#40"+i);
				ll.addView(pter[i]);
				
				sendmail[i] = new Button(this);
				sendmail[i].setLayoutParams(llbtnparams1);
				sendmail[i].setId(2);
				sendmail[i].setText("Send Mail");
				sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
				sendmail[i].setTextColor(0xffffffff);
				sendmail[i].setTextSize(14);
				sendmail[i].setTypeface(null, Typeface.BOLD);
				sendmail[i].setTag(array_email[i]+"&#40"+ownersname[i]+"&#40"+i);
				ll.addView(sendmail[i]);
				
			}
			else if((width > 500 && width<1000) || (height > 500 && height<1000))
			{
				
				ll = new LinearLayout(this);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(llparams);
				dynamic.addView(ll);

				LinearLayout.LayoutParams lltxtparams = new LinearLayout.LayoutParams(
						500,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lltxtparams.setMargins(5, 20, 0, 20);
				
				tvstatus[i]=new TextView(this);
				tvstatus[i].setLayoutParams(lltxtparams);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTag(i);
//				tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
				tvstatus[i].setTextColor(Color.WHITE);
				ll.addView(tvstatus[i]);
				
				LinearLayout.LayoutParams lllayoutparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lllayoutparams.setMargins(20, 0, 0, 5);
				lllayoutparams.gravity=Gravity.CENTER;
				
				LinearLayout llbtn=new LinearLayout(this);
				llbtn.setOrientation(LinearLayout.VERTICAL);
				llbtn.setLayoutParams(lllayoutparams);
				ll.addView(llbtn);
				
				LinearLayout.LayoutParams llbtnparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llbtnparams.setMargins(0, 0, 0, 0);
				llbtnparams.gravity=Gravity.CENTER_HORIZONTAL;

				LinearLayout.LayoutParams llbtnparams1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llbtnparams1.setMargins(0, 10, 0, 0);
				llbtnparams1.gravity=Gravity.CENTER_HORIZONTAL;
				
				view[i]=new Button(this);
				view[i].setLayoutParams(llbtnparams1);
				view[i].setTag(i);
				view[i].setText("View PDF");
				view[i].setBackgroundResource(R.drawable.buttonrepeat);
				view[i].setTextColor(0xffffffff);
				view[i].setTextSize(14);
				view[i].setTypeface(null, Typeface.BOLD);
				llbtn.addView(view[i]);
				
				download[i] = new Button(this);
				download[i].setLayoutParams(llbtnparams1);
				download[i].setId(2);
				download[i].setText("Download");
				download[i].setBackgroundResource(R.drawable.buttonrepeat);
				download[i].setTextColor(0xffffffff);
				download[i].setTextSize(14);
				download[i].setTypeface(null, Typeface.BOLD);
				download[i].setTag(i);
				llbtn.addView(download[i]);
				
				pter[i] = new Button(this);
				pter[i].setLayoutParams(llbtnparams1);
				pter[i].setId(2);
				pter[i].setText("Email Report");
				pter[i].setBackgroundResource(R.drawable.buttonrepeat);
				pter[i].setTextColor(0xffffffff);
				pter[i].setTextSize(14);
				pter[i].setTypeface(null, Typeface.BOLD);
				pter[i].setTag(i+"&#40"+array_email[i]+"&#40"+ownersname[i]+"&#40"+i);
				llbtn.addView(pter[i]);
				
				sendmail[i] = new Button(this);
				sendmail[i].setLayoutParams(llbtnparams);
				sendmail[i].setId(2);
				sendmail[i].setText("Send Mail");
				sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
				sendmail[i].setTextColor(0xffffffff);
				sendmail[i].setTextSize(14);
				sendmail[i].setTypeface(null, Typeface.BOLD);
				sendmail[i].setTag(array_email[i]+"&#40"+ownersname[i]+"&#40"+i);
				llbtn.addView(sendmail[i]);
				
			
			}
			else
			{
				
				ll = new LinearLayout(this);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(llparams);
				dynamic.addView(ll);
				
				LinearLayout.LayoutParams lltxtparams = new LinearLayout.LayoutParams(
						320,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lltxtparams.setMargins(5, 20, 0, 20);
				
				tvstatus[i]=new TextView(this);
				tvstatus[i].setLayoutParams(lltxtparams);
				tvstatus[i].setText(data[i]);
				tvstatus[i].setTag(i);
//				tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
				tvstatus[i].setTextColor(Color.WHITE);
				ll.addView(tvstatus[i]);
				
				LinearLayout.LayoutParams lllayoutparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lllayoutparams.setMargins(5, 0, 0, 5);
				lllayoutparams.gravity=Gravity.CENTER;
				
				LinearLayout llbtn=new LinearLayout(this);
				llbtn.setOrientation(LinearLayout.VERTICAL);
				llbtn.setLayoutParams(lllayoutparams);
				ll.addView(llbtn);
				
				LinearLayout.LayoutParams llbtnparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llbtnparams.setMargins(0, 0, 0, 0);
				llbtnparams.gravity=Gravity.CENTER_HORIZONTAL;

				LinearLayout.LayoutParams llbtnparams1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llbtnparams1.setMargins(0, 10, 0, 0);
				llbtnparams1.gravity=Gravity.CENTER_HORIZONTAL;
				
				view[i]=new Button(this);
				view[i].setLayoutParams(llbtnparams1);
				view[i].setTag(i);
				view[i].setText("View PDF");
				view[i].setBackgroundResource(R.drawable.buttonrepeat);
				view[i].setTextColor(0xffffffff);
				view[i].setTextSize(14);
				view[i].setTypeface(null, Typeface.BOLD);
				llbtn.addView(view[i]);
				
				download[i] = new Button(this);
				download[i].setLayoutParams(llbtnparams1);
				download[i].setId(2);
				download[i].setText("Download");
				download[i].setBackgroundResource(R.drawable.buttonrepeat);
				download[i].setTextColor(0xffffffff);
				download[i].setTextSize(14);
				download[i].setTypeface(null, Typeface.BOLD);
				download[i].setTag(i);
				llbtn.addView(download[i]);
				
				pter[i] = new Button(this);
				pter[i].setLayoutParams(llbtnparams1);
				pter[i].setId(2);
				pter[i].setText("Email Report");
				pter[i].setBackgroundResource(R.drawable.buttonrepeat);
				pter[i].setTextColor(0xffffffff);
				pter[i].setTextSize(14);
				pter[i].setTypeface(null, Typeface.BOLD);
				pter[i].setTag(i+"&#40"+array_email[i]+"&#40"+ownersname[i]+"&#40"+i);
				llbtn.addView(pter[i]);
				
				sendmail[i] = new Button(this);
				sendmail[i].setLayoutParams(llbtnparams);
				sendmail[i].setId(2);
				sendmail[i].setText("Send Mail");
				sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
				sendmail[i].setTextColor(0xffffffff);
				sendmail[i].setTextSize(14);
				sendmail[i].setTypeface(null, Typeface.BOLD);
				sendmail[i].setTag(array_email[i]+"&#40"+ownersname[i]+"&#40"+i);
				llbtn.addView(sendmail[i]);
				
			}
			
			if (pdfpath[i].equals("N/A")) {
				sendmail[i].setVisibility(View.VISIBLE);
				download[i].setVisibility(View.GONE);
				view[i].setVisibility(View.GONE);
				pter[i].setVisibility(View.GONE);
			}
			else
			{
				sendmail[i].setVisibility(View.GONE);
				download[i].setVisibility(View.VISIBLE);
				view[i].setVisibility(View.VISIBLE);
				pter[i].setVisibility(View.VISIBLE);
			}
			
			path = pdfpath[i];
			String[] filenamesplit = path.split("/");
			final String filename = filenamesplit[filenamesplit.length - 1];
			System.out.println("the file name is" + filename);
			File sdDir = new File(Environment.getExternalStorageDirectory()
					.getPath());
			File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
					+ filename);

			if (file.exists()&&!pdfpath[i].equals("N/A")) {
				pter[i].setVisibility(View.VISIBLE);
				download[i].setVisibility(View.GONE);
			} else if (!file.exists()&&!pdfpath[i].equals("N/A")) {
				pter[i].setVisibility(View.VISIBLE);
				download[i].setVisibility(View.GONE);
			}
			
			view[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Button b = (Button) v;
					String buttonvalue = v.getTag().toString();
					System.out.println("buttonvalue is" + buttonvalue);
					int s = Integer.parseInt(buttonvalue);
					path = pdfpath[s];
					String[] filenamesplit = path
							.split("/");
					final String filename = filenamesplit[filenamesplit.length - 1];
					System.out
							.println("The File Name is "
									+ filename);
					File sdDir = new File(Environment.getExternalStorageDirectory()
							.getPath());
					File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
							+ filename);
					
					if(file.exists())
					{
						View_Pdf_File(filename,path);
					}
					else
					{
						if (cf.isInternetOn() == true) {
							cf.show_ProgressDialog("Downloading... Please wait.");
							new Thread() {
								public void run() {
									Looper.prepare();
									try {
										String extStorageDirectory = Environment
												.getExternalStorageDirectory()
												.toString();
										File folder = new File(
												extStorageDirectory,
												"DownloadedPdfFile");
										folder.mkdir();
										File file = new File(folder,
												filename);
										try {
											file.createNewFile();
											Downloader.DownloadFile(path,
													file);
										} catch (IOException e1) {
											e1.printStackTrace();
										}

										show_handler = 2;
										handler.sendEmptyMessage(0);

									} catch (Exception e) {
										// TODO Auto-generated catch block
										System.out.println("The error is "
												+ e.getMessage());
										e.printStackTrace();
										show_handler = 1;
										handler.sendEmptyMessage(0);

									}
								}

								private Handler handler = new Handler() {
									@Override
									public void handleMessage(Message msg) {
										cf.pd.dismiss();
										// dialog1.dismiss();
										if (show_handler == 1) {
											show_handler = 0;
											toast = new ShowToast(
													SearchOrder.this,
													"There is a problem on your application. Please contact Paperless administrator.");

										} else if (show_handler == 2) {
											show_handler = 0;
//											toast = new ShowToast(
//													VehicleInspection.this,
//													"Report downloaded successfully");
											
//											alertDialog = new AlertDialog.Builder(VehicleInspection.this).create();
//											alertDialog
//													.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
//											alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
//													new DialogInterface.OnClickListener() {
//														public void onClick(DialogInterface dialog, int which) {
//															Call_Dynamic_Pdf_Display();
//														}
//													});
//											alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
//													new DialogInterface.OnClickListener() {
//
//														@Override
//														public void onClick(DialogInterface dialog, int which) {
//															// TODO Auto-generated method stub
//															Call_Dynamic_Pdf_Display();
//														}
//													});
//
//											alertDialog.show();
											
//											Call_Dynamic_Pdf_Display();
											
											View_Pdf_File(filename,path);

										}
									}
								};
							}.start();
						} else {
							toast = new ShowToast(SearchOrder.this,
									"Internet connection not available");

						}
					}
				}
			});

			download[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					// TODO Auto-generated method stub
					Button b = (Button) v;
					if (b.getText().toString().equals("Download")) {
						if (cf.isInternetOn() == true) {
							cf.show_ProgressDialog("Downloading...");
							new Thread() {
								public void run() {
									Looper.prepare();
									try {
										Button b = (Button) v;
										String buttonvalue = v.getTag()
												.toString();
										int s = Integer
												.parseInt(buttonvalue);
										path = pdfpath[s];
										String[] filenamesplit = path
												.split("/");
										final String filename = filenamesplit[filenamesplit.length - 1];
										System.out
												.println("The File Name is "
														+ filename);
										String extStorageDirectory = Environment
												.getExternalStorageDirectory()
												.toString();
										File folder = new File(
												extStorageDirectory,
												"DownloadedPdfFile");
										folder.mkdir();
										File file = new File(folder,
												filename);
										try {
											file.createNewFile();
											Downloader.DownloadFile(path,
													file);
										} catch (IOException e1) {
											e1.printStackTrace();
										}

										show_handler = 2;
										handler.sendEmptyMessage(0);

									} catch (Exception e) {
										// TODO Auto-generated catch block
										System.out.println("The error is "
												+ e.getMessage());
										e.printStackTrace();
										show_handler = 1;
										handler.sendEmptyMessage(0);

									}
								}

								private Handler handler = new Handler() {
									@Override
									public void handleMessage(Message msg) {
										cf.pd.dismiss();
										// dialog1.dismiss();
										if (show_handler == 1) {
											show_handler = 0;
											toast = new ShowToast(
													SearchOrder.this,
													"There is a problem on your application. Please contact Paperless administrator.");

										} else if (show_handler == 2) {
											show_handler = 0;
//											toast = new ShowToast(
//													SearchOrder.this,
//													"Downloaded Successfully");
//											Get_Data1(cur1);
											
											alertDialog = new AlertDialog.Builder(SearchOrder.this).create();
											alertDialog
													.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
											alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
													new DialogInterface.OnClickListener() {
														public void onClick(DialogInterface dialog, int which) {
															Get_Data1(cur1);
														}
													});
											alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
													new DialogInterface.OnClickListener() {

														@Override
														public void onClick(DialogInterface dialog, int which) {
															// TODO Auto-generated method stub
															Get_Data1(cur1);
														}
													});

											alertDialog.show();
											

										}
									}
								};
							}.start();
						} else {
							toast = new ShowToast(SearchOrder.this,
									"Internet connection not available");

						}
					} else {
						toast = new ShowToast(SearchOrder.this,
								"This TAB is under Construction");
					}

				}
			});

			pter[i].setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// toast = new ShowToast(OnlineList.this,
					// "This TAB is under Construction");

					String buttonvalue = v.getTag().toString();
					String[] arrayvalue=buttonvalue.split("&#40");
					String value=arrayvalue[0];
					String emailaddress=arrayvalue[1];
					String name=arrayvalue[2];
					
					String ivalue=arrayvalue[3];
					System.out.println("ivalue ="+ivalue);
					int ival=Integer.parseInt(ivalue);
					String ivalsplit=datasend[ival];
					System.out.println("ivaluesplit ="+ivalsplit);
					String[] arrayivalsplit=ivalsplit.split("&#40");
					String pn=arrayivalsplit[0];
					String status=arrayivalsplit[1];
					
					int s = Integer.parseInt(value);
					path = pdfpath[s];
					String[] filenamesplit = path.split("/");
					String filename = filenamesplit[filenamesplit.length - 1];
					
//					Intent intent=new Intent(SearchOrder.this,EmailReport3.class);
//					intent.putExtra("emailaddress", emailaddress);
//					intent.putExtra("pdfpath", path);
//					intent.putExtra("name", name);
//					intent.putExtra("classidentifier", "SearchOrder");
//					intent.putExtra("policynumber", pn);
//					intent.putExtra("status", status);
//					startActivity(intent);
////					finish();
					
					Intent intent=new Intent(SearchOrder.this,EmailReport2.class);
					intent.putExtra("policynumber", pn);
					intent.putExtra("status", status);
					intent.putExtra("mailid", emailaddress);
					intent.putExtra("classidentifier", "SearchOrder");
					intent.putExtra("ownersname", ownersname[ival]);
					startActivity(intent);
					finish();

					
				}
			});
			
			sendmail[i].setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					String buttonvalue = v.getTag().toString();
					String[] arrayvalue=buttonvalue.split("&#40");
					String emailaddress=arrayvalue[0];
					String name=arrayvalue[1];
					
					String ivalue=arrayvalue[2];
					System.out.println("ivalue ="+ivalue);
					int ival=Integer.parseInt(ivalue);
					String ivalsplit=datasend[ival];
					System.out.println("ivaluesplit ="+ivalsplit);
					String[] arrayivalsplit=ivalsplit.split("&#40");
					String pn=arrayivalsplit[0];
					String status=arrayivalsplit[1];
					
//					Intent intent=new Intent(SearchOrder.this,EmailReport3.class);
//					intent.putExtra("emailaddress", emailaddress);
//					intent.putExtra("pdfpath", "N/A");
//					intent.putExtra("name", name);
//					intent.putExtra("classidentifier", "SearchOrder");
//					intent.putExtra("policynumber", pn);
//					intent.putExtra("status", status);
//					startActivity(intent);
////					finish();
					
					Intent intent=new Intent(SearchOrder.this,EmailReport2.class);
					intent.putExtra("policynumber", pn);
					intent.putExtra("status", status);
					intent.putExtra("mailid", emailaddress);
					intent.putExtra("classidentifier", "SearchOrder");
					intent.putExtra("ownersname", ownersname[ival]);
					startActivity(intent);
					finish();
					
				}
			});
			
			if (i % 2 == 0) {
				ll.setBackgroundColor(Color.parseColor("#13456d"));
			} else {
				ll.setBackgroundColor(Color.parseColor("#386588"));
			}

		}
	}
	
	private void View_Pdf_File(String filename,String filepath)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(SearchOrder.this,
					ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			// finish();
        }
	}


//	private void Search() {
//		customername = actcustomer.getText().toString();
//		if (spinner_state.getSelectedItem().toString().equals("--Select--")) {
//			stateid = "0";
//		}
//		if (et_zip.getText().toString().trim().equals("")) {
//			zipcode = "";
//		}
//		if (et_city.getText().toString().trim().equals("")) {
//			cityname = "";
//		}
//
//		if (cf.isInternetOn() == true) {
//			cf.show_ProgressDialog("Processing");
//			new Thread() {
//				public void run() {
//					Looper.prepare();
//					try {
//						SoapObject request = new SoapObject(cf.NAMESPACE,
//								"SearchHomeowner_Agent");
//						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
//								SoapEnvelope.VER11);
//						envelope.dotNet = true;
//						request.addProperty("AgentID",
//								Integer.parseInt(cf.agentid));
//						request.addProperty("OwnerName", customername);// ""
//						request.addProperty("City", cityname);// ""
//						request.addProperty("State", Integer.parseInt(stateid));// 0
//						request.addProperty("County", country);// ""
//						request.addProperty("Zip", zipcode);//
//						request.addProperty("StatusID",
//								Integer.parseInt(statusid));// 0
//						request.addProperty("SubStatusID", 0);// Integer.parseInt(substatusid)
//						request.addProperty("FromDate", et_fromdate.getText()
//								.toString());// 12/31/2012
//						request.addProperty("ToDate", et_todate.getText()
//								.toString());// 2/28/2013
//						request.addProperty("InspType", inspectiontypeid);// 28
//						request.addProperty("PolicyNumber", "");
//
//						envelope.setOutputSoapObject(request);
//						System.out.println("REQUEt " + request);
//						HttpTransportSE androidHttpTransport = new HttpTransportSE(
//								cf.URL);
//						androidHttpTransport.call(cf.NAMESPACE
//								+ "SearchHomeowner_Agent", envelope);
//						result = (SoapObject) envelope.getResponse();
//						System.out.println("SearchHomeowner_Agent result is"
//								+ result);
//						SoapObject obj = (SoapObject) result.getProperty(0);
//						srid = obj.getProperty("s_SRID").toString();
//						System.out.println("srid value is" + srid);
//
//						show_handler = 5;
//						handler.sendEmptyMessage(0);
//
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						show_handler = 3;
//						handler.sendEmptyMessage(0);
//					} catch (XmlPullParserException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						show_handler = 3;
//						handler.sendEmptyMessage(0);
//					}
//				}
//
//				private Handler handler = new Handler() {
//					@Override
//					public void handleMessage(Message msg) {
//						cf.pd.dismiss();
//						if (show_handler == 3) {
//							show_handler = 0;
//							toast = new ShowToast(
//									SearchOrder.this,
//									"There is a problem on your Network. Please try again later with better Network.");
//
//						} else if (show_handler == 4) {
//							show_handler = 0;
//							toast = new ShowToast(
//									SearchOrder.this,
//									"There is a problem on your application. Please contact Paperless administrator.");
//
//						} else if (show_handler == 5) {
//							show_handler = 0;
//							Get_Data();
//							// display();
//
//						}
//					}
//				};
//			}.start();
//		} else {
//			toast = new ShowToast(SearchOrder.this,
//					"Internet Connection is not Available");
//		}
//	}
//
//	private void Get_Data() {
//		if (srid.equals("0")) {
//
//			toast = new ShowToast(SearchOrder.this,
//					"Sorry, No record found for your search criteria.");
//			dynamic.removeAllViews();
//			((RelativeLayout) findViewById(R.id.noterel))
//					.setVisibility(vv.GONE);
//
//		} else {
//			Cnt = result.getPropertyCount();
//			System.out.println("SearchHomeowner_Agent property count is" + Cnt);
//			pdfpath = new String[Cnt];
//			data = new String[Cnt];
//			array_email = new String[Cnt];
//			dta = "";
//			for (int j = 0; j < Cnt; j++) {
//				SoapObject result1 = (SoapObject) result.getProperty(j);
//
//				// String s_SRID = result1.getProperty("s_SRID")
//				// .toString();
//				// dta += " " + s_SRID;
//
//				String ownersName = result1.getProperty("ownersName")
//						.toString();
//				data[j] = " " + ownersName + " | ";
//				String PolicyNumber = result1.getProperty("PolicyNumber")
//						.toString();
//				data[j] += PolicyNumber + " | ";
//				String county = result1.getProperty("County").toString();
//				data[j] += county + " | ";
//				String LocnPhone = result1.getProperty("Phone").toString();
//				data[j] += LocnPhone + " | ";
//				String email = result1.getProperty("Email").toString();
//				data[j] += email + " | ";
//				array_email[j] = email;
//				String IMC = result1.getProperty("IMC").toString();
//				data[j] += IMC + " and ";
//				String inspectorname = result1.getProperty("InspectorName")
//						.toString();
//				data[j] += inspectorname + " | ";
//				String Status = result1.getProperty("Status").toString();
//				data[j] += Status + " | ";
//				String registerdate = result1.getProperty("RegisterDate")
//						.toString();
//				data[j] += "[Reg: " + registerdate + "]";
//				String Assgdate = result1.getProperty("AssgDate").toString();
//				data[j] += " [Assg: " + Assgdate + "]";
//				String InspDate = result1.getProperty("InspDate").toString();
//				data[j] += " [Insp: " + InspDate + "]" + " ~ ";
//
//				String COMMpdf = result1.getProperty("COMMpdf").toString();
//				pdfpath[j] = COMMpdf;
//
//				// String id =
//				// result1.getProperty("id").toString();
//				// dta += id + " | ";
//				// String Address =
//				// result1.getProperty("Address")
//				// .toString();
//				// dta += Address + " | ";
//				// String Address2 =
//				// result1.getProperty("Address2")
//				// .toString();
//				// dta += Address2 + " | ";
//				// String City = result1.getProperty("City")
//				// .toString();
//				// dta += City + " | ";
//				// String Zipcode =
//				// result1.getProperty("Zipcode")
//				// .toString();
//				// dta += Zipcode + " | ";
//				// dta += PolicyNumber + " | ";
//				// String Carrier =
//				// result1.getProperty("Carrier")
//				// .toString();
//				// dta += Carrier + " | ";
//				// String Additional_services =
//				// result1.getProperty(
//				// "Additional_services").toString();
//				// dta += Additional_services + " | ";
//				// String Inspection_Type = result1.getProperty(
//				// "Inspection_Type").toString();
//				// dta += Inspection_Type + " | ";
//				// String email = result1.getProperty("email")
//				// .toString();
//				// dta += email + " | ";
//				// String LocnCity =
//				// result1.getProperty("LocnCity")
//				// .toString();
//				// dta += LocnCity + " | ";
//				// String inspectorfk = result1.getProperty(
//				// "inspectorfk").toString();
//				// dta += inspectorfk + " | ";
//				// String Scheduled_date = result1.getProperty(
//				// "Scheduled_date").toString();
//				// dta += Scheduled_date + " | ";
//				// String AcceptedDate = result1.getProperty(
//				// "AcceptedDate").toString();
//				// dta += AcceptedDate + " | ";
//				// String zipcode1 =
//				// result1.getProperty("zipcode1")
//				// .toString();
//				// dta += zipcode1 + " | ";
//				// String inspid2 =
//				// result1.getProperty("inspid2")
//				// .toString();
//				// dta += inspid2 + " | ";
//				// String fourpoint =
//				// result1.getProperty("fourpoint")
//				// .toString();
//				// dta += fourpoint + " | ";
//				// String inspid3 =
//				// result1.getProperty("inspid3")
//				// .toString();
//				// dta += inspid3 + " | ";
//				// String wsi =
//				// result1.getProperty("wsi").toString();
//				// dta += wsi + " | ";
//				// String fourpointptsummary =
//				// result1.getProperty(
//				// "fourpointptsummary").toString();
//				// dta += fourpointptsummary + " | ";
//				// String approvefourpoint =
//				// result1.getProperty(
//				// "approvefourpoint").toString();
//				// dta += approvefourpoint + " | ";
//				// String approveWSI = result1.getProperty(
//				// "approveWSI").toString();
//				// dta += approveWSI + " | ";
//				// String IssueSrid =
//				// result1.getProperty("IssueSrid")
//				// .toString();
//				// dta += IssueSrid + " | ";
//				// String IssueID =
//				// result1.getProperty("IssueID")
//				// .toString();
//				// dta += IssueID + " | ";
//				// String InspectionTypeid =
//				// result1.getProperty(
//				// "InspectionTypeid").toString();
//				// dta += InspectionTypeid + " | ";
//				// String MainInspType = result1.getProperty(
//				// "MainInspType").toString();
//				// dta += MainInspType + " | ";
//				// String Inspectionstatus =
//				// result1.getProperty(
//				// "Inspectionstatus").toString();
//				// dta += Inspectionstatus + " | ";
//				// String COMMpdf =
//				// result1.getProperty("COMMpdf")
//				// .toString();
//				// dta += COMMpdf + " | ";
//				// String State = result1.getProperty("State")
//				// .toString();
//				// dta += State + " | ";
//
//			}
//			display();
//			// System.out.println("dta=" + dta + "\n");
//
//		}
//	}
//
//	
//	private void display() {
////		cf.pd.dismiss();
//		dynamic.removeAllViews();
//
//		LinearLayout.LayoutParams tvheadparams = new LinearLayout.LayoutParams(
//				ViewGroup.LayoutParams.MATCH_PARENT,
//				ViewGroup.LayoutParams.WRAP_CONTENT);
//
//		/*
//		 * TextView tvhead = new TextView(this);
//		 * tvhead.setLayoutParams(tvheadparams);
//		 * tvhead.setBackgroundColor(Color.parseColor("#073861"));
//		 * tvhead.setText(
//		 * "Note : Home Owner | Policy number | County | Phone | Inspection Company and Inspector | Status | Date Information"
//		 * ); tvhead.setTextColor(Color.parseColor("#76A4C8"));
//		 * tvhead.setTypeface(null, Typeface.BOLD); tvhead.setTextSize(16);
//		 * dynamic.addView(tvhead);
//		 */
//		((RelativeLayout) findViewById(R.id.noterel)).setVisibility(vv.VISIBLE);
//		ScrollView sv = new ScrollView(this);
//		dynamic.addView(sv);
//
//		final LinearLayout l1 = new LinearLayout(this);
//		l1.setOrientation(LinearLayout.VERTICAL);
//		sv.addView(l1);
//
//		this.data = dta.split("~");
//		System.out.println("the data length is" + data.length);
//		for (int i = 0; i < data.length - 1; i++) {
//
//			TextView[] tv = new TextView[data.length];
//			RelativeLayout[] rl = new RelativeLayout[data.length];
//			Button[] btn = new Button[data.length];
//
//			LinearLayout.LayoutParams llparams = new LinearLayout.LayoutParams(
//					ViewGroup.LayoutParams.MATCH_PARENT,
//					ViewGroup.LayoutParams.WRAP_CONTENT);
//
//			rl[i] = new RelativeLayout(this);
//			rl[i].setLayoutParams(llparams);
//			l1.addView(rl[i]);
//
//			RelativeLayout.LayoutParams txtparams = new RelativeLayout.LayoutParams(
//					ViewGroup.LayoutParams.MATCH_PARENT,
//					ViewGroup.LayoutParams.WRAP_CONTENT);
//			txtparams.setMargins(10, 0, 100, 0);
//
//			RelativeLayout.LayoutParams btnparams = new RelativeLayout.LayoutParams(
//					ViewGroup.LayoutParams.WRAP_CONTENT,
//					ViewGroup.LayoutParams.MATCH_PARENT);
//			btnparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//			btnparams.setMargins(0, 0, 20, 0);
//
//			LinearLayout lltxt = new LinearLayout(this);
//			lltxt.setLayoutParams(txtparams);
//			rl[i].addView(lltxt);
//
//			LinearLayout.LayoutParams llbtnparams = new LinearLayout.LayoutParams(
//					ViewGroup.LayoutParams.MATCH_PARENT,
//					ViewGroup.LayoutParams.WRAP_CONTENT);
//			llbtnparams.setMargins(0, 20, 0, 20);
//
//			tv[i] = new TextView(this);
//			tv[i].setLayoutParams(llbtnparams);
//			tv[i].setText(data[i]);
//			tv[i].setTag(i);
//			tv[i].setTextColor(Color.parseColor("#76A4C8"));
//			lltxt.addView(tv[i]);
//
//			LinearLayout llbtn = new LinearLayout(this);
//			llbtn.setLayoutParams(btnparams);
//			rl[i].addView(llbtn);
//
//			// LinearLayout.LayoutParams llbtnparams1 = new
//			// LinearLayout.LayoutParams(
//			// ViewGroup.LayoutParams.MATCH_PARENT,
//			// ViewGroup.LayoutParams.MATCH_PARENT);
//			// // llbtnparams1.gravity=Gravity.CENTER_VERTICAL;
//			// // llbtnparams1.setMargins(0, 20, 0, 20);
//
//			btn[i] = new Button(this);
//			btn[i].setLayoutParams(llbtnparams);
//			btn[i].setId(2);
//			btn[i].setText("View PDF");
//			btn[i].setBackgroundResource(R.drawable.buttonrepeat);
//			btn[i].setTextColor(0xffffffff);
//			btn[i].setTextSize(14);
//			btn[i].setTypeface(null, Typeface.BOLD);
//			btn[i].setTag(i);
//			btn[i].setGravity(Gravity.CENTER);
//			llbtn.addView(btn[i]);
//
//			if (pdfpath[i].equals("N/A")) {
//				btn[i].setVisibility(View.GONE);
//			}
//
//			btn[i].setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//					// TODO Auto-generated method stub
//					Button b = (Button) v;
//					String buttonvalue = v.getTag().toString();
//					System.out.println("buttonvalue is" + buttonvalue);
//					int s = Integer.parseInt(buttonvalue);
//					String path = pdfpath[s];
//
//					System.out.println("The path to pdf file is " + path);
//
//					Intent intent = new Intent(SearchOrder.this,
//							ViewPdfFile.class);
//					intent.putExtra("path", path);
//					startActivity(intent);
//				}
//			});
//
//			if (i % 2 == 0) {
//				rl[i].setBackgroundColor(Color.parseColor("#13456d"));
//			} else {
//				rl[i].setBackgroundColor(Color.parseColor("#386588"));
//			}
//
//		}
//	}

	private String LoadInspectionType() {
		cf.CreateTable(2);
		Cursor cur = cf.db.rawQuery("select * from " + cf.DashBoard,
				null);
		int rows = cur.getCount();
		System.out.println("Inspection type count is "+rows);
		array_InspName = new String[rows + 1];
		array_InspName[0] = "All";
		
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String InspName = cf.decode(cur.getString(cur
						.getColumnIndex("inspname")));
				
				array_InspName[i] = InspName;
				i++;
			} while (cur.moveToNext());

		}
		cur.close();
		return "true";

	}
	
	public String LoadState() {
		try {
			dbh = new DataBaseHelper(SearchOrder.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = cf.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = cf.decode(cur.getString(cur
							.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}

	public void LoadCounty(final String stateid) {
		try {
			dbh = new DataBaseHelper(SearchOrder.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(SearchOrder.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner_country.setAdapter(countyadapter);
	}

	public void LoadInspectionStatus() {
		if (cf.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Loading data... Please wait."
					+ "</font></b>";
			final ProgressDialog pd = ProgressDialog.show(SearchOrder.this, "",
					Html.fromHtml(source), true);
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin1 = cf
								.Calling_WS_LoadCompany("LoadInspectionStatus");
						LoadInspectionStatus(chklogin1);
						System.out.println("response LoadInspectionStatus"
								+ chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									SearchOrder.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									SearchOrder.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							ArrayAdapter<String> statusadapter = new ArrayAdapter<String>(
									SearchOrder.this,
									android.R.layout.simple_spinner_item,
									array_statusname);
							statusadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinner_status.setAdapter(statusadapter);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(SearchOrder.this,
					"Internet connection not Available");

		}
	}

	public void LoadInspectionStatus(SoapObject objInsert) {
		cf.CreateTable(14);
		cf.db.execSQL("delete from " + cf.LoadInspectionStatus);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadInspectionStatus property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("id"));
				String Name = String.valueOf(obj.getProperty("Name"));
				cf.db.execSQL("insert into " + cf.LoadInspectionStatus
						+ " (id,Name) values('" + cf.encode(id) + "','"
						+ cf.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoadInspectionStatus,
				null);
		int rows = cur.getCount();
		array_statusid = new String[rows + 1];
		array_statusname = new String[rows + 1];
		array_statusid[0] = "0";
		array_statusname[0] = "All";
		System.out.println("LoadInspectionStatus count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String ID = cf.decode(cur.getString(cur.getColumnIndex("id")));
				String Category = cf.decode(cur.getString(cur
						.getColumnIndex("Name")));
				array_statusid[i] = ID;
				array_statusname[i] = Category;
				i++;
			} while (cur.moveToNext());

		}
	}

	// private void LoadInspectionSubStatus(final String id) {
	// if (cf.isInternetOn() == true) {
	// // cf.show_ProgressDialog("Processing");
	// String source = "<b><font color=#00FF33>" + "Processing"
	// + " . Please wait...</font></b>";
	// final ProgressDialog pd = ProgressDialog.show(SearchOrder.this, "",
	// Html.fromHtml(source), true);
	// new Thread() {
	// public void run() {
	// Looper.prepare();
	// try {
	// System.out.println("Inside substatus");
	// SoapObject chklogin = cf.Calling_WS_LoadCounty(id,
	// "LoadInspectionSubStatus");
	// SoapObject obj = (SoapObject) chklogin.getProperty(0);
	// // InsertData(obj);
	// LoadInspectionSubStatus(chklogin);
	// System.out.println("response LoadInspectionSubStatus"
	// + chklogin);
	// // cf.pd.dismiss();
	// show_handler = 5;
	// handler.sendEmptyMessage(0);
	// } catch (SocketException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// show_handler = 3;
	// handler.sendEmptyMessage(0);
	//
	// } catch (NetworkErrorException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// show_handler = 3;
	// handler.sendEmptyMessage(0);
	//
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// show_handler = 3;
	// handler.sendEmptyMessage(0);
	//
	// } catch (TimeoutException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// show_handler = 3;
	// handler.sendEmptyMessage(0);
	//
	// } catch (XmlPullParserException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// show_handler = 3;
	// handler.sendEmptyMessage(0);
	//
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// show_handler = 4;
	// handler.sendEmptyMessage(0);
	//
	// }
	//
	// }
	//
	// private Handler handler = new Handler() {
	// @Override
	// public void handleMessage(Message msg) {
	// // cf.pd.dismiss();
	// pd.dismiss();
	// if (show_handler == 3) {
	// show_handler = 0;
	// toast = new ShowToast(
	// SearchOrder.this,
	// "There is a problem on your Network. Please try again later with better Network.");
	//
	// } else if (show_handler == 4) {
	// show_handler = 0;
	// toast = new ShowToast(
	// SearchOrder.this,
	// "There is a problem on your application. Please contact Paperless administrator.");
	//
	// } else if (show_handler == 5) {
	// show_handler = 0;
	// System.out.println("array_substatusid length="+array_substatusid.length);
	// ArrayAdapter<String> substatusadapter = new ArrayAdapter<String>(
	// SearchOrder.this,
	// android.R.layout.simple_spinner_item,
	// array_substatusid);
	// substatusadapter
	// .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	// spinner_substatus.setAdapter(substatusadapter);
	// }
	// }
	// };
	// }.start();
	//
	// } else {
	// toast = new ShowToast(SearchOrder.this,
	// "Internet Connection is not Available");
	//
	// }
	// }
	//
	// private void LoadInspectionSubStatus(SoapObject objInsert) {
	// cf.CreateTable(15);
	// cf.db.execSQL("delete from " + cf.LoadInspectionSubStatus);
	// int n = objInsert.getPropertyCount();
	// System.out.println("LoadInspectionSubStatus property count" + n);
	// for (int i = 0; i < n; i++) {
	// SoapObject obj = (SoapObject) objInsert.getProperty(i);
	// try {
	// String id = String.valueOf(obj.getProperty("id"));
	// // String Name = String.valueOf(obj.getProperty("Name"));
	// cf.db.execSQL("insert into " + cf.LoadInspectionSubStatus
	// + " (id) values('" + cf.encode(id) + "');");
	//
	// } catch (Exception e) {
	// // TODO: handle exception
	// }
	// }
	// Cursor cur = cf.db.rawQuery("select * from "
	// + cf.LoadInspectionSubStatus, null);
	// int rows = cur.getCount();
	// array_substatusid = new String[rows];
	// System.out.println("LoadInspectors count is " + rows);
	// cur.moveToFirst();
	// if (cur.getCount() >= 1) {
	// int i = 0;
	// do {
	// String id = cf.decode(cur.getString(cur.getColumnIndex("id")));
	// array_substatusid[i] = id;
	// i++;
	// } while (cur.moveToNext());
	//
	// }
	//
	// }

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		Intent back = new Intent(SearchOrder.this, HomeScreen.class);
		startActivity(back);
		finish();
	}

}
