package idsoft.agentmodule;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParserException;

import android.R.string;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EmailReport2 extends Activity {
	CheckBox ckagency, ckagent, ckcustomer, ckother, ckattachreport;
	EditText etagency, etagent, etcustomer, etother, etto, etsubject, etbody;
	CommonFunctions cf;
	ShowToast toast;
	String agentid, agentname, agencyname, agentemail, agencyemail, srid, chklogin, path,to,classidentifier,mailid,ownersname,policynumber;
	boolean report;
	int show_handler;
	LinearLayout llreport;
	TextView tvpercentage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email_report2);
		cf = new CommonFunctions(this);
		ImageView iv=(ImageView)findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);

		ckagency = (CheckBox) findViewById(R.id.e_report2_ckagencymailid);
		ckagent = (CheckBox) findViewById(R.id.e_report2_ckagentmailid);
		ckcustomer = (CheckBox) findViewById(R.id.e_report2_ckcustomermailid);
		ckother = (CheckBox) findViewById(R.id.e_report2_ckothermailid);
		ckattachreport = (CheckBox) findViewById(R.id.e_report2_ckattachreport);
		etagency = (EditText) findViewById(R.id.e_report2_etagencymail);
		etagent = (EditText) findViewById(R.id.e_report2_etagentmail);
		etcustomer = (EditText) findViewById(R.id.e_report2_etcustomermail);
		etother = (EditText) findViewById(R.id.e_report2_etothermail);
		etto = (EditText) findViewById(R.id.e_report2_etto);
		etsubject = (EditText) findViewById(R.id.e_report2_etsubject);
		etbody = (EditText) findViewById(R.id.e_report2_etbody);
		llreport = (LinearLayout) findViewById(R.id.e_report2_llreport);
		tvpercentage=(TextView)findViewById(R.id.e_report2_tvpercentage);
		
		Intent intent=getIntent();
		Bundle bundle=intent.getExtras();
		policynumber=bundle.getString("policynumber");
		String status=bundle.getString("status");
		mailid=bundle.getString("mailid");
		classidentifier=bundle.getString("classidentifier");
		ownersname=bundle.getString("ownersname");
		System.out.println("policynumber="+policynumber);
		etbody.setText("Owners Name ="+ownersname+"\n"+"Policy Number = "+policynumber+"\n"+"Status = "+status);
		
		if(policynumber.equals("AgentInspection2"))
		{
			etbody.setText("Owners Name ="+ownersname);
			llreport.setVisibility(View.VISIBLE);
			path=CompletedInspection.reportpath;
			System.out.println("The path is "+path);
		}
		else if(policynumber.equals("VehicleInspection"))
		{
			etbody.setText("Vehicle Inspection Report for "+ownersname);
			etsubject.setText("Re:Vehicle Inspection Report");
			llreport.setVisibility(View.VISIBLE);
			path=VehicleInspection.reportpath;
			System.out.println("The path is "+path);
		}
		else if(policynumber.equals("Sync"))
		{
			etbody.setText("Owners Name ="+ownersname);
			llreport.setVisibility(View.VISIBLE);
			path=SyncInspection.reportpath;
			System.out.println("The path is "+path);
		}
		else
		{
			etbody.setText("Owners Name ="+ownersname+"\n"+"Policy Number = "+policynumber+"\n"+"Status = "+status);
			Call_Pdf_Available();
		}

		cf.CreateTable(1);
		Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			agentid = cf.decode(cur.getString(cur.getColumnIndex("Agentid")));
			agentname = cf
					.decode(cur.getString(cur.getColumnIndex("FirstName")));
			agentname += " "
					+ cf.decode(cur.getString(cur.getColumnIndex("LastName")));
			agencyname = cf.decode(cur.getString(cur
					.getColumnIndex("AgencyName")));
			agentemail = cf.decode(cur.getString(cur.getColumnIndex("Email")));
			System.out.println("Agent Email " + agentemail);
			agencyemail = cf.decode(cur.getString(cur
					.getColumnIndex("AgencyEmail")));
			System.out.println("Agency Email " + agencyemail);
		}
		cur.close();

		etagency.setText(agencyemail);
		etagent.setText(agentemail);
		etcustomer.setText(mailid);
	
		cf.Device_Information();

		ckagency.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String agencymailid=etagency.getText().toString();
				if (isChecked) {
					if(cf.eMailValidation(agencymailid))
					{
						etto.append(agencymailid + ",");
						etagency.setEnabled(false);
					}
					else
					{
						toast = new ShowToast(EmailReport2.this, "Agency Mail Id is in invalid format");
//						etagency.setText("");
						etagency.requestFocus();
						ckagency.setChecked(false);
					}
				} else /*if(isChecked==false)*/{
					String agency=etto.getText().toString();
					if(agency.contains(etagency.getText().toString()+","))
					{
						agency=agency.replace(etagency.getText().toString()+",", "");
						etto.setText(agency);
						etagency.setEnabled(true);
					}
				}

			}
		});

		ckagent.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String agentmailid=etagent.getText().toString();
				if (isChecked) {
					if(cf.eMailValidation(agentmailid))
					{
						etto.append(agentmailid + ",");
						etagent.setEnabled(false);
					}
					else
					{
						toast = new ShowToast(EmailReport2.this, "Agent Mail Id is in invalid format");
//						etagent.setText("");
						etagent.requestFocus();
						ckagent.setChecked(false);
					}
				} else {
					String agency=etto.getText().toString();
					String agent=etagent.getText().toString();
					if(agency.contains(agent+","))
					{
						agency=agency.replace(agent+",", "");
						etto.setText(agency);
						etagent.setEnabled(true);
					}
				}

			}
		});

		ckcustomer.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String customermailid=etcustomer.getText().toString();
				if (isChecked) {
					if(cf.eMailValidation(customermailid))
					{
						etto.append(customermailid + ",");
						etcustomer.setEnabled(false);
					}
					else
					{
						toast = new ShowToast(EmailReport2.this, "Customer Mail Id is in invalid format");
//						etcustomer.setText("");
						etcustomer.requestFocus();
						ckcustomer.setChecked(false);
					}
				} else {
					String agency=etto.getText().toString();
					String customer=etcustomer.getText().toString();
					if(agency.contains(customer+","))
					{
						agency=agency.replace(customer+",", "");
						etto.setText(agency);
						etcustomer.setEnabled(true);
					}
				}

			}
		});
		
		ckother.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					if(etother.getText().toString().trim().equals(""))
					{
						toast = new ShowToast(EmailReport2.this, "Please enter Mail Id");
//						etother.setText("");
						etother.requestFocus();
						ckother.setChecked(false);
					}
					else
					{
						String othermailid=etother.getText().toString();
						if(othermailid.contains(","))
						{
							String[] splitvalue=othermailid.split(",");
							String valid="";
							for(int i=0;i<splitvalue.length;i++)
							{
								if(cf.eMailValidation(splitvalue[i]))
								{
									valid +="true";
								}
								else
								{
									valid +="false";
								}
							}
							if(valid.contains("false"))
							{
								toast = new ShowToast(EmailReport2.this, "Mail Id is in invalid format");
//								etother.setText("");
								etother.requestFocus();
								ckother.setChecked(false);
							}
							else
							{
								etto.append(etother.getText().toString() + ",");
								etother.setEnabled(false);
							}
						}
						else
						{
							if(cf.eMailValidation(etother.getText().toString()))
							{
								etto.append(etother.getText().toString() + ",");
								etother.setEnabled(false);
							}
							else
							{
								toast = new ShowToast(EmailReport2.this, "Mail Id is in invalid format");
//								etother.setText("");
								etother.requestFocus();
								ckother.setChecked(false);
							}
						}
					}
				} else {
					String agency=etto.getText().toString();
					String other=etother.getText().toString();
					if(agency.contains(other+","))
					{
						agency=agency.replace(other+",", "");
						etto.setText(agency);
						etother.setEnabled(true);
					}
				}

			}
		});
		
		etagency.addTextChangedListener(new CustomTextWatcher(etagency));
		etagent.addTextChangedListener(new CustomTextWatcher(etagent));
		etcustomer.addTextChangedListener(new CustomTextWatcher(etcustomer));
		etother.addTextChangedListener(new CustomTextWatcher(etother));
//		etbody.addTextChangedListener(new CustomTextWatcher(etbody));
		etsubject.addTextChangedListener(new CustomTextWatcher(etsubject));
		
		etbody.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (etbody.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etbody.setText("");
		        }
				int length=etbody.getText().toString().length();
				if(length==0)
				{
					tvpercentage.setText("0%");
				}
				else
				{
					length=length/3;
					tvpercentage.setText(String.valueOf(length)+"%");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.e_report2_placeorder:
			Intent placeintent=new Intent(EmailReport2.this,OrderInspection.class);
			startActivity(placeintent);
			finish();
			
			if(classidentifier.equals("EmailReport"))
			{
				EmailReport.er.finish();
			}
			else if(classidentifier.equals("SearchOrder"))
			{
				SearchOrder.so.finish();
			}
			else if(classidentifier.equals("AgentInspection2"))
			{
				AgentInspection2.ai.finish();
			}
			else if(classidentifier.equals("Sync"))
			{
				SyncInspection.ai.finish();
			}
			else if(classidentifier.equals("VehicleInspection"))
			{
				VehicleInspection.vl.finish();
			}
			else if(classidentifier.equals("OnlineList"))
			{
				OnlineList.ol.finish();
			}
			
			break;
		
		case R.id.e_report2_btnsend:
			Check_Validation();
			break;

		case R.id.e_report2_btncancel:
			if(classidentifier.equals("EmailReport"))
			{
				Intent intenthome = new Intent(EmailReport2.this, EmailReport.class);
				intenthome.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intenthome);
				finish();
			}
			else if(classidentifier.equals("SearchOrder"))
			{
				Intent intent=new Intent(EmailReport2.this,SearchOrder.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				finish();
			}
			else if(classidentifier.equals("AgentInspection2"))
			{
				Intent intent=new Intent(EmailReport2.this,AgentInspection2.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				finish();
			}
			else if(classidentifier.equals("Sync"))
			{
				Intent intent=new Intent(EmailReport2.this,SyncInspection.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				finish();
			}
			else if(classidentifier.equals("VehicleInspection"))
			{
				Intent intent=new Intent(EmailReport2.this,VehicleInspection.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				finish();
			}
			else if(classidentifier.equals("OnlineList"))
			{
				Intent intent=new Intent(EmailReport2.this,OnlineList.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.putExtra("status", OnlineList.strstatus);
				intent.putExtra("inspid",OnlineList.inspid);
				intent.putExtra("inspectionname", OnlineList.strinspectionname);
				startActivity(intent);
				finish();
			}
			
			break;

		case R.id.e_report2_btnviewreport:
//			Intent intent = new Intent(EmailReport2.this, ViewPdfFile.class);
//			intent.putExtra("path", path);
//			startActivity(intent);
			String[] filenamesplit = path.split("/");
			final String filename = filenamesplit[filenamesplit.length - 1];
			System.out.println("The File Name is " + filename);
			File sdDir = new File(Environment.getExternalStorageDirectory()
					.getPath());
			File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
					+ filename);
			if(file.exists())
			{
				View_Pdf_File(filename);
			}
			else
			{
				if (cf.isInternetOn() == true) {
					cf.show_ProgressDialog("Downloading... Please wait.");
					new Thread() {
						public void run() {
							Looper.prepare();
							try {
								String extStorageDirectory = Environment
										.getExternalStorageDirectory()
										.toString();
								File folder = new File(
										extStorageDirectory,
										"DownloadedPdfFile");
								folder.mkdir();
								File file = new File(folder,
										filename);
								try {
									file.createNewFile();
									Downloader.DownloadFile(path,
											file);
								} catch (IOException e1) {
									e1.printStackTrace();
								}

								show_handler = 2;
								handler.sendEmptyMessage(0);

							} catch (Exception e) {
								// TODO Auto-generated catch block
								System.out.println("The error is "
										+ e.getMessage());
								e.printStackTrace();
								show_handler = 1;
								handler.sendEmptyMessage(0);

							}
						}

						private Handler handler = new Handler() {
							@Override
							public void handleMessage(Message msg) {
								cf.pd.dismiss();
								// dialog1.dismiss();
								if (show_handler == 1) {
									show_handler = 0;
									toast = new ShowToast(
											EmailReport2.this,
											"There is a problem on your application. Please contact Paperless administrator.");

								} else if (show_handler == 2) {
									show_handler = 0;
//									toast = new ShowToast(
//											VehicleInspection.this,
//											"Report downloaded successfully");
									
//									alertDialog = new AlertDialog.Builder(VehicleInspection.this).create();
//									alertDialog
//											.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
//									alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
//											new DialogInterface.OnClickListener() {
//												public void onClick(DialogInterface dialog, int which) {
//													Call_Dynamic_Pdf_Display();
//												}
//											});
//									alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
//											new DialogInterface.OnClickListener() {
//
//												@Override
//												public void onClick(DialogInterface dialog, int which) {
//													// TODO Auto-generated method stub
//													Call_Dynamic_Pdf_Display();
//												}
//											});
//
//									alertDialog.show();
									
//									Call_Dynamic_Pdf_Display();
									
									View_Pdf_File(filename);

								}
							}
						};
					}.start();
				} else {
					toast = new ShowToast(EmailReport2.this,
							"Internet connection not available");

				}
			}
			break;

		case R.id.e_report2_toclear:
			ckagency.setChecked(false);
			ckagent.setChecked(false);
			ckcustomer.setChecked(false);
			ckother.setChecked(false);
			etto.setText("");
			break;

		case R.id.e_report2_home:
			Intent intenthome = new Intent(EmailReport2.this, HomeScreen.class);
			startActivity(intenthome);
			finish();
			break;

		}
	}
	
	private void View_Pdf_File(String filename)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(EmailReport2.this,
					ViewPdfFile.class);
			intentview.putExtra("path", path);
			startActivity(intentview);
			// finish();
        }
	}
	
	private void Check_Validation()
	{
		if(!etto.getText().toString().trim().equals(""))
		{
			if(!etsubject.getText().toString().trim().equals(""))
			{
				if(!etbody.getText().toString().equals(""))
				{
					Send_Mail();
				}
				else
				{
					toast = new ShowToast(EmailReport2.this, "Please enter Body");
				}
			}
			else
			{
				toast = new ShowToast(EmailReport2.this, "Please enter Subject");
			}
		}
		else
		{
			toast = new ShowToast(EmailReport2.this, "Please select Mail Id");
		}
	}
	
	private void Send_Mail()
	{
		cf.CreateTable(1);
		Cursor cur1 = cf.db.rawQuery("select * from " + cf.LoginPage, null);
		cur1.moveToFirst();
		if (cur1.getCount() >= 1) {
			agentid = cf
					.decode(cur1.getString(cur1.getColumnIndex("agentid")));
		}
		cur1.close();
		
		to=etto.getText().toString();
		to=to.substring(0,to.length()-1);
		if(!ckattachreport.isChecked())
		{
			path="";
		}
		
			if (cf.isInternetOn() == true) {
				cf.show_ProgressDialog("Sending Mail... Please wait.");
				new Thread() {
					String chklogin;
					public void run() {
						Looper.prepare();
						try {
							chklogin = cf
									.Calling_WS_EmailReport(agentid,to,etsubject.getText().toString(),
											etbody.getText().toString(),path,"Reportsreadymail");
							System.out.println("response EmailReport"
									+ chklogin);
							show_handler = 5;
							handler.sendEmptyMessage(0);
						} catch (SocketException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (NetworkErrorException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (TimeoutException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (XmlPullParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 4;
							handler.sendEmptyMessage(0);

						}

					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							cf.pd.dismiss();
							if (show_handler == 3) {
								show_handler = 0;
								toast = new ShowToast(
										EmailReport2.this,
										"There is a problem on your Network. Please try again later with better Network.");

							} else if (show_handler == 4) {
								show_handler = 0;
								toast = new ShowToast(
										EmailReport2.this,
										"There is a problem on your application. Please contact Paperless administrator.");

							} else if (show_handler == 5) {
								show_handler = 0;
								
								if(chklogin.toLowerCase().equals("true"))
								{
									Intent intenthome = new Intent(EmailReport2.this, HomeScreen.class);
									startActivity(intenthome);
									finish();
									toast = new ShowToast(
											EmailReport2.this,
											"Email sent successfully.");
								}
								else
								{
									Intent intenthome = new Intent(EmailReport2.this, HomeScreen.class);
									startActivity(intenthome);
									finish();
									toast = new ShowToast(
											EmailReport2.this,
											"There is a problem on your application. Please contact Paperless administrator.");
								}
							}
						}
					};
				}.start();
			} else {
				toast = new ShowToast(EmailReport2.this,
						"Internet connection not available");
			}
	}

	private void Call_Pdf_Available() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.Customer_Details
				+ " where PolicyNumber='" + cf.encode(policynumber)
				+ "'", null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			srid = cf.decode(cur.getString(cur.getColumnIndex("s_SRID")));
			System.out.println("The srid is " + srid);
		}
		cur.close();

		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing... Please wait"
					+ "</font></b>";
			final ProgressDialog pd = ProgressDialog.show(EmailReport2.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						chklogin = cf.Calling_WS_ViewCustomerPDF(srid,
								"ViewCustomerPDF");
						System.out.println("response ViewCustomerPDF"
								+ chklogin);

						if (!chklogin.toLowerCase().equals("false")) {

							report = true;
							path=chklogin;
						} else {
							report = false;
							path="N/A";
						}

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									EmailReport2.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									EmailReport2.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							if (report == true) {
								llreport.setVisibility(View.VISIBLE);
							} else {
								llreport.setVisibility(View.GONE);
							}

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(EmailReport2.this,
					"Internet connection not available");

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(classidentifier.equals("EmailReport"))
		{
			Intent intenthome = new Intent(EmailReport2.this, EmailReport.class);
			intenthome.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intenthome);
			finish();
		}
		else if(classidentifier.equals("SearchOrder"))
		{
			Intent intent=new Intent(EmailReport2.this,SearchOrder.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
			finish();
		}
		else if(classidentifier.equals("AgentInspection2"))
		{
			Intent intent=new Intent(EmailReport2.this,AgentInspection2.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
			finish();
		}
		else if(classidentifier.equals("Sync"))
		{
			Intent intent=new Intent(EmailReport2.this,SyncInspection.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
			finish();
		}
		else if(classidentifier.equals("VehicleInspection"))
		{
			Intent intent=new Intent(EmailReport2.this,VehicleInspection.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
			finish();
		}
		else if(classidentifier.equals("OnlineList"))
		{
			Intent intent=new Intent(EmailReport2.this,OnlineList.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra("status", OnlineList.strstatus);
			intent.putExtra("inspid",OnlineList.inspid);
			intent.putExtra("inspectionname", OnlineList.strinspectionname);
			startActivity(intent);
			finish();
		}
	}

}
