package idsoft.agentmodule;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

public class AgentInspection2 extends Activity {
	CommonFunctions cf;
	ShowToast toast;
	public Uri CapturedImageURI;
	LinearLayout lladdress, llgeneralinfo, lladdaimage, llcoverpagelogo,
				 lldisplaypdf,lltypeofstructutecomments,llsignsofneglectcomments,
				 llunusualcomments,lladdnewcondition,lladdnewconditioncomments,llimagecount;
	TextView tvimagecount;
	TableLayout tldynamiclist;
	RelativeLayout rlnote;
	EditText etdate, ettime, etinspaddress1, etinspaddress2, etcity, etzip,
			etmailaddress1, etmailaddress2, etcity2, etzip2, etcoverpagelogo,
			etsquarefootage, etpolicynumber, etpolicyname,ettypeofstructureother,etfirstname,etlastname,
			ettypeofstructurecomments,etsignofneglectcomments,etunusualcommnents,etaddnewconditioncomments,etaddnewconditiontitle;
	String strdate, strtime,AgentId,strcoverpagelogofilename,AgentEmail,AgencyEmail,path,state,stateid,county,countyid,city,zipidentifier;
	static String reportpath;
	ArrayAdapter<String> stateadapter, countyadapter, countyadapter2,
			captionadapter;
	String strstate,lselectedlogo,strstateid, strcounty,strcountyid, strstate2,strstateid2, strcounty2,strcountyid2,
			strinspaddress1="",strinspaddress2="", strcity="", strzip="", strmailaddress1="", strmailaddress2="",
			strcity2="", strzip2="", strselect = "--Select--", strtypeofstructure="",
			strsignofneglect="No", strunusual="No", strcustomdefect="", imageidentifier,filePath2="",
			strelevationvalue, strcaptionvalue,strpolicynumber="",strpolicyname="",strsquarefootage,updatefilepath;
	String[] arraystateid, arraystatename, arraycountyid, arraycountyname,
			arraystateid2, arraystatename2, arraycountyid2, arraycountyname2;
	String[] array_caption = { "--Select--", "Add photo caption" };
	String[] array_elevation = { "--Select--", "Front elevation",
			"Right elevation", "Rear elevation", "Left elevation",
			"Roof system", "Interior photograph" };
	String[] elevation_name,caption_name,file_name,pdfpath,data,datasend;
	
	Spinner spinnerstate, spinnercounty, spinnerstate2, spinnercounty2;
	int show_handler,cadd=0,flag,i,propertycount=0,newconditioncount,llid=0;
	CheckBox cbaddresscheck;
	LinearLayout llspinner, llsinglefamilyhomeother,lladdnewconditiondynamic;
	ImageView plus1,plus2,plus3,plus4,minus1,minus2,minus3,minus4,ivimageedit;
	ImageView ivcoverpagelogo, ivcoverpagelogoselectedimage,ivcoverpagelogoclose;
	ArrayList<String> arraylistcaption = new ArrayList<String>();
	ArrayList<String> arraylistfilepah = new ArrayList<String>();
	int arraylistfilepahlength=0;
	RadioButton rbtypeofstructuresinglefamily, rbtypeofstructurecondo,
			rbtypeofstructuretownhome, rbtypeofstructureother,
			rbsignsofneglectyes, rbsignsofneglectnone,
			rbsignsofneglectnotapplicable, rbhazardyes, rbhazardno,
			rbhazardnotapplicable, rbcustomdefectyes, rbcustomdefectno;

	TableLayout.LayoutParams tlparams;
	TableRow.LayoutParams trparams;
	LinearLayout.LayoutParams rlparams, llparams;
	RelativeLayout.LayoutParams rlimageparams;
	
	Button addnewcondition;
	String order_result,image_result,statevalue="false",currentdate,tagelevation,tagcaption,tagfilepath;
	byte[] bytecoverpagelogo;
	MarshalBase64 marshal;
	DatePickerDialog datePicker;
	TimePickerDialog timepicker;
	boolean call_county2=true,countysetselection=false;
	static AgentInspection2 ai;
	DataBaseHelper dbh;
	Bitmap bitmapdb,bitmap;
	EditText ettitle, etcomments;
	RadioButton rbyes,rbno;
	String title="",comments="";
	String newcondition;
	LinearLayout llparent;
	ArrayList<String> arraylist=new ArrayList<String>();
	AlertDialog alertDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.agentinspection2);
		cf = new CommonFunctions(this);
		ai=this;
		ImageView iv=(ImageView)findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);
		
		final Calendar c = Calendar.getInstance();
		int year1 = c.get(Calendar.YEAR);
		int month1 = c.get(Calendar.MONTH);
		int day1 = c.get(Calendar.DAY_OF_MONTH);
		currentdate=(month1+1)+"/"+day1+"/"+year1;

		cf.CreateTable(31);
		cf.CreateTable(17);
		try {
			Cursor cur = cf.db.rawQuery("select * from " + cf.AddAImage, null);
			cur.moveToFirst();System.out.println("coun"+cur.moveToFirst());
			int result = cur.getColumnIndex("imgid");
			if (result == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.AddAImage+" ADD COLUMN imgid INTEGER ");
			}
			int result1 = cur.getColumnIndex("agentid");
			if (result1 == -1) 
			{
				cf.db.execSQL("ALTER TABLE " + cf.AddAImage+" ADD COLUMN agentid VARCHAR(100)");
			}
		}
       catch (Exception e) {
		// TODO: handle exception
    	   System.out.println("catch"+e.getMessage());
	    }
		
		cf.CreateTable(1);
		Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
//			AgentName = cur.getString(cur.getColumnIndex("FirstName"));
//			AgentName += " "+cur.getString(cur.getColumnIndex("LastName"));
			AgentId = cur.getString(cur.getColumnIndex("Agentid"));
//			AgencyId = cur.getString(cur.getColumnIndex("CitizenAgencyID"));
			AgentEmail = cf.decode(cur.getString(cur
					.getColumnIndex("Email")));
			AgencyEmail = cf.decode(cur.getString(cur
					.getColumnIndex("AgencyEmail")));
		}
		cur.close();
		
		//cf.db.execSQL("delete from " + cf.AddAImage);
		try {
			int id = 0;
			Cursor mCursor= cf.db.rawQuery("SELECT MAX(imgid) AS imgid FROM AddAImage ", null);
	     
	              if (mCursor.getCount() > 0) {
	                mCursor.moveToFirst();
	                id = mCursor.getInt(mCursor.getColumnIndex("imgid"));
	              }
		    	System.out.println("id="+id);
				Cursor cur1 = cf.db.rawQuery("select * from " + cf.AgentInspection + " where AgentId='"+AgentId+"' and aid='"+id+"'",null);
				int cr1 = cur1.getCount();System.out.println("cr1="+cr1);
				if(cr1>0)
				{
					/*cur1.moveToFirst();
					int cflag = cur1.getInt(cur1.getColumnIndex("flag"));System.out.println("cflag="+cflag);
					if(cflag==0)
					{
						
					}
					else
					{
						cf.db.execSQL("delete from " + cf.AddAImage + " where imgid='"+id+"' and agentid='"+AgentId+"'");
						System.out.println("delete from " + cf.AddAImage + " where imgid='"+id+"' and agentid='"+AgentId+"'");
					}*/
				}
				else
				{
					cf.db.execSQL("delete from " + cf.AddAImage + " where imgid='"+id+"' and agentid='"+AgentId+"'");
				}
			
		}
	    catch (Exception e) {
			// TODO: handle exception
	    	   System.out.println("catch"+e.getMessage());
		    }
		
		
		//Call_AgentInformationList();

		llgeneralinfo = (LinearLayout) findViewById(R.id.agentinsp_linearlayoutgeneralinfoborder);
		lladdress = (LinearLayout) findViewById(R.id.agentinsp_linearlayoutaddress);
		lladdaimage = (LinearLayout) findViewById(R.id.agentinsp_linearlayoutaddaimage);
		llcoverpagelogo = (LinearLayout) findViewById(R.id.agentinsp_linearlayoutcoverpagelogo);
		lldisplaypdf = (LinearLayout) findViewById(R.id.agentinsp_linearlayoutdiaplaypdf);
		llimagecount = (LinearLayout) findViewById(R.id.agentinspection_linearlayoutnoofimages);
		tvimagecount = (TextView) findViewById(R.id.agentinspection_tvimagecount);
		rlnote = (RelativeLayout) findViewById(R.id.agentinsp_tableLayout2);
		etdate = (EditText) findViewById(R.id.agentinsp_date);
		ettime = (EditText) findViewById(R.id.agentinsp_time);
		etfirstname = (EditText) findViewById(R.id.agentinsp_etfirstname);
		etlastname = (EditText) findViewById(R.id.agentinsp_etlastname);
		spinnerstate = (Spinner) findViewById(R.id.agentinsp_spinnerstate);
		spinnerstate2 = (Spinner) findViewById(R.id.agentinsp_spinnerstate2);
		spinnercounty = (Spinner) findViewById(R.id.agentinsp_spinnercounty);
		spinnercounty2 = (Spinner) findViewById(R.id.agentinsp_spinnercounty2);
		cbaddresscheck = (CheckBox) findViewById(R.id.agentinsp_addresscheck);
		etinspaddress1 = (EditText) findViewById(R.id.agentinsp_etinspaddress1);
		etinspaddress2 = (EditText) findViewById(R.id.agentinsp_etinspaddress2);
		etcity = (EditText) findViewById(R.id.agentinsp_etcity);
		etzip = (EditText) findViewById(R.id.agentinsp_etzip);
		etmailaddress1 = (EditText) findViewById(R.id.agentinsp_etmailingaddress1);
		etmailaddress2 = (EditText) findViewById(R.id.agentinsp_etmailingaddress2);
		etcity2 = (EditText) findViewById(R.id.agentinsp_etcity2);
		etzip2 = (EditText) findViewById(R.id.agentinsp_etzip2);
		etcoverpagelogo = (EditText) findViewById(R.id.agentinsp_etcoverpagelogo);
		etsquarefootage = (EditText) findViewById(R.id.agentinsp_squarefootage);
		etpolicynumber = (EditText) findViewById(R.id.agentinsp_policynumber);
		etpolicyname = (EditText) findViewById(R.id.agentinsp_policyname);
		ettypeofstructureother = (EditText) findViewById(R.id.agentinsp_ettypeofstructureother);
		etsignofneglectcomments = (EditText) findViewById(R.id.agentinsp_etarethereanysignsofneglectcomments);
		etunusualcommnents = (EditText) findViewById(R.id.agentinsp_etunusualcomments);
		etaddnewconditioncomments = (EditText) findViewById(R.id.agentinsp_etnewconditioncomments);
		etaddnewconditiontitle = (EditText) findViewById(R.id.agentinsp_etaddnewcommentstitle);
		llspinner = (LinearLayout) findViewById(R.id.agentinsp_llspinner);
		llsinglefamilyhomeother = (LinearLayout) findViewById(R.id.agentinsp_llsinglefamilyhomeother);
		llsignsofneglectcomments = (LinearLayout) findViewById(R.id.agentinsp_llarethereanysignsofneglectcomments);
		llunusualcomments = (LinearLayout) findViewById(R.id.agentinsp_llunusualcomments);
		lladdnewcondition = (LinearLayout) findViewById(R.id.agentinsp_lladdnewcondition);
		lladdnewconditioncomments = (LinearLayout) findViewById(R.id.agentinsp_llnewconditioncomments);
		lladdnewconditiondynamic = (LinearLayout) findViewById(R.id.agentinsp_lladdnewconditiondynamic);
		tldynamiclist = (TableLayout) findViewById(R.id.agentinsp_tladdaphoto);
		ivcoverpagelogo = (ImageView) findViewById(R.id.agentinsp_coverpagelogo);
		ivcoverpagelogoclose = (ImageView) findViewById(R.id.agentinsp_coverpagelogoclose);
		ivcoverpagelogoselectedimage = (ImageView) findViewById(R.id.agentinsp_coverpagelogoselectedimage);
		rbtypeofstructuresinglefamily = (RadioButton) findViewById(R.id.agentinsp_singlefamilyhome);
		rbtypeofstructurecondo = (RadioButton) findViewById(R.id.agentinsp_condo);
		rbtypeofstructuretownhome = (RadioButton) findViewById(R.id.agentinsp_townhome);
		rbtypeofstructureother = (RadioButton) findViewById(R.id.agentinsp_typeofstructureother);
		rbsignsofneglectyes = (RadioButton) findViewById(R.id.agentinsp_neglectyes);
		rbsignsofneglectnotapplicable = (RadioButton) findViewById(R.id.agentinsp_neglectnotapplicable);
		rbsignsofneglectnone = (RadioButton) findViewById(R.id.agentinsp_neglectnonenoted);
		rbhazardyes = (RadioButton) findViewById(R.id.agentinsp_unusualyes);
		rbhazardno = (RadioButton) findViewById(R.id.agentinsp_unusualno);
		rbhazardnotapplicable = (RadioButton) findViewById(R.id.agentinsp_unusualnotapplicable);
		rbcustomdefectyes = (RadioButton) findViewById(R.id.agentinsp_customdefectyes);
		rbcustomdefectno = (RadioButton) findViewById(R.id.agentinsp_customdefectno);
		plus1 = (ImageView) findViewById(R.id.agentinsp_plus1);
		plus2 = (ImageView) findViewById(R.id.agentinsp_plus2);
		plus3 = (ImageView) findViewById(R.id.agentinsp_plus3);
		plus4 = (ImageView) findViewById(R.id.agentinsp_plus4);
		minus1 = (ImageView) findViewById(R.id.agentinsp_minus1);
		minus2 = (ImageView) findViewById(R.id.agentinsp_minus2);
		minus3 = (ImageView) findViewById(R.id.agentinsp_minus3);
		minus4 = (ImageView) findViewById(R.id.agentinsp_minus4);
		addnewcondition=(Button)findViewById(R.id.agentinsp_addnewcondition);

		cbaddresscheck
				.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
		etpolicynumber.addTextChangedListener(new CustomTextWatcher(etpolicynumber));
		etpolicyname.addTextChangedListener(new CustomTextWatcher(etpolicyname));
//		etsquarefootage.addTextChangedListener(new CustomTextWatcher(etsquarefootage));
		ettypeofstructureother.addTextChangedListener(new CustomTextWatcher(ettypeofstructureother));
		etsignofneglectcomments.addTextChangedListener(new CustomTextWatcher(etsignofneglectcomments));
		etunusualcommnents.addTextChangedListener(new CustomTextWatcher(etunusualcommnents));
		etaddnewconditiontitle.addTextChangedListener(new CustomTextWatcher(etaddnewconditiontitle));
		etaddnewconditioncomments.addTextChangedListener(new CustomTextWatcher(etaddnewconditioncomments));
		etfirstname.addTextChangedListener(new CustomTextWatcher(etfirstname));
		etlastname.addTextChangedListener(new CustomTextWatcher(etlastname));
		etinspaddress1.addTextChangedListener(new CustomTextWatcher(etinspaddress1));
		etinspaddress2.addTextChangedListener(new CustomTextWatcher(etinspaddress2));
		etcity.addTextChangedListener(new CustomTextWatcher(etcity));
		etzip.addTextChangedListener(new CustomTextWatcher(etzip));
		etmailaddress1.addTextChangedListener(new CustomTextWatcher(etmailaddress1));
		etmailaddress2.addTextChangedListener(new CustomTextWatcher(etmailaddress2));
		etcity2.addTextChangedListener(new CustomTextWatcher(etcity2));
		etzip2.addTextChangedListener(new CustomTextWatcher(etzip2));
		
		etsquarefootage.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etsquarefootage.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etsquarefootage.setText("");
		        }
				if (etsquarefootage.getText().toString().trim().matches("^0") )
	            {
	                // Not allowed
					etsquarefootage.setText("");
	            }
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		

		statevalue=LoadState();
		
		if(statevalue=="true")
		{
			stateadapter = new ArrayAdapter<String>(
					AgentInspection2.this,
					android.R.layout.simple_spinner_item,
					arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate.setAdapter(stateadapter);

			stateadapter = new ArrayAdapter<String>(
					AgentInspection2.this,
					android.R.layout.simple_spinner_item,
					arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate2.setAdapter(stateadapter);
		}

		spinnerstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate = spinnerstate.getSelectedItem().toString();
				int stateid = spinnerstate.getSelectedItemPosition();
				strstateid = arraystateid[stateid];
				if (!strstate.equals("--Select--")) {
					LoadCounty(strstateid);
					spinnercounty.setEnabled(true);

				} else {
					System.out.println("inside spinner state else");
					// spinnercounty.setAdapter(null);
					spinnercounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(
							AgentInspection2.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercounty.setAdapter(countyadapter);

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		spinnerstate2.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				call_county2=true;
				return false;
			}
		});

		spinnerstate2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate2 = spinnerstate2.getSelectedItem().toString();
				int stateid = spinnerstate2.getSelectedItemPosition();
				strstateid2 = arraystateid[stateid];
				if(call_county2==true)
				{
					if (!strstate2.equals("--Select--")) {
						LoadCounty2(strstateid2);
						spinnercounty2.setEnabled(true);

					} else {
						// spinnercounty2.setAdapter(null);
						spinnercounty2.setEnabled(false);
						arraycountyname2 = new String[0];
						countyadapter2 = new ArrayAdapter<String>(
								AgentInspection2.this,
								android.R.layout.simple_spinner_item,
								arraycountyname2);
						countyadapter2
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						spinnercounty2.setAdapter(countyadapter2);
					}
				}
				else
				{
					spinnercounty2.setEnabled(true);
					spinnercounty2.setSelection(spinnercounty.getSelectedItemPosition());
				}
				

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnercounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty = spinnercounty.getSelectedItem().toString();
				int id = spinnercounty.getSelectedItemPosition();
				strcountyid = arraycountyid[id];

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnercounty2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty2 = spinnercounty2.getSelectedItem().toString();
				int id = spinnercounty2.getSelectedItemPosition();
				strcountyid2 = arraycountyid2[id];

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		addnewcondition.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("addnewcondition clicked");
//				addnewcondition.setVisibility(View.GONE);
//				lladdnewcondition.setVisibility(View.VISIBLE);
				AddNewConditionDynamic();
			}
		});
		
		etzip.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(etzip.getText().toString().trim().length()==5)
				{
					spinnerstate.setSelection(0);
					zipidentifier="zip1";
					Load_State_County_City(etzip);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		etzip2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etzip2.getText().toString().trim().length() == 5) {
					spinnerstate2.setSelection(0);
					zipidentifier = "zip2";
					Load_State_County_City(etzip2);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

	}
	
	private void AddNewConditionDynamic()
	{
		lladdnewconditiondynamic.setVisibility(View.VISIBLE);
		newconditioncount=lladdnewconditiondynamic.getChildCount();
		System.out.println("the child count is "+newconditioncount);
		
		llid=llid+1;
		
//		final LinearLayout llparent;
		final LinearLayout llchild1;
		final LinearLayout llchild2;
//		final EditText ettitle, etcomments;
//		final RadioButton rbyes,rbno;
		final ImageView clear;
		final TextView tvstar,tvcolon;
		
		llparent=new LinearLayout(this);
		llparent.setOrientation(LinearLayout.VERTICAL);
		llparent.setId(llid);
		llparent.setTag(llid);
		lladdnewconditiondynamic.addView(llparent);

		LinearLayout.LayoutParams llchild1params = null, llchild2params = null, llettitleparams = null, 
							llrbyesparams = null, llrbnoparams = null, llclearparams = null, lletcommentsparams = null;

		arraylist.add(String.valueOf(llid));
		
		Display display = getWindowManager().getDefaultDisplay();
	    DisplayMetrics displayMetrics = new DisplayMetrics();
	    display.getMetrics(displayMetrics);

	    int width = displayMetrics.widthPixels;
	    int height = displayMetrics.heightPixels;
	    
	    System.out.println("The width is "+width);
	    System.out.println("The height is "+height);
		
		if(width > 1023 || height > 1023)
		{
			llchild1params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llchild1params.setMargins(10, 10, 10, 10);
			
			llchild2params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llchild2params.setMargins(200, 0, 0, 10);
			
			llettitleparams = new LinearLayout.LayoutParams(
					150,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llettitleparams.setMargins(4, 0, 0, 0);
			
			llrbyesparams = new LinearLayout.LayoutParams(
					175,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llrbyesparams.setMargins(20, 0, 0, 0);
			
			llrbnoparams = new LinearLayout.LayoutParams(
					175,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			
			llclearparams = new LinearLayout.LayoutParams(
					40,40);
			llclearparams.setMargins(10, 0, 0, 0);
			llclearparams.gravity=Gravity.CENTER_VERTICAL;
			
			lletcommentsparams = new LinearLayout.LayoutParams(
					250,
					ViewGroup.LayoutParams.WRAP_CONTENT);
		}
		else
		{
			llchild1params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llchild1params.setMargins(5, 5, 5, 5);
			
			llchild2params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llchild2params.setMargins(150, 0, 0, 10);
			
			llettitleparams = new LinearLayout.LayoutParams(
					100,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llettitleparams.setMargins(4, 0, 0, 0);
			
			llrbyesparams = new LinearLayout.LayoutParams(
					100,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llrbyesparams.setMargins(20, 0, 0, 0);
			
			llrbnoparams = new LinearLayout.LayoutParams(
					100,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			
			llclearparams = new LinearLayout.LayoutParams(
					40,40);
			llclearparams.setMargins(10, 0, 0, 0);
			llclearparams.gravity=Gravity.CENTER_VERTICAL;
			
			lletcommentsparams = new LinearLayout.LayoutParams(
					175,
					ViewGroup.LayoutParams.WRAP_CONTENT);
		}
		
		
		llchild1=new LinearLayout(this);
		llchild1.setLayoutParams(llchild1params);
		llchild1.setOrientation(LinearLayout.HORIZONTAL);
		llchild1.setPadding(5, 0, 0, 0);
		llparent.addView(llchild1);
		
		tvstar=new TextView(this);
		tvstar.setTextAppearance(AgentInspection2.this, R.style.stitle);
		tvstar.setText("*");
		llchild1.addView(tvstar);
		
		int maxLength = 50;
		InputFilter[] fArray = new InputFilter[1];
		fArray[0] = new InputFilter.LengthFilter(maxLength);
		
		ettitle=new EditText(this);
		ettitle.setLayoutParams(llettitleparams);
		ettitle.setTag("ettitle");
		ettitle.setBackgroundResource(R.drawable.editbox);
		ettitle.setHint("Enter your title");
		ettitle.setPadding(5, 0, 0, 0);
		ettitle.setMinHeight(50);
		ettitle.setWidth(0);
		ettitle.setFilters(fArray);
		llchild1.addView(ettitle);
		
		tvcolon=new TextView(this);
		tvcolon.setText(":");
		tvcolon.setTextAppearance(AgentInspection2.this, R.style.ccolon);
		llchild1.addView(tvcolon);
		
		rbyes=new RadioButton(this);
		rbyes.setLayoutParams(llrbyesparams);
		rbyes.setTag("rbyes");
		rbyes.setText("Yes");
		rbyes.setId(llid);
		rbyes.setTextColor(0xffcbddeb);
		llchild1.addView(rbyes);
		
		rbno=new RadioButton(this);
		rbno.setLayoutParams(llrbnoparams);
		rbno.setTag("rbno");
		rbno.setText("No");
		rbno.setId(llid);
		rbno.setTextColor(0xffcbddeb);
		llchild1.addView(rbno);
		
		clear=new ImageView(this);
		clear.setLayoutParams(llclearparams);
		clear.setTag(llid);
		clear.setBackgroundResource(R.drawable.iconcross);
		llchild1.addView(clear);
		
		llchild2=new LinearLayout(this);
		llchild2.setLayoutParams(llchild2params);
		llchild2.setVisibility(View.GONE);
		llparent.addView(llchild2);
		
		int maxLength1 = 100;
		InputFilter[] fArray1 = new InputFilter[1];
		fArray1[0] = new InputFilter.LengthFilter(maxLength1);
		
		etcomments=new EditText(this);
		etcomments.setLayoutParams(lletcommentsparams);
		etcomments.setTag("etcomments");
		etcomments.setBackgroundResource(R.drawable.editbox);
		etcomments.setPadding(5, 0, 0, 0);
		etcomments.setMinHeight(50);
		etcomments.setWidth(0);
		etcomments.setFilters(fArray1);
		llchild2.addView(etcomments);
		
		rbyes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int id=v.getId();
				LinearLayout ll=(LinearLayout) (findViewById(id));
				RadioButton yes=(RadioButton)ll.findViewWithTag("rbyes");
				RadioButton no=(RadioButton)ll.findViewWithTag("rbno");
				llchild2.setVisibility(View.VISIBLE);
				yes.setChecked(true);
				no.setChecked(false);
			}
		});
		
		rbno.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int id=v.getId();
				LinearLayout ll=(LinearLayout) (findViewById(id));
				RadioButton yes=(RadioButton)ll.findViewWithTag("rbyes");
				RadioButton no=(RadioButton)ll.findViewWithTag("rbno");
				llchild2.setVisibility(View.GONE);
				yes.setChecked(false);
				no.setChecked(true);
				etcomments.setText("");
			}
		});
		
		clear.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				final String value=v.getTag().toString();
				final int val=Integer.parseInt(value);
				
				final Dialog dialog3 = new Dialog(AgentInspection2.this);
				dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog3.setContentView(R.layout.confirm_delete);
				dialog3.setCancelable(false);
				Button btnyes = (Button) dialog3
						.findViewById(R.id.confirmdelete_yes);
				Button btnno = (Button) dialog3
						.findViewById(R.id.confirmdelete_no);
				
				btnyes.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						View llv=findViewById(val);
						lladdnewconditiondynamic.removeView(llv);
						arraylist.remove(value);
						System.out.println("Array length is "+arraylist.size());
						dialog3.dismiss();
					}
				});

				btnno.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog3.dismiss();
					}
				});

				dialog3.show();
			}
		});
		
		View v1=findViewById(llid);
		EditText etit=(EditText)v1.findViewWithTag("ettitle");
		EditText etcom=(EditText)v1.findViewWithTag("etcomments");
		etit.addTextChangedListener(new CustomTextWatcher(etit));
		etcom.addTextChangedListener(new CustomTextWatcher(etcom));
		
	}
	
//	private void AddNewConditionDynamic()
//	{
//		lladdnewconditiondynamic.setVisibility(View.VISIBLE);
//		newconditioncount=lladdnewconditiondynamic.getChildCount();
//		newconditioncount=newconditioncount+1;
//		
//		arrayllvalue=new String[newconditioncount];
//		
//		final LinearLayout[] llparent;
//		final LinearLayout[] llchild1;
//		final LinearLayout[] llchild2;
////		final EditText[] ettitle;
////		final EditText[] etcomments;
////		final RadioButton[] rbyes,rbno;
//		final ImageView[] clear;
//		final TextView[] tvstar,tvcolon;
//		
//		llparent=new LinearLayout[newconditioncount];
//		llchild1=new LinearLayout[newconditioncount];
//		llchild2=new LinearLayout[newconditioncount];
//		ettitle=new EditText[newconditioncount];
//		etcomments=new EditText[newconditioncount];
//		rbyes=new RadioButton[newconditioncount];
//		rbno=new RadioButton[newconditioncount];
//		clear=new ImageView[newconditioncount];
//		tvstar=new TextView[newconditioncount];
//		tvcolon=new TextView[newconditioncount];
//		
//		llparent[newconditioncount-1]=new LinearLayout(this);
//		llparent[newconditioncount-1].setOrientation(LinearLayout.VERTICAL);
//		lladdnewconditiondynamic.addView(llparent[newconditioncount-1]);
//		
//		LinearLayout.LayoutParams llchild1params = new LinearLayout.LayoutParams(
//				ViewGroup.LayoutParams.WRAP_CONTENT,
//				ViewGroup.LayoutParams.WRAP_CONTENT);
//		llchild1params.setMargins(10, 10, 10, 10);
//		
//		LinearLayout.LayoutParams llchild2params = new LinearLayout.LayoutParams(
//				ViewGroup.LayoutParams.WRAP_CONTENT,
//				ViewGroup.LayoutParams.WRAP_CONTENT);
//		llchild2params.setMargins(200, 0, 0, 10);
//		
//		LinearLayout.LayoutParams llettitleparams = new LinearLayout.LayoutParams(
//				150,
//				ViewGroup.LayoutParams.WRAP_CONTENT);
//		llettitleparams.setMargins(4, 0, 0, 0);
//		
//		LinearLayout.LayoutParams llrbyesparams = new LinearLayout.LayoutParams(
//				175,
//				ViewGroup.LayoutParams.WRAP_CONTENT);
//		llrbyesparams.setMargins(20, 0, 0, 0);
//		
//		LinearLayout.LayoutParams llrbnoparams = new LinearLayout.LayoutParams(
//				175,
//				ViewGroup.LayoutParams.WRAP_CONTENT);
//		
//		LinearLayout.LayoutParams llclearparams = new LinearLayout.LayoutParams(
//				40,40);
//		llclearparams.setMargins(10, 0, 0, 0);
//		llclearparams.gravity=Gravity.CENTER_VERTICAL;
//		
//		LinearLayout.LayoutParams lletcommentsparams = new LinearLayout.LayoutParams(
//				250,
//				ViewGroup.LayoutParams.WRAP_CONTENT);
//		
//		llchild1[newconditioncount-1]=new LinearLayout(this);
//		llchild1[newconditioncount-1].setLayoutParams(llchild1params);
//		llchild1[newconditioncount-1].setOrientation(LinearLayout.HORIZONTAL);
//		llchild1[newconditioncount-1].setPadding(5, 0, 0, 0);
//		llparent[newconditioncount-1].addView(llchild1[newconditioncount-1]);
//		
//		tvstar[newconditioncount-1]=new TextView(this);
//		tvstar[newconditioncount-1].setTextAppearance(AgentInspection2.this, R.style.stitle);
//		tvstar[newconditioncount-1].setText("*");
//		llchild1[newconditioncount-1].addView(tvstar[newconditioncount-1]);
//		
//		int maxLength = 50;
//		InputFilter[] fArray = new InputFilter[1];
//		fArray[0] = new InputFilter.LengthFilter(maxLength);
//		
//		ettitle[newconditioncount-1]=new EditText(this);
//		ettitle[newconditioncount-1].setLayoutParams(llettitleparams);
//		ettitle[newconditioncount-1].setBackgroundResource(R.drawable.editbox);
//		ettitle[newconditioncount-1].setHint("Enter your title");
//		ettitle[newconditioncount-1].setPadding(5, 0, 0, 0);
//		ettitle[newconditioncount-1].setMinHeight(50);
//		ettitle[newconditioncount-1].setWidth(0);
//		ettitle[newconditioncount-1].setFilters(fArray);
//		llchild1[newconditioncount-1].addView(ettitle[newconditioncount-1]);
//		
//		/*ettitle[newconditioncount-1].addTextChangedListener(new TextWatcher() {
//
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before,
//					int count) {
//				// TODO Auto-generated method stub
//				if (ettitle[newconditioncount-1].getText().toString().startsWith(" ")) {
//					// Not allowed
//					ettitle[newconditioncount-1].setText("");
//				}
//			}
//
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//
//			}
//		});*/
//		
//		tvcolon[newconditioncount-1]=new TextView(this);
//		tvcolon[newconditioncount-1].setText(":");
//		tvcolon[newconditioncount-1].setTextAppearance(AgentInspection2.this, R.style.ccolon);
//		llchild1[newconditioncount-1].addView(tvcolon[newconditioncount-1]);
//		
//		rbyes[newconditioncount-1]=new RadioButton(this);
//		rbyes[newconditioncount-1].setLayoutParams(llrbyesparams);
//		rbyes[newconditioncount-1].setText("Yes");
//		rbyes[newconditioncount-1].setTextColor(0xffcbddeb);
//		llchild1[newconditioncount-1].addView(rbyes[newconditioncount-1]);
//		
//		rbno[newconditioncount-1]=new RadioButton(this);
//		rbno[newconditioncount-1].setLayoutParams(llrbnoparams);
//		rbno[newconditioncount-1].setText("No");
//		rbno[newconditioncount-1].setTextColor(0xffcbddeb);
//		llchild1[newconditioncount-1].addView(rbno[newconditioncount-1]);
//		
//		clear[newconditioncount-1]=new ImageView(this);
//		clear[newconditioncount-1].setLayoutParams(llclearparams);
//		clear[newconditioncount-1].setTag(newconditioncount);
//		clear[newconditioncount-1].setBackgroundResource(R.drawable.iconcross);
//		llchild1[newconditioncount-1].addView(clear[newconditioncount-1]);
//		
//		llchild2[newconditioncount-1]=new LinearLayout(this);
//		llchild2[newconditioncount-1].setLayoutParams(llchild2params);
//		llchild2[newconditioncount-1].setVisibility(View.GONE);
//		llparent[newconditioncount-1].addView(llchild2[newconditioncount-1]);
//		
//		int maxLength1 = 100;
//		InputFilter[] fArray1 = new InputFilter[1];
//		fArray1[0] = new InputFilter.LengthFilter(maxLength1);
//		
//		etcomments[newconditioncount-1]=new EditText(this);
//		etcomments[newconditioncount-1].setLayoutParams(lletcommentsparams);
//		etcomments[newconditioncount-1].setBackgroundResource(R.drawable.editbox);
//		etcomments[newconditioncount-1].setPadding(5, 0, 0, 0);
//		etcomments[newconditioncount-1].setMinHeight(50);
//		etcomments[newconditioncount-1].setWidth(0);
//		etcomments[newconditioncount-1].setFilters(fArray1);
//		llchild2[newconditioncount-1].addView(etcomments[newconditioncount-1]);
//		
//		/*etcomments[newconditioncount-1].addTextChangedListener(new TextWatcher() {
//
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before,
//					int count) {
//				// TODO Auto-generated method stub
//				if (etcomments[newconditioncount-1].getText().toString().startsWith(" ")) {
//					// Not allowed
//					etcomments[newconditioncount-1].setText("");
//				}
//			}
//
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,
//					int after) {
//				// TODO Auto-generated method stub
//
//			}
//
//			@Override
//			public void afterTextChanged(Editable s) {
//				// TODO Auto-generated method stub
//
//			}
//		});*/
//		
//		rbyes[newconditioncount-1].setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				llchild2[newconditioncount-1].setVisibility(View.VISIBLE);
//				rbyes[newconditioncount-1].setChecked(true);
//				rbno[newconditioncount-1].setChecked(false);
//			}
//		});
//		
//		rbno[newconditioncount-1].setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				llchild2[newconditioncount-1].setVisibility(View.GONE);
//				rbyes[newconditioncount-1].setChecked(false);
//				rbno[newconditioncount-1].setChecked(true);
//				etcomments[newconditioncount-1].setText("");
//			}
//		});
//		
//		clear[newconditioncount-1].setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				String value=v.getTag().toString();
//				int val=Integer.parseInt(value);
//				lladdnewconditiondynamic.removeView(llparent[val]);
//				newconditioncount=newconditioncount-1;
//			}
//		});
//	}

	public void clicker(View v) {
		switch (v.getId()) {
		
		case R.id.agentinsp_placeorder:
			Intent placeintent=new Intent(AgentInspection2.this,OrderInspection.class);
			startActivity(placeintent);
			finish();
			break;
		
		case R.id.agentinsp_plus1:
			llgeneralinfo.setVisibility(View.VISIBLE);
			findViewById(R.id.agentinsp_plus1).setVisibility(View.GONE);
			findViewById(R.id.agentinsp_minus1).setVisibility(View.VISIBLE);
			break;

		case R.id.agentinsp_minus1:
			llgeneralinfo.setVisibility(View.GONE);
			findViewById(R.id.agentinsp_plus1).setVisibility(View.VISIBLE);
			findViewById(R.id.agentinsp_minus1).setVisibility(View.GONE);
			break;

		case R.id.agentinsp_plus2:
			lladdress.setVisibility(View.VISIBLE);
			findViewById(R.id.agentinsp_plus2).setVisibility(View.GONE);
			findViewById(R.id.agentinsp_minus2).setVisibility(View.VISIBLE);
			break;

		case R.id.agentinsp_minus2:
			lladdress.setVisibility(View.GONE);
			findViewById(R.id.agentinsp_plus2).setVisibility(View.VISIBLE);
			findViewById(R.id.agentinsp_minus2).setVisibility(View.GONE);
			break;

		case R.id.agentinsp_plus3:
			lladdaimage.setVisibility(View.VISIBLE);
			findViewById(R.id.agentinsp_plus3).setVisibility(View.GONE);
			findViewById(R.id.agentinsp_minus3).setVisibility(View.VISIBLE);
			break;

		case R.id.agentinsp_minus3:
			lladdaimage.setVisibility(View.GONE);
			findViewById(R.id.agentinsp_plus3).setVisibility(View.VISIBLE);
			findViewById(R.id.agentinsp_minus3).setVisibility(View.GONE);
			break;

		case R.id.agentinsp_plus4:
			llcoverpagelogo.setVisibility(View.VISIBLE);
			findViewById(R.id.agentinsp_plus4).setVisibility(View.GONE);
			findViewById(R.id.agentinsp_minus4).setVisibility(View.VISIBLE);
			break;

		case R.id.agentinsp_minus4:
			llcoverpagelogo.setVisibility(View.GONE);
			findViewById(R.id.agentinsp_plus4).setVisibility(View.VISIBLE);
			findViewById(R.id.agentinsp_minus4).setVisibility(View.GONE);
			break;

		case R.id.agentinsp_getdate:
			showDialog(0);
			break;

		case R.id.agentinsp_gettime:
			showDialog(1);
			break;

		case R.id.agentinsp_home:
			Intent intenthome = new Intent(AgentInspection2.this,
					HomeScreen.class);
			intenthome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intenthome);
			finish();
			break;

		case R.id.agentinsp_submit:
			// toast = new ShowToast(AgentInspection2.this,
			// "This tab under construction");
//			Check_Validation1();
			Check_Validation();
			// cf.CreateTable(17);
			// cf.db.execSQL("delete from " + cf.AddAImage);
			break;

		case R.id.agentinsp_cancel:
//			toast = new ShowToast(AgentInspection2.this,
//					"This tab under construction");
			Intent intenthome1 = new Intent(AgentInspection2.this,
					HomeScreen.class);
			startActivity(intenthome1);
			finish();
			// cf.CreateTable(17);
			// cf.db.execSQL("delete from " + cf.AddAImage);
			break;

		case R.id.agentinsp_coverpagelogo:
			Gallery_Camera_Dialog();
			break;
			
		case R.id.agentinsp_coverpagelogoclose:
			final Dialog dialog3 = new Dialog(AgentInspection2.this);
			dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog3.setContentView(R.layout.confirm_delete);
			dialog3.setCancelable(false);
			Button btnyes = (Button) dialog3
					.findViewById(R.id.confirmdelete_yes);
			Button btnno = (Button) dialog3
					.findViewById(R.id.confirmdelete_no);
			TextView tvtext = (TextView) dialog3
					.findViewById(R.id.confirmdelete_tvtext);
			
			tvtext.setText("the Cover page logo?");
			
			btnyes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ivcoverpagelogo.setVisibility(View.VISIBLE);
					ivcoverpagelogoclose.setVisibility(View.GONE);
					etcoverpagelogo.setText("");
					ivcoverpagelogoselectedimage.setImageBitmap(null);
					ivcoverpagelogoselectedimage.setVisibility(View.GONE);
					dialog3.dismiss();
					toast=new ShowToast(AgentInspection2.this, "Cover page logo deleted successfully");
				}
			});

			btnno.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog3.dismiss();
				}
			});

			dialog3.show();
			
			break;

		case R.id.agentinsp_gallery:
			int count=tldynamiclist.getChildCount();
			if(count<31)
			{
				Gallery_Open();
			}
			else
			{
				toast=new ShowToast(AgentInspection2.this, "You cannot upload images more than 30");
			}
			break;

		case R.id.agentinsp_camera:
			int count1=tldynamiclist.getChildCount();
			if(count1<31)
			{
				Camera_Open();
			}
			else
			{
				toast=new ShowToast(AgentInspection2.this, "You cannot upload images more than 30");
			}
			break;

		case R.id.agentinsp_singlefamilyhome:
			rbtypeofstructuresinglefamily.setChecked(true);
			rbtypeofstructurecondo.setChecked(false);
			rbtypeofstructuretownhome.setChecked(false);
			rbtypeofstructureother.setChecked(false);
			strtypeofstructure = rbtypeofstructuresinglefamily.getText()
					.toString();
			ettypeofstructureother.setText("");
			llsinglefamilyhomeother.setVisibility(View.GONE);
			break;

		case R.id.agentinsp_condo:
			rbtypeofstructuresinglefamily.setChecked(false);
			rbtypeofstructurecondo.setChecked(true);
			rbtypeofstructuretownhome.setChecked(false);
			rbtypeofstructureother.setChecked(false);
			strtypeofstructure = rbtypeofstructurecondo.getText().toString();
			ettypeofstructureother.setText("");
			llsinglefamilyhomeother.setVisibility(View.GONE);
			break;

		case R.id.agentinsp_townhome:
			rbtypeofstructuresinglefamily.setChecked(false);
			rbtypeofstructurecondo.setChecked(false);
			rbtypeofstructuretownhome.setChecked(true);
			rbtypeofstructureother.setChecked(false);
			strtypeofstructure = rbtypeofstructuretownhome.getText().toString();
			ettypeofstructureother.setText("");
			llsinglefamilyhomeother.setVisibility(View.GONE);
			break;

		case R.id.agentinsp_typeofstructureother:
			rbtypeofstructuresinglefamily.setChecked(false);
			rbtypeofstructurecondo.setChecked(false);
			rbtypeofstructuretownhome.setChecked(false);
			rbtypeofstructureother.setChecked(true);
			strtypeofstructure = rbtypeofstructureother.getText().toString();
			llsinglefamilyhomeother.setVisibility(View.VISIBLE);
			break;

		case R.id.agentinsp_neglectyes:
			rbsignsofneglectyes.setChecked(true);
			rbsignsofneglectnotapplicable.setChecked(false);
			rbsignsofneglectnone.setChecked(false);
			strsignofneglect = rbsignsofneglectyes.getText().toString();
			llsignsofneglectcomments.setVisibility(View.VISIBLE);
			break;

		case R.id.agentinsp_neglectnotapplicable:
			rbsignsofneglectyes.setChecked(false);
			rbsignsofneglectnotapplicable.setChecked(true);
			rbsignsofneglectnone.setChecked(false);
			strsignofneglect = rbsignsofneglectnotapplicable.getText()
					.toString();
			etsignofneglectcomments.setText("");
			llsignsofneglectcomments.setVisibility(View.GONE);
			break;

		case R.id.agentinsp_neglectnonenoted:
			rbsignsofneglectyes.setChecked(false);
			rbsignsofneglectnotapplicable.setChecked(false);
			rbsignsofneglectnone.setChecked(true);
			strsignofneglect = rbsignsofneglectnone.getText().toString();
			etsignofneglectcomments.setText("");
			llsignsofneglectcomments.setVisibility(View.GONE);
			break;

		case R.id.agentinsp_unusualyes:
			rbhazardyes.setChecked(true);
			rbhazardno.setChecked(false);
			rbhazardnotapplicable.setChecked(false);
			strunusual = rbhazardyes.getText().toString();
			llunusualcomments.setVisibility(View.VISIBLE);
			break;

		case R.id.agentinsp_unusualno:
			rbhazardyes.setChecked(false);
			rbhazardno.setChecked(true);
			rbhazardnotapplicable.setChecked(false);
			strunusual = rbhazardno.getText().toString();
			etunusualcommnents.setText("");
			llunusualcomments.setVisibility(View.GONE);
			break;

		case R.id.agentinsp_unusualnotapplicable:
			rbhazardyes.setChecked(false);
			rbhazardno.setChecked(false);
			rbhazardnotapplicable.setChecked(true);
			strunusual = rbhazardnotapplicable.getText().toString();
			etunusualcommnents.setText("");
			llunusualcomments.setVisibility(View.GONE);
			break;

		case R.id.agentinsp_customdefectyes:
			rbcustomdefectyes.setChecked(true);
			rbcustomdefectno.setChecked(false);
			strcustomdefect = rbcustomdefectyes.getText().toString();
			lladdnewconditioncomments.setVisibility(View.VISIBLE);
			break;

		case R.id.agentinsp_customdefectno:
			rbcustomdefectyes.setChecked(false);
			rbcustomdefectno.setChecked(true);
			strcustomdefect = rbcustomdefectno.getText().toString();
			etaddnewconditioncomments.setText("");
			lladdnewconditioncomments.setVisibility(View.GONE);
			break;
			
//		case R.id.agentinsp_addnewcondition:
//			System.out.println("addnewcondition clicked");
//			addnewcondition.setVisibility(View.GONE);
//			lladdnewcondition.setVisibility(View.VISIBLE);
			
		case R.id.agentinsp_clearaddcomments:
			addnewcondition.setVisibility(View.VISIBLE);
			lladdnewcondition.setVisibility(View.GONE);
			lladdnewconditioncomments.setVisibility(View.GONE);
			etaddnewconditiontitle.setText("");
			etaddnewconditioncomments.setText("");
			rbcustomdefectyes.setChecked(false);
			rbcustomdefectno.setChecked(false);
		}
	}
	
	private void Check_Validation1()
	{
		int count=arraylist.size();
		System.out.println("The arraylist size is "+count);
		if(count<1)
		{
			title="";
			comments="";
		}
		else
		{
			for(int i=0;i<count;i++)
			{
				String value=arraylist.get(i);
				int id=Integer.parseInt(value);
				LinearLayout ll=(LinearLayout) (findViewById(id));
				EditText etit=(EditText)ll.findViewWithTag("ettitle");
				EditText etcom=(EditText)ll.findViewWithTag("etcomments");
				RadioButton yes=(RadioButton)ll.findViewWithTag("rbyes");
				RadioButton no=(RadioButton)ll.findViewWithTag("rbno");
				if(!etit.getText().toString().trim().equals(""))
					{
						if(yes.isChecked()||no.isChecked())
						{
							if(yes.isChecked())
							{
								if(etcom.getText().toString().trim().equals(""))
								{
//									toast=new ShowToast(AgentInspection2.this, "Please enter the comments for "+etit.getText().toString());
//									etcom.requestFocus();
//									break;
									newcondition +="false"+"~";
								}
								else
								{
									String titletext=etit.getText().toString().trim();
									titletext=titletext.replaceAll("~", "");
									
									String commenttext=etcom.getText().toString().trim();
									commenttext=commenttext.replaceAll("~", "");
											
									title+=titletext+"~";
									comments+=commenttext+"~";
									newcondition +="true"+"~";
								}
							}
							else
							{
								String titletext=etit.getText().toString().trim();
								titletext=titletext.replaceAll("~", "");
								
								String commenttext=etcom.getText().toString().trim();
								commenttext=commenttext.replaceAll("~", "");
										
								title+=titletext+"~";
								comments+="No"+"~";
								newcondition +="true"+"~";
							}
						}
						else
						{
//							toast=new ShowToast(AgentInspection2.this, "Please select the option for "+etit.getText().toString());
//							etit.requestFocus();
//							break;
							newcondition +="false"+"~";
						}
					}
					else
					{
//						toast = new ShowToast(AgentInspection2.this,
//								"Please enter title");
//						etit.requestFocus();
//						etit.setText("");
//						break;
						newcondition +="false"+"~";
					}
			}
		}
	}

	private void Check_Validation()
	{
		if(lladdnewconditiondynamic.getVisibility()==View.VISIBLE)
		{
			newcondition="";
			title="";
			comments="";
			System.out.println("View is Visible");
			Check_Validation1();
		}
		else
		{
			System.out.println("View is not visible");
			newcondition="true";
			title="";
			comments="";
		}
		
		if(strtypeofstructure.equals("Other"))
		{
			if(ettypeofstructureother.getText().toString().trim().equals(""))
			{
				strtypeofstructure="Other";
			}
			else
			{
				strtypeofstructure=ettypeofstructureother.getText().toString().trim();
			}
		}

		if(lladdnewcondition.getVisibility()==View.VISIBLE)
		{
			if(etaddnewconditiontitle.getText().toString().trim().equals(""))
			{
				strcustomdefect="";
			}
			else
			{
				if(lladdnewconditioncomments.getVisibility()==View.VISIBLE)
				{
					strcustomdefect=etaddnewconditioncomments.getText().toString().trim();
				}
			}
		}
		else
		{
			strcustomdefect="N/A";
		}
		if(llsignsofneglectcomments.getVisibility()==View.VISIBLE)
		{
			if(etsignofneglectcomments.getText().toString().trim().equals(""))
			{
				strsignofneglect="";
			}
			else
			{
				strsignofneglect=etsignofneglectcomments.getText().toString().trim();
			}
			
		}
		if(llunusualcomments.getVisibility()==View.VISIBLE)
		{
			if(etunusualcommnents.getText().toString().trim().equals(""))
			{
				strunusual="";
			}
			else
			{
				strunusual=etunusualcommnents.getText().toString().trim();
			}
			
		}
		
		/*if(!etsquarefootage.getText().toString().trim().equals(""))
		{*/
			if(!etdate.getText().toString().trim().equals(""))
			{
				if(!ettime.getText().toString().trim().equals(""))
				{
					if(!strtypeofstructure.equals("")&&!strtypeofstructure.equals("Other"))
					{
						if(!strsignofneglect.equals(""))
						{
							if(!strunusual.equals(""))
							{
								if(!newcondition.contains("false"))//!strcustomdefect.equals("")
								{
									/*if(!etfirstname.getText().toString().trim().equals(""))
									{
									if(!etlastname.getText().toString().trim().equals(""))
									{
									if(!etinspaddress1.getText().toString().trim().equals(""))
									{
										
												
													if(!etzip.getText().toString().trim().equals(""))
													{
														if((etzip.getText().toString().length()==5))
														{
															if((!etzip.getText().toString().contains("00000")))
															{
																if(!etcity.getText().toString().trim().equals(""))
																{
																	if(!strstate.equals("--Select--"))
																	{
																		if(!strcounty.equals("--Select--"))
																		{*/
//														if(cbaddresscheck.isChecked())
//														{
//															if(!etcoverpagelogo.getText().toString().trim().equals(""))
//															{
//																Agent_Order();
//															}
//															else
//															{
//																toast=new ShowToast(AgentInspection2.this, "Please Add a cover page logo");
//																etcoverpagelogo.requestFocus();
//																plus4.setVisibility(View.VISIBLE);
//																minus4.setVisibility(View.GONE);
//																llcoverpagelogo.setVisibility(View.VISIBLE);
//															}
//														}
//														else
//														{
															/*if(!etmailaddress1.getText().toString().trim().equals(""))
															{
																	if(!etzip2.getText().toString().trim().equals(""))
																			{
																				if((etzip2.getText().toString().length()==5))
																				{
																					if(!(etzip2.getText().toString().contains("00000")))
																					{
																						if(!etcity2.getText().toString().trim().equals(""))
																						{
																							if(!strstate2.equals("--Select--"))
																							{
																								if(!strcounty2.equals("--Select--"))
																								{*/
																				if(!etcoverpagelogo.getText().toString().trim().equals(""))
																				{
																					Agent_Order();
																				}
																				else
																				{
																					toast=new ShowToast(AgentInspection2.this, "Please upload cover page logo");
																					etcoverpagelogo.requestFocus();
																					plus4.setVisibility(View.VISIBLE);
																					minus4.setVisibility(View.GONE);
																					llcoverpagelogo.setVisibility(View.VISIBLE);
																				}
																								/*}
																								else
																								{
																									toast=new ShowToast(AgentInspection2.this, "Please select County under Mailing Address");
																									plus2.setVisibility(View.VISIBLE);
																									minus2.setVisibility(View.GONE);
																									lladdress.setVisibility(View.VISIBLE);
																								}
																							}
																							else
																							{
																								toast=new ShowToast(AgentInspection2.this, "Please select State under Mailing Address");
																								plus2.setVisibility(View.VISIBLE);
																								minus2.setVisibility(View.GONE);
																								lladdress.setVisibility(View.VISIBLE);
																								
																							}
																						}
																						else
																						{
																							toast=new ShowToast(AgentInspection2.this, "Please enter City under Mailing Address");
																							etcity2.requestFocus();
																							plus2.setVisibility(View.VISIBLE);
																							minus2.setVisibility(View.GONE);
																							lladdress.setVisibility(View.VISIBLE);
																						}
																					}
																					else
																					{
																						toast=new ShowToast(AgentInspection2.this, "Please enter a valid zip code under Mailing Address");
																						etzip2.requestFocus();
																						etzip2.setText("");
																						lladdress.setVisibility(View.VISIBLE);
																						plus2.setVisibility(View.GONE);
																						minus2.setVisibility(View.VISIBLE);
																					}
																			}
																			else
																			{
																				toast=new ShowToast(AgentInspection2.this, "Zip should be 5 characters under Mailing Address");
																				etzip2.requestFocus();
																				plus2.setVisibility(View.VISIBLE);
																				minus2.setVisibility(View.GONE);
																				lladdress.setVisibility(View.VISIBLE);
																			}
																				
																			}
																			else
																			{
																				toast=new ShowToast(AgentInspection2.this, "Please enter Zip under Mailing Address");
																				etzip2.requestFocus();
																				plus2.setVisibility(View.VISIBLE);
																				minus2.setVisibility(View.GONE);
																				lladdress.setVisibility(View.VISIBLE);
																			}
																		
																	
															}
															else
															{
																toast=new ShowToast(AgentInspection2.this, "Please enter Mailing Address1");
																etmailaddress1.requestFocus();
																plus2.setVisibility(View.VISIBLE);
																minus2.setVisibility(View.GONE);
																lladdress.setVisibility(View.VISIBLE);
															}
//														}
																		}
																		else
																		{
																			toast=new ShowToast(AgentInspection2.this, "Please select County under Inspection Address");
																			plus2.setVisibility(View.VISIBLE);
																			minus2.setVisibility(View.GONE);
																			lladdress.setVisibility(View.VISIBLE);
																		}
																	}
																	else
																	{
																		toast=new ShowToast(AgentInspection2.this, "Please select State under Inspection Address");
																		plus2.setVisibility(View.VISIBLE);
																		minus2.setVisibility(View.GONE);
																		lladdress.setVisibility(View.VISIBLE);
																	}
																		}
																		else
																		{
																			toast=new ShowToast(AgentInspection2.this, "Please enter City under Inspection Address");
																			etcity.requestFocus();
																			plus2.setVisibility(View.VISIBLE);
																			minus2.setVisibility(View.GONE);
																			lladdress.setVisibility(View.VISIBLE);
																		}
															}
															else
															{
																toast=new ShowToast(AgentInspection2.this, "Please enter a valid Zip under Inspection Address");
																etzip.requestFocus();
																etzip.setText("");
																lladdress.setVisibility(View.VISIBLE);
																plus2.setVisibility(View.GONE);
																minus2.setVisibility(View.VISIBLE);
															}
													}
													else
													{
														toast=new ShowToast(AgentInspection2.this, "Zip should be 5 Characters under Inspection Address");
														etzip.requestFocus();
														plus2.setVisibility(View.VISIBLE);
														minus2.setVisibility(View.GONE);
														lladdress.setVisibility(View.VISIBLE);
													}
													}
													else
													{
														toast=new ShowToast(AgentInspection2.this, "Please enter Zip under Inspection Address");
														etzip.requestFocus();
														plus2.setVisibility(View.VISIBLE);
														minus2.setVisibility(View.GONE);
														lladdress.setVisibility(View.VISIBLE);
													}
												
											
									}
									else
									{
										toast=new ShowToast(AgentInspection2.this, "Please enter Inspection Address1");
										etinspaddress1.requestFocus();
										plus2.setVisibility(View.VISIBLE);
										minus2.setVisibility(View.GONE);
										lladdress.setVisibility(View.VISIBLE);
									}
								}
								else
								{
									toast=new ShowToast(AgentInspection2.this, "Please enter Last Name");
									etlastname.requestFocus();
									plus2.setVisibility(View.VISIBLE);
									minus2.setVisibility(View.GONE);
									lladdress.setVisibility(View.VISIBLE);
								}
								}
								else
								{
									toast=new ShowToast(AgentInspection2.this, "Please enter First Name");
									etfirstname.requestFocus();
									plus2.setVisibility(View.VISIBLE);
									minus2.setVisibility(View.GONE);
									lladdress.setVisibility(View.VISIBLE);
								}*/
								}
								else
								{
								int count=arraylist.size();
									for(int i=0;i<count;i++)
									{
										String value=arraylist.get(i);
										int id=Integer.parseInt(value);
										LinearLayout ll=(LinearLayout) (findViewById(id));
										EditText etit=(EditText)ll.findViewWithTag("ettitle");
										EditText etcom=(EditText)ll.findViewWithTag("etcomments");
										RadioButton yes=(RadioButton)ll.findViewWithTag("rbyes");
										RadioButton no=(RadioButton)ll.findViewWithTag("rbno");
										if(!etit.getText().toString().trim().equals(""))
											{
												if(yes.isChecked()||no.isChecked())
												{
													if(yes.isChecked())
													{
														if(etcom.getText().toString().trim().equals(""))
														{
															toast=new ShowToast(AgentInspection2.this, "Please enter the comments for "+etit.getText().toString());
															etcom.requestFocus();
															break;
														}
													}
												}
												else
												{
													toast=new ShowToast(AgentInspection2.this, "Please select the option for "+etit.getText().toString());
													etit.requestFocus();
													break;
												}
											}
											else
											{
												toast = new ShowToast(AgentInspection2.this,
														"Please enter title");
												etit.requestFocus();
												etit.setText("");
												break;
											}
									}
								}
							}
							else
							{
								if(llunusualcomments.getVisibility()==View.VISIBLE)
								{
									toast=new ShowToast(AgentInspection2.this, "Please enter comments for was there any unusual/obvious hazard conditions observed");
									etunusualcommnents.setText("");
									etunusualcommnents.requestFocus();
									plus1.setVisibility(View.VISIBLE);
									minus1.setVisibility(View.GONE);
									llgeneralinfo.setVisibility(View.VISIBLE);
								}
								else
								{
									toast=new ShowToast(AgentInspection2.this, "Please select the option for was there any unusual/obvious hazard conditions observed");
									ettime.requestFocus();
									plus1.setVisibility(View.VISIBLE);
									minus1.setVisibility(View.GONE);
									llgeneralinfo.setVisibility(View.VISIBLE);
								}
								
							}
						}
						else
						{
							if(llsignsofneglectcomments.getVisibility()==View.VISIBLE)
							{
								toast=new ShowToast(AgentInspection2.this, "Please enter comments for Are there any signs of neglect?");
								etsignofneglectcomments.setText("");
								etsignofneglectcomments.requestFocus();
								plus1.setVisibility(View.VISIBLE);
								minus1.setVisibility(View.GONE);
								llgeneralinfo.setVisibility(View.VISIBLE);
								
							}
							else
							{
								toast=new ShowToast(AgentInspection2.this, "Please select the option for Are there any signs of neglect?");
								plus1.setVisibility(View.VISIBLE);
								minus1.setVisibility(View.GONE);
								llgeneralinfo.setVisibility(View.VISIBLE);
							}
						}
					}
					else
					{
						if(llsinglefamilyhomeother.getVisibility()==View.VISIBLE&&ettypeofstructureother.getText().toString().trim().equals(""))
						{
							toast=new ShowToast(AgentInspection2.this, "Please enter the other text for Type of Structure");
							ettypeofstructureother.requestFocus();
							plus1.setVisibility(View.VISIBLE);
							minus1.setVisibility(View.GONE);
							llgeneralinfo.setVisibility(View.VISIBLE);
						}
						else
						{
							toast=new ShowToast(AgentInspection2.this, "Please select the option for Type of Structure");
							plus1.setVisibility(View.VISIBLE);
							minus1.setVisibility(View.GONE);
							llgeneralinfo.setVisibility(View.VISIBLE);
						}
						
					}
				}
				else
				{
					toast=new ShowToast(AgentInspection2.this, "Please enter Time of Survey");
					ettime.requestFocus();
					plus1.setVisibility(View.VISIBLE);
					minus1.setVisibility(View.GONE);
					llgeneralinfo.setVisibility(View.VISIBLE);
				}
			}
			else
			{
				toast=new ShowToast(AgentInspection2.this, "Please enter Date of Survey");
				etdate.requestFocus();
				plus1.setVisibility(View.VISIBLE);
				minus1.setVisibility(View.GONE);
				llgeneralinfo.setVisibility(View.VISIBLE);
			}
		/*}
		else
		{
			toast=new ShowToast(AgentInspection2.this, "Please enter Square Footage");
			etsquarefootage.requestFocus();
			plus1.setVisibility(View.VISIBLE);
			minus1.setVisibility(View.GONE);
			llgeneralinfo.setVisibility(View.VISIBLE);
			
		}*/
		
	}
	
	private void Agent_Order()
	{
		System.out.println("comes in");
		if(!title.equals(""))
		{
			title=title.substring(0,title.length()-1);
		}
		
		if(!comments.equals(""))
		{
			comments=comments.substring(0,comments.length()-1);
		}
		
		if(cbaddresscheck.isChecked())
		{
			cadd=1;
			strstateid2=strstateid;
			strstate2=strstate;
			strcountyid2=strcountyid;
			strcounty2=strcounty;
		}
		else
		{
			cadd=0;
		}
		cf.CreateTable(17);
		Cursor cur=cf.db.rawQuery("select * from "+cf.AddAImage, null);
		
		if(cur.getCount()<1)
		{
			flag=1;
		}
		else
		{
			flag=0;
		}
		cur.close();
		strsquarefootage=etsquarefootage.getText().toString();
		strpolicynumber=etpolicynumber.getText().toString();
		strpolicyname=etpolicyname.getText().toString();
		strcity=etcity.getText().toString();
		strzip=etzip.getText().toString();
		strcity2=etcity2.getText().toString();
		strzip2=etzip2.getText().toString();
		strinspaddress1=etinspaddress1.getText().toString();
		if(etinspaddress2.getText().toString().trim().equals(""))
		{
			strinspaddress2="N/A";
		}
		else
		{
			strinspaddress2=etinspaddress2.getText().toString();
		}
		strmailaddress1=etmailaddress1.getText().toString();
		if(etmailaddress2.getText().toString().trim().equals(""))
		{
			strmailaddress2="N/A";
		}
		else
		{
			strmailaddress2=etmailaddress2.getText().toString();
		}
		
		SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyyy");
        final String strdateofsurvey = timeFormat.format(Date.parse(etdate.getText().toString()));
		
		
        cf.CreateTable(31);
        if(strsquarefootage.equals(""))
        {
        	strsquarefootage="0";
        }
        if(strzip.equals(""))
        {
        	strzip="0";
        }
        if(strzip2.equals(""))
        {
        	strzip2="0";
        }
       
        try
        {
        	cf.db.execSQL("INSERT INTO "
					+ cf.AgentInspection
					+ " (AgentId,policynum,policyname,squarefootage,dateofsurvey,timeofsurvey,typeofstructure,otherstructure,signs,signscomments,unusual,"+
				 "unusualcomments,FirstName,LastName,InspectionAddress1,InspectionAddress2,zip,city,state,county,chkmail,maddress1,maddress2,"+
				 "mzip,mcity,mstate,mcounty,logo,flag,additinaltitle,additonalcomments,pdfpath)"
					+ " VALUES ('"+Integer.parseInt(AgentId)+"','" + cf.encode(strpolicynumber) + "','"+cf.encode(strpolicyname)+"','"
					+ Long.parseLong(strsquarefootage) + "','"+strdateofsurvey+"','"+ettime.getText().toString()+"','"+cf.encode(strtypeofstructure)+"','','"
					+ cf.encode(strsignofneglect) + "','','"+cf.encode(strunusual)+"','','"
					+ cf.encode(etfirstname.getText().toString()) + "','"+cf.encode(etlastname.getText().toString())+"','"
					+ cf.encode(strinspaddress1) + "','"+cf.encode(strinspaddress2)+"','"+Integer.parseInt(strzip)+"','"
					+ cf.encode(strcity) + "','"+cf.encode(strstate)+"','"+cf.encode(strcounty)+"','"+cadd+"','"
					+ cf.encode(strmailaddress1) + "','"+cf.encode(strmailaddress2)+"','"+Integer.parseInt(strzip2)+"','"
					+ cf.encode(strcity2)+"','"+cf.encode(strstate2)+"','"+cf.encode(strcounty2)+"','"
					+ cf.encode(etcoverpagelogo.getText().toString())+"','1','"+cf.encode(title)+"','"+cf.encode(comments)+"','')");
        	toast = new ShowToast(AgentInspection2.this,"Agent Inspection submitted successfully");

			Intent intent = new Intent(AgentInspection2.this,HomeScreen.class);
		    overridePendingTransition(0, 0);
		    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		    finish();

		    overridePendingTransition(0, 0);
		    startActivity(intent);
        }
        catch (Exception e) {
			// TODO: handle exception
        	System.out.println("catch"+e.getMessage());
		}
		/*if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Submitting Agent Inspection... Please wait.");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(cf.NAMESPACE,
								"SaveAgentInspection");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("Id",propertycount+1);
						request.addProperty("UserId", Integer.parseInt(AgentId));
						request.addProperty("PolicyNumber", strpolicynumber);
						request.addProperty("PolicyName", strpolicyname);
						request.addProperty("Firstname", etfirstname.getText().toString());
						request.addProperty("Lastname", etlastname.getText().toString());
						request.addProperty("SquareFootage", Long.parseLong(strsquarefootage));
						request.addProperty("DateofSurvey", strdateofsurvey);
						request.addProperty("TimeofSurvey", ettime.getText().toString());
						request.addProperty("TypeofStructure", strtypeofstructure);
						request.addProperty("SignsofNeglect", strsignofneglect);
						request.addProperty("HazardCondition", strunusual);
						//new condition
						request.addProperty("AddiConditions", title);//"title1~title2~title3~title4~title5~title6~title7~title8~title9~title10~title1~title2~title3~title4~title5~title6~title7~title8~title9~title10~title1~title2~title3~title4~title5~title6~title7~title8~title9~title10"
						request.addProperty("CustomeDefect", comments);//"dshfgdhksgfdskdgfk~ghfjfdhgldhflghdfl~hfvjdfhgldhfgldhf~jfhdlgfhdlg~hfdshlshflhl~ghgkgkgkgkgkgkjgh~gkgkgkgkgkg~kgkjgkjgjgkg~gkjgkjgkjgkjgkj~gjgkjgkjggjgjgjggj~dshfgdhksgfdskdgfk~ghfjfdhgldhflghdfl~hfvjdfhgldhfgldhf~jfhdlgfhdlg~hfdshlshflhl~ghgkgkgkgkgkgkjgh~gkgkgkgkgkg~kgkjgkjgjgkg~gkjgkjgkjgkjgkj~gjgkjgkjggjgjgjggj~dshfgdhksgfdskdgfk~ghfjfdhgldhflghdfl~hfvjdfhgldhfgldhf~jfhdlgfhdlg~hfdshlshflhl~ghgkgkgkgkgkgkjgh~gkgkgkgkgkg~kgkjgkjgjgkg~gkjgkjgkjgkjgkj~gjgkjgkjggjgjgjggj"
						//new condition
//						request.addProperty("CustomeDefect", strcustomdefect);
						request.addProperty("InspectionAddress1", strinspaddress1);
						request.addProperty("InspectionAddress2", strinspaddress2);
						request.addProperty("InspectionState", strstate);
						request.addProperty("InspectionCounty", strcounty);
						request.addProperty("InspectionCity", strcity);
						request.addProperty("InspectionZip", Integer.parseInt(strzip));
						request.addProperty("MailingAddress1", strmailaddress1);
						request.addProperty("MailingAddress2", strmailaddress2);
						request.addProperty("MailingState", strstate2);
						request.addProperty("MailingCounty", strcounty2);
						request.addProperty("MailingCity", strcity2);
						request.addProperty("MailingZip", Integer.parseInt(strzip2));
						request.addProperty("CoverPageLogo", bytecoverpagelogo);
//						request.addProperty("Logoname", strcoverpagelogofilename);
						request.addProperty("Logoname", (propertycount+1)+"Ag_Coverpagelogo"+AgentId+".jpg");
						request.addProperty("Flag", flag);
						
						
						envelope.setOutputSoapObject(request);
						marshal.register(envelope);

						System.out.println("SaveAgentInspection request is " + request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL);
						System.out.println("Before http call");
						androidHttpTransport.call(cf.NAMESPACE
								+ "SaveAgentInspection", envelope);
						order_result = envelope.getResponse().toString();
						System.out.println("SaveAgentInspection result is"
								+ order_result);

						show_handler = 5;
						handler.sendEmptyMessage(0);

						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									AgentInspection2.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									AgentInspection2.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							
							Call_SaveImages(order_result);
							

						}
					}
				};
			}.start();
		} else {
			toast = new ShowToast(AgentInspection2.this,
					"Internet connection not available");
		}
	*/
	}
	
	private void Call_SaveImages(String result)
	{
		if(flag==1&&result.toLowerCase().equals("true"))
		{
//			Call_AgentInformationList();
			toast = new ShowToast(AgentInspection2.this,
					"Agent Inspection submitted successfully");
//			Intent intent=new Intent(AgentInspection2.this,AgentInspection2.class);
//			startActivity(intent);
//			finish();
			Intent intent = getIntent();
		    overridePendingTransition(0, 0);
		    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		    finish();

		    overridePendingTransition(0, 0);
		    startActivity(intent);
		}
		else if((flag==1||flag==0)&&result.toLowerCase().equals("false"))
		{
			toast = new ShowToast(
					AgentInspection2.this,
					"There is a problem on your application. Please contact Paperless administrator.");
		}
		else if(flag==0&&result.toLowerCase().equals("true"))
		{
//			cf.CreateTable(20);
//			cf.db.execSQL("insert into "+cf.AgentInspection_Pdf+"(filename) values('"+cf.encode(image_result)+"')");
			Save_AgentInspection_Image1();
//			Call_Dynamic_Pdf_Display();
		}
		
	}
	
	private void Save_AgentInspection_Image1()
	{
		cf.CreateTable(17);
		Cursor cur=cf.db.rawQuery("select * from "+cf.AddAImage, null);
		cur.moveToFirst();
		int cnt=cur.getCount();
		elevation_name=new String[cnt];
		caption_name=new String[cnt];
		file_name=new String[cnt];
		if(cnt>=1)
		{
			int i=0;
			do
			{
				final String elevation=cf.decode(cur.getString(cur.getColumnIndex("elevation")));
				final String caption=cf.decode(cur.getString(cur.getColumnIndex("caption")));
				String filepath=cf.decode(cur.getString(cur.getColumnIndex("filepath")));
				
				elevation_name[i]=elevation;
				caption_name[i]=caption;
				file_name[i]=filepath;
				
				i++;
				
			}while(cur.moveToNext());
		}
		cur.close();
		if(elevation_name.length>=1)
		{
			Save_AgentInspection_Image();
		}
	}
	
	private void Save_AgentInspection_Image()
	{
			if (cf.isInternetOn() == true) {
				cf.show_ProgressDialog("Submitting Agent Inspection... Please wait.");
				new Thread() {
					public void run() {
						Looper.prepare();
						try {
							
							for(i=0;i<elevation_name.length;i++)
							{
								String[] filenamesplit = file_name[i]
										.split("/");
								final String filename = filenamesplit[filenamesplit.length - 1];
								System.out
										.println("The File Name is "
												+ filename);
								
								Bitmap bitmap = cf.ShrinkBitmap(file_name[i], 400, 400);

								System.out.println(" bimap "+bitmap);
								marshal = new MarshalBase64();
								ByteArrayOutputStream out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								final byte[] byteimage = out.toByteArray();
								if(i==elevation_name.length-1)
								{
									flag=1;
								}
								else
								{
									flag=0;
								}
							
							SoapObject request = new SoapObject(cf.NAMESPACE,
									"SaveAgentElevationImages");
							SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
									SoapEnvelope.VER11);
							envelope.dotNet = true;
							request.addProperty("AgentInfoPk",propertycount+1);
							request.addProperty("AgentID", Integer.parseInt(AgentId));
							request.addProperty("ImageOrder", i);
							request.addProperty("ElevationType", elevation_name[i]);
							request.addProperty("Caption", caption_name[i]);
							request.addProperty("Image", byteimage);
//							request.addProperty("Imagename", filename);
							request.addProperty("Imagename", "Ag_"+AgentId+"i"+(propertycount+1)+i+".jpg");
							request.addProperty("Flag", flag);
														
							
							envelope.setOutputSoapObject(request);
							marshal.register(envelope);

							System.out.println("SaveAgentElevationImages request is " + request);
							HttpTransportSE androidHttpTransport = new HttpTransportSE(
									cf.URL);
							System.out.println("Before http call");
							androidHttpTransport.call(cf.NAMESPACE
									+ "SaveAgentElevationImages", envelope);
							image_result = envelope.getResponse().toString();
							System.out.println("SaveAgentElevationImages result is"
									+ image_result);
							}
							
							show_handler = 5;
							handler.sendEmptyMessage(0);

							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);
						} catch (XmlPullParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);
						}
					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							cf.pd.dismiss();
							if (show_handler == 3) {
								show_handler = 0;
								toast = new ShowToast(
										AgentInspection2.this,
										"There is a problem on your Network. Please try again later with better Network.");

							} else if (show_handler == 4) {
								show_handler = 0;
								toast = new ShowToast(
										AgentInspection2.this,
										"There is a problem on your application. Please contact Paperless administrator.");

							} else if (show_handler == 5) {
								show_handler = 0;
								
								if(flag==1&&image_result.toLowerCase().equals("true"))
								{
//									Call_AgentInformationList();
//									Intent intent=new Intent(AgentInspection2.this,AgentInspection2.class);
//									startActivity(intent);
//									finish();
									
									toast = new ShowToast(AgentInspection2.this,
											"Agent Inspection submitted successfully");
									
									Intent intent = getIntent();
								    overridePendingTransition(0, 0);
								    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
								    finish();

								    overridePendingTransition(0, 0);
								    startActivity(intent);
								}
								else if(flag==1&&image_result.toLowerCase().equals("false"))
								{
									toast = new ShowToast(
											AgentInspection2.this,
											"There is a problem on your application. Please contact Paperless administrator.");
								}

							}
						}
					};
				}.start();
			} else {
				toast = new ShowToast(AgentInspection2.this,
						"Internet connection not available");
			}
		
	}
	
	public void Call_AgentInformationList() {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Loading Agent Inspection... Please wait."
					+ "</font></b>";
			final ProgressDialog pd = ProgressDialog.show(AgentInspection2.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			
			new Thread() {
				SoapObject chklogin;
				public void run() {
					Looper.prepare();
					try {
						chklogin = cf
								.Calling_WS_AGENTINFORMATIONLIST(AgentId,"AGENTINFORMATIONLIST");
						System.out.println("response AGENTINFORMATIONLIST"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									AgentInspection2.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									AgentInspection2.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							Call_AgentInformationList(chklogin);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(AgentInspection2.this,
					"Internet connection not available");

		}
//	}
	}

	public void Call_AgentInformationList(SoapObject objInsert) {
		cf.CreateTable(20);
		cf.db.execSQL("delete from " + cf.AgentInspection_Pdf);
		if(!objInsert.equals(null))
		{
			propertycount = objInsert.getPropertyCount();
			System.out.println("AgentInspection_Pdf property count" + propertycount);
		if(propertycount>=1)
		{
		for (int i = 0; i < propertycount; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String filename = String.valueOf(obj.getProperty("PDFPath"));
				String FirstName = String.valueOf(obj.getProperty("Firstname"));
				String LastName = String.valueOf(obj.getProperty("Lastname"));
				String InspectionAddress1 = String.valueOf(obj.getProperty("InspectionAddress1"));
				String InspectionAddress2 = String.valueOf(obj.getProperty("InspectionAddress2"));
				String InspectionCity = String.valueOf(obj.getProperty("InspectionCity"));
				String InspectionState = String.valueOf(obj.getProperty("InspectionState"));
				String InspectionCounty = String.valueOf(obj.getProperty("InspectionCounty"));
				String PolicyNumber = String.valueOf(obj.getProperty("PolicyNumber"));
				String InspectionZip = String.valueOf(obj.getProperty("InspectionZip"));
				
				cf.db.execSQL("insert into " + cf.AgentInspection_Pdf
//						+ " (filename) values('"+ cf.encode(filename)
				+ " (filename,FirstName,LastName,InspectionAddress1,InspectionAddress2,InspectionCity," +
				"InspectionState,InspectionCounty,PolicyNumber,InspectionZip) values('"+ cf.encode(filename) + "','"+ 
				cf.encode(FirstName) + "','"+ cf.encode(LastName) + "','"+ cf.encode(InspectionAddress1)
				+ "','"+ cf.encode(InspectionAddress2) + "','"+ cf.encode(InspectionCity) + "','"+ 
				cf.encode(InspectionState) + "','"+ cf.encode(InspectionCounty)
				+ "','"+ cf.encode(PolicyNumber) + "','"+cf.encode(InspectionZip)+"');");
				

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		Call_Dynamic_Pdf_Display();
		}
		else
		{
			lldisplaypdf.removeAllViews();
			rlnote.setVisibility(View.GONE);
		}
		}
		
	}
	
	private void Call_Dynamic_Pdf_Display()
	{
		lldisplaypdf.removeAllViews();
		ScrollView sv = new ScrollView(this);
		lldisplaypdf.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		LinearLayout.LayoutParams viewparams1 = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, 1);
		viewparams1.setMargins(20, 0, 0, 0);

		/*View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v, viewparams1);*/

		Cursor cur=cf.db.rawQuery("select * from "+cf.AgentInspection_Pdf, null);
		
		int rows = cur.getCount();


		TextView[] tvstatus = new TextView[rows];
		Button[] view = new Button[rows];
		final Button[] download = new Button[rows];
		final Button[] pter = new Button[rows];
		// LinearLayout[] l2 = new LinearLayout[rows];
		RelativeLayout[] rl = new RelativeLayout[rows];
		data = new String[rows];
		datasend = new String[rows];
		pdfpath = new String[rows];
		final String[] ownersname=new String[rows];
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			i = 0;
			do {
				String file_name = cf.decode(cur.getString(cur
						.getColumnIndex("filename")));
				String FirstName = cf.decode(cur.getString(cur
						.getColumnIndex("FirstName")));
				ownersname[i] =FirstName+" ";
				data[i] = " " + FirstName + " | ";
				String LastName = cf.decode(cur.getString(cur
						.getColumnIndex("LastName")));
				ownersname[i] +=LastName;
				data[i] += LastName + " | ";
				String Address1 = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionAddress1")));
				data[i] += Address1 + " | ";
				String Address2 = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionAddress2")));
				data[i] += Address2 + " | ";
				String City = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionCity")));
				data[i] += City + " | ";
				String State = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionState")));
				data[i] += State + " | ";
				String County = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionCounty")));
				data[i] += County + " | ";
				String InspectionZip = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionZip")));
				data[i] += InspectionZip + " | ";
				String PolicyNumber = cf.decode(cur.getString(cur
						.getColumnIndex("PolicyNumber")));
				data[i] += PolicyNumber + " | ";
				datasend[i] = PolicyNumber;
				
				
				pdfpath[i] = file_name;

				
				LinearLayout.LayoutParams llparams;
				RelativeLayout.LayoutParams tvparams;
				RelativeLayout.LayoutParams downloadparams,pterparams,viewparams;
				LinearLayout.LayoutParams lltxtparams;
				
				rlnote.setVisibility(View.VISIBLE);
				
				
				Display display = getWindowManager().getDefaultDisplay();
			    DisplayMetrics displayMetrics = new DisplayMetrics();
			    display.getMetrics(displayMetrics);

			    int width = displayMetrics.widthPixels;
			    int height = displayMetrics.heightPixels;
			    
			    System.out.println("The width is "+width);
			    System.out.println("The height is "+height);
				
				if(width > 1023 || height > 1023)
				{
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					llparams.setMargins(0, 0, 0, 0);
					
					tvparams = new RelativeLayout.LayoutParams(
							680,
							ViewGroup.LayoutParams.MATCH_PARENT);
					tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
					tvparams.setMargins(0, 0, 0, 0);
					

					downloadparams = new RelativeLayout.LayoutParams(
							110, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams.addRule(RelativeLayout.CENTER_VERTICAL);
					downloadparams.setMargins(830, 0, 0, 0);
					
					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					viewparams.addRule(RelativeLayout.CENTER_VERTICAL);
					viewparams.setMargins(700, 0, 0, 0);
					
					rl[i] = new RelativeLayout(this);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					lltxt.setPadding(20, 0, 20, 0);
					rl[i].addView(lltxt);
					
					lltxtparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					lltxtparams.setMargins(0, 30, 0, 30);
	
					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
//					tvstatus[i].setText("The Record is ");
					tvstatus[i].setText(data[i]);
//					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextColor(Color.WHITE);
					tvstatus[i].setTextSize(14);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setLayoutParams(downloadparams);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							110, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.gravity=Gravity.CENTER_VERTICAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
					LinearLayout llpter=new LinearLayout(this);
					llpter.setLayoutParams(downloadparams);
					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i);
					pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);
//					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					llpter.addView(pter[i]);
					
					LinearLayout llview=new LinearLayout(this);
					llview.setLayoutParams(viewparams);
					rl[i].addView(llview);
	
					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					llview.addView(view[i]);
				}
				
				else if((width > 500 && width<1000) || (height > 500 && height<1000))
				{

					
					System.out.println("Phoneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llparams.setMargins(0, 0, 0, 0);
					
					
					
					tvparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
					tvparams.setMargins(10, 0, 170, 0);
						
					lltxtparams = new LinearLayout.LayoutParams(
							320, ViewGroup.LayoutParams.MATCH_PARENT);
					lltxtparams.setMargins(0, 0, 0, 0);
										
					downloadparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
					downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					downloadparams.setMargins(0, 0, 20, 0);

					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					viewparams.setMargins(20, 0, 150, 0);
					
					rl[i] = new RelativeLayout(this);
					rl[i].setPadding(0, 20, 0, 20);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					rl[i].addView(lltxt);
					
					

					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
//					tvstatus[i].setText("The Redord is ");
					tvstatus[i].setText(data[i]);
					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextSize(14);
//					tvstatus[i].setGravity(Gravity.CENTER_VERTICAL);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setOrientation(LinearLayout.VERTICAL);
					lldownload.setLayoutParams(downloadparams);
					lldownload.setGravity(Gravity.CENTER_HORIZONTAL);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.topMargin=10;
					downloadparams1.gravity=Gravity.CENTER_HORIZONTAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
//					LinearLayout llpter=new LinearLayout(this);
//					llpter.setLayoutParams(downloadparams);
//					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);
					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(pter[i]);
					
//					LinearLayout llview=new LinearLayout(this);
//					llview.setLayoutParams(viewparams);
//					rl[i].addView(llview);

					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(view[i]);
				}
				
				else
				{
					
					System.out.println("Phoneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llparams.setMargins(0, 0, 0, 0);
					
					
					
					tvparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
					tvparams.setMargins(10, 0, 0, 0);
						
					lltxtparams = new LinearLayout.LayoutParams(
							320, ViewGroup.LayoutParams.MATCH_PARENT);
					lltxtparams.setMargins(0, 0, 0, 0);
										
					downloadparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
					downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					downloadparams.setMargins(0, 0, 20, 0);

					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					viewparams.setMargins(20, 0, 150, 0);
					
					rl[i] = new RelativeLayout(this);
					rl[i].setPadding(0, 20, 0, 20);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					rl[i].addView(lltxt);
					
					

					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
//					tvstatus[i].setText("The Redord is ");
					tvstatus[i].setText(data[i]);
					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextSize(14);
//					tvstatus[i].setGravity(Gravity.CENTER_VERTICAL);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setOrientation(LinearLayout.VERTICAL);
					lldownload.setLayoutParams(downloadparams);
					lldownload.setGravity(Gravity.CENTER_HORIZONTAL);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.topMargin=10;
					downloadparams1.gravity=Gravity.CENTER_HORIZONTAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
//					LinearLayout llpter=new LinearLayout(this);
//					llpter.setLayoutParams(downloadparams);
//					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);
					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(pter[i]);
					
//					LinearLayout llview=new LinearLayout(this);
//					llview.setLayoutParams(viewparams);
//					rl[i].addView(llview);

					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(view[i]);
					
				}

				path = pdfpath[i];
				String[] filenamesplit = path.split("/");
				final String filename = filenamesplit[filenamesplit.length - 1];
				System.out.println("the file name is" + filename);
				File sdDir = new File(Environment.getExternalStorageDirectory()
						.getPath());
				File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
						+ filename);

				if (file.exists()) {
					pter[i].setVisibility(View.VISIBLE);
					download[i].setVisibility(View.GONE);
				} else {
					pter[i].setVisibility(View.VISIBLE);
					download[i].setVisibility(View.GONE);
				}

				view[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Button b = (Button) v;
						String buttonvalue = v.getTag().toString();
						System.out.println("buttonvalue is" + buttonvalue);
						int s = Integer.parseInt(buttonvalue);
						path = pdfpath[s];
						String[] filenamesplit = path
								.split("/");
						final String filename = filenamesplit[filenamesplit.length - 1];
						System.out
								.println("The File Name is "
										+ filename);
						File sdDir = new File(Environment.getExternalStorageDirectory()
								.getPath());
						File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
								+ filename);
						
						if(file.exists())
						{
							View_Pdf_File(filename,path);
						}
						else
						{
							if (cf.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading... Please wait.");
								new Thread() {
									public void run() {
										Looper.prepare();
										try {
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {
										@Override
										public void handleMessage(Message msg) {
											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												toast = new ShowToast(
														AgentInspection2.this,
														"There is a problem on your application. Please contact Paperless administrator.");

											} else if (show_handler == 2) {
												show_handler = 0;
//												toast = new ShowToast(
//														VehicleInspection.this,
//														"Report downloaded successfully");
												
//												alertDialog = new AlertDialog.Builder(VehicleInspection.this).create();
//												alertDialog
//														.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
//												alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
//														new DialogInterface.OnClickListener() {
//															public void onClick(DialogInterface dialog, int which) {
//																Call_Dynamic_Pdf_Display();
//															}
//														});
//												alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
//														new DialogInterface.OnClickListener() {
//
//															@Override
//															public void onClick(DialogInterface dialog, int which) {
//																// TODO Auto-generated method stub
//																Call_Dynamic_Pdf_Display();
//															}
//														});
//
//												alertDialog.show();
												
//												Call_Dynamic_Pdf_Display();
												
												View_Pdf_File(filename,path);

											}
										}
									};
								}.start();
							} else {
								toast = new ShowToast(AgentInspection2.this,
										"Internet connection not available");

							}
						}
					}
				});

				download[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(final View v) {
						// TODO Auto-generated method stub
						Button b = (Button) v;
						if (b.getText().toString().equals("Download")) {
							if (cf.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading");
								new Thread() {
									public void run() {
										Looper.prepare();
										try {
											Button b = (Button) v;
											String buttonvalue = v.getTag()
													.toString();
											int s = Integer
													.parseInt(buttonvalue);
											path = pdfpath[s];
											String[] filenamesplit = path
													.split("/");
											String filename = filenamesplit[filenamesplit.length - 1];
											System.out
													.println("The File Name is "
															+ filename);
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {
										@Override
										public void handleMessage(Message msg) {
											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												toast = new ShowToast(
														AgentInspection2.this,
														"There is a problem on your application. Please contact Paperless administrator.");

											} else if (show_handler == 2) {
												show_handler = 0;
//												toast = new ShowToast(
//														AgentInspection2.this,
//														"Report downloaded successfully");
//												Call_Dynamic_Pdf_Display();
												
												alertDialog = new AlertDialog.Builder(AgentInspection2.this).create();
												alertDialog
														.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
												alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
														new DialogInterface.OnClickListener() {
															public void onClick(DialogInterface dialog, int which) {
																Call_Dynamic_Pdf_Display();
															}
														});
												alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
														new DialogInterface.OnClickListener() {

															@Override
															public void onClick(DialogInterface dialog, int which) {
																// TODO Auto-generated method stub
																Call_Dynamic_Pdf_Display();
															}
														});

												alertDialog.show();
												

											}
										}
									};
								}.start();
							} else {
								toast = new ShowToast(AgentInspection2.this,
										"Internet connection not available");

							}
						} else {
							toast = new ShowToast(AgentInspection2.this,
									"This TAB is under Construction");
						}

					}
				});

				pter[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						String buttonvalue = v.getTag().toString();
						String[] splitvalue=buttonvalue.split("&#40");
						int s = Integer.parseInt(splitvalue[0]);
						String name=splitvalue[1];
						path = pdfpath[s];
						reportpath=path;
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];
						
						
						String ivalue=splitvalue[2];
						System.out.println("ivalue ="+ivalue);
						int ival=Integer.parseInt(ivalue);
						String ivalsplit=datasend[ival];
						System.out.println("ivaluesplit ="+ivalsplit);
						String pn=datasend[ival];
						String status="";

//						Intent intent=new Intent(AgentInspection2.this,EmailReport3.class);
//						intent.putExtra("emailaddress", "");
//						intent.putExtra("pdfpath", path);
//						intent.putExtra("name", name);
//						intent.putExtra("classidentifier", "AgentInspection2");
//						intent.putExtra("policynumber", pn);
//						intent.putExtra("status", status);
//						startActivity(intent);
////						finish();
						
						Intent intent=new Intent(AgentInspection2.this,EmailReport2.class);
						intent.putExtra("policynumber", "AgentInspection2");
						intent.putExtra("status", status);
						intent.putExtra("mailid", "");
						intent.putExtra("classidentifier", "AgentInspection2");
						intent.putExtra("ownersname", ownersname[s]);
						startActivity(intent);
						finish();

					}
				});

				if (i % 2 == 0) {
					rl[i].setBackgroundColor(Color.parseColor("#13456d"));
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#386588"));
				}
				/*if (data.length != (i + 1)) {
					View v1 = new View(this);
					v1.setBackgroundResource(R.color.white);
					l1.addView(v1, LayoutParams.FILL_PARENT, 1);
				}*/

				i++;
			} while (cur.moveToNext());

			/*LinearLayout.LayoutParams viewparams2 = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT, 1);
			viewparams2.setMargins(20, 0, 20, 0);

			View v2 = new View(this);
			v2.setBackgroundResource(R.color.black);
			dynamic.addView(v2, viewparams2);*/

		}
		else
		{
			rlnote.setVisibility(View.GONE);
		}
		cur.close();
	}
	

	private void Gallery_Open() {
		imageidentifier = "addaimage";
		Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
		i.setType("image/*");
		startActivityForResult(i, 0);
	}

	private void Camera_Open() {
		imageidentifier = "addaimage";
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		CapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
		startActivityForResult(intent, 1);
	}
	
	private void View_Pdf_File(String filename,String filepath)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(AgentInspection2.this,
					ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			// finish();
        }
	}

	private void Gallery_Camera_Dialog() {
		final Dialog dialog = new Dialog(AgentInspection2.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alert);
		dialog.setCancelable(false);
		// dialog.setCanceledOnTouchOutside(true);
		Button btnchoosefromgallery = (Button) dialog
				.findViewById(R.id.alert_choosefromgallery);
		Button btntakeapicturefromcamera = (Button) dialog
				.findViewById(R.id.alert_takeapicturefromcamera);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.alert_helpclose);
		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		btnchoosefromgallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				imageidentifier = "uploadaphoto";
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
				i.setType("image/*");
				startActivityForResult(i, 0);

			}
		});
		btntakeapicturefromcamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				imageidentifier = "uploadaphoto";
				String fileName = "temp.jpg";
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				CapturedImageURI = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
				startActivityForResult(intent, 1);
			}
		});
		dialog.show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
//		String selectedImagePath;
//		System.gc();
		if (requestCode == 0 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String filePath = cursor.getString(columnIndex);

			File f = new File(filePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			System.out.println("The file length is" + length);
			System.out.println("The file length in kb is" + kb);
			System.out.println("The file length in mb is" + mb);

			if (mb >= 2) {
				toast = new ShowToast(AgentInspection2.this,
						"File size exceeds!! Too large to attach");
			} else {
				
				if (imageidentifier == "uploadaphoto") {
					Bitmap bm = decodeFile(filePath);
					
					System.out.println("The bitmap is "+bm);
					if(bm==null)
					{
						System.out.println("Inside if");
						toast = new ShowToast(AgentInspection2.this,
								"File corrupted!! Cant able to attach");
					}
					else
					{
						Call_UploadPhoto_Dialog(filePath);
					}
				}

				else if (imageidentifier == "addaimage") {
				try
				{
					boolean filecheck=false;
					arraylistfilepahlength=arraylistfilepah.size();
					System.out.println("array list file length "+arraylistfilepahlength);
					if(arraylistfilepahlength==0)
					{
						filecheck=true;
					}
					else
					{
						if(arraylistfilepah.contains(filePath))
						{
							System.out.println("inside arraylist contains if");
							filecheck=false;
						}
						else
						{
							System.out.println("inside arraylist contains else");
							filecheck=Check_Duplicate_Image(filePath);
						}
					}
					
					System.out.println("filecheck value is "+filecheck);
					
					if(filecheck)
					{
						bitmapdb = decodeFile(filePath);
						
						System.out.println("The bitmap is "+bitmapdb);
						if(bitmapdb==null)
						{
							System.out.println("Inside if");
							toast = new ShowToast(AgentInspection2.this,
									"File corrupted!! Cant able to attach");
						}
						else
						{
//							if (imageidentifier == "uploadaphoto") {
//								Call_UploadPhoto_Dialog(filePath);
//
//							}
//
//							else if (imageidentifier == "addaimage") {

								Call_Elevationdialog(filePath);

//							}
						}
					}
					else
					{
						toast=new ShowToast(AgentInspection2.this, "Image already selected, Please choose different one");
					}
			}
			catch (OutOfMemoryError e) {
				// TODO: handle exception
				System.out.println("Out of memory error "+e.getMessage());
				toast=new ShowToast(AgentInspection2.this, "File size exceeds!! Too large to attach");
			}
			}
			}
		} else if (requestCode == 1 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = managedQuery(CapturedImageURI, projection, null,
					null, null);
			int column_index_data = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			String capturedImageFilePath = cursor.getString(column_index_data);
			String selectedImagePath = capturedImageFilePath;

			File f = new File(selectedImagePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			System.out.println("The file length is" + length);
			System.out.println("The file length in kb is" + kb);
			System.out.println("The file length in mb is" + mb);

			if (mb >= 2) {
				toast = new ShowToast(AgentInspection2.this,
						"File size exceeds!! Too large to attach");
			} else {
				try
				{
					bitmapdb = decodeFile(selectedImagePath);
				if(bitmapdb==null)
				{
					toast = new ShowToast(AgentInspection2.this,
							"File corrupted!! Cant able to attach");
				}
				else
				{
					if (imageidentifier == "uploadaphoto") {
						Call_UploadPhoto_Dialog(selectedImagePath);

					} else if (imageidentifier == "addaimage") {
						Call_Elevationdialog(selectedImagePath);
					}
				}
			}
			catch (OutOfMemoryError e) {
				// TODO: handle exception
				toast=new ShowToast(AgentInspection2.this, "File size exceeds!! Too large to attach");
			}
			}
		}
		else if(requestCode==2&&resultCode==RESULT_OK)
		{
			Bitmap bitmapdb=null;
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			filePath2 = cursor.getString(columnIndex);

			File f = new File(filePath2);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			System.out.println("The file length is" + length);
			System.out.println("The file length in kb is" + kb);
			System.out.println("The file length in mb is" + mb);

			if (mb >= 2) {
				toast = new ShowToast(AgentInspection2.this,
						"File size exceeds!! Too large to attach");
			} 
			 else {
					try
					{
						boolean filecheck=false;
						arraylistfilepahlength=arraylistfilepah.size();
						System.out.println("array list file length "+arraylistfilepahlength);
						if(arraylistfilepahlength==0)
						{
							filecheck=true;
						}
						else
						{
							if(arraylistfilepah.contains(filePath2))
							{
								System.out.println("inside arraylist contains if");
								filecheck=false;
							}
							else
							{
								System.out.println("inside arraylist contains else");
								filecheck=Check_Duplicate_Image(filePath2);
							}
						}
						
						System.out.println("filecheck value is "+filecheck);
						
						if(filecheck)
						{
							bitmapdb = decodeFile(filePath2);
							if(bitmapdb==null)
							{
								toast = new ShowToast(AgentInspection2.this,
										"File corrupted!! Cant able to attach");
								filePath2=tagfilepath;
								System.out.println("filepath2 is "+filePath2);
								Bitmap bm = decodeFile(tagfilepath);
								ivimageedit.setImageBitmap(bm);
							}
							else
							{
								System.out.println("Inside else request 2");
								ivimageedit.setImageBitmap(bitmapdb);
								cf.db.execSQL("update AddAImage set filePath='"+cf.encode(filePath2)+"' where filePath='"+cf.encode(tagfilepath)+"'");
							}
						}
						else
						{
							toast=new ShowToast(AgentInspection2.this, "Image already selected, Please choose different one");
						}
				}
				catch (OutOfMemoryError e) {
					// TODO: handle exception
					System.out.println("Out of memory error "+e.getMessage());
					toast=new ShowToast(AgentInspection2.this, "File size exceeds!! Too large to attach");
				}
				}
//			else {
//				try
//				{
//					bitmapdb = decodeFile(filePath2);
//				if(bitmapdb==null)
//				{
//					toast = new ShowToast(AgentInspection2.this,
//							"File corrupted!! Cant able to attach");
//					filePath2=tagfilepath;
//					System.out.println("filepath2 is "+filePath2);
//					Bitmap bm = decodeFile(tagfilepath);
//					ivimageedit.setImageBitmap(bm);
//				}
//				else
//				{
//					System.out.println("Inside else request 2");
//					ivimageedit.setImageBitmap(bitmapdb);
//					cf.db.execSQL("update AddAImage set filePath='"+cf.encode(filePath2)+"' where filePath='"+cf.encode(tagfilepath)+"'");
//				}
//			}
//			catch (OutOfMemoryError e) {
//				// TODO: handle exception
//				toast=new ShowToast(AgentInspection2.this, "File size exceeds!! Too large to attach");
//			}
//			}
		}

	}
	
	private boolean Check_Duplicate_Image(String filePath)
	{
		String res="";
		for(int i=0;i<arraylistfilepah.size();i++)
		{
			String filepath2=arraylistfilepah.get(i);
			Bitmap b1 = decodeFile(filePath);
			Bitmap b2 = decodeFile(filepath2);
			boolean result=imagesAreEqual(b1,b2);
			if(result==false)
			{
				res+="true";
				System.out.println("res="+res);
			}
			else
			{
				res+="false";
				System.out.println("res="+res);
			}
			
		}
		if(res.contains("false"))
		{
			return false;
		}
		else
		{
			return true;
		}
		
	}
	
	public boolean imagesAreEqual(Bitmap i1, Bitmap i2)
    {
        if (i1.getHeight() != i2.getHeight())
         return false;
        if (i1.getWidth() != i2.getWidth()) return false;

        for (int y = 0; y < i1.getHeight(); ++y)
           for (int x = 0; x < i1.getWidth(); ++x)
                if (i1.getPixel(x, y) != i2.getPixel(x, y)) return false;

        return true;
    }


	private void Call_UploadPhoto_Dialog(final String filePath) {
//		final Bitmap bitmapdb = BitmapFactory.decodeFile(filePath);
		final Bitmap bitmapdb = decodeFile(filePath);

		final Dialog dialog = new Dialog(AgentInspection2.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.upload_photo_alert);
		dialog.setCancelable(false);

		ImageView ivimage = (ImageView) dialog
				.findViewById(R.id.upload_photo_alertimage);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.upload_photo_alert_close);
		Button btnsave = (Button) dialog
				.findViewById(R.id.upload_photo_alert_save);
		Button btncancel = (Button) dialog
				.findViewById(R.id.upload_photo_alert_cancel);

		ivimage.setImageBitmap(bitmapdb);

		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				ivcoverpagelogoclose.setVisibility(View.VISIBLE);
				ivcoverpagelogoselectedimage.setVisibility(View.VISIBLE);
				ivcoverpagelogoselectedimage.setImageBitmap(bitmapdb);
				etcoverpagelogo.setText(filePath);
				lselectedlogo = filePath;
				findViewById(R.id.agentinsp_coverpagelogo).setVisibility(
						View.GONE);
				
//				ByteArrayOutputStream stream = new ByteArrayOutputStream();
//				bitmapdb.compress(Bitmap.CompressFormat.PNG, 0, stream);
//				bytecoverpagelogo = stream.toByteArray();
				System.out.println("The filepath is "+filePath);;
				
				String[] filenamesplit = filePath
						.split("/");
				strcoverpagelogofilename = filenamesplit[filenamesplit.length - 1];
				System.out
						.println("The File Name is "
								+ strcoverpagelogofilename);
				
				Bitmap bitmap = cf.ShrinkBitmap(filePath, 400, 400);

				System.out.println(" bimap "+bitmap);
				marshal = new MarshalBase64();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.PNG, 100, out);
				bytecoverpagelogo = out.toByteArray();
				System.out.println("bytecoverpagelogo length is "+bytecoverpagelogo.length);

				toast=new ShowToast(AgentInspection2.this, "Logo saved successfully");
			}
		});

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();

	}
	
	private void Call_Elevationdialog(final String filePath) {
		
		System.out.println("Inside call elevation dialog");
		
		final Dialog dialog = new Dialog(AgentInspection2.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.elevationtype_and_caption);
		dialog.setCancelable(false);

		final Spinner spinnerelevation = (Spinner) dialog
				.findViewById(R.id.elevation_elevationtype);
		final Spinner spinnercaption = (Spinner) dialog
				.findViewById(R.id.elevation_caption);
		final ImageView ivimage = (ImageView) dialog
				.findViewById(R.id.elevation_image);
		final Button btnsave = (Button) dialog
				.findViewById(R.id.elevation_save);
		final Button btncancel = (Button) dialog
				.findViewById(R.id.elevation_cancel);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.elevation_close);
		
		System.out.println("Before bitmap");
//		BitmapFactory.Options options = new BitmapFactory.Options();
//		options.inSampleSize = 8;
//		bitmapdb = BitmapFactory.decodeFile(filePath, options);
//		bitmapdb = BitmapFactory.decodeFile(filePath);
		bitmapdb = decodeFile(filePath);
		ivimage.setImageBitmap(bitmapdb);
		System.out.println("After bitmap");

		cf.CreateTable(16);
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoadCaptionValue,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			arraylistcaption.clear();
			arraylistcaption.add("--Select--");
			arraylistcaption.add("Add photo caption");
			do {
				String captionvalue = cf.decode(cur.getString(cur
						.getColumnIndex("caption")));
				arraylistcaption.add(captionvalue);

			} while (cur.moveToNext());
			ArrayAdapter<String> captionadapter = new ArrayAdapter<String>(
					AgentInspection2.this,
					android.R.layout.simple_spinner_item, arraylistcaption);
			captionadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnercaption.setAdapter(captionadapter);
		}
		else {
			arraylistcaption.clear();
			arraylistcaption.add("--Select--");
			arraylistcaption.add("Add photo caption");
			captionadapter = new ArrayAdapter<String>(AgentInspection2.this,
					android.R.layout.simple_spinner_item, array_caption);
			captionadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnercaption.setAdapter(captionadapter);
		}
		cur.close();

		ArrayAdapter<String> elevationadapter = new ArrayAdapter<String>(
				AgentInspection2.this, android.R.layout.simple_spinner_item,
				array_elevation);
		elevationadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerelevation.setAdapter(elevationadapter);

		spinnerelevation
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						strelevationvalue = spinnerelevation.getSelectedItem()
								.toString();
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		spinnercaption.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcaptionvalue = spinnercaption.getSelectedItem().toString();
				if (strcaptionvalue.equals("Add photo caption")) {
					System.out.println("Inside add photo caption");
					final Dialog dialog1 = new Dialog(AgentInspection2.this);
					dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog1.setContentView(R.layout.addcaption);
					dialog1.getWindow()
							.setSoftInputMode(
									WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
					dialog1.setCancelable(false);

					final ImageView ivclose = (ImageView) dialog1
							.findViewById(R.id.addcaption_close);
					final EditText etadd = (EditText) dialog1
							.findViewById(R.id.addcaption_etcaption);
					final Button btnadd = (Button) dialog1
							.findViewById(R.id.addcaption_add);
					final Button btncancel = (Button) dialog1
							.findViewById(R.id.addcaption_cancel);

					ivclose.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							spinnercaption.setSelection(0);
						}
					});

					btncancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog1.dismiss();
							spinnercaption.setSelection(0);
						}
					});

					btnadd.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							String addvalue = etadd.getText().toString();
							if (addvalue.trim().equals("")) {
								toast = new ShowToast(AgentInspection2.this,
										"Please add caption");
							} else {
								arraylistcaption.add(addvalue);
								cf.db.execSQL("insert into "
										+ cf.LoadCaptionValue
										+ " (caption) values('" + cf.encode(addvalue)
										+ "')");
								captionadapter = new ArrayAdapter<String>(
										AgentInspection2.this,
										android.R.layout.simple_spinner_item,
										arraylistcaption);
								captionadapter
										.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
								spinnercaption.setAdapter(captionadapter);
								spinnercaption.setSelection(captionadapter
										.getPosition(addvalue));

								dialog1.dismiss();

							}
						}
					});
					dialog1.show();

				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!strelevationvalue.equals("--Select--")) {
					if (!strcaptionvalue.equals("--Select--")) {
						dialog.dismiss();
						Dynamic_image_list(strelevationvalue, strcaptionvalue, filePath);

//						ByteArrayOutputStream stream = new ByteArrayOutputStream();
//						bitmapdb.compress(Bitmap.CompressFormat.PNG, 0, stream);
//						byte[] img1 = stream.toByteArray();
                        int imgno=0;
						try
						{
							Cursor cur=cf.db.rawQuery("select * from "+cf.AgentInspection, null);
							
							if(cur.getCount()>0)
							{
								imgno=(cur.getCount())+1;
							}
							else
							{
								imgno=1;
							}
						}
						catch (Exception e) {
							// TODO: handle exception
						}
						cf.CreateTable(17);
						cf.db.execSQL("insert into "
								+ cf.AddAImage
								+ " (imgid,agentid,caption,elevation,filepath) values('"+imgno+"','"+AgentId+"','"
								+ cf.encode(strcaptionvalue) + "','" + cf.encode(strelevationvalue)
								+ "','" + cf.encode(filePath) + "')");
						
						arraylistfilepah.add(filePath);
						
						Cursor acur=cf.db.rawQuery("select * from "+ cf.AgentInspection, null);
						
						if(acur.getCount()>0)
						{
							imgno=(acur.getCount())+1;
						}
						
						Cursor cur=cf.db.rawQuery("select * from " + cf.AddAImage + " where imgid='"+imgno+"'" , null);
						System.out.println("select * from "+cf.AddAImage + "where imgid='"+imgno+"'");
						System.out.println("selllAdd a Image count is "+cur.getCount());
						
						
						toast=new ShowToast(AgentInspection2.this, strelevationvalue+" saved successfully");
						
						int rows=cur.getCount();
						llimagecount.setVisibility(View.VISIBLE);
						tvimagecount.setText((rows)+"/30");
						
//						if (!bitmapdb.isRecycled()) {
//							bitmapdb.recycle();
//						}
//						bitmapdb=null;
//						System.gc();

					} else {
						toast = new ShowToast(AgentInspection2.this,
								"Please select Caption");
					}
				} else {
					toast = new ShowToast(AgentInspection2.this,
							"Please select Elevation Type");
				}
			}
		});

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
//				if (!bitmapdb.isRecycled()) {
//					bitmapdb.recycle();
//				}
//				bitmapdb=null;
//				System.gc();
			}
		});

		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
//				if (!bitmapdb.isRecycled()) {
//					bitmapdb.recycle();
//				}
//				bitmapdb=null;
//				System.gc();
			}
		});

		dialog.show();
	}

	private void Dynamic_image_list(String elevation, String caption, final String filePath) {
		
		
		Bitmap bitmap = null;
		try
		{
		final int rows=tldynamiclist.getChildCount();
		System.out.println("layout child count is "+rows);
		
		final TableRow[] tr;
		final LinearLayout[] rlelevation, rlcaption, llbutton;
		final RelativeLayout[] rlimage;
		final TextView[] tvelevation;
		final TextView[] tvcaption;
		final ImageView[] ivimage;
		final Button[] btnedit, btndelete;
		
//		bitmap.recycle();
//		bitmap=null;
//		System.gc();
		
//		bitmap=BitmapFactory.decodeFile(filePath);
		bitmap = decodeFile(filePath);
		
		tr=new TableRow[rows];
		rlelevation=new LinearLayout[rows];
		rlcaption=new LinearLayout[rows];
		llbutton=new LinearLayout[rows];
		rlimage=new RelativeLayout[rows];
		tvelevation=new TextView[rows];
		tvcaption=new TextView[rows];
		ivimage=new ImageView[rows];
		btnedit=new Button[rows];
		btndelete=new Button[rows];
		
		updatefilepath=filePath;
		tldynamiclist.setVisibility(View.VISIBLE);
		
		Display display = getWindowManager().getDefaultDisplay();
	    DisplayMetrics displayMetrics = new DisplayMetrics();
	    display.getMetrics(displayMetrics);

	    int width = displayMetrics.widthPixels;
	    int height = displayMetrics.heightPixels;
	    
	    System.out.println("The width is "+width);
	    System.out.println("The height is "+height);

		if (width > 1023 || height > 1023) {
			tlparams = new TableLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			tlparams.setMargins(2, 0, 2, 2);

			trparams = new TableRow.LayoutParams(215, 100);
			trparams.setMargins(1, 1, 1, 1);

			rlparams = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			rlparams.setMargins(20, 0, 0, 0);
			// rlparams.addRule(RelativeLayout.CENTER_IN_PARENT);
			rlparams.gravity = Gravity.CENTER;

			rlimageparams = new RelativeLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			rlimageparams.setMargins(10, 10, 10, 10);
			rlimageparams.addRule(RelativeLayout.CENTER_IN_PARENT);

			llparams = new LinearLayout.LayoutParams(80,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llparams.setMargins(20, 30, 0, 0);

			tr[rows-1] = new TableRow(this);
			tr[rows-1].setLayoutParams(tlparams);
			tr[rows-1].setBackgroundColor(0xffc0c0c0);
			tldynamiclist.addView(tr[rows-1]);

			rlelevation[rows-1] = new LinearLayout(this);
			rlelevation[rows-1].setLayoutParams(trparams);
			rlelevation[rows-1].setBackgroundColor(0xffffffff);
			tr[rows-1].addView(rlelevation[rows-1]);

			tvelevation[rows-1] = new TextView(this);
			tvelevation[rows-1].setLayoutParams(rlparams);
			tvelevation[rows-1].setText(elevation);
			tvelevation[rows-1].setTextSize(16);
			tvelevation[rows-1].setTypeface(null, Typeface.BOLD);
			rlelevation[rows-1].addView(tvelevation[rows-1]);

			rlimage[rows-1] = new RelativeLayout(this);
			rlimage[rows-1].setLayoutParams(trparams);
			rlimage[rows-1].setBackgroundColor(0xffffffff);
			tr[rows-1].addView(rlimage[rows-1]);

			ivimage[rows-1] = new ImageView(this);
			ivimage[rows-1].setLayoutParams(rlimageparams);
			ivimage[rows-1].setImageBitmap(bitmap);
			rlimage[rows-1].addView(ivimage[rows-1]);

			rlcaption[rows-1] = new LinearLayout(this);
			rlcaption[rows-1].setLayoutParams(trparams);
			rlcaption[rows-1].setBackgroundColor(0xffffffff);
			tr[rows-1].addView(rlcaption[rows-1]);

			tvcaption[rows-1] = new TextView(this);
			tvcaption[rows-1].setLayoutParams(rlparams);
			tvcaption[rows-1].setText(caption);
			tvcaption[rows-1].setTextSize(16);
			tvcaption[rows-1].setTypeface(null, Typeface.BOLD);
			rlcaption[rows-1].addView(tvcaption[rows-1]);

			llbutton[rows-1] = new LinearLayout(this);
			llbutton[rows-1].setLayoutParams(trparams);
			llbutton[rows-1].setBackgroundColor(0xffffffff);
			tr[rows-1].addView(llbutton[rows-1]);

			btnedit[rows-1] = new Button(this);
			btnedit[rows-1].setLayoutParams(llparams);
			btnedit[rows-1].setText("Edit");
			btnedit[rows-1].setTag(caption+"&#40"+elevation+"&#40"+filePath);
			System.out.println("caption="+caption+" elevation="+elevation+" filepath="+filePath);
			btnedit[rows-1].setBackgroundResource(R.drawable.buttonrepeat);
			btnedit[rows-1].setTextColor(0xffffffff);
			llbutton[rows-1].addView(btnedit[rows-1]);

			btndelete[rows-1] = new Button(this);
			btndelete[rows-1].setLayoutParams(llparams);
			btndelete[rows-1].setText("Delete");
			btndelete[rows-1].setTag(caption+"&#40"+elevation+"&#40"+filePath);
			btndelete[rows-1].setBackgroundResource(R.drawable.buttonrepeat);
			btndelete[rows-1].setTextColor(0xffffffff);
			llbutton[rows-1].addView(btndelete[rows-1]);

		}
		// else if(width > 480 && width < 801)
		// {
		//
		// System.out.println("Inside 800");
		// tlparams = new TableLayout.LayoutParams(
		// ViewGroup.LayoutParams.WRAP_CONTENT,
		// ViewGroup.LayoutParams.WRAP_CONTENT);
		// tlparams.setMargins(2, 0, 2, 2);
		//
		// trparams = new TableRow.LayoutParams(165,100);
		// trparams.setMargins(1, 1, 1, 1);
		//
		// TableRow.LayoutParams trparams1 = new
		// TableRow.LayoutParams(90,ViewGroup.LayoutParams.MATCH_PARENT);
		// trparams1.setMargins(1, 1, 1, 1);
		//
		// rlparams = new LinearLayout.LayoutParams(
		// ViewGroup.LayoutParams.WRAP_CONTENT,
		// ViewGroup.LayoutParams.WRAP_CONTENT);
		// rlparams.setMargins(10, 0, 0, 0);
		// rlparams.gravity = Gravity.CENTER;
		//
		// RelativeLayout.LayoutParams rllparams = new
		// RelativeLayout.LayoutParams(
		// ViewGroup.LayoutParams.WRAP_CONTENT,
		// ViewGroup.LayoutParams.WRAP_CONTENT);
		// // rlparams.setMargins(10, 10, 0, 0);
		// rllparams.addRule(RelativeLayout.CENTER_IN_PARENT);
		// // rllparams.gravity = Gravity.CENTER;
		//
		// rlimageparams = new RelativeLayout.LayoutParams(
		// ViewGroup.LayoutParams.WRAP_CONTENT,
		// ViewGroup.LayoutParams.WRAP_CONTENT);
		// rlimageparams.setMargins(5, 5, 5, 5);
		// rlimageparams.addRule(RelativeLayout.CENTER_IN_PARENT);
		//
		// llparams = new LinearLayout.LayoutParams(80,
		// ViewGroup.LayoutParams.WRAP_CONTENT);
		// llparams.setMargins(0, 10, 0, 0);
		// llparams.gravity=Gravity.CENTER_HORIZONTAL;
		//
		// tr[rows-1] = new TableRow(this);
		// tr[rows-1].setLayoutParams(tlparams);
		// tr[rows-1].setBackgroundColor(0xffc0c0c0);
		// tldynamiclist.addView(tr[rows-1]);
		//
		// rlelevation[rows-1] = new LinearLayout(this);
		// rlelevation[rows-1].setLayoutParams(trparams);
		// rlelevation[rows-1].setBackgroundColor(0xffffffff);
		// tr[rows-1].addView(rlelevation[rows-1]);
		//
		// tvelevation[rows-1] = new TextView(this);
		// tvelevation[rows-1].setLayoutParams(rlparams);
		// tvelevation[rows-1].setText(elevation);
		// tvelevation[rows-1].setTextSize(16);
		// tvelevation[rows-1].setTypeface(null, Typeface.BOLD);
		// rlelevation[rows-1].addView(tvelevation[rows-1]);
		//
		// rlimage[rows-1] = new RelativeLayout(this);
		// rlimage[rows-1].setLayoutParams(trparams);
		// rlimage[rows-1].setBackgroundColor(0xffffffff);
		// tr[rows-1].addView(rlimage[rows-1]);
		//
		// ivimage = new ImageView(this);
		// ivimage.setLayoutParams(rlimageparams);
		// ivimage.setImageBitmap(bitmap);
		// rlimage[rows-1].addView(ivimage);
		//
		// RelativeLayout rlcaption[rows-1] = new RelativeLayout(this);
		// rlcaption[rows-1].setLayoutParams(trparams1);
		// rlcaption[rows-1].setBackgroundColor(0xffffffff);
		// tr[rows-1].addView(rlcaption[rows-1]);
		//
		// tvcaption[rows-1] = new TextView(this);
		// tvcaption[rows-1].setLayoutParams(rllparams);
		// tvcaption[rows-1].setText(caption);
		// tvcaption[rows-1].setTextSize(16);
		// tvcaption[rows-1].setTypeface(null, Typeface.BOLD);
		// rlcaption[rows-1].addView(tvcaption[rows-1]);
		//
		// llbutton[rows-1] = new LinearLayout(this);
		// llbutton[rows-1].setOrientation(LinearLayout.VERTICAL);
		// llbutton[rows-1].setLayoutParams(trparams);
		// llbutton[rows-1].setBackgroundColor(0xffffffff);
		// tr[rows-1].addView(llbutton[rows-1]);
		//
		// btnedit[rows-1] = new Button(this);
		// btnedit[rows-1].setLayoutParams(llparams);
		// btnedit[rows-1].setText("Edit");
		// btnedit[rows-1].setBackgroundResource(R.drawable.buttonrepeat);
		// btnedit[rows-1].setTextColor(0xffffffff);
		// llbutton[rows-1].addView(btnedit[rows-1]);
		//
		// btndelete[rows-1] = new Button(this);
		// btndelete[rows-1].setLayoutParams(llparams);
		// btndelete[rows-1].setText("Delete");
		// btndelete[rows-1].setBackgroundResource(R.drawable.buttonrepeat);
		// btndelete[rows-1].setTextColor(0xffffffff);
		// llbutton[rows-1].addView(btndelete[rows-1]);
		//
		//
		// }
		else {
			System.out.println("Phone agent inspection");
			tlparams = new TableLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			tlparams.setMargins(2, 0, 2, 2);

			trparams = new TableRow.LayoutParams(90, 125);
			trparams.setMargins(1, 1, 1, 1);

			TableRow.LayoutParams trparams1 = new TableRow.LayoutParams(90,
					ViewGroup.LayoutParams.MATCH_PARENT);
			trparams1.setMargins(1, 1, 1, 1);

			rlparams = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			rlparams.setMargins(10, 0, 0, 0);
			rlparams.gravity = Gravity.CENTER;

			RelativeLayout.LayoutParams rllparams = new RelativeLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			// rlparams.setMargins(10, 10, 0, 0);
			rllparams.addRule(RelativeLayout.CENTER_IN_PARENT);
			// rllparams.gravity = Gravity.CENTER;

			rlimageparams = new RelativeLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			rlimageparams.setMargins(5, 5, 5, 5);
			rlimageparams.addRule(RelativeLayout.CENTER_IN_PARENT);

			llparams = new LinearLayout.LayoutParams(80,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llparams.setMargins(0, 10, 0, 0);

			tr[rows-1] = new TableRow(this);
			tr[rows-1].setLayoutParams(tlparams);
			tr[rows-1].setBackgroundColor(0xffc0c0c0);
			tldynamiclist.addView(tr[rows-1]);

			rlelevation[rows-1] = new LinearLayout(this);
			rlelevation[rows-1].setLayoutParams(trparams);
			rlelevation[rows-1].setBackgroundColor(0xffffffff);
			tr[rows-1].addView(rlelevation[rows-1]);

			tvelevation[rows-1] = new TextView(this);
			tvelevation[rows-1].setLayoutParams(rlparams);
			tvelevation[rows-1].setText(elevation);
			tvelevation[rows-1].setTextSize(16);
			tvelevation[rows-1].setTypeface(null, Typeface.BOLD);
			rlelevation[rows-1].addView(tvelevation[rows-1]);

			rlimage[rows-1] = new RelativeLayout(this);
			rlimage[rows-1].setLayoutParams(trparams);
			rlimage[rows-1].setBackgroundColor(0xffffffff);
			tr[rows-1].addView(rlimage[rows-1]);

			ivimage[rows-1] = new ImageView(this);
			ivimage[rows-1].setLayoutParams(rlimageparams);
			ivimage[rows-1].setImageBitmap(bitmap);
			rlimage[rows-1].addView(ivimage[rows-1]);

			RelativeLayout rlcaption1 = new RelativeLayout(this);
			rlcaption1.setLayoutParams(trparams1);
			rlcaption1.setBackgroundColor(0xffffffff);
			tr[rows-1].addView(rlcaption1);

			tvcaption[rows-1] = new TextView(this);
			tvcaption[rows-1].setLayoutParams(rllparams);
			tvcaption[rows-1].setText(caption);
			tvcaption[rows-1].setTextSize(16);
			tvcaption[rows-1].setTypeface(null, Typeface.BOLD);
			rlcaption1.addView(tvcaption[rows-1]);

			llbutton[rows-1] = new LinearLayout(this);
			llbutton[rows-1].setOrientation(LinearLayout.VERTICAL);
			llbutton[rows-1].setGravity(Gravity.CENTER_HORIZONTAL);
			llbutton[rows-1].setLayoutParams(trparams);
			llbutton[rows-1].setBackgroundColor(0xffffffff);
			tr[rows-1].addView(llbutton[rows-1]);

			btnedit[rows-1] = new Button(this);
			btnedit[rows-1].setLayoutParams(llparams);
			btnedit[rows-1].setText("Edit");
			btnedit[rows-1].setTag(caption+"&#40"+elevation+"&#40"+filePath);
			btnedit[rows-1].setBackgroundResource(R.drawable.buttonrepeat);
			btnedit[rows-1].setTextColor(0xffffffff);
			llbutton[rows-1].addView(btnedit[rows-1]);

			btndelete[rows-1] = new Button(this);
			btndelete[rows-1].setLayoutParams(llparams);
			btndelete[rows-1].setText("Delete");
			btndelete[rows-1].setTag(caption+"&#40"+elevation+"&#40"+filePath);
			btndelete[rows-1].setBackgroundResource(R.drawable.buttonrepeat);
			btndelete[rows-1].setTextColor(0xffffffff);
			llbutton[rows-1].addView(btndelete[rows-1]);

		}

		btnedit[rows-1].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Call_Elevationdialog2(tvelevation[rows-1].getText().toString(),tvcaption[rows-1].getText().toString(),bitmap);
				
				filePath2="";
				
				String tagvalue=v.getTag().toString();
				String[] arraytag=tagvalue.split("&#40");
				tagcaption=arraytag[0];
				tagelevation=arraytag[1];
				tagfilepath=arraytag[2];
				
				System.out.println("Tag Caption "+tagcaption);
				System.out.println("Tag elevation "+tagelevation);
				System.out.println("Tag file path "+tagfilepath);
				
				final Dialog dialog2 = new Dialog(AgentInspection2.this);
				dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog2.setContentView(R.layout.elevationtype_and_caption);
				dialog2.setCancelable(false);

				final Spinner spinnerelevation = (Spinner) dialog2
						.findViewById(R.id.elevation_elevationtype);
				final Spinner spinnercaption = (Spinner) dialog2
						.findViewById(R.id.elevation_caption);
				ivimageedit = (ImageView) dialog2
						.findViewById(R.id.elevation_image);
				final Button btnsave = (Button) dialog2
						.findViewById(R.id.elevation_save);
				final Button btncancel = (Button) dialog2
						.findViewById(R.id.elevation_cancel);
				ImageView ivclose = (ImageView) dialog2
						.findViewById(R.id.elevation_close);
				
//				Bitmap bm=BitmapFactory.decodeFile(tagfilepath);
				System.out.println("Tagfile path ="+tagfilepath);
				Bitmap bm = decodeFile(tagfilepath);
				ivimageedit.setImageBitmap(bm);

//				ivimageedit.setImageBitmap(bitmap);
				
				ivimageedit.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
						i.setType("image/*");
						startActivityForResult(i, 2);
					}
				});
				
				final String oldcaptionvalue=tvcaption[rows-1].getText().toString();
				final String oldelevationvalue=tvelevation[rows-1].getText().toString();
				
				cf.CreateTable(16);
				Cursor cur = cf.db.rawQuery("select * from "
						+ cf.LoadCaptionValue, null);
				cur.moveToFirst();
				if (cur.getCount() >= 1) {
					arraylistcaption.clear();
					arraylistcaption.add("--Select--");
					arraylistcaption.add("Add photo caption");
					do {
						String captionvalue = cf.decode(cur.getString(cur
								.getColumnIndex("caption")));
						arraylistcaption.add(captionvalue);

					} while (cur.moveToNext());
					ArrayAdapter<String> captionadapter = new ArrayAdapter<String>(
							AgentInspection2.this,
							android.R.layout.simple_spinner_item,
							arraylistcaption);
					captionadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercaption.setAdapter(captionadapter);
					spinnercaption.setSelection(captionadapter
							.getPosition(tvcaption[rows-1].getText().toString()));
				} else {
					arraylistcaption.clear();
					arraylistcaption.add("--Select--");
					arraylistcaption.add("Add photo caption");
					captionadapter = new ArrayAdapter<String>(
							AgentInspection2.this,
							android.R.layout.simple_spinner_item, array_caption);
					captionadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercaption.setAdapter(captionadapter);
				}
				cur.close();

				ArrayAdapter<String> elevationadapter = new ArrayAdapter<String>(
						AgentInspection2.this,
						android.R.layout.simple_spinner_item, array_elevation);
				elevationadapter
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinnerelevation.setAdapter(elevationadapter);

				spinnerelevation.setSelection(elevationadapter
						.getPosition(tvelevation[rows-1].getText().toString()));

				spinnerelevation
						.setOnItemSelectedListener(new OnItemSelectedListener() {

							@Override
							public void onItemSelected(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub
								strelevationvalue = spinnerelevation
										.getSelectedItem().toString();
							}

							@Override
							public void onNothingSelected(AdapterView<?> arg0) {
								// TODO Auto-generated method stub

							}
						});

				spinnercaption
						.setOnItemSelectedListener(new OnItemSelectedListener() {

							@Override
							public void onItemSelected(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub
								strcaptionvalue = spinnercaption
										.getSelectedItem().toString();
								if (strcaptionvalue.equals("Add photo caption")) {
									System.out
											.println("Inside add photo caption");
									final Dialog dialog1 = new Dialog(
											AgentInspection2.this);
									dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
									dialog1.setContentView(R.layout.addcaption);
									dialog1.setCancelable(false);

									final ImageView ivclose = (ImageView) dialog1
											.findViewById(R.id.addcaption_close);
									final EditText etadd = (EditText) dialog1
											.findViewById(R.id.addcaption_etcaption);
									final Button btnadd = (Button) dialog1
											.findViewById(R.id.addcaption_add);
									final Button btncancel = (Button) dialog1
											.findViewById(R.id.addcaption_cancel);

									ivclose.setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(View v) {
											// TODO Auto-generated method stub
											dialog1.dismiss();
										}
									});

									btncancel
											.setOnClickListener(new OnClickListener() {

												@Override
												public void onClick(View v) {
													// TODO Auto-generated
													// method stub
													dialog1.dismiss();
												}
											});

									btnadd.setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(View v) {
											// TODO Auto-generated method stub
											if (etadd.getText().toString()
													.trim().equals("")) {
												toast = new ShowToast(
														AgentInspection2.this,
														"Please add Caption");
											} else {
												arraylistcaption.add(etadd
														.getText().toString());
												cf.db.execSQL("insert into "
														+ cf.LoadCaptionValue
														+ " (caption) values('"
														+ cf.encode(etadd.getText()
																.toString())
														+ "')");
												captionadapter = new ArrayAdapter<String>(
														AgentInspection2.this,
														android.R.layout.simple_spinner_item,
														arraylistcaption);
												captionadapter
														.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
												spinnercaption
														.setAdapter(captionadapter);
												spinnercaption
														.setSelection(captionadapter
																.getPosition(etadd
																		.getText()
																		.toString()));

												dialog1.dismiss();

											}
										}
									});
									dialog1.show();

								}
							}

							@Override
							public void onNothingSelected(AdapterView<?> arg0) {
								// TODO Auto-generated method stub

							}
						});

				btnsave.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (!strelevationvalue.equals("--Select--")) {
							if (!strcaptionvalue.equals("--Select--")) {
								dialog2.dismiss();
								tvelevation[rows-1].setText(strelevationvalue);
								tvcaption[rows-1].setText(strcaptionvalue);
								if(filePath2.equals(""))
								{
									filePath2=tagfilepath;
								}
								
									Bitmap bitmapdb = decodeFile(filePath2);
									System.out.println("bitmap is "+bitmapdb);
									ivimage[rows-1].setImageBitmap(bitmapdb);
								System.out.println("filepath 2 is "+filePath2);
								
								arraylistfilepah.remove(tagfilepath);
								arraylistfilepah.add(filePath2);

								cf.CreateTable(17);
								cf.db.execSQL("update " + cf.AddAImage
										+ " set caption='" + cf.encode(strcaptionvalue)
										+ "',elevation='" + cf.encode(strelevationvalue)+"',filepath='"+cf.encode(filePath2)
										+ "' where filepath='" + cf.encode(filePath2) + "'");
								
								Cursor cur=cf.db.rawQuery("select * from "+cf.AddAImage, null);
								System.out.println("Add a Image count is "+cur.getCount());
								
								btnedit[rows-1].setTag(strcaptionvalue+"&#40"+strelevationvalue+"&#40"+filePath2);
								btndelete[rows-1].setTag(strcaptionvalue+"&#40"+strelevationvalue+"&#40"+filePath2);
								
								if(!oldcaptionvalue.equals(strcaptionvalue)||!oldelevationvalue.equals(strelevationvalue)||!tagfilepath.equals(filePath2))
								{
									toast=new ShowToast(AgentInspection2.this, strelevationvalue+" updated successfully");
								}

							} else {
								toast = new ShowToast(AgentInspection2.this,
										"Please select Caption");
							}
						} else {
							toast = new ShowToast(AgentInspection2.this,
									"Please select Elevation Type");
						}
					}
				});

				btncancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog2.dismiss();
					}
				});

				ivclose.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog2.dismiss();
					}
				});

				dialog2.show();

			}
		});

		btndelete[rows-1].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String tagvalue=v.getTag().toString();
				String[] arraytag=tagvalue.split("&#40");
				tagcaption=arraytag[0];
				tagelevation=arraytag[1];
				tagfilepath=arraytag[2];
				
				System.out.println("Rows value is "+rows);
				
				final Dialog dialog3 = new Dialog(AgentInspection2.this);
				dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog3.setContentView(R.layout.confirm_delete);
				dialog3.setCancelable(false);
				Button btnyes = (Button) dialog3
						.findViewById(R.id.confirmdelete_yes);
				Button btnno = (Button) dialog3
						.findViewById(R.id.confirmdelete_no);
				TextView tvtext = (TextView) dialog3
						.findViewById(R.id.confirmdelete_tvtext);
				
				tvtext.setText(tagelevation+"?");
				
				btnyes.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						tldynamiclist.removeView(tr[rows-1]);
						if (tldynamiclist.getChildCount() < 2) {
							tldynamiclist.setVisibility(View.GONE);
						}
						dialog3.dismiss();

						cf.CreateTable(17);
						// cf.db.execSQL("insert into " + cf.AddAImage
						// + " (caption,elevation,image) values('"
						// + strcaptionvalue + "','" + strelevationvalue
						// + "','" + img1 + "')");
						
//						cf.db.execSQL("delete from " + cf.AddAImage
//								+ " where filepath='" + tagfilepath + "'");
						
						arraylistfilepah.remove(tagfilepath);
						System.out.println("arraylistfilepah length is "+arraylistfilepah.size());
						
						cf.db.execSQL("delete from " + cf.AddAImage
								+ " where oid='"+rows+"'");
						
                        Cursor acur=cf.db.rawQuery("select * from "+cf.AgentInspection, null);
						int img=0;
						if(acur.getCount()>0)
						{
							img=(acur.getCount())+1;
						}
						Cursor cur=cf.db.rawQuery("select * from "+ cf.AddAImage + " where imgid='"+img+"'" , null);
						System.out.println("secccccAdd a Image count is "+cur.getCount());
						
						int rows=cur.getCount();
						llimagecount.setVisibility(View.VISIBLE);
						tvimagecount.setText((rows)+"/30");
						
						if(rows==0)
						{
							llimagecount.setVisibility(View.GONE);
						}

					}
				});

				btnno.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog3.dismiss();
					}
				});

				dialog3.show();
			}
		});
	}
	catch (OutOfMemoryError e) {
		// TODO: handle exception
		System.out.println("Out of memory error "+e.getMessage());
		toast=new ShowToast(AgentInspection2.this, "File size exceeds!! Too large to attach");
	}
	}

	class myCheckBoxChnageClicker implements CheckBox.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub

			if (isChecked) {
				strinspaddress1 = etinspaddress1.getText().toString();
				strinspaddress2 = etinspaddress2.getText().toString();
				strcity = etcity.getText().toString();
				strzip = etzip.getText().toString();
				/*if (!strinspaddress1.equals("")) {
						if (!strstate.equals("--Select--")) {
							if (!strcounty.equals("--Select--")
									&& !strcounty.equals("")) {
								if (!strcity.equals("")) {
									if (!strzip.equals("")) {
										if ((strzip.length()==5)) {*/
										
										call_county2=false;
										
										etmailaddress1.setText(strinspaddress1);
										etmailaddress2.setText(strinspaddress2);
										etcity2.setText(strcity);
										etzip2.setText(strzip);
										strstate2=strstate;
										strstateid2=strstateid;
										strcounty2=strcounty;
										strcountyid2=strcountyid;
										
										arraystateid2=arraystateid;
										arraystatename2=arraystatename;
										arraycountyid2=arraycountyid;
										arraycountyname2=arraycountyname;
										
										spinnerstate2.setAdapter(stateadapter);
										spinnercounty2.setAdapter(countyadapter);
										spinnerstate2.setSelection(spinnerstate.getSelectedItemPosition());

									/*} else {
										toast = new ShowToast(
												AgentInspection2.this,
												"Zip should be 5 characters");
										etzip.requestFocus();
										cbaddresscheck.setChecked(false);
									}
									} else {
										toast = new ShowToast(
												AgentInspection2.this,
												"Please enter Zip");
										etzip.requestFocus();
										cbaddresscheck.setChecked(false);
									}
								} else {
									toast = new ShowToast(
											AgentInspection2.this,
											"Please enter City");
									etcity.requestFocus();
									cbaddresscheck.setChecked(false);
								}
							} else {
								toast = new ShowToast(AgentInspection2.this,
										"Please select County");
								cbaddresscheck.setChecked(false);
							}
						} else {
							toast = new ShowToast(AgentInspection2.this,
									"Please select state");
							cbaddresscheck.setChecked(false);
						}
				} else {
					toast = new ShowToast(AgentInspection2.this,
							"Please enter Inspection Address1");
					etinspaddress1.requestFocus();
					cbaddresscheck.setChecked(false);
				}*/
			} else {

				etmailaddress1.setText("");
				etmailaddress2.setText("");
				etcity2.setText("");
				etzip2.setText("");
				spinnerstate2.setSelection(stateadapter.getPosition(strselect));
				
				call_county2=true;
			}

		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			// set date picker as current date
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			datePicker = new DatePickerDialog (this, 
					datePickerListener, year, month, day); 
			   
			return datePicker;

		case 1:
			// set time picker as current time
			final Calendar c1 = Calendar.getInstance();
			int hour = c1.get(Calendar.HOUR_OF_DAY);
			int minute = c1.get(Calendar.MINUTE);
			timepicker= new TimePickerDialog(this, timePickerListener, hour, minute,
					false);
			return timepicker;
		}
		return null;
	}

	public String LoadState() {
		try {
			dbh = new DataBaseHelper(AgentInspection2.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = cf.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = cf.decode(cur.getString(cur
							.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}

	public void LoadCounty(final String stateid) {
		try {
			dbh = new DataBaseHelper(AgentInspection2.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(AgentInspection2.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty.setAdapter(countyadapter);
		
		if(countysetselection)
		{
			spinnercounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		
	}

	public void LoadCounty2(final String stateid) {
		try {
			dbh = new DataBaseHelper(AgentInspection2.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid2 = new String[rows + 1];
			arraycountyname2 = new String[rows + 1];
			arraycountyid2[0] = "--Select--";
			arraycountyname2[0] = "--Select--";
			System.out.println("LoadCounty2 count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid2[i] = id;
					arraycountyname2[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData2();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData2() {
		countyadapter2 = new ArrayAdapter<String>(AgentInspection2.this,
				android.R.layout.simple_spinner_item, arraycountyname2);
		countyadapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty2.setAdapter(countyadapter2);
		
		if(countysetselection)
		{
			spinnercounty2.setSelection(countyadapter2.getPosition(county));
			countysetselection=false;
		}
		
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			int year = selectedYear;
			int month = selectedMonth;
			int day = selectedDay;

			etdate.setText(new StringBuilder().append(month + 1).append("/")
					.append(day).append("/").append(year));
			
			Date date1 = null,date2 = null;
			try
			{
				System.out.println("Inside try");
				String formatString = "MM/dd/yyyy";
				SimpleDateFormat df = new SimpleDateFormat(formatString);
				date1 = df.parse(currentdate);
				date2 = df.parse(etdate.getText().toString());
				System.out.println("current date "+date1);
				System.out.println("current date "+date1);
				if (date2.compareTo(date1)<0) {
					System.out.println("inside date if");
					toast=new ShowToast(AgentInspection2.this, "Please select current or future date");
					etdate.setText("");
				}
				else
				{
					System.out.println("inside date else");
					etdate.setText(new StringBuilder().append(month + 1)
							.append("/").append(day).append("/").append(year));
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
			//updating date picker to current date
			final Calendar c = Calendar.getInstance();
			datePicker.updateDate(c.get(Calendar.YEAR), 
					 c.get(Calendar.MONTH), c.get(Calendar.DATE));

		}
	};

	private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
		public void onTimeSet(TimePicker view, int selectedHour,
				int selectedMinute) {
			int hour = selectedHour;
			int minute = selectedMinute;
			StringBuilder sb = new StringBuilder();
			if (hour >= 12) {
				sb.append(hour - 12).append(":").append(minute).append(" PM");
			} else {
				sb.append(hour).append(":").append(minute).append(" AM");
			}
			ettime.setText(sb);
			
			//updating time picker to current time
			final Calendar c = Calendar.getInstance();
			timepicker.updateTime(c.get(Calendar.HOUR_OF_DAY), 
					 c.get(Calendar.MINUTE));
		}
	};
	
	private void Load_State_County_City(final EditText et)
	{

		if (cf.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(AgentInspection2.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = cf
								.Calling_WS_GETADDRESSDETAILS(et.getText().toString(),"GETADDRESSDETAILS");
						System.out.println("response GETADDRESSDETAILS" + chklogin1);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									AgentInspection2.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									AgentInspection2.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							if (chklogin1.toString().equals("anyType{}"))
							{
								et.setText("");
								toast = new ShowToast(AgentInspection2.this,
										"Please enter a valid Zip");
							}
							else
							{
								Load_State_County_City(chklogin1);
							}
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(AgentInspection2.this,
					"Internet connection not available");

		}
	
	}
	
	private void Load_State_County_City(SoapObject objInsert)
	{
			SoapObject obj = (SoapObject) objInsert.getProperty(0);
			state=String.valueOf(obj.getProperty("s_state"));
			stateid=String.valueOf(obj.getProperty("i_state"));
			county=String.valueOf(obj.getProperty("A_County"));
			countyid=String.valueOf(obj.getProperty("i_County"));
			city=String.valueOf(obj.getProperty("city"));
			
			System.out.println("State :"+state);
			System.out.println("County :"+county);
			System.out.println("City :"+city);
			
			if(zipidentifier.equals("zip1"))
			{
				spinnerstate.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity.setText(city);
			}
			else if(zipidentifier.equals("zip2"))
			{
				spinnerstate2.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity2.setText(city);
			}
			
	}
	
	private static Bitmap decodeFile(String file) {
	    try {
	    	
	    	File f=new File(file);
	    	
	        // Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

	        // The new size we want to scale to
	        final int REQUIRED_SIZE = 150;

	        // Find the correct scale value. It should be the power of 2.
	        int scale = 1;
	        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
	                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
	            scale *= 2;

	        // Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize = scale;
	        o.inJustDecodeBounds = false;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {
	    }

	    return null;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(AgentInspection2.this, HomeScreen.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
		//cf.CreateTable(17);
		//cf.db.execSQL("delete from " + cf.AddAImage);
	}
	
	public class DynamicTextWatcher implements TextWatcher
	{

		public DynamicTextWatcher(EditText ettitle) {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			
		}
		
	}

}
