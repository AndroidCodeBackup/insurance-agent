package idsoft.agentmodule;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.URL;
import java.util.concurrent.TimeoutException;

import org.ksoap2.serialization.SoapObject;
import org.xmlpull.v1.XmlPullParserException;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class LoginPage extends Activity {
	EditText etpassword;
	AutoCompleteTextView autousername;
	Button login, cancel;
	String username, password, passwordresetstring;
	CommonFunctions cf;
	ShowToast toast;
	int show_handler;
	public static String agentname;
	private static final String LOG = null;
	String[] linarr;
	TextView tvlines;
	String insertsql = "", insertsql1 = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login_page);
		
		cf = new CommonFunctions(this);
		ImageView iv = (ImageView) findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);
		check_logintable();
		Parse_Code();
		tvlines = new TextView(this);
		etpassword = (EditText) findViewById(R.id.loginpage_etpassword);
		autousername = (AutoCompleteTextView) findViewById(R.id.loginpage_autocomtvusername);
		
		autousername.addTextChangedListener(new CustomTextWatcher(autousername));
		etpassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				 if (etpassword.getText().toString().startsWith(" "))
			        {
			            // Not allowed
					 etpassword.setText("");
			        }
				 String result = s.toString().replaceAll(" ", "");
				    if (!s.toString().equals(result)) {
				         etpassword.setText(result);
				         etpassword.setSelection(result.length());
				         // alert the user
				    }
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
	}
	
	private void Parse_Code()
	{
		//Parse code for receiving notifications
		
				Parse.initialize(this, "Xk7bqFPgDuWSanf4LO3YKtAEhzrtj6VeiDcPaPXk", "gcihUrbh6NyBcsMgy3ULpE6xkXZCY9qaKnmK5vbg");

				ParseAnalytics.trackAppOpened(getIntent());

				// inform the Parse Cloud that it is ready for notifications
				cf.CreateTable(0);
				Cursor cur = cf.db.rawQuery("select * from " + cf.LoginPage, null);
				if (cur.getCount() >= 1) {
					PushService.setDefaultPushCallback(this, HomeScreen.class);
				}
				else
				{
					PushService.setDefaultPushCallback(this, LoginPage.class);
				}
//				PushService.setDefaultPushCallback(this, HomeScreen.class);
				ParseInstallation.getCurrentInstallation().saveInBackground();
				
				//Parse code for receiving notifications
	}

	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.loginpage_btnlogin:
			check_username_password_notempty();
			// Insert_State();
			// Insert_County();
			break;

		case R.id.loginpage_btncancel:
			cancel();
			break;
			
		case R.id.loginpage_btnsignup:
			Intent intent=new Intent(LoginPage.this,Signup.class);
			startActivity(intent);
			finish();
			break;
		}
	}

	private void check_logintable() {
		cf.CreateTable(0);
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoginPage, null);
		if (cur.getCount() >= 1) {
			Intent intent = new Intent(LoginPage.this, HomeScreen.class);
			startActivity(intent);
			finish();
		}
		cur.close();
	}

	private void check_username_password_notempty() {
		username = autousername.getText().toString();
		password = etpassword.getText().toString();
		if (username.trim().equals("") && password.trim().equals("")) {
			toast = new ShowToast(LoginPage.this,
					"Please enter USERNAME and PASSWORD");
			autousername.requestFocus();
			autousername.setText("");
			etpassword.setText("");
		} else {
			if (username.trim().equals("")) {
				toast = new ShowToast(LoginPage.this,
						"Please enter USERNAME");
				autousername.requestFocus();
				autousername.setText("");
			} else {
				if (password.trim().equals("")) {
					toast = new ShowToast(LoginPage.this,
							"Please enter PASSWORD");
					etpassword.requestFocus();
					etpassword.setText("");
				} else {
					login();
				}
			}
		}
	}

	private void login() {
		if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Processing.. Please wait.");
			new Thread() {
				String chkauth;
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = cf.Calling_WS_LoginPage(username,
								password, "CheckUserAuthentication");
						System.out.println("The result for login is " + chklogin);
						SoapObject obj = (SoapObject) chklogin.getProperty(0);
						chkauth = String.valueOf(obj
								.getProperty("userAuthentication"));
						if (chkauth.equals("true")) /*
													 * USERAUTHENTICATION IS
													 * TRUE
													 */
						{
							String agentid = String.valueOf(obj
									.getProperty("userId"));
//							Insert_County_State();
							
							cf.CreateTable(0);
							cf.db.execSQL("insert into " + cf.LoginPage
									+ " (username,password,agentid)"
									+ " values('" + cf.encode(username) + "','"
									+ cf.encode(password) + "','"
									+ cf.encode(agentid) + "');");

							InsertAgentInformation(agentid);
							System.out
									.println("response check user auth" + obj);
							
							show_handler = 5;
							handler.sendEmptyMessage(0);

//							cf.CreateTable(0);
//							cf.db.execSQL("insert into " + cf.LoginPage
//									+ " (username,password,agentid)"
//									+ " values('" + cf.encode(username) + "','"
//									+ cf.encode(password) + "','"
//									+ cf.encode(agentid) + "');");
//
//							cf.pd.dismiss();
//							Cursor cur=cf.db.rawQuery("select * from "+cf.AgentInformation, null);
//							System.out.println("Agent Information count is "+cur.getCount());
//							if(cur.getCount()>=1)
//							{
//								Intent intent = new Intent(LoginPage.this,
//										HomeScreen.class);
//								startActivity(intent);
//								finish();
//							}
//							else
//							{
//								toast = new ShowToast(
//										LoginPage.this,
//										"There is a problem on your application. Please contact Paperless administrator.");
//							}
						} else /* USERAUTHENTICATION IS FALSE */
						{
							show_handler = 1;
							handler.sendEmptyMessage(0);

						}
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private void InsertAgentInformation(String id) {
					// TODO Auto-generated method stub
					try {
						SoapObject obj = cf.Calling_WS_AgentInformation(id,
								"LoadAgentInformation");
						// SoapObject obj = (SoapObject)
						// chklogin.getProperty(0);
						System.out.println("Iobj" + obj);
						String FirstName = String.valueOf(obj
								.getProperty("FirstName"));
						String LastName = String.valueOf(obj
								.getProperty("LastName"));
						String AgencyName = String.valueOf(obj
								.getProperty("AgencyName"));
						String S_address1 = String.valueOf(obj
								.getProperty("S_address1"));
						String s_address2 = String.valueOf(obj
								.getProperty("s_address2"));
						String s_city = String.valueOf(obj
								.getProperty("s_city"));
						String AgencyState = String.valueOf(obj
								.getProperty("AgencyState"));
						String AgencyCounty = String.valueOf(obj
								.getProperty("AgencyCounty"));
						String s_zip = String.valueOf(obj.getProperty("s_zip"));
						String License = String.valueOf(obj
								.getProperty("License"));
						String AgencyWorkPhone = String.valueOf(obj
								.getProperty("AgencyWorkPhone"));
						String AgencyFaxPhone = String.valueOf(obj
								.getProperty("AgencyFaxPhone"));
						String Email = String.valueOf(obj.getProperty("Email"));
						String CitizenAgencyID = String.valueOf(obj
								.getProperty("CitizenAgencyID"));
						String s_username = String.valueOf(obj
								.getProperty("s_username"));
						String s_password = String.valueOf(obj
								.getProperty("s_password"));
						String AgentStatus = String.valueOf(obj
								.getProperty("RolesId"));
						String RolesDesc = String.valueOf(obj
								.getProperty("RolesDesc"));
						String website = String.valueOf(obj
								.getProperty("Website"));
						String AgencyEmail = String.valueOf(obj
								.getProperty("AgencyEmail"));
						String AgentHeadshot = String.valueOf(obj
								.getProperty("AgentHeadshot"));
						String AgentLogo = String.valueOf(obj
								.getProperty("AgentLogo"));
						String agentid = String.valueOf(obj
								.getProperty("AgentID"));
						String agencyid = String.valueOf(obj
								.getProperty("AgencyID"));
						
						System.out.println("Headshot ext "+AgentHeadshot);
						Bitmap headshot = loadBitmap(AgentHeadshot);
						System.out.println("Bitmap headshot " + headshot);
						System.out.println("Imagelogo ext "+AgentLogo);
						Bitmap agencylogo = loadBitmap(AgentLogo);
						System.out.println("Agencylogo headshot " + agencylogo);

						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						headshot.compress(Bitmap.CompressFormat.PNG, 0, stream);
						byte[] img1_headshot = stream.toByteArray();

						ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
						agencylogo.compress(Bitmap.CompressFormat.PNG, 0,
								stream1);
						byte[] img2_agencylogo = stream1.toByteArray();

						cf.CreateTable(1);
						cf.db.execSQL("insert into "
								+ cf.AgentInformation
								+ " (Agentid,FirstName,LastName,AgencyName,S_address1,s_address2,"
								+ "s_city,AgencyState,AgencyCounty,s_zip,License,AgencyWorkPhone,AgencyFaxPhone,Email,"
								+ "CitizenAgencyID,s_username,s_password,"
								+ "AgentStatus,RolesDesc,website,Img_Headshot,Img_Agencylogo,AgencyEmail,Agent_id_2,Agencyid)"
								+ " values('" + cf.encode(id) + "','"
								+ cf.encode(FirstName) + "','"
								+ cf.encode(LastName) + "','"
								+ cf.encode(AgencyName) + "','"
								+ cf.encode(S_address1) + "','"
								+ cf.encode(s_address2) + "','"
								+ cf.encode(s_city) + "','"
								+ cf.encode(AgencyState) + "','"
								+ cf.encode(AgencyCounty) + "','"
								+ cf.encode(s_zip) + "','" + cf.encode(License)
								+ "','" + cf.encode(AgencyWorkPhone) + "','"
								+ cf.encode(AgencyFaxPhone) + "','"
								+ cf.encode(Email) + "','"
								+ cf.encode(CitizenAgencyID) + "','"
								+ cf.encode(s_username) + "','"
								+ cf.encode(s_password) + "','"
								+ cf.encode(AgentStatus) + "','"
								+ cf.encode(RolesDesc) + "','"
								+ cf.encode(website) + "','" + img1_headshot
								+ "','" + img2_agencylogo + "','"
								+cf.encode(AgencyEmail)+"','"+cf.encode(agentid)+"','"
								+ cf.encode(agencyid) + "');");
						
						try
						{
							URL ulrn = new URL(AgentHeadshot);
						    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
						    InputStream is = con.getInputStream();
						    Bitmap bmp = BitmapFactory.decodeStream(is);
						   
						    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
						    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
						    byte[] b = baos.toByteArray();
						    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
						    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

						    String FILENAME = id +".png";System.out.println("FILENAME "+FILENAME);
							FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
							fos.write(decode);
							fos.close();	
						    
						}
						catch (Exception e1){
							
						}
						
						try
						{
							URL ulrn = new URL(AgentLogo);
						    HttpURLConnection con = (HttpURLConnection)ulrn.openConnection();
						    InputStream is = con.getInputStream();
						    Bitmap bmp = BitmapFactory.decodeStream(is);
						   
						    ByteArrayOutputStream baos = new ByteArrayOutputStream();  
						    bmp.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object   
						    byte[] b = baos.toByteArray();
						    String headshot_insp = Base64.encodeToString(b, Base64.DEFAULT);
						    byte[] decode = Base64.decode(headshot_insp.toString(), 0);

						    String FILENAME = id+"Agency"+".png";System.out.println("FILENAME "+FILENAME);
							FileOutputStream fos = openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
							fos.write(decode);
							fos.close();	
						    
						}
						catch (Exception e1){
							
						}

						

					} catch (Exception e) {// + "');");
						// TODO Auto-generated catch block
						e.printStackTrace();
						// show_handler = 4;
						// handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 1) {
							show_handler = 0;
							toast = new ShowToast(LoginPage.this,
									"Invalid Username or Password");
							autousername.setText("");
							etpassword.setText("");
							autousername.requestFocus();
						} else if (show_handler == 2) {
							show_handler = 0;
							toast = new ShowToast(LoginPage.this,
									"You are not eligible to login");

						} else if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									LoginPage.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									LoginPage.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 12) {
							show_handler = 0;

						}
						else if (show_handler == 5) {
							show_handler = 0;
							
//							cf.CreateTable(0);
//							cf.db.execSQL("insert into " + cf.LoginPage
//									+ " (username,password,agentid)"
//									+ " values('" + cf.encode(username) + "','"
//									+ cf.encode(password) + "','"
//									+ cf.encode(agentid) + "');");

//							cf.pd.dismiss();
							Cursor cur=cf.db.rawQuery("select * from "+cf.AgentInformation, null);
							System.out.println("Agent Information count is "+cur.getCount());
							if(cur.getCount()>=1)
							{
								Intent intent = new Intent(LoginPage.this,
										HomeScreen.class);
								startActivity(intent);
								finish();
								System.gc();
							}
							else
							{
								toast = new ShowToast(
										LoginPage.this,
										"There is problem on your login. Please contact Paperless administrator.");
								
								cf.CreateTable(0);
								cf.db.execSQL("delete from " + cf.LoginPage);
								
							}

						}

					}
				};
			}.start();
		} else {
			toast = new ShowToast(LoginPage.this,
					"Internet connection not available");

		}
	}

	public Bitmap loadBitmap(String src) {

		try {
			URL url = new URL(src);

			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();

			connection.setDoInput(true);
			connection.connect();

			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;

		}

		catch (IOException e) {

			e.printStackTrace();
			return null;

		}
		catch (OutOfMemoryError e) {

			e.printStackTrace();
			return null;

		}
		catch (Exception e) {

			e.printStackTrace();
			return null;

		}
	}

	private void cancel() {
		finish();
	}
}
