package idsoft.agentmodule;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ShowToast {
	private Context con;

	public ShowToast(Context c, String s) {
		this.con = c;
		/*Toast toast = Toast.makeText(c, s, Toast.LENGTH_SHORT);
		TextView tv = (TextView) toast.getView().findViewById(
				android.R.id.message);
		tv.setTextColor(Color.BLACK);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();*/
		Toast toast = Toast.makeText(c, s, Toast.LENGTH_LONG);
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		 View layout = inflater.inflate(R.layout.toast, null);

			TextView tv = (TextView) layout.findViewById(
					R.id.text);
			toast.setGravity(Gravity.CENTER, 0, 0);
			tv.setText(s);
		    toast.setView(layout); toast.show();

	}

}
