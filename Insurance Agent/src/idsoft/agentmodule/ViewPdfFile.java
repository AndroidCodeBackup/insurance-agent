package idsoft.agentmodule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

public class ViewPdfFile extends Activity{
	WebView wv; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewpdffile);
		wv=(WebView)findViewById(R.id.viewpdffile_webView1);
		
		Bundle bundle=getIntent().getExtras();
		String path=bundle.getString("path");
		System.out.println("Path is "+path);
		wv.getSettings().setJavaScriptEnabled(true);
	    wv.getSettings().setPluginsEnabled(true);
	    wv.loadUrl("https://docs.google.com/gview?embedded=true&url="+path);
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		Intent inthome = new Intent(ViewPdfFile.this, OnlineList.class);
//		startActivity(inthome);
		finish();
	}
	
}
