package idsoft.agentmodule;

import java.io.IOException;

import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

public class OrderInspection extends Activity {
	private static final float NaN = 0;
	Button placeorder, home;
	String redcolor = "<font color=red> * </font>", ImageName;
	CheckBox cb[], checkbox[];
	EditText et[];
	ImageView img[];
	int i, j, Cnt, n, width, height,noofbuild,ii;
	boolean rgcheck = false, rgcheck2 = false, call_county2 = true, oncreate,countysetselection=false;
	SoapObject chkaddservice;
	String insptypeid, statevalue = "false",zipidentifier,state,county,city,stateid,countyid;
	String strinspectionbasesq_feet = "0", strinspectionbase_fee = "0",
			strinspectionadditionalsq_feet = "0",
			strinspectionaddtional_fee = "0", strbuildingbasesq_feet = "0",
			strbuildingbase_fee = "0", strbuildingaddsq_feet = "0",
			strbuildingadd_fee = "0", strsquarefootagevalue = "0",
			strtotalvalue = "0", stradditionalinspectionvalue = "0",
			stradditionalbuildingvalue = "0", strdiscountvalue = "0";
	float inspectionbasesq_feet, inspectionbase_fee,
			inspectionadditionalsq_feet, inspectionaddtional_fee,
			buildingbasesq_feet, buildingbase_fee, buildingaddsq_feet,
			buildingadd_fee, squarefootagevalue, totalvalue,
			additionalinspectionvalue, additionalbuildingvalue = 0,
			discountvalue,inspectionbase_fee2,discountvalue2,value=0;
	// lastnametxt.setText(Html.fromHtml(cf.redcolor+" "+"Last Name "));
	Spinner spinneragent, spinneragency, spinnercarrier, spinnerinspectiontype,
			spinnerinspectioncategory, spinnerstate, spinnercounty, spinnerimc,
			spinnerinspector, spinneradditionalbuilding, spinnerstate2,
			spinneraddi, spinnercounty2, spinneryear, spinnerbuildintype,
			spinnerinsurancecarrier;
	RadioButton rbcarrieryes, rbcarrierno, rbadditionalinspectionyes,
			rbadditionalinspectionno, rbadditionalbuildingyes,
			rbadditionalbuildingno, rbinsurancecarrieryes,
			rbinsurancecarrierno;
	RadioGroup rgtime;
	LinearLayout llagent, llgeneralinfo, llcreateanaccount, llspinner,
			llcalculatefees, lladditionalinspecton, llotheryear,
			llnoofbuildings, lladditionalservice, llotherbuildingtype;
	RelativeLayout rladditionalinspecton, rladditionalservice;
	ImageView plus1, plus2, minus1, minus2, descriptionimage;
	String[] arrayid, arraycategory, array_Cat_ID, array_Insp_ID,
			array_InspName, array_Type_ID, arraystateid, arraystatename,
			arraycountyid, arraycountyname, arraycompanyid, arraycompanyname,
			arrayinspectorname, arrayinspectorid, arraystateid2, arr_bType,
			arraystatename2, arraycountyid2, arraycountyname2,
			array_carrier_name, array_carrier_id;
	String yearbuilt[] = { "--Select--", "Other", "2014","2013", "2012", "2011",
			"2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003",
			"2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995",
			"1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987",
			"1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979",
			"1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971",
			"1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963",
			"1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955",
			"1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947",
			"1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939",
			"1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931",
			"1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923",
			"1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915",
			"1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907",
			"1906", "1905", "1904", "1903", "1902", "1901", "1900" };
	TextView description;
	CommonFunctions cf;
	ShowToast toast;
	int show_handler;
	String strinsurancecarrier, stradditionalbuilding, stradditionalinspection,
			strpreferredtime, strinspaddress1, strinspaddress2, strstate,
			strcounty, strcity = "", strzip = "", strmailaddress1 = "",
			strmailaddress2 = "", strstate2, strcounty2, strcity2 = "",
			strzip2 = "", strselect = "--Select--", strcouponcode = "",
			strcompanyname, strinspectorname, strinspectorid, AgentName,
			AgentId, AgencyId, stryear, strtime = "", strcountyid,
			strcountyid2, strstateid, strstateid2, strcompanyid = "0",
			strcarriername, strcarrierid = "0", strbuildingname,
			strinspectioncategoryname, strinspectioncategoryid = "0",
			strinsptypename, stradditionalbuildingid = "",
			stradditionalinspectionid = "", str_building_edittext_value = "",
			strpolicyno = "", strnoofstories, strnoofbuildings, strdate,
			strfirstname, strlastname, strmail, strphone, currentdate;
	CheckBox cbaddresscheck;
	EditText etinspaddress1, etinspaddress2, etcity, etzip, etmailaddress1,
			etmailaddress2, etcity2, etzip2, ettotalfees, etotherbuildingtype,
			etinspectionfees, etadditionalservicefees,
			etadditionalbuildingfees, etdiscount, etsquarefootage,
			etcouponcode, etdate, etnoofstories, etnoofbuildings, etfirstname,
			etlastname, etmail, etphone, etpolicyno, etotheryear;
	EditText[] dynamic_etsquarefootage,dynamic_etnoofstories;
	ArrayAdapter<String> stateadapter, countyadapter, countyadapter2;
	String order_result,Agent_Id2;
	DatePickerDialog datePicker;
	int keyDel;
	int id_len;
	DataBaseHelper dbh;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.placeorder);
		cf = new CommonFunctions(this);
		ImageView iv = (ImageView) findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);

		final Calendar c = Calendar.getInstance();
		final int year1 = c.get(Calendar.YEAR);
		int month1 = c.get(Calendar.MONTH);
		int day1 = c.get(Calendar.DAY_OF_MONTH);
		currentdate = (month1 + 1) + "/" + day1 + "/" + year1;
		System.out.println("The current date is" + currentdate);

		// home = (Button) findViewById(R.id.orderinspection_home);
		placeorder = (Button) findViewById(R.id.orderinsp_btnfreeinspectionquotes);
		spinnercarrier = (Spinner) findViewById(R.id.orderinspection_spinnercarrier);
		spinneradditionalbuilding = (Spinner) findViewById(R.id.orderinspection_spinneradditionalbuilding);
		spinnerinspectiontype = (Spinner) findViewById(R.id.orderinspection_inspectiontype);
		spinnerinspectioncategory = (Spinner) findViewById(R.id.orderinspection_inspectioncategory);
		spinnerstate = (Spinner) findViewById(R.id.orderinspection_spinnerstate);
		spinnercounty = (Spinner) findViewById(R.id.orderinspection_spinnercounty);
		spinnerstate2 = (Spinner) findViewById(R.id.orderinspection_spinnerstate2);
		spinnercounty2 = (Spinner) findViewById(R.id.orderinspection_spinnercounty2);
		spinnerimc = (Spinner) findViewById(R.id.orderinspection_preferredimc);
		spinnerinspector = (Spinner) findViewById(R.id.orderinspection_preferredinspector);
		spinneryear = (Spinner) findViewById(R.id.orderinspection_spinneryear);
		spinnerbuildintype = (Spinner) findViewById(R.id.orderinspection_buildingtype);
		spinnerinsurancecarrier = (Spinner) findViewById(R.id.orderinspection_spinnercarrier);
		rgtime = (RadioGroup) findViewById(R.id.orderinsp_radiogrouptime);
		llgeneralinfo = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutgeneralinfoborder);
		llcreateanaccount = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutcreateanaccountborder);
		llspinner = (LinearLayout) findViewById(R.id.llspinner);
		llcalculatefees = (LinearLayout) findViewById(R.id.orderinsp_llcalculatefees);
		lladditionalinspecton = (LinearLayout) findViewById(R.id.orderinspection_lladditionalinspection);
		rladditionalinspecton = (RelativeLayout) findViewById(R.id.orderinspection_lladditionalinspection1);
		llotherbuildingtype = (LinearLayout) findViewById(R.id.orderinspection_llothetbuildingtype);
		llnoofbuildings = (LinearLayout) findViewById(R.id.orderinspection_linearlayoutnoofbuildings);
		lladditionalservice = (LinearLayout) findViewById(R.id.dynmaicadditonalinsp);
		rladditionalservice = (RelativeLayout) findViewById(R.id.dynmaicadditonalinsp1);
		llotheryear = (LinearLayout) findViewById(R.id.orderinspection_llotheryear);
		plus1 = (ImageView) findViewById(R.id.orderinspection_plus1);
		plus2 = (ImageView) findViewById(R.id.orderinspection_plus2);
		minus1 = (ImageView) findViewById(R.id.orderinspection_minus1);
		minus2 = (ImageView) findViewById(R.id.orderinspection_minus2);
		descriptionimage = (ImageView) findViewById(R.id.orderinspection_descriptionimage);
		description = (TextView) findViewById(R.id.orderinspection_descriptiontext);
		cbaddresscheck = (CheckBox) findViewById(R.id.orderinspection_addresscheck);
		etinspaddress1 = (EditText) findViewById(R.id.orderinspection_etinspectionaddress1);
		etinspaddress2 = (EditText) findViewById(R.id.orderinspection_etinspectionaddress2);
		etcity = (EditText) findViewById(R.id.orderinspection_etcity);
		etzip = (EditText) findViewById(R.id.orderinspection_etzip);
		etmailaddress1 = (EditText) findViewById(R.id.orderinspection_etmailingaddress1);
		etmailaddress2 = (EditText) findViewById(R.id.orderinspection_etmailingaddress2);
		etcity2 = (EditText) findViewById(R.id.orderinspection_etcity2);
		etzip2 = (EditText) findViewById(R.id.orderinspection_etzip2);
		ettotalfees = (EditText) findViewById(R.id.orderinsp_ettotalfees);
		etinspectionfees = (EditText) findViewById(R.id.orderinsp_etinspectionfees);
		etadditionalservicefees = (EditText) findViewById(R.id.orderinsp_etadditionalservicefees);
		etadditionalbuildingfees = (EditText) findViewById(R.id.orderinsp_etadditionalbuildingfees);
		etdiscount = (EditText) findViewById(R.id.orderinsp_etdiscount);
		ettotalfees = (EditText) findViewById(R.id.orderinsp_ettotalfees);
		etsquarefootage = (EditText) findViewById(R.id.orderinsp_etsquarefootage);
		etcouponcode = (EditText) findViewById(R.id.orderinspection_couponcode);
		etdate = (EditText) findViewById(R.id.orderinsp_etinspdate);
		etnoofstories = (EditText) findViewById(R.id.orderinspection_etnoofstories);
		etnoofbuildings = (EditText) findViewById(R.id.orderinspection_noofbuildings);
		etotherbuildingtype = (EditText) findViewById(R.id.orderinspection_etotherbuildingtype);
		etfirstname = (EditText) findViewById(R.id.orderinspection_etfirstanme);
		etlastname = (EditText) findViewById(R.id.orderinspection_etlastname);
		etmail = (EditText) findViewById(R.id.orderinspection_etmail);
		etphone = (EditText) findViewById(R.id.orderinspection_etphone);
		etpolicyno = (EditText) findViewById(R.id.orderinspection_policynumber);
		etotheryear = (EditText) findViewById(R.id.orderinspection_etotheryear);
		rbadditionalinspectionyes = (RadioButton) findViewById(R.id.orderinspection_rbadditionalinspectionyes);
		rbadditionalinspectionno = (RadioButton) findViewById(R.id.orderinspection_rbadditionalinspecionno);
		rbadditionalbuildingyes = (RadioButton) findViewById(R.id.orderinspection_rbadditionalbuildingyes);
		rbadditionalbuildingno = (RadioButton) findViewById(R.id.orderinspection_rbadditionalbuildingno);
		rbinsurancecarrieryes = (RadioButton) findViewById(R.id.orderinspection_rbinsurancecarrieryes);
		rbinsurancecarrierno = (RadioButton) findViewById(R.id.orderinspection_rbinsurancecarrierno);

		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getMetrics(displayMetrics);

		width = displayMetrics.widthPixels;
		height = displayMetrics.heightPixels;

		rgtime.setOnCheckedChangeListener(new check(4));

		cf.CreateTable(1);
		Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			AgentName = cur.getString(cur.getColumnIndex("FirstName"));
			AgentName += " " + cur.getString(cur.getColumnIndex("LastName"));
			AgentId = cur.getString(cur.getColumnIndex("Agentid"));
			Agent_Id2 = cur.getString(cur.getColumnIndex("Agent_id_2"));
			AgencyId = cur.getString(cur.getColumnIndex("Agencyid"));
		}

		LoadCompany();

		spinnerimc.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcompanyname = spinnerimc.getSelectedItem().toString();
				int companyid = spinnerimc.getSelectedItemPosition();
				strcompanyid = arraycompanyid[companyid];
				if (!strcompanyname.equals("--Select--")) {
					LoadInspectors(strcompanyid);
					LoadInspectionType(strinspectioncategoryid, strcompanyid);
				} else {
					spinnerinspector.setAdapter(null);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		try {
			spinnerinspector
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> arg0,
								View arg1, int arg2, long arg3) {
							// TODO Auto-generated method stub
							System.out.println("inside spinner");
							strinspectorname = spinnerinspector
									.getSelectedItem().toString();
							if (strinspectorname.equals("--Select--")) {
								strinspectorid = "0";
							} else {
								int insid = spinnerinspector
										.getSelectedItemPosition();
								strinspectorid = arrayinspectorid[insid];
							}
							// int insid =
							// spinnerinspector.getSelectedItemPosition();
							// strinspectorid = arrayinspectorid[insid];

						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
							// TODO Auto-generated method stub

						}
					});
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception is " + e.getMessage());
		}

		LoadInspectionCategory();
		spinnerinspectioncategory
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						strinspectioncategoryname = spinnerinspectioncategory
								.getSelectedItem().toString();
						int position = spinnerinspectioncategory
								.getSelectedItemPosition();
						strinspectioncategoryid = arrayid[position];
						LoadBuildingType(strinspectioncategoryid);
						LoadInspectionType(strinspectioncategoryid,
								strcompanyid);
						CheckNoofBuildingsEditBox(strinspectioncategoryid,
								strcompanyid);

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		oncreate = true;
		spinnerinspectiontype
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						if (!oncreate) {
							lladditionalinspecton.removeAllViews();
							lladditionalservice.removeAllViews();
							lladditionalinspecton.setVisibility(View.GONE);
							lladditionalservice.setVisibility(View.GONE);
							rladditionalinspecton.setVisibility(View.GONE);
							rladditionalservice.setVisibility(View.GONE);
							etinspectionfees.setText("");
							etadditionalbuildingfees.setText("");
							etadditionalservicefees.setText("");
							etdiscount.setText("");
							ettotalfees.setText("");
							value=0;

							rgcheck = true;
							rgcheck2 = true;
							try {
								System.out.println("Inside radipo try");
								// rgadditionalbuilding.clearCheck();
								if (rgcheck) {
									rbadditionalinspectionyes.setChecked(false);
									rbadditionalinspectionno.setChecked(false);
								}
							} catch (Exception e) {
								System.out.println("Inside radipo catch");
								// TODO: handle exception
							}
							try {
								System.out.println("Inside radipo try");
								// rgadditionalbuilding.clearCheck();
								if (rgcheck2) {
									rbadditionalbuildingyes.setChecked(false);
									rbadditionalbuildingno.setChecked(false);
								}

							} catch (Exception e) {
								System.out.println("Inside radipo catch");
								// TODO: handle exception
							}
						}

						strinsptypename = spinnerinspectiontype
								.getSelectedItem().toString();
						int position = spinnerinspectiontype
								.getSelectedItemPosition();
						insptypeid = array_Insp_ID[position];
						LoadInspectionTypeDescription(insptypeid, strcompanyid);
						CheckNoofBuildingsEditBox(strinspectioncategoryid,
								strcompanyid);
						oncreate = false;

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		statevalue = LoadState();

		if (statevalue == "true") {
			stateadapter = new ArrayAdapter<String>(OrderInspection.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate.setAdapter(stateadapter);

			stateadapter = new ArrayAdapter<String>(OrderInspection.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate2.setAdapter(stateadapter);
		}

		spinnerstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate = spinnerstate.getSelectedItem().toString();
				int stateid = spinnerstate.getSelectedItemPosition();
				strstateid = arraystateid[stateid];
				if (!strstate.equals("--Select--")) {
					LoadCounty(strstateid);
					spinnercounty.setEnabled(true);
					
				} else {
					System.out.println("inside spinner state else");
					// spinnercounty.setAdapter(null);
					spinnercounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(
							OrderInspection.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercounty.setAdapter(countyadapter);
					
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnerstate2.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				call_county2 = true;
				return false;
			}
		});

		spinnerstate2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate2 = spinnerstate2.getSelectedItem().toString();
				int stateid = spinnerstate2.getSelectedItemPosition();
				strstateid2 = arraystateid[stateid];
				if (call_county2 == true) {
					if (!strstate2.equals("--Select--")) {
						LoadCounty2(strstateid2);
						spinnercounty2.setEnabled(true);

					} else {
						// spinnercounty2.setAdapter(null);
						spinnercounty2.setEnabled(false);
						arraycountyname2 = new String[0];
						countyadapter2 = new ArrayAdapter<String>(
								OrderInspection.this,
								android.R.layout.simple_spinner_item,
								arraycountyname2);
						countyadapter2
								.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
						spinnercounty2.setAdapter(countyadapter2);
					}
				} else {
					spinnercounty2.setEnabled(true);
					spinnercounty2.setSelection(spinnercounty
							.getSelectedItemPosition());
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnercounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty = spinnercounty.getSelectedItem().toString();
				int countyid = spinnercounty.getSelectedItemPosition();
				strcountyid = arraycountyid[countyid];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnercounty2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty2 = spinnercounty2.getSelectedItem().toString();
				int countyid = spinnercounty2.getSelectedItemPosition();
				strcountyid2 = arraycountyid2[countyid];

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		spinnerinsurancecarrier
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						strcarriername = spinnerinsurancecarrier
								.getSelectedItem().toString();
						int position = spinnerinsurancecarrier
								.getSelectedItemPosition();
						strcarrierid = array_carrier_id[position];

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		spinnerbuildintype
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						strbuildingname = spinnerbuildintype.getSelectedItem()
								.toString();
						if (strbuildingname.equals("Other")) {
							llotherbuildingtype.setVisibility(View.VISIBLE);
						} else {
							llotherbuildingtype.setVisibility(View.GONE);
						}

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		spinneryear.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				stryear = spinneryear.getSelectedItem().toString();
				if (stryear.equals("Other")) {
					llotheryear.setVisibility(View.VISIBLE);
				} else {
					llotheryear.setVisibility(View.GONE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		ArrayAdapter<String> yearadapter = new ArrayAdapter<String>(
				OrderInspection.this, android.R.layout.simple_spinner_item,
				yearbuilt);
		yearadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinneryear.setAdapter(yearadapter);

		cbaddresscheck
				.setOnCheckedChangeListener(new myCheckBoxChnageClicker());
		// etsquarefootage.addTextChangedListener(new
		// CustomTextWatcher(etsquarefootage));
		etcouponcode
				.addTextChangedListener(new CustomTextWatcher(etcouponcode));
		etpolicyno.addTextChangedListener(new CustomTextWatcher(etpolicyno));
		etnoofstories.addTextChangedListener(new CustomTextWatcher(
				etnoofstories));
		// etnoofbuildings.addTextChangedListener(new
		// CustomTextWatcher(etnoofbuildings));
		etfirstname.addTextChangedListener(new CustomTextWatcher(etfirstname));
		etlastname.addTextChangedListener(new CustomTextWatcher(etlastname));
		etmail.addTextChangedListener(new CustomTextWatcher(etmail));
		// etphone.addTextChangedListener(new CustomTextWatcher(etphone));
		etinspaddress1.addTextChangedListener(new CustomTextWatcher(
				etinspaddress1));
		etinspaddress2.addTextChangedListener(new CustomTextWatcher(
				etinspaddress2));
		etcity.addTextChangedListener(new CustomTextWatcher(etcity));
//		etzip.addTextChangedListener(new CustomTextWatcher(etzip));
		etmailaddress1.addTextChangedListener(new CustomTextWatcher(
				etmailaddress1));
		etmailaddress2.addTextChangedListener(new CustomTextWatcher(
				etmailaddress2));
		etcity2.addTextChangedListener(new CustomTextWatcher(etcity2));
//		etzip2.addTextChangedListener(new CustomTextWatcher(etzip2));
		etotherbuildingtype.addTextChangedListener(new CustomTextWatcher(
				etotherbuildingtype));

		etotheryear.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (etotheryear.getText().toString().trim().matches("^0")) {
					// Not allowed
					etotheryear.setText("");
				}
				if (!etotheryear.getText().toString().trim().equals("")) {
					if (Integer.parseInt((etotheryear.getText().toString()
							.trim())) > year1) {
						etotheryear.setText("");
						toast = new ShowToast(OrderInspection.this,
								"Please enter a valid year");
					}
				}
				if (Arrays.asList(yearbuilt).contains(
						etotheryear.getText().toString().trim())) {
					etotheryear.setText("");
					toast = new ShowToast(OrderInspection.this,
							"Please enter a year that is not in the year list");
				}

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void afterTextChanged(Editable s) {
			}
		});
		
		etphone.setLongClickable(false);

		etphone.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etphone.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etphone.setText("");
		        }
				if (etphone.getText().toString().trim().matches("^0") )
	            {
	                // Not allowed
					etphone.setText("");
	            }

				etphone.setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						// TODO Auto-generated method stub
						if (keyCode == KeyEvent.KEYCODE_DEL)
						{
							keyDel = 1;
							System.out.println("Inside delete");
							
							String str = etphone.getText().toString();
							int len=str.length();
							
							if(len==9)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							if(len==5)
							{
//								str = str.substring(0, str.length()-2);
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							if(len==1)
							{
								str = str.substring(0, str.length()-1);
								etphone.setText(str);
								etphone.setSelection(str.length());
							}
							
							return false;
						}
						else
						{
							keyDel = 0;
							return false;
						}
					}
				});

				if (keyDel == 0) {
					String a = "";
					String str = etphone.getText().toString();
					String replaced = str.replaceAll(Pattern.quote("("), "");
					replaced = replaced.replaceAll(Pattern.quote("-"), "");
					replaced = replaced.replaceAll(Pattern.quote(")"), "");
//					replaced = replaced.replaceAll(Pattern.quote(" "), "");
					char[] id_char = replaced.toCharArray();
					id_len = replaced.length();
					System.out.println("The length is "+id_len);
					for (int i = 0; i < id_len; i++) {
						if (i == 0) {
							a = "(" + id_char[i];
						} else if (i == 2) {
//							a += id_char[i] + ") ";
							a += id_char[i] + ")";
						} else if (i == 5) {
							a += id_char[i] + "-";
						} else
							a += id_char[i];
							keyDel = 0;
					}
					etphone.removeTextChangedListener(this);
					etphone.setText(a);
					if (before > 0)
						etphone.setSelection(start);
					else
						etphone.setSelection(a.length());
					etphone.addTextChangedListener(this);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		etsquarefootage.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etsquarefootage.getText().toString().startsWith(" ")) {
					// Not allowed
					etsquarefootage.setText("");
				}
				if (etsquarefootage.getText().toString().trim().matches("^0")) {
					// Not allowed
					etsquarefootage.setText("");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		etnoofbuildings.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etnoofbuildings.getText().toString().startsWith(" ")) {
					// Not allowed
					etnoofbuildings.setText("");
				}
				if (etnoofbuildings.getText().toString().trim().matches("^0")) {
					// Not allowed
					etnoofbuildings.setText("");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (!etnoofbuildings.getText().toString().equals("")) {
					noofbuild = Integer.parseInt(etnoofbuildings.getText().toString());
					if (noofbuild > 1) {
						Dynamic_Noofbuildings();
						llnoofbuildings.setVisibility(View.VISIBLE);
						etsquarefootage.setBackgroundResource(R.drawable.edittextborder);
						etnoofstories.setBackgroundResource(R.drawable.edittextborder);
						etsquarefootage.setText("");
						etnoofstories.setText("");
						etsquarefootage.setEnabled(false);
						etnoofstories.setEnabled(false);
						etinspectionfees.setText("");
						etadditionalbuildingfees.setText("");
						etadditionalservicefees.setText("");
						etdiscount.setText("");
						ettotalfees.setText("");
						
					}
					else
					{
						llnoofbuildings.removeAllViews();
						llnoofbuildings.setVisibility(View.GONE);
						etsquarefootage.setBackgroundResource(R.drawable.editbox);
						etnoofstories.setBackgroundResource(R.drawable.editbox);
//						etsquarefootage.setText("");
						etnoofstories.setText("1");
						etsquarefootage.setEnabled(true);
						etnoofstories.setEnabled(true);
						etinspectionfees.setText("");
						etadditionalbuildingfees.setText("");
						etadditionalservicefees.setText("");
						etdiscount.setText("");
						ettotalfees.setText("");
					}
				}
				else
				{
					llnoofbuildings.removeAllViews();
					llnoofbuildings.setVisibility(View.GONE);
					etsquarefootage.setBackgroundResource(R.drawable.editbox);
					etnoofstories.setBackgroundResource(R.drawable.editbox);
					etsquarefootage.setText("");
					etnoofstories.setText("1");
					etsquarefootage.setEnabled(true);
					etnoofstories.setEnabled(true);
					etinspectionfees.setText("");
					etadditionalbuildingfees.setText("");
					etadditionalservicefees.setText("");
					etdiscount.setText("");
					ettotalfees.setText("");
				}
			}
		});

		etotheryear.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etotheryear.getText().toString().trim().length() >= 1) {
						if (etotheryear.getText().toString().trim().length() < 4) {
							etotheryear.setText("");
							toast = new ShowToast(OrderInspection.this,
									"Please enter a valid year");
						}
					}
				}
			}
		});

		etmail.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etmail.getText().toString().trim().length() >= 1) {
						if (!cf.eMailValidation(etmail.getText().toString()
								.trim())) {
							etmail.setText("");
							toast = new ShowToast(OrderInspection.this,
									"Please enter a valid Email");
						}
					}
				}
			}
		});
		
		etcouponcode.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (!hasFocus) {
					if (etcouponcode.getText().toString().trim().length() >= 1) {
						Check_Couponcode();
					}
				}
			}
		});
		
		etzip.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(etzip.getText().toString().trim().length()==5)
				{
					spinnerstate.setSelection(0);
					zipidentifier="zip1";
					Load_State_County_City(etzip);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		etzip2.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etzip2.getText().toString().trim().length() == 5) {
					spinnerstate2.setSelection(0);
					zipidentifier = "zip2";
					Load_State_County_City(etzip2);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		// TextWatcher textWatcher = new TextWatcher() {
		//
		// @Override
		// public void beforeTextChanged(CharSequence charSequence, int i, int
		// i1, int i2) {
		//
		// }
		//
		// @Override
		// public void onTextChanged(CharSequence charSequence, int i, int i1,
		// int i2) {
		//
		// }
		//
		// @Override
		// public void afterTextChanged(Editable editable) {
		// //here, after we introduced something in the EditText we get the
		// string from it
		// if(!etsquarefootage.getText().toString().trim().equals(""))
		// {
		// Calculate_discount();
		// }
		//
		//
		// }
		// };
		//
		// etsquarefootage.addTextChangedListener(textWatcher);

	}

	private void Dynamic_Noofbuildings() {
		llnoofbuildings.setVisibility(View.VISIBLE);
		llnoofbuildings.removeAllViews();
		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getMetrics(displayMetrics);

		int width = displayMetrics.widthPixels;
		int height = displayMetrics.heightPixels;

		System.out.println("The width is " + width);
		System.out.println("The height is " + height);
		noofbuild = Integer.parseInt(etnoofbuildings.getText().toString());
		dynamic_etsquarefootage = new EditText[noofbuild];
		dynamic_etnoofstories = new EditText[noofbuild];
		LinearLayout.LayoutParams llparams = null, llparams1 = null, 
				llparams2 = null, llparamssquarefootagetitle = null, 
				llparamscolon = null, llparamedittext = null;
		System.out.println("The no is " + noofbuild);
		for (ii = 0; ii < noofbuild; ii++) {
			if (width > 1023 || height > 1023) {

				llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams.setMargins(10, 10, 10, 10);

				llparams1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams1.gravity = Gravity.CENTER_VERTICAL;

				llparams2 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams2.gravity = Gravity.CENTER_VERTICAL;
				llparams2.setMargins(35, 0, 0, 0);

				llparamssquarefootagetitle = new LinearLayout.LayoutParams(150,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamssquarefootagetitle.setMargins(5, 0, 0, 0);
				llparamssquarefootagetitle.gravity = Gravity.CENTER_VERTICAL;

				llparamscolon = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamscolon.gravity = Gravity.CENTER_VERTICAL;

				llparamedittext = new LinearLayout.LayoutParams(250,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamedittext.setMargins(20, 0, 0, 0);
				llparamedittext.gravity = Gravity.CENTER_VERTICAL;
			}
			else if((width > 500 && width<1000) || (height > 500 && height<1000))
			{
				llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams.setMargins(10, 10, 10, 10);

				llparams1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams1.gravity = Gravity.CENTER_VERTICAL;

				llparams2 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams2.gravity = Gravity.CENTER_VERTICAL;
				llparams2.setMargins(20, 0, 0, 0);

				llparamssquarefootagetitle = new LinearLayout.LayoutParams(80,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamssquarefootagetitle.setMargins(5, 0, 0, 0);
				llparamssquarefootagetitle.gravity = Gravity.CENTER_VERTICAL;

				llparamscolon = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamscolon.gravity = Gravity.CENTER_VERTICAL;

				llparamedittext = new LinearLayout.LayoutParams(100,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamedittext.setMargins(20, 0, 0, 0);
				llparamedittext.gravity = Gravity.CENTER_VERTICAL;
			}
			else
			{
				llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams.setMargins(10, 10, 10, 10);

				llparams1 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams1.gravity = Gravity.CENTER_VERTICAL;

				llparams2 = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparams2.gravity = Gravity.CENTER_VERTICAL;
				llparams2.setMargins(10, 0, 0, 0);

				llparamssquarefootagetitle = new LinearLayout.LayoutParams(60,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamssquarefootagetitle.setMargins(5, 0, 0, 0);
				llparamssquarefootagetitle.gravity = Gravity.CENTER_VERTICAL;

				llparamscolon = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamscolon.gravity = Gravity.CENTER_VERTICAL;

				llparamedittext = new LinearLayout.LayoutParams(100,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				llparamedittext.setMargins(20, 0, 0, 0);
				llparamedittext.gravity = Gravity.CENTER_VERTICAL;
			}

			LinearLayout llhor = new LinearLayout(this);
			llhor.setLayoutParams(llparams);
			llhor.setOrientation(LinearLayout.HORIZONTAL);
			llnoofbuildings.addView(llhor);

			LinearLayout llsquarefootage = new LinearLayout(this);
			llsquarefootage.setLayoutParams(llparams1);
			llsquarefootage.setOrientation(LinearLayout.HORIZONTAL);
			llhor.addView(llsquarefootage);

			TextView tvsquarefootagestar = new TextView(this);
			tvsquarefootagestar.setLayoutParams(llparams1);
			tvsquarefootagestar.setText("*");
			tvsquarefootagestar.setTextColor(0xffff0000);
			llsquarefootage.addView(tvsquarefootagestar);

			TextView tvsquareefootage = new TextView(this);
			tvsquareefootage.setLayoutParams(llparamssquarefootagetitle);
			tvsquareefootage.setText("Square Footage");
			tvsquareefootage.setTextColor(0xffcbddeb);
			llsquarefootage.addView(tvsquareefootage);

			TextView tvsquareefootagecolon = new TextView(this);
			tvsquareefootagecolon.setLayoutParams(llparamscolon);
			tvsquareefootagecolon.setText(":");
			tvsquareefootagecolon.setTextSize(14);
			tvsquareefootagecolon.setTextColor(0xffcbddeb);
			llsquarefootage.addView(tvsquareefootagecolon);

			dynamic_etsquarefootage[ii] = new EditText(this);
			dynamic_etsquarefootage[ii].setLayoutParams(llparamedittext);
			dynamic_etsquarefootage[ii].setPadding(5, 0, 0, 0);
			dynamic_etsquarefootage[ii]
					.setBackgroundResource(R.drawable.editbox);
			dynamic_etsquarefootage[ii].setMinHeight(50);
			dynamic_etsquarefootage[ii].setKeyListener(DigitsKeyListener
					.getInstance("0123456789"));
			int maxLength = 5;
			InputFilter[] fArray = new InputFilter[1];
			fArray[0] = new InputFilter.LengthFilter(maxLength);
			dynamic_etsquarefootage[ii].setFilters(fArray);
			llsquarefootage.addView(dynamic_etsquarefootage[ii]);

			LinearLayout llnoofstories = new LinearLayout(this);
			llnoofstories.setLayoutParams(llparams2);
			llnoofstories.setOrientation(LinearLayout.HORIZONTAL);
			llhor.addView(llnoofstories);

			TextView tvnoofstoriesstar = new TextView(this);
			tvnoofstoriesstar.setLayoutParams(llparams1);
			tvnoofstoriesstar.setText("*");
			tvnoofstoriesstar.setTextColor(0xffff0000);
			llnoofstories.addView(tvnoofstoriesstar);

			TextView tvnoofstories = new TextView(this);
			tvnoofstories.setLayoutParams(llparamssquarefootagetitle);
			tvnoofstories.setText("No.Of Stories");
			tvnoofstories.setTextColor(0xffcbddeb);
			llnoofstories.addView(tvnoofstories);

			TextView tvnoofstoriescolon = new TextView(this);
			tvnoofstoriescolon.setLayoutParams(llparamscolon);
			tvnoofstoriescolon.setText(":");
			tvnoofstoriescolon.setTextSize(14);
			tvnoofstoriescolon.setTextColor(0xffcbddeb);
			llnoofstories.addView(tvnoofstoriescolon);

			dynamic_etnoofstories[ii] = new EditText(this);
			dynamic_etnoofstories[ii].setLayoutParams(llparamedittext);
			dynamic_etnoofstories[ii].setPadding(5, 0, 0, 0);
			dynamic_etnoofstories[ii].setBackgroundResource(R.drawable.editbox);
			dynamic_etnoofstories[ii].setMinHeight(50);
			dynamic_etnoofstories[ii].setKeyListener(DigitsKeyListener
					.getInstance("0123456789"));
			int maxLength1 = 5;
			InputFilter[] fArray1 = new InputFilter[1];
			fArray1[0] = new InputFilter.LengthFilter(maxLength1);
			dynamic_etnoofstories[ii].setFilters(fArray1);
			llnoofstories.addView(dynamic_etnoofstories[ii]);

			dynamic_etsquarefootage[ii]
					.addTextChangedListener(new TextWatcher() {

						@Override
						public void onTextChanged(CharSequence s, int start,
								int before, int count) {
							// TODO Auto-generated method stub
							System.out.println("ii value is " + ii);
							if (dynamic_etsquarefootage[ii].getText()
									.toString().startsWith(" ")) {
								// Not allowed
								dynamic_etsquarefootage[ii].setText("");
							}
							if (dynamic_etsquarefootage[ii].getText()
									.toString().trim().matches("^0")) {
								// Not allowed
								dynamic_etsquarefootage[ii].setText("");
							}
						}

						@Override
						public void beforeTextChanged(CharSequence s,
								int start, int count, int after) {
							// TODO Auto-generated method stub

						}

						@Override
						public void afterTextChanged(Editable s) {
							// TODO Auto-generated method stub

						}
					});

			dynamic_etnoofstories[ii].addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					// TODO Auto-generated method stub
					if (dynamic_etnoofstories[ii].getText().toString()
							.startsWith(" ")) {
						// Not allowed
						dynamic_etnoofstories[ii].setText("");
					}
					if (dynamic_etnoofstories[ii].getText().toString().trim()
							.matches("^0")) {
						// Not allowed
						dynamic_etnoofstories[ii].setText("");
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			if (ii == noofbuild - 1) {
				ii = 0;
				break;
			}
		}
	}

	private void CheckNoofBuildingsEditBox(String categoryid, String companyid) {
		
//		For Multiple buildings(i.e) for Commercial Insurance
		
//		System.out.println("category id " + categoryid);
//		System.out.println("inspid " + insptypeid);
//		if (categoryid.equals("2")) {
//			if (insptypeid.equals("9")) {
//				etnoofbuildings.setEnabled(true);
//				etnoofbuildings.setBackgroundResource(R.drawable.editbox);
//				etnoofbuildings.setText("1");
//			}
//			// else if(insptypeid.equals("10"))
//			// {
//			// etnoofbuildings.setEnabled(true);
//			// etnoofbuildings.setBackgroundResource(R.drawable.editbox);
//			// etnoofbuildings.setText("1");
//			// }
//			else {
//				etnoofbuildings.setEnabled(false);
//				etnoofbuildings
//						.setBackgroundResource(R.drawable.edittextborder);
//				etnoofbuildings.setText("1");
//			}
//		} else {
//			etnoofbuildings.setEnabled(false);
//			etnoofbuildings.setBackgroundResource(R.drawable.edittextborder);
//			etnoofbuildings.setText("1");
//			
//		}
		
//		End of For Multiple buildings(i.e) for Commercial Insurance
		
		etnoofbuildings.setEnabled(false);
		etnoofbuildings
				.setBackgroundResource(R.drawable.edittextborder);
		etnoofbuildings.setText("1");
		
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.orderinspection_plus1:
			System.out.println("plus1");
			llgeneralinfo.setVisibility(View.VISIBLE);
			plus1.setVisibility(View.GONE);
			minus1.setVisibility(View.VISIBLE);
			break;

		case R.id.orderinspection_minus1:
			System.out.println("minus1");
			llgeneralinfo.setVisibility(View.GONE);
			plus1.setVisibility(View.VISIBLE);
			minus1.setVisibility(View.GONE);
			break;

		case R.id.orderinspection_plus2:
			System.out.println("plus2");
			llcreateanaccount.setVisibility(View.VISIBLE);
			plus2.setVisibility(View.GONE);
			minus2.setVisibility(View.VISIBLE);
			break;

		case R.id.orderinspection_minus2:
			System.out.println("minus2");
			llcreateanaccount.setVisibility(View.GONE);
			plus2.setVisibility(View.VISIBLE);
			minus2.setVisibility(View.GONE);
			break;

		case R.id.orderinspection_home:
			Intent inthome = new Intent(OrderInspection.this, HomeScreen.class);
			startActivity(inthome);
			finish();
			break;

		case R.id.orderinsp_btncalculatefees:
//			if (etsquarefootage.getText().toString().trim().equals("")) {
//				toast = new ShowToast(OrderInspection.this,
//						"Please enter Square Footage");
//				etsquarefootage.requestFocus();
//			} else {
//				if (strcompanyname.equals("--Select--")) {
//					toast = new ShowToast(OrderInspection.this,
//							"Please select Preferred IMC");
//				} else {
					inspectionbasesq_feet = 0;
					inspectionbase_fee = 0;
					inspectionadditionalsq_feet = 0;
					inspectionaddtional_fee = 0;
					buildingbasesq_feet = 0;
					buildingbase_fee = 0;
					buildingaddsq_feet = 0;
					buildingadd_fee = 0;
					discountvalue2 = 0;
					Calculate_Fees();

//				}
//			}

			break;

		case R.id.orderinsp_btnfreeinspectionquotes:
			String identifier = "";
			if (lladditionalinspecton.getVisibility() == View.VISIBLE) {
				for (i = 0; i < Cnt; i++) {

					if (checkbox[i].isChecked()) {
						identifier += "true";
					} else {
						identifier += "false";
					}
				}
				if (identifier.contains("true")) {
					for (j = 0; j < Cnt; j++) {

						if (checkbox[j].isChecked()
								&& et[j].getText().toString().trim().equals("")) {
							System.out.println("Inside if");
							toast = new ShowToast(OrderInspection.this,
									"Please enter Square Footage");
							et[j].requestFocus();
//							j = Cnt;
							break;
						} else {
							if (j == Cnt - 1) {
								System.out.println("Inside else");
								Check_Validation();
							}
						}
					}
				} else {
					Check_Validation();
				}
			} else {
				Check_Validation();
			}

			// Check_Validation();

			break;

		case R.id.orderinsp_getdate:
			showDialogDate(etdate);
			// showDialog(0);
			break;

		case R.id.orderinspection_rbadditionalinspectionyes:
			rbadditionalinspectionyes.setChecked(true);
			rbadditionalinspectionno.setChecked(false);
			lladditionalservice.setVisibility(View.VISIBLE);
			lladditionalservice.removeAllViews();
			LoadingAdditionalService();
			break;

		case R.id.orderinspection_rbadditionalinspecionno:
			rbadditionalinspectionyes.setChecked(false);
			rbadditionalinspectionno.setChecked(true);
			lladditionalservice.setVisibility(View.GONE);
			rladditionalservice.setVisibility(View.GONE);
			lladditionalservice.removeAllViews();
			etadditionalservicefees.setText("");
			break;

		case R.id.orderinspection_rbadditionalbuildingyes:
			rbadditionalbuildingyes.setChecked(true);
			rbadditionalbuildingno.setChecked(false);
			lladditionalinspecton.setVisibility(View.VISIBLE);
			lladditionalinspecton.removeAllViews();
			String value = spinnerinspectiontype.getSelectedItem().toString();
			int position = spinnerinspectiontype.getSelectedItemPosition();
			String id = array_Insp_ID[position];
			System.out.println("id is " + id);
			LoadAdditionalBuilding(id);
			break;

		case R.id.orderinspection_rbadditionalbuildingno:
			rbadditionalbuildingyes.setChecked(false);
			rbadditionalbuildingno.setChecked(true);
			lladditionalinspecton.setVisibility(View.GONE);
			rladditionalinspecton.setVisibility(View.GONE);
			lladditionalinspecton.removeAllViews();
			strbuildingbasesq_feet = null;
			strbuildingbase_fee = null;
			strbuildingaddsq_feet = null;
			strbuildingadd_fee = null;
			etadditionalbuildingfees.setText("");
			break;

		case R.id.orderinspection_rbinsurancecarrieryes:
			rbinsurancecarrieryes.setChecked(true);
			rbinsurancecarrierno.setChecked(false);
			strinsurancecarrier = rbinsurancecarrieryes.getText().toString()
					.trim();
			spinnercarrier.setVisibility(View.VISIBLE);
			LoadInsuranceCarrier();
			break;

		case R.id.orderinspection_rbinsurancecarrierno:
			rbinsurancecarrieryes.setChecked(false);
			rbinsurancecarrierno.setChecked(true);
			spinnercarrier.setVisibility(View.GONE);
			strcarrierid = "0";
			break;

		}
	}

	private void Check_Validation() {
		boolean mailvalidation, phonevalidation;

		if (spinneryear.getSelectedItem().toString().equals("Other")) {
			stryear = etotheryear.getText().toString().trim();
		} else {
			stryear = spinneryear.getSelectedItem().toString();
		}
		System.out.println("The year is " + stryear);

		if (etmail.getText().toString().trim().equals("")) {
			mailvalidation = true;
		} else {
			if (cf.eMailValidation(etmail.getText().toString().trim())) {
				mailvalidation = true;
			} else {
				mailvalidation = false;
			}
		}

		if (etphone.getText().toString().trim().equals("")) {
			phonevalidation = true;
		} else {
			if (etphone.getText().toString().trim().length() < 13) {
				phonevalidation = false;
			} else {
				phonevalidation = true;
			}
		}
		
		
		boolean boolnoofbuildings = true;
		if(llnoofbuildings.getVisibility()==View.VISIBLE)
		{
			for (int ii = 0; ii < noofbuild; ii++) {
				if (dynamic_etsquarefootage[ii].getText().toString()
						.equals("")) {
					boolnoofbuildings=false;
					break;
				}  else {
					if (dynamic_etnoofstories[ii].getText().toString()
							.equals("")) {
						boolnoofbuildings=false;
						break;
					} else {
						if (ii == noofbuild - 1) {
							boolnoofbuildings=true;
						}
				}
				}
			}
		}
		else
		{
			if(etnoofbuildings.getText().toString().trim().equals(""))
			{
				boolnoofbuildings=false;
			}
			else
			{
				boolnoofbuildings=true;
			}
		}
		
		
		boolean boolnoofstories=true;
		if(llnoofbuildings.getVisibility()==View.VISIBLE)
		{
			boolnoofstories=true;
		}
		else
		{
			if(etnoofstories.getText().toString().trim().equals(""))
			{
				boolnoofstories=false;
			}
			else
			{
				boolnoofstories=true;
			}
		}
		
		
		boolean boolsquarefootage=true;
		if(llnoofbuildings.getVisibility()==View.VISIBLE)
		{
			boolsquarefootage=true;
		}
		else
		{
			if(etsquarefootage.getText().toString().trim().equals(""))
			{
				boolsquarefootage=false;
			}
			else
			{
				int val=Integer.parseInt(etsquarefootage.getText().toString().trim());
				if(val<399||val>100000)
				{
					boolsquarefootage=false;
				}
				else
				{
					boolsquarefootage=true;
				}
			}
		}
		

		if (boolsquarefootage) {
			if (!spinneryear.getSelectedItem().toString().equals("--Select--")) {
				if (!stryear.equals("")) {
					if (boolnoofstories) {
						if (!etnoofbuildings.getText().toString().trim()
								.equals("")) {
							if(boolnoofbuildings)
							{
							if (!strcompanyname.equals("--Select--")) {
								if (!etdate.getText().toString().trim()
										.equals("")) {
									if (!strtime.equals("")) {
										if (!ettotalfees.getText().toString()
												.trim().equals("")) {
											if (!etfirstname.getText()
													.toString().trim()
													.equals("")) {
												if (!etlastname.getText()
														.toString().trim()
														.equals("")) {
													if (mailvalidation) {
														if (phonevalidation) {
															if (!etinspaddress1
																	.getText()
																	.toString()
																	.trim()
																	.equals("")) {
																
																		
																			if (!etzip
																					.getText()
																					.toString()
																					.trim()
																					.equals("")) {
																				if ((etzip
																						.getText()
																						.toString()
																						.length() == 5)) {
																					if ((!etzip
																							.getText()
																							.toString()
																							.contains(
																									"00000"))) {
																						if (!etcity
																								.getText()
																								.toString()
																								.trim()
																								.equals("")) {
																							if (!strstate
																									.equals("--Select--")) {
																								if (!strcounty
																										.equals("--Select--")) {
																						if (!etmailaddress1
																								.getText()
																								.toString()
																								.trim()
																								.equals("")) {
																							
																									
																										if (!etzip2
																												.getText()
																												.toString()
																												.trim()
																												.equals("")) {
																											if ((etzip2
																													.getText()
																													.toString()
																													.length() == 5)) {
																												if ((!etzip2
																														.getText()
																														.toString()
																														.contains(
																																"00000"))) {
																													if (!etcity2
																															.getText()
																															.toString()
																															.trim()
																															.equals("")) {
																														if (!strstate2
																																.equals("--Select--")) {
																															if (!strcounty2
																																	.equals("--Select--")) {
																													Place_Order();
																													
																															} else {
																																toast = new ShowToast(
																																		OrderInspection.this,
																																		"Please select County under Mailing Address");
																																llcreateanaccount
																																		.setVisibility(View.VISIBLE);
																																plus2.setVisibility(View.GONE);
																																minus2.setVisibility(View.VISIBLE);
																															}
																														} else {
																															toast = new ShowToast(
																																	OrderInspection.this,
																																	"Please select State under Mailing Address");
																															llcreateanaccount
																																	.setVisibility(View.VISIBLE);
																															plus2.setVisibility(View.GONE);
																															minus2.setVisibility(View.VISIBLE);

																														}
																															} else {
																																toast = new ShowToast(
																																		OrderInspection.this,
																																		"Please Enter City under Mailing Address");
																																etcity2.requestFocus();
																																etcity2.setText("");
																																llcreateanaccount
																																		.setVisibility(View.VISIBLE);
																																plus2.setVisibility(View.GONE);
																																minus2.setVisibility(View.VISIBLE);
																															}
																												} else {
																													toast = new ShowToast(
																															OrderInspection.this,
																															"Please enter a valid Zip under Mailing Address");
																													etzip2.requestFocus();
																													etzip2.setText("");
																													llcreateanaccount
																															.setVisibility(View.VISIBLE);
																													plus2.setVisibility(View.GONE);
																													minus2.setVisibility(View.VISIBLE);
																												}
																											} else {
																												toast = new ShowToast(
																														OrderInspection.this,
																														"Zip should be 5 characters under Mailing Address");
																												etzip2.requestFocus();
																												etzip2.setText("");
																												llcreateanaccount
																														.setVisibility(View.VISIBLE);
																												plus2.setVisibility(View.GONE);
																												minus2.setVisibility(View.VISIBLE);
																											}
																										} else {
																											toast = new ShowToast(
																													OrderInspection.this,
																													"Please enter Zip under Mailing Address");
																											etzip2.requestFocus();
																											etzip2.setText("");
																											llcreateanaccount
																													.setVisibility(View.VISIBLE);
																											plus2.setVisibility(View.GONE);
																											minus2.setVisibility(View.VISIBLE);
																										}
																									
																								
																						} else {
																							toast = new ShowToast(
																									OrderInspection.this,
																									"Please enter Mailing Address1");
																							etmailaddress1
																									.requestFocus();
																							etmailaddress1
																									.setText("");
																							llcreateanaccount
																									.setVisibility(View.VISIBLE);
																							plus2.setVisibility(View.GONE);
																							minus2.setVisibility(View.VISIBLE);
																						}
																						// }
																								} else {
																									toast = new ShowToast(
																											OrderInspection.this,
																											"Please select County under Inspection Address");
																									llcreateanaccount
																											.setVisibility(View.VISIBLE);
																									plus2.setVisibility(View.GONE);
																									minus2.setVisibility(View.VISIBLE);
																								}
																							} else {
																								toast = new ShowToast(
																										OrderInspection.this,
																										"Please select State under Inspection Address");
																								llcreateanaccount
																										.setVisibility(View.VISIBLE);
																								plus2.setVisibility(View.GONE);
																								minus2.setVisibility(View.VISIBLE);
																							}
																								} else {
																									toast = new ShowToast(
																											OrderInspection.this,
																											"Please enter City under Inspection Address");
																									etcity.requestFocus();
																									etcity.setText("");
																									llcreateanaccount
																											.setVisibility(View.VISIBLE);
																									plus2.setVisibility(View.GONE);
																									minus2.setVisibility(View.VISIBLE);
																								}
																					} else {
																						toast = new ShowToast(
																								OrderInspection.this,
																								"Please enter a valid Zip under Inspection Address");
																						etzip.requestFocus();
																						etzip.setText("");
																						llcreateanaccount
																								.setVisibility(View.VISIBLE);
																						plus2.setVisibility(View.GONE);
																						minus2.setVisibility(View.VISIBLE);
																					}
																				} else {
																					toast = new ShowToast(
																							OrderInspection.this,
																							"Zip should be 5 characters under Inspection Address");
																					etzip.requestFocus();
																					etzip.setText("");
																					llcreateanaccount
																							.setVisibility(View.VISIBLE);
																					plus2.setVisibility(View.GONE);
																					minus2.setVisibility(View.VISIBLE);
																				}
																			} else {
																				toast = new ShowToast(
																						OrderInspection.this,
																						"Please enter Zip under Inspection Address");
																				etzip.requestFocus();
																				etzip.setText("");
																				llcreateanaccount
																						.setVisibility(View.VISIBLE);
																				plus2.setVisibility(View.GONE);
																				minus2.setVisibility(View.VISIBLE);
																			}
																		
																	
															} else {
																toast = new ShowToast(
																		OrderInspection.this,
																		"Please enter Inspection Address1");
																etinspaddress1
																		.requestFocus();
																etinspaddress1
																		.setText("");
																llcreateanaccount
																		.setVisibility(View.VISIBLE);
																plus2.setVisibility(View.GONE);
																minus2.setVisibility(View.VISIBLE);
															}
														} else {
															toast = new ShowToast(
																	OrderInspection.this,
																	"Please enter valid Phone");
															etphone.requestFocus();
															etphone.setText("");
															llcreateanaccount
																	.setVisibility(View.VISIBLE);
															plus2.setVisibility(View.GONE);
															minus2.setVisibility(View.VISIBLE);
														}
													} else {
														toast = new ShowToast(
																OrderInspection.this,
																"Please enter valid Email");
														etmail.requestFocus();
														etmail.setText("");
														llcreateanaccount
																.setVisibility(View.VISIBLE);
														plus2.setVisibility(View.GONE);
														minus2.setVisibility(View.VISIBLE);
													}
												} else {
													toast = new ShowToast(
															OrderInspection.this,
															"Please enter Lastname");
													etlastname.requestFocus();
													etlastname.setText("");
													llcreateanaccount
															.setVisibility(View.VISIBLE);
													plus2.setVisibility(View.GONE);
													minus2.setVisibility(View.VISIBLE);
												}
											} else {
												toast = new ShowToast(
														OrderInspection.this,
														"Please enter Firstname");
												etfirstname.requestFocus();
												etfirstname.setText("");
												llcreateanaccount
														.setVisibility(View.VISIBLE);
												plus2.setVisibility(View.GONE);
												minus2.setVisibility(View.VISIBLE);
											}
										} else {
											toast = new ShowToast(
													OrderInspection.this,
													"Please Calculate Fees");
											etinspectionfees.requestFocus();
											etinspectionfees.setText("");
											llgeneralinfo
													.setVisibility(View.VISIBLE);
											plus1.setVisibility(View.GONE);
											minus1.setVisibility(View.VISIBLE);
										}
									} else {
										toast = new ShowToast(
												OrderInspection.this,
												"Please select Preferred Time");
										llgeneralinfo
												.setVisibility(View.VISIBLE);
										plus1.setVisibility(View.GONE);
										minus1.setVisibility(View.VISIBLE);
									}
								} else {
									toast = new ShowToast(OrderInspection.this,
											"Please enter Date");
									llgeneralinfo.setVisibility(View.VISIBLE);
									plus1.setVisibility(View.GONE);
									minus1.setVisibility(View.VISIBLE);
									etdate.requestFocus();
									etdate.setText("");
								}
							} else {
								toast = new ShowToast(OrderInspection.this,
										"Please select Preferred IMC");
								llgeneralinfo.setVisibility(View.VISIBLE);
								plus1.setVisibility(View.GONE);
								minus1.setVisibility(View.VISIBLE);
							}
						}
						else
						{
							int no=Integer.parseInt(etnoofbuildings.getText().toString());
							for (int ii = 0; ii < no; ii++) {
								if (dynamic_etsquarefootage[ii].getText().toString()
										.equals("")) {
									toast = new ShowToast(OrderInspection.this,
											"Please enter Square Footage");
									dynamic_etsquarefootage[ii].requestFocus();
									break;
								} else {
									if (dynamic_etnoofstories[ii].getText().toString()
											.equals("")) {
										toast = new ShowToast(OrderInspection.this,
												"Please enter No.Of Stories");
										dynamic_etnoofstories[ii].requestFocus();
										break;
									}
								}
							}
						}
						} else {
							toast = new ShowToast(OrderInspection.this,
									"Please enter No Of Buildings");
							etnoofbuildings.requestFocus();
							etnoofbuildings.setText("");
							llgeneralinfo.setVisibility(View.VISIBLE);
							plus1.setVisibility(View.GONE);
							minus1.setVisibility(View.VISIBLE);
						}
					} else {
						toast = new ShowToast(OrderInspection.this,
								"Please enter No Of Stories");
						etnoofstories.requestFocus();
						etnoofstories.setText("");
						llgeneralinfo.setVisibility(View.VISIBLE);
						plus1.setVisibility(View.GONE);
						minus1.setVisibility(View.VISIBLE);
					}
				} else {
					toast = new ShowToast(OrderInspection.this,
							"Please select the Year of Construction");
					etotheryear.requestFocus();
					etotheryear.setText("");
					llgeneralinfo.setVisibility(View.VISIBLE);
					plus1.setVisibility(View.GONE);
					minus1.setVisibility(View.VISIBLE);
				}
			} else {
				toast = new ShowToast(OrderInspection.this,
						"Please select Year");
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
		} else {
			
			if(etsquarefootage.getText().toString().trim().equals(""))
			{
				toast = new ShowToast(OrderInspection.this,
						"Please enter Square Footage");
				etsquarefootage.requestFocus();
				etsquarefootage.setText("");
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
			else
			{
				toast = new ShowToast(OrderInspection.this,
						"Please enter the Square Footage greater than 399 and less than 1,00,000");
				etsquarefootage.requestFocus();
				etsquarefootage.setText("");
				llgeneralinfo.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
			
		}
	}

	private void Place_Order() {
		stradditionalbuildingid="";str_building_edittext_value="";stradditionalbuildingid="";
		if (lladditionalservice.getVisibility() == View.VISIBLE) {
			Get_Additional_Inspection_Id();
			if (!stradditionalinspectionid.equals("")) {
				stradditionalinspectionid = stradditionalinspectionid
						.substring(0, stradditionalinspectionid.length() - 1);
			}
		}

		if (lladditionalinspecton.getVisibility() == View.VISIBLE) {
			Get_Additional_Building_Id();
			if (!stradditionalbuildingid.equals("")) {
				stradditionalbuildingid = stradditionalbuildingid.substring(0,
						stradditionalbuildingid.length() - 1);
				str_building_edittext_value = str_building_edittext_value
						.substring(0, str_building_edittext_value.length() - 1);
			}
		}
		if (cbaddresscheck.isChecked()) {
			strstateid2 = strstateid;
			strcountyid2 = strcountyid;
			strcounty2 = strcounty;
		}
		if (spinneryear.getSelectedItem().toString().equals("Other")) {
			stryear = etotheryear.getText().toString();
		} else {
			stryear = spinneryear.getSelectedItem().toString();
		}
		// if(strinsurancecarrier.equals("No"))
		// {
		// strcarrierid="0";
		// }
		strcouponcode = etcouponcode.getText().toString();
		strsquarefootagevalue = etsquarefootage.getText().toString();
		strpolicyno = etpolicyno.getText().toString();
		strnoofstories = etnoofstories.getText().toString();
		strnoofbuildings = etnoofbuildings.getText().toString();
		strdate = etdate.getText().toString();
		strfirstname = etfirstname.getText().toString();
		strlastname = etlastname.getText().toString();
		strmail = etmail.getText().toString();
		strphone = etphone.getText().toString();
		strcity = etcity.getText().toString();
		strzip = etzip.getText().toString();
		strcity2 = etcity2.getText().toString();
		strzip2 = etzip2.getText().toString();
		strinspaddress1 = etinspaddress1.getText().toString();
		strinspaddress2 = etinspaddress2.getText().toString();
		strmailaddress1 = etmailaddress1.getText().toString();
		strmailaddress2 = etmailaddress2.getText().toString();

		if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Ordering Inspection... Please wait.");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(cf.NAMESPACE,
								"OrderInspction_Agent");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("CouponID", etcouponcode.getText()
								.toString());
						request.addProperty("UserId", Integer.parseInt(AgentId));
						request.addProperty("userName", AgentName);
						request.addProperty("Mailing_Address1", strmailaddress1);
						request.addProperty("Mailing_Address2", strmailaddress2);
						request.addProperty("Mailing_StateId",
								Integer.parseInt(strstateid2));
						request.addProperty("Mailing_County", strcounty2);
						request.addProperty("Mailing_Zip", strzip2);
						request.addProperty("Mailing_City", strcity2);
						request.addProperty("Insp_Address1", strinspaddress1);
						request.addProperty("Insp_Address2", strinspaddress2);
						request.addProperty("Insp_StateId",
								Integer.parseInt(strstateid));
						request.addProperty("Insp_County", strcounty);
						request.addProperty("Insp_Zip", strzip);
						request.addProperty("Insp_City", strcity);
						request.addProperty("Firstname", strfirstname);
						request.addProperty("Lastname", strlastname);
						request.addProperty("Email", strmail);
						request.addProperty("Phone", strphone);
						request.addProperty("MainInspTypeID",
								Integer.parseInt(insptypeid));
						request.addProperty("AdditionalInspTypes",
								stradditionalinspectionid);
						request.addProperty("AdditionalBuildingTypes",
								stradditionalbuildingid);
						request.addProperty("AddBuildindSqFootage",
								str_building_edittext_value);
						request.addProperty("NoOfBuildings",
								Integer.parseInt(strnoofbuildings));
						request.addProperty("PolicyNumber", strpolicyno);
						request.addProperty("CarrierID",
								Integer.parseInt(strcarrierid));
						request.addProperty("AgentID",
								Integer.parseInt(Agent_Id2));
						request.addProperty("AgencyID",
								Integer.parseInt(AgencyId));
						request.addProperty("sqFootage",
								Long.parseLong(strsquarefootagevalue));
						request.addProperty("YearConstruction",
								Integer.parseInt(stryear));
						request.addProperty("NoOfStories",
								Integer.parseInt(strnoofstories));
						request.addProperty("PreInspID",
								Integer.parseInt(strinspectorid));
						request.addProperty("PreCompanyID",
								Integer.parseInt(strcompanyid));
						request.addProperty("PreDateInsp", strdate);
						request.addProperty("PreTime", strtime);
						request.addProperty("Discount", etdiscount.getText()
								.toString());

						envelope.setOutputSoapObject(request);
						System.out.println("OrderInspction_Agent request is "
								+ request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL);
						System.out.println("Before http call");
						androidHttpTransport.call(cf.NAMESPACE
								+ "OrderInspction_Agent", envelope);
						// SoapObject result = (SoapObject)
						// envelope.getResponse();
						order_result = envelope.getResponse().toString();
						System.out.println("OrderInspction_Agent result is"
								+ order_result);
						// SoapObject obj = (SoapObject) result.getProperty(0);
						// String res =
						// obj.getProperty("OrderInspction_AgentResult").toString();
						// System.out.println("result value is" + res);

						show_handler = 5;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;

							if (order_result.equals("1")) {

								toast = new ShowToast(OrderInspection.this,
										"Order Placed Successfully");

								Intent intent = new Intent(
										OrderInspection.this, HomeScreen.class);
								startActivity(intent);
								finish();

							} else {
								toast = new ShowToast(
										OrderInspection.this,
										"There is a problem on your application. Please contact Paperless administrator.");
							}

						}
					}
				};
			}.start();
		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");
		}
	}

	private void Get_Additional_Inspection_Id() {
		stradditionalinspectionid="";
		for (int i = 0; i < n; i++) {
			if (cb[i].isChecked()) {
				String value = cb[i].getTag().toString();
				String[] splittedvalue = value.split(":");
				stradditionalinspectionid += splittedvalue[1] + ",";
			}
		}
	}

	private void Get_Additional_Building_Id() {
		stradditionalbuildingid="";str_building_edittext_value="";
		for (i = 0; i < Cnt; i++) {
			if (checkbox[i].isChecked()) {
				String value = checkbox[i].getTag().toString();
				String[] splittedvalue = value.split(":");
				stradditionalbuildingid += splittedvalue[5] + ",";

				str_building_edittext_value += et[i].getText().toString() + ",";

			}
		}
	}

	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
		// getCalender();
		Calendar c = Calendar.getInstance();
		int mYear = c.get(Calendar.YEAR);
		int mMonth = c.get(Calendar.MONTH);
		int mDay = c.get(Calendar.DAY_OF_MONTH);
		System.out.println("the selected " + mDay);
		DatePickerDialog dialog = new DatePickerDialog(OrderInspection.this,
				new mDateSetListener(edt), mYear, mMonth, mDay);
		dialog.show();
	}

	class mDateSetListener implements DatePickerDialog.OnDateSetListener {
		EditText v;

		mDateSetListener(EditText v) {
			this.v = v;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			// getCalender();
			int mYear = year;
			int mMonth = monthOfYear;
			int mDay = dayOfMonth;
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			System.out.println(v.getText().toString());

			Date date1 = null, date2 = null;
			try {
				System.out.println("Inside try");
				String formatString = "MM/dd/yyyy";
				SimpleDateFormat df = new SimpleDateFormat(formatString);
				date1 = df.parse(currentdate);
				date2 = df.parse(v.getText().toString());
				System.out.println("current date " + date1);
				System.out.println("selected date " + date2);
				/*if (date2.compareTo(date1) <= 0) {
					System.out.println("inside date if");
					toast = new ShowToast(OrderInspection.this,
							"Please select Future Date");
					v.setText("");
				} else {
					System.out.println("inside date else");
				}*/
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 0:
			// set date picker as current date
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			System.out.println("Date picker");

			datePicker = new DatePickerDialog(this, datePickerListener, year,
					month, day);

			return datePicker;

		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
		// when dialog box is closed, below method will be called.
		boolean fired = false;

		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			if (fired == true) {
				System.out.println("Double fire occured");
				final Calendar c1 = Calendar.getInstance();
				datePicker.updateDate(c1.get(Calendar.YEAR),
						c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
				// return;
			} else {
				// first time fired
				fired = true;

				int year = selectedYear;
				int month = selectedMonth;
				int day = selectedDay;

				// set selected date into textview

				etdate.setText(new StringBuilder().append(month + 1)
						.append("/").append(day).append("/").append(year));

				Date date1 = null, date2 = null;
				try {
					System.out.println("Inside try");
					String formatString = "MM/dd/yyyy";
					SimpleDateFormat df = new SimpleDateFormat(formatString);
					date1 = df.parse(currentdate);
					date2 = df.parse(etdate.getText().toString());
					System.out.println("current date " + date1);
					System.out.println("selected date " + date2);
					if (date2.compareTo(date1) <= 0) {
						System.out.println("inside date if");
						toast = new ShowToast(OrderInspection.this,
								"Please select Future Date");
						etdate.setText("");
					} else {
						System.out.println("inside date else");
						etdate.setText(new StringBuilder().append(month + 1)
								.append("/").append(day).append("/")
								.append(year));
					}
				} catch (Exception e) {
					// TODO: handle exception
				}

				// updating date picker to current date
				final Calendar c1 = Calendar.getInstance();
				datePicker.updateDate(c1.get(Calendar.YEAR),
						c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
				fired = false;

			}

			/*
			 * int year = selectedYear; int month = selectedMonth; int day =
			 * selectedDay;
			 * 
			 * // set selected date into textview
			 * 
			 * etdate.setText(new StringBuilder().append(month + 1)
			 * .append("/").append(day).append("/").append(year));
			 * 
			 * Date date1 = null,date2 = null; try {
			 * System.out.println("Inside try"); String formatString =
			 * "MM/dd/yyyy"; SimpleDateFormat df = new
			 * SimpleDateFormat(formatString); date1 = df.parse(currentdate);
			 * date2 = df.parse(etdate.getText().toString());
			 * System.out.println("current date "+date1);
			 * System.out.println("selected date "+date2); if
			 * (date2.compareTo(date1)<=0) {
			 * System.out.println("inside date if"); toast=new
			 * ShowToast(OrderInspection.this, "Please Select Future Date");
			 * etdate.setText(""); } else {
			 * System.out.println("inside date else"); etdate.setText(new
			 * StringBuilder().append(month + 1)
			 * .append("/").append(day).append("/").append(year)); } } catch
			 * (Exception e) { // TODO: handle exception }
			 * 
			 * 
			 * //updating date picker to current date final Calendar c1 =
			 * Calendar.getInstance();
			 * datePicker.updateDate(c1.get(Calendar.YEAR),
			 * c1.get(Calendar.MONTH), c1.get(Calendar.DATE));
			 */

		}
	};

	private void Calculate_Fees() {
		if(!etnoofbuildings.getText().toString().trim().equals(""))
		{
			noofbuild = Integer.parseInt(etnoofbuildings.getText().toString());
			if (noofbuild <= 1) {
				if (etsquarefootage.getText().toString().trim().equals("")) {
					toast = new ShowToast(OrderInspection.this,
							"Please enter Square Footage");
					etsquarefootage.requestFocus();
				} else {
					
					llcalculatefees.setVisibility(View.VISIBLE);
					
					squarefootagevalue = Float.parseFloat(etsquarefootage
							.getText().toString());
					inspectionbasesq_feet = Float
							.parseFloat(strinspectionbasesq_feet);
					inspectionbase_fee = Float
							.parseFloat(strinspectionbase_fee);System.out.println("elsefee"+inspectionbase_fee);
					inspectionadditionalsq_feet = Float
							.parseFloat(strinspectionadditionalsq_feet);
					inspectionaddtional_fee = Float
							.parseFloat(strinspectionaddtional_fee);System.out.println("squarefootagevalue="+squarefootagevalue);
					if (squarefootagevalue <= inspectionbasesq_feet) {
//						String test = String
//								.format("%.02f", inspectionbase_fee);
//						etinspectionfees.setText(test);
						System.out.println("first");
						 etinspectionfees.setText(String.valueOf(inspectionbase_fee));
						Calculate_discount();
						etadditionalbuildingfees.setText("");
						Calculate_AdditionalBuilding_Fess();
						
						System.out.println("the value is "+value);
						value *=Float.parseFloat(etnoofbuildings.getText().toString());
						System.out.println("the value is "+value);
						etadditionalservicefees.setText(String.valueOf(value));
						
					} else {System.out.println("second");
						if ((squarefootagevalue - inspectionbasesq_feet) <= inspectionadditionalsq_feet) {
							System.out.println("secondfirst");
							inspectionbase_fee = inspectionbase_fee
									+ inspectionaddtional_fee;
							System.out.println("inspectionbase_fee="+inspectionbase_fee);

						} else if ((squarefootagevalue - inspectionbasesq_feet) > inspectionadditionalsq_feet) {
							System.out.println("secondsecond"+inspectionbase_fee+"a"+squarefootagevalue+"b"+inspectionbasesq_feet+"c"+inspectionadditionalsq_feet+"d"+inspectionaddtional_fee);
						    float temp = ((squarefootagevalue - inspectionbasesq_feet) / inspectionadditionalsq_feet) * inspectionaddtional_fee;
							System.out.println("temp"+temp);
							if(Float.isNaN(temp))
							{
								System.out.println("cameinside");
								temp=0;
							}
							else
							{
								System.out.println("cameelse");
							}
							inspectionbase_fee = inspectionbase_fee
									+ temp;
							System.out.println("inspectionbase_fee="+inspectionbase_fee);
						}

//						String test = String
//								.format("%.02f", inspectionbase_fee);
//						etinspectionfees.setText(test);

						 etinspectionfees.setText(String.valueOf(inspectionbase_fee));
						Calculate_discount();
						etadditionalbuildingfees.setText("");
						Calculate_AdditionalBuilding_Fess();
						
						System.out.println("the value is "+value);
						value *=Float.parseFloat(etnoofbuildings.getText().toString());
						System.out.println("the value is "+value);
						etadditionalservicefees.setText(String.valueOf(value));
						
					}
				}
			} else {
				for (int ii = 0; ii < noofbuild; ii++) {
					if (dynamic_etsquarefootage[ii].getText().toString()
							.equals("")) {
						toast = new ShowToast(OrderInspection.this,
								"Please enter Square Footage");
						dynamic_etsquarefootage[ii].requestFocus();
						// ii=noofbuild;
						break;
					} else {
						if (dynamic_etnoofstories[ii].getText().toString()
								.equals("")) {
							toast = new ShowToast(OrderInspection.this,
									"Please enter No.Of Stories");
							dynamic_etnoofstories[ii].requestFocus();
							// ii=noofbuild;
							break;
						} else {
							if (ii == noofbuild - 1) {
								System.out.println("Calculate fees2");
								
								llcalculatefees.setVisibility(View.VISIBLE);
								
								Calculate_Fees2();
								Calculate_AdditionalBuilding_Fess();
//								if(!etadditionalservicefees.getText().toString().equals(""))
//								{
//									float addfees=Float.parseFloat(etadditionalservicefees.getText().toString());
//									addfees *=Float.parseFloat(etnoofbuildings.getText().toString());
//									etadditionalservicefees.setText(String.valueOf(addfees));
									System.out.println("the value is "+value);
									value *=Float.parseFloat(etnoofbuildings.getText().toString());
									System.out.println("the value is "+value);
									etadditionalservicefees.setText(String.valueOf(value));
//									((EditText) findViewById(R.id.orderinsp_etadditionalservicefees))
//									.setText(String.valueOf(value));
//								}
							}
						}
					}
				}
			}
		}
		else
		{
			toast = new ShowToast(OrderInspection.this,
					"Please enter No Of Buildings");
		}
		
		
//		squarefootagevalue = Float.parseFloat(etsquarefootage.getText()
//				.toString());
//		inspectionbasesq_feet = Float.parseFloat(strinspectionbasesq_feet);
//		inspectionbase_fee = Float.parseFloat(strinspectionbase_fee);
//		inspectionadditionalsq_feet = Float
//				.parseFloat(strinspectionadditionalsq_feet);
//		inspectionaddtional_fee = Float.parseFloat(strinspectionaddtional_fee);
//		if (squarefootagevalue <= inspectionbasesq_feet) {
//			String test = String.format("%.02f", inspectionbase_fee);
//			etinspectionfees.setText(test);
//			// etinspectionfees.setText(String.valueOf(inspectionbase_fee));
//			Calculate_discount();
//			etadditionalbuildingfees.setText("");
//			Calculate_AdditionalBuilding_Fess();
//		} else {
//			if ((squarefootagevalue - inspectionbasesq_feet) <= inspectionadditionalsq_feet) {
//				inspectionbase_fee = inspectionbase_fee
//						+ inspectionaddtional_fee;
//
//			} else if ((squarefootagevalue - inspectionbasesq_feet) > inspectionadditionalsq_feet) {
//				inspectionbase_fee = inspectionbase_fee
//						+ (((squarefootagevalue - inspectionbasesq_feet) / inspectionadditionalsq_feet) * inspectionaddtional_fee);
//
//			}
//
//			String test = String.format("%.02f", inspectionbase_fee);
//			etinspectionfees.setText(test);
//
//			// etinspectionfees.setText(String.valueOf(inspectionbase_fee));
//			Calculate_discount();
//			etadditionalbuildingfees.setText("");
//			Calculate_AdditionalBuilding_Fess();
//
//		}

	}
	
	private void Calculate_Fees2()
	{
		inspectionbase_fee2 = 0;
		for(int i=0;i<noofbuild;i++)
		{
			squarefootagevalue = Float.parseFloat(dynamic_etsquarefootage[i].getText()
					.toString());
			inspectionbasesq_feet = Float.parseFloat(strinspectionbasesq_feet);
//			inspectionbase_fee = Float.parseFloat(strinspectionbase_fee);
			inspectionbase_fee2 = Float.parseFloat(strinspectionbase_fee);
			inspectionadditionalsq_feet = Float
					.parseFloat(strinspectionadditionalsq_feet);
			inspectionaddtional_fee = Float.parseFloat(strinspectionaddtional_fee);
			if (squarefootagevalue <= inspectionbasesq_feet) {
				inspectionbase_fee += Float.parseFloat(strinspectionbase_fee);
				inspectionbase_fee2 = Float.parseFloat(strinspectionbase_fee);
				
//				String test = String
//						.format("%.02f", inspectionbase_fee);
//				etinspectionfees.setText(test);
				etinspectionfees.setText(String.valueOf(inspectionbase_fee));
				
				Calculate_discount2();
			} else {
				if ((squarefootagevalue - inspectionbasesq_feet) <= inspectionadditionalsq_feet) {
					inspectionbase_fee2 = inspectionbase_fee2
							+ inspectionaddtional_fee;

				} else if ((squarefootagevalue - inspectionbasesq_feet) > inspectionadditionalsq_feet) {
					float temp = (((squarefootagevalue - inspectionbasesq_feet) / inspectionadditionalsq_feet) * inspectionaddtional_fee);
					if(Float.isNaN(temp))
					{
						temp=0;
					}
					inspectionbase_fee2 = inspectionbase_fee2
							+ temp;

				}
				inspectionbase_fee += inspectionbase_fee2;
				
//				String test = String
//						.format("%.02f", inspectionbase_fee);
//				etinspectionfees.setText(test);
				etinspectionfees.setText(String.valueOf(inspectionbase_fee));
				
				Calculate_discount2();
			}
//			if(ii==noofbuild-1)
//			{
//				String test = String.format("%.02f", inspectionbase_fee);
//				etinspectionfees.setText(test);
////				etinspectionfees.setText(String.valueOf(inspectionbase_fee));
////				Calculate_discount();
//				etadditionalbuildingfees.setText("");
//				Calculate_AdditionalBuilding_Fess();
//			}
		}
	}
	
	private void Calculate_discount2() {
		System.out.println("Inside calculate discount");
		String value = spinnerinspectiontype.getSelectedItem().toString();
		int position = spinnerinspectiontype.getSelectedItemPosition();
		insptypeid = array_Insp_ID[position];

		String strcompanyname = spinnerimc.getSelectedItem().toString();
		int companyid = spinnerimc.getSelectedItemPosition();
		String strcompanyid = arraycompanyid[companyid];

		strcouponcode = etcouponcode.getText().toString();

		System.out.println("The inspetion fees is "
				+ etinspectionfees.getText().toString());

		LoadDiscount(strcouponcode, String.valueOf(inspectionbase_fee2),
				strcompanyid, insptypeid);
		// LoadDiscount(strcouponcode, String.valueOf(inspectionbase_fee),
		// strcompanyid,
		// insptypeid);

	}

	private void Calculate_AdditionalBuilding_Fess() {
		if (lladditionalinspecton.getVisibility() == View.VISIBLE) {
			for (i = 0; i < Cnt; i++) {
				if (checkbox[i].isChecked()) {
					if (!et[i].getText().toString().trim().equals("")) {
						additionalbuildingvalue = 0;
						String allvalue = checkbox[i].getTag().toString();
						String[] arrayallvalue = allvalue.split(":");
						strbuildingbasesq_feet = arrayallvalue[0];
						strbuildingbase_fee = arrayallvalue[1];
						strbuildingaddsq_feet = arrayallvalue[2];
						strbuildingadd_fee = arrayallvalue[3];

						if (!etadditionalbuildingfees.getText().toString()
								.trim().equals("")) {
							stradditionalbuildingvalue = etadditionalbuildingfees
									.getText().toString();
							additionalbuildingvalue = Float
									.parseFloat(stradditionalbuildingvalue);
						} else {
							additionalbuildingvalue = 0;
						}

						float etvalue = Float.parseFloat(et[i].getText()
								.toString());
						System.out.println("The etvalue is " + etvalue);

						buildingbasesq_feet = Float
								.parseFloat(strbuildingbasesq_feet);
						buildingbase_fee = Float
								.parseFloat(strbuildingbase_fee);
						buildingaddsq_feet = Float
								.parseFloat(strbuildingaddsq_feet);
						buildingadd_fee = Float.parseFloat(strbuildingadd_fee);

						if (etvalue <= buildingbasesq_feet) {
							additionalbuildingvalue += buildingbase_fee;
						} else {
							if ((etvalue - buildingbasesq_feet) <= buildingaddsq_feet) {
								buildingbase_fee = buildingbase_fee
										+ buildingadd_fee;

							} else if ((etvalue - buildingbasesq_feet) > buildingaddsq_feet) {
								buildingbase_fee = buildingbase_fee
										+ (((etvalue - buildingbasesq_feet) / buildingaddsq_feet) * buildingadd_fee);

							}
							additionalbuildingvalue += buildingbase_fee;

							// strbuildingbase_fee =
							// String.valueOf(buildingbase_fee);
						}
//						String test = String.format("%.02f",
//								additionalbuildingvalue);
//						float additionalbuildingfeesvalue=Float.parseFloat(test);
						float additionalbuildingfeesvalue=additionalbuildingvalue;
						additionalbuildingfeesvalue *=Float.parseFloat(etnoofbuildings.getText().toString());
//						etadditionalbuildingfees.setText(test);
						etadditionalbuildingfees.setText(String.valueOf(additionalbuildingfeesvalue));
						// etadditionalbuildingfees.setText(String
						// .valueOf(additionalbuildingvalue));

					} else {
						toast = new ShowToast(OrderInspection.this,
								"Enter Square Feet");
						et[i].requestFocus();
					}
				}
			}
		}

	}

	private void Display_Fees() {
		if (etadditionalservicefees.getText().toString().trim().equals("")) {
			additionalinspectionvalue = 0;
		} else {
			additionalinspectionvalue = Float
					.parseFloat(etadditionalservicefees.getText().toString());
		}
		if (etadditionalbuildingfees.getText().toString().trim().equals("")) {
			additionalbuildingvalue = 0;
		} else {
			additionalbuildingvalue = Float.parseFloat(etadditionalbuildingfees
					.getText().toString());
		}
		totalvalue = Float.parseFloat(etinspectionfees.getText().toString())
				+ additionalinspectionvalue + additionalbuildingvalue
				- Float.parseFloat(etdiscount.getText().toString());
		System.out.println("The total value is " + totalvalue);
//		String test = String.format("%.02f", totalvalue);
//		ettotalfees.setText(String.valueOf(test));
		 ettotalfees.setText(String.valueOf(totalvalue));
	}

	private void Calculate_discount() {
		System.out.println("Inside calculate discount");
		String value = spinnerinspectiontype.getSelectedItem().toString();
		int position = spinnerinspectiontype.getSelectedItemPosition();
		insptypeid = array_Insp_ID[position];

		String strcompanyname = spinnerimc.getSelectedItem().toString();
		int companyid = spinnerimc.getSelectedItemPosition();
		String strcompanyid = arraycompanyid[companyid];

		strcouponcode = etcouponcode.getText().toString();

		System.out.println("The inspetion fees is "
				+ etinspectionfees.getText().toString());

		LoadDiscount(strcouponcode, etinspectionfees.getText().toString(),
				strcompanyid, insptypeid);
		// LoadDiscount(strcouponcode, String.valueOf(inspectionbase_fee),
		// strcompanyid,
		// insptypeid);

	}

	public void LoadDiscount(final String couponcode,
			final String inspectionfee, final String companyid,
			final String inspectionid) {
		System.out.println("Inside load discount");
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>"
					+ "Calculating Discount..."
					+ " Please wait.</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				SoapObject chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = cf.Calling_WS_LoadInspectionType_Discount(
								couponcode, inspectionfee, companyid,
								inspectionid, "LoadInspectionType_Discount");
						System.out
								.println("response LoadInspectionType_Discount"
										+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;

							strdiscountvalue = String.valueOf(chklogin
									.getProperty("Discount"));
							discountvalue2 += Float.parseFloat(strdiscountvalue);
//							etdiscount
//									.setText(String.valueOf(strdiscountvalue));
							etdiscount
							.setText(String.valueOf(strdiscountvalue));
//							String test = String
//									.format("%.02f", discountvalue2);
//							etdiscount
//							.setText(String.valueOf(test));
							Display_Fees();

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}

	class myCheckBoxChnageClicker implements CheckBox.OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub

			if (isChecked) {
				strinspaddress1 = etinspaddress1.getText().toString();
				strinspaddress2 = etinspaddress2.getText().toString();
				strcity = etcity.getText().toString();
				strzip = etzip.getText().toString();
				if (!strinspaddress1.equals("")) {
					if (!strstate.equals("--Select--")) {
						if (!strcounty.equals("--Select--")
								&& !strcounty.equals("")) {
							if (!strcity.equals("")) {
								if (!strzip.equals("")) {
									if ((strzip.length() == 5)) {

										call_county2 = false;

										etmailaddress1.setText(strinspaddress1);
										etmailaddress2.setText(strinspaddress2);
										etcity2.setText(strcity);
										etzip2.setText(strzip);

										arraystateid2 = arraystateid;
										arraystatename2 = arraystatename;
										arraycountyid2 = arraycountyid;
										arraycountyname2 = arraycountyname;

										spinnerstate2.setAdapter(stateadapter);
										spinnercounty2
												.setAdapter(countyadapter);
										spinnerstate2.setSelection(spinnerstate
												.getSelectedItemPosition());

									} else {
										toast = new ShowToast(
												OrderInspection.this,
												"Zip should be 5 characters");
										etzip.requestFocus();
										cbaddresscheck.setChecked(false);
									}
								} else {
									toast = new ShowToast(OrderInspection.this,
											"Please enter Zip");
									etzip.requestFocus();
									cbaddresscheck.setChecked(false);
								}
							} else {
								toast = new ShowToast(OrderInspection.this,
										"Please enter City");
								etcity.requestFocus();
								cbaddresscheck.setChecked(false);
							}
						} else {
							toast = new ShowToast(OrderInspection.this,
									"Please select County");
							cbaddresscheck.setChecked(false);
						}
					} else {
						toast = new ShowToast(OrderInspection.this,
								"Please select State");
						cbaddresscheck.setChecked(false);
					}
				} else {
					toast = new ShowToast(OrderInspection.this,
							"Please enter Inspection Address1");
					etinspaddress1.requestFocus();
					cbaddresscheck.setChecked(false);
				}
			} else {

				etmailaddress1.setText("");
				etmailaddress2.setText("");
				etcity2.setText("");
				etzip2.setText("");
				spinnerstate2.setSelection(stateadapter.getPosition(strselect));

				call_county2 = true;
			}

		}

	}

	public void LoadInspectionType(final String categoryid,
			final String companyid) {

		// if(!companyid.equals("0")&&categoryid.equals("0"))
		// {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = cf
								.Calling_WS_LoadInspectionTypes(categoryid,
										companyid, "LoadInspectionTypes");
						LoadInspectionType(chklogin);
						System.out.println("response LoadInspectionTypes"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							ArrayAdapter<String> inspectiontypeadapter = new ArrayAdapter<String>(
									OrderInspection.this,
									android.R.layout.simple_spinner_item,
									array_InspName);
							inspectiontypeadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerinspectiontype
									.setAdapter(inspectiontypeadapter);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
		// }
	}

	public void LoadInspectionType(SoapObject objInsert) {
		System.out.println("Inside data base insert");

		cf.CreateTable(5);
		cf.db.execSQL("delete from " + cf.LoadInspectionType);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadInspectionType property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String Cat_ID = String.valueOf(obj.getProperty("Cat_ID"));
				String Insp_ID = String.valueOf(obj.getProperty("Insp_ID"));
				String InspName = String.valueOf(obj.getProperty("InspName"));
				String Type_ID = String.valueOf(obj.getProperty("Type_ID"));
				cf.db.execSQL("insert into " + cf.LoadInspectionType
						+ " (Cat_ID,Insp_ID,InspName,Type_ID) values('"
						+ cf.encode(Cat_ID) + "','" + cf.encode(Insp_ID)
						+ "','" + cf.encode(InspName) + "','"
						+ cf.encode(Type_ID) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadInspectionTypeData();
	}

	public void LoadInspectionTypeData() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoadInspectionType,
				null);
		int rows = cur.getCount();
		array_Cat_ID = new String[rows];
		array_Insp_ID = new String[rows];
		array_InspName = new String[rows];
		array_Type_ID = new String[rows];
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 0;
			do {
				String Cat_ID = cf.decode(cur.getString(cur
						.getColumnIndex("Cat_ID")));
				String Insp_ID = cf.decode(cur.getString(cur
						.getColumnIndex("Insp_ID")));
				String InspName = cf.decode(cur.getString(cur
						.getColumnIndex("InspName")));
				String Type_ID = cf.decode(cur.getString(cur
						.getColumnIndex("Type_ID")));

				array_Cat_ID[i] = Cat_ID;
				array_Insp_ID[i] = Insp_ID;
				array_InspName[i] = InspName;
				array_Type_ID[i] = Type_ID;
				i++;
			} while (cur.moveToNext());

		}
	}

	public void LoadBuildingType(final String id) {

		System.out.println("Inside Load Inspection Types");
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = cf.Calling_WS_LoadBuildingTypes(
								id, "FillBuildingType");
						LoadBuildingType(chklogin);

						System.out.println("response FillBuildingType"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							ArrayAdapter<String> btypeadapter = new ArrayAdapter<String>(
									OrderInspection.this,
									android.R.layout.simple_spinner_item,
									arr_bType);
							btypeadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerbuildintype.setAdapter(btypeadapter);

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}

	public void LoadBuildingType(SoapObject objInsert) {
		cf.CreateTable(18);
		cf.db.execSQL("delete from " + cf.LoadbuildingType);
		int n = objInsert.getPropertyCount();
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String buildingtype = String.valueOf(obj.getProperty("Type"));
				cf.db.execSQL("insert into " + cf.LoadbuildingType
						+ " (id,buildingtype) values('1','"
						+ cf.encode(buildingtype) + "');");

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("catch" + e.getMessage());
			}
		}
		LoadBuildingTypeData();
	}

	public void LoadBuildingTypeData() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoadbuildingType,
				null);
		int rows = cur.getCount();
		arr_bType = new String[rows];
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 0;
			do {

				arr_bType[i] = cf.decode(cur.getString(cur
						.getColumnIndex("buildingtype")));

				i++;
			} while (cur.moveToNext());
		}
	}

	public void LoadInspectionTypeDescription(final String id,
			final String companyid) {

		System.out.println("Inside Load Inspection Types");
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				SoapObject chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = cf.Calling_WS_LoadInspectionTypeDescription(
								id, strcompanyid, "LoadInspectionTypeDetails");
						System.out
								.println("response LoadInspectionTypedescriptionDetails"
										+ chklogin);
						// LoadInspectionTypeDescription(chklogin);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;

							if (chklogin.getPropertyCount() < 1) {
								System.out.println("Inside if");
								toast = new ShowToast(OrderInspection.this,
										"Company not in list");
							} else {
								System.out.println("Inside else");
								LoadInspectionTypeDescription(chklogin);
							}

							// String
							// Description=String.valueOf(chklogin.getProperty("Description"));
							// String
							// ImageName=String.valueOf(chklogin.getProperty("ImageName"));
							//
							// // description.setText(Description);
							//
							// Drawable drawable =
							// LoadImageFromWebOperations(ImageName);
							// descriptionimage.setImageDrawable(drawable);

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}

	public void LoadInspectionTypeDescription(SoapObject chklogin) {

		SoapObject objInsert = (SoapObject) chklogin.getProperty(0);

		String Description = String.valueOf(objInsert
				.getProperty("Description"));
		if(Description.equals("anyType{}"))
		{
			Description="";
		}
		ImageName = String.valueOf(objInsert.getProperty("ImageName"));
		strinspectionbasesq_feet = String.valueOf(objInsert
				.getProperty("Base_Sqft"));
		strinspectionbase_fee = String.valueOf(objInsert
				.getProperty("Base_Fee"));
		System.out.println("inspectionbase_fee is " + strinspectionbase_fee);
		strinspectionadditionalsq_feet = String.valueOf(objInsert
				.getProperty("Additional_Sqft"));
		strinspectionaddtional_fee = String.valueOf(objInsert
				.getProperty("Additional_Fee"));

		description.setText(Description);
		System.out.println("The Image Name is " + ImageName);

		// Drawable drawable = LoadImageFromWebOperations(ImageName);
		// descriptionimage.setImageDrawable(drawable);

		new DownloadImageTask(descriptionimage).execute(ImageName);

	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog mDialog;
		private ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected void onPreExecute() {

			// mDialog =
			// ProgressDialog.show(ChartActivity.this,"Please wait...",
			// "Retrieving data ...", true);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", "image download error");
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			// set image of your imageview
			bmImage.setImageBitmap(result);
			// close
			// mDialog.dismiss();
		}
	}

	public void LoadInspectionCategory() {
		if (cf.isInternetOn() == true) {

			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = cf
								.Calling_WS_LoadInspectionCategory("LoadInspectionCategory");
						SoapObject obj = (SoapObject) chklogin.getProperty(0);
						// InsertData(obj);
						LoadInspectionCategory(chklogin);
						System.out.println("response LoadInspectionCategory"
								+ chklogin);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							ArrayAdapter<String> inspectioncategoryadapter = new ArrayAdapter<String>(
									OrderInspection.this,
									android.R.layout.simple_spinner_item,
									arraycategory);
							inspectioncategoryadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerinspectioncategory
									.setAdapter(inspectioncategoryadapter);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}

	public void LoadInspectionCategory(SoapObject objInsert) {
		cf.CreateTable(4);
		cf.db.execSQL("delete from " + cf.LoadInspectionCategory);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadInspectionCategory property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String ID = String.valueOf(obj.getProperty("ID"));
				String Category = String.valueOf(obj.getProperty("Category"));
				cf.db.execSQL("insert into " + cf.LoadInspectionCategory
						+ " (ID,Category) values('" + cf.encode(ID) + "','"
						+ cf.encode(Category) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadInspectionCategoryData();

	}

	public void LoadInspectionCategoryData() {
		cf.CreateTable(4);
		Cursor cur = cf.db.rawQuery("select * from "
				+ cf.LoadInspectionCategory, null);
		int rows = cur.getCount();
		arrayid = new String[rows];
		arraycategory = new String[rows];
		// arrayid[0]="--Select--";
		// arraycategory[0]="--Select--";
		System.out.println("LoadInspectionCategory count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 0;
			do {
				String ID = cf.decode(cur.getString(cur.getColumnIndex("ID")));
				String Category = cf.decode(cur.getString(cur
						.getColumnIndex("Category")));
				arrayid[i] = ID;
				arraycategory[i] = Category;
				i++;
			} while (cur.moveToNext());

		}
	}

	public void LoadCompany() {
		if (cf.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin1 = cf
								.Calling_WS_LoadCompany("LoadCompany");
						SoapObject obj1 = (SoapObject) chklogin1.getProperty(0);
						// InsertData(obj);
						LoadCompany(chklogin1);
						System.out.println("response LoadCompany" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							ArrayAdapter<String> imcadapter = new ArrayAdapter<String>(
									OrderInspection.this,
									android.R.layout.simple_spinner_item,
									arraycompanyname);
							imcadapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerimc.setAdapter(imcadapter);
							// LoadInspectionCategory();
							spinnerimc.setSelection(imcadapter
									.getPosition("Inspection Depot, Inc."));
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}

	public void LoadCompany(SoapObject objInsert) {
		cf.CreateTable(8);
		cf.db.execSQL("delete from " + cf.LoadCompany);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadCompany property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("id"));
				String Name = String.valueOf(obj.getProperty("Name"));
				cf.db.execSQL("insert into " + cf.LoadCompany
						+ " (id,Name) values('" + cf.encode(id) + "','"
						+ cf.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadCompanyData();

	}

	public void LoadCompanyData() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoadCompany, null);
		int rows = cur.getCount();
		// arraycompanyid = new String[rows + 1];
		// arraycompanyname = new String[rows + 1];
		arraycompanyid = new String[rows];
		arraycompanyname = new String[rows];
		// arraycompanyid[0] = "0";
		// arraycompanyname[0] = "--Select--";
		System.out.println("LoadCompany count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 0;
			do {
				String ID = cf.decode(cur.getString(cur.getColumnIndex("id")));
				String Category = cf.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arraycompanyid[i] = ID;
				arraycompanyname[i] = Category;
				i++;
			} while (cur.moveToNext());

		}
	}

	public void LoadInspectors(final String id) {
		if (cf.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = cf.Calling_WS_LoadInspectors(id,
								"LoadInspectors");
						SoapObject obj = (SoapObject) chklogin.getProperty(0);
						// InsertData(obj);
						LoadInspectors(chklogin);
						System.out
								.println("response LoadInspectors" + chklogin);
						// pd.dismiss();
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							ArrayAdapter<String> inspectoradapter = new ArrayAdapter<String>(
									OrderInspection.this,
									android.R.layout.simple_spinner_item,
									arrayinspectorname);
							inspectoradapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerinspector.setAdapter(inspectoradapter);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}

	public void LoadInspectors(SoapObject objInsert) {
		cf.CreateTable(9);
		cf.db.execSQL("delete from " + cf.LoadInspectors);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadInspectors property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String id = String.valueOf(obj.getProperty("id"));
				String Name = String.valueOf(obj.getProperty("Name"));
				cf.db.execSQL("insert into " + cf.LoadInspectors
						+ " (id,Name) values('" + cf.encode(id) + "','"
						+ cf.encode(Name) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadInspectorsData();

	}

	public void LoadInspectorsData() {
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoadInspectors, null);
		int rows = cur.getCount();
		arrayinspectorid = new String[rows + 1];
		arrayinspectorname = new String[rows + 1];
		arrayinspectorid[i] = "0";
		arrayinspectorname[i] = "--Select--";
		System.out.println("LoadInspectors count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 1;
			do {
				String id = cf.decode(cur.getString(cur.getColumnIndex("id")));
				String Name = cf.decode(cur.getString(cur
						.getColumnIndex("Name")));
				arrayinspectorid[i] = id;
				arrayinspectorname[i] = Name;
				i++;
			} while (cur.moveToNext());

		}
	}

	public String LoadState() {
		try {
			dbh = new DataBaseHelper(OrderInspection.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = cf.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = cf.decode(cur.getString(cur
							.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}

	public void LoadCounty(final String stateid) {
		try {
			dbh = new DataBaseHelper(OrderInspection.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(OrderInspection.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty.setAdapter(countyadapter);
		
		if(countysetselection)
		{
			spinnercounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		
	}

	public void LoadCounty2(final String stateid) {
		try {
			dbh = new DataBaseHelper(OrderInspection.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid2 = new String[rows + 1];
			arraycountyname2 = new String[rows + 1];
			arraycountyid2[0] = "--Select--";
			arraycountyname2[0] = "--Select--";
			System.out.println("LoadCounty2 count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid2[i] = id;
					arraycountyname2[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData2();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData2() {
		countyadapter2 = new ArrayAdapter<String>(OrderInspection.this,
				android.R.layout.simple_spinner_item, arraycountyname2);
		countyadapter2
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty2.setAdapter(countyadapter2);
		
		if(countysetselection)
		{
			spinnercounty2.setSelection(countyadapter2.getPosition(county));
			countysetselection=false;
		}
		
	}

	public void LoadAdditionalBuilding(final String id) {

		System.out.println("Inside Load Inspection Types");
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				SoapObject chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = cf.Calling_WS_LoadInspectionTypeDescription(
								id, strcompanyid, "LoadAdditionalBuilding");
						// LoadAdditionalBuilding(chklogin);

						System.out.println("response LoadAdditionalBuilding"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;

							try {
								LoadAdditionalBuilding(chklogin);
							} catch (SocketTimeoutException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								toast = new ShowToast(
										OrderInspection.this,
										"There is a problem on your Network. Please try again later with better Network.");
							} catch (NetworkErrorException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								toast = new ShowToast(
										OrderInspection.this,
										"There is a problem on your Network. Please try again later with better Network.");
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								toast = new ShowToast(
										OrderInspection.this,
										"There is a problem on your Network. Please try again later with better Network.");
							} catch (XmlPullParserException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								toast = new ShowToast(
										OrderInspection.this,
										"There is a problem on your Network. Please try again later with better Network.");
							}

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}

	private void LoadAdditionalBuilding(SoapObject objInsert)
			throws NetworkErrorException, IOException, XmlPullParserException,
			SocketTimeoutException {

		Cnt = objInsert.getPropertyCount();
		checkbox = new CheckBox[Cnt];
		et = new EditText[Cnt];
		if (Cnt >= 1) {
			rladditionalinspecton.setVisibility(View.GONE);
			lladditionalinspecton.setVisibility(View.VISIBLE);
			lladditionalinspecton.removeAllViews();
			// findViewById(R.id.orderinspection_tvadditionalbuilding).setVisibility(View.VISIBLE);
		
			for (j = 0; j < Cnt; j++) {
				SoapObject obj = (SoapObject) objInsert.getProperty(j);
				String name = String.valueOf(obj.getProperty("Name"));
				String id = String.valueOf(obj.getProperty("ID"));
				strbuildingbasesq_feet = String.valueOf(obj
						.getProperty("Base_Sqft"));
				strbuildingbase_fee = String.valueOf(obj
						.getProperty("Base_Fee"));
				strbuildingaddsq_feet = String.valueOf(obj
						.getProperty("Additional_Sqft"));
				strbuildingadd_fee = String.valueOf(obj
						.getProperty("Additional_Fee"));

				LinearLayout.LayoutParams llparams, llchkbox, lledit;

				if (width > 1023 || height > 1023) {
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llparams.setMargins(10, 10, 10, 10);

					llchkbox = new LinearLayout.LayoutParams(300,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llchkbox.setMargins(10, 10, 10, 10);

					lledit = new LinearLayout.LayoutParams(150,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					lledit.setMargins(50, 10, 0, 10);
				} else if (width > 500 && width < 900) {
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llparams.setMargins(10, 10, 10, 10);

					llchkbox = new LinearLayout.LayoutParams(400,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llchkbox.setMargins(10, 10, 10, 10);

					lledit = new LinearLayout.LayoutParams(220,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					lledit.setMargins(50, 10, 0, 10);
				} else {
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llparams.setMargins(10, 10, 10, 10);

					llchkbox = new LinearLayout.LayoutParams(200,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llchkbox.setMargins(10, 10, 10, 10);

					lledit = new LinearLayout.LayoutParams(120,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					lledit.setMargins(50, 10, 0, 10);
				}

				LinearLayout ll = new LinearLayout(this);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				lladditionalinspecton.addView(ll);

				checkbox[j] = new CheckBox(this);
				checkbox[j].setLayoutParams(llchkbox);
				checkbox[j].setText(name);
				checkbox[j].setTag(strbuildingbasesq_feet + ":"
						+ strbuildingbase_fee + ":" + strbuildingaddsq_feet
						+ ":" + strbuildingadd_fee + ":" + j + ":" + id);
				ll.addView(checkbox[j]);

				et[j] = new EditText(this);
				et[j].setLayoutParams(lledit);
				et[j].setEnabled(false);
				et[j].setInputType(InputType.TYPE_CLASS_NUMBER);
				et[j].setKeyListener(DigitsKeyListener
						.getInstance("0123456789"));
				int maxLength = 5;
				InputFilter[] fArray = new InputFilter[1];
				fArray[0] = new InputFilter.LengthFilter(maxLength);
				et[j].setFilters(fArray);
				et[j].setHint("Enter Sq.ft");
				ll.addView(et[j]);

				et[j].addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start, int before,
							int count) {
						// TODO Auto-generated method stub
						if (et[j].getText().toString().startsWith(" ")) {
							// Not allowed
							et[j].setText("");
						}
						if (et[j].getText().toString().trim().matches("^0")) {
							// Not allowed
							et[j].setText("");
						}
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start, int count,
							int after) {
						// TODO Auto-generated method stub

					}

					@Override
					public void afterTextChanged(Editable s) {
						// TODO Auto-generated method stub

					}
				});

				checkbox[j].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String Tag = v.getTag().toString();
						System.out.println("The tag is " + Tag);
						String[] arrayallvalue = Tag.split(":");
						int position = Integer.parseInt(arrayallvalue[4]);
						if (checkbox[position].isChecked() == false) {
							et[position].setText("");
							et[position].setEnabled(false);
						} else {
							et[position].setEnabled(true);
						}

					}
				});
				
				if(j==Cnt-1)
				{
					j=0;
					break;
				}
				
			}
		}

		else {
			rladditionalinspecton.setVisibility(View.VISIBLE);
			lladditionalinspecton.setVisibility(View.GONE);
		}

	}

	public void LoadingAdditionalService() {
		// TODO Auto-generated method stub
		System.out.println("straddload");
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						chkaddservice = cf
								.Calling_WS_LoadInspectionTypeDescription(
										insptypeid, strcompanyid,
										"LoadAdditionalService");
						System.out.println("response LoadAdditionalService"
								+ chkaddservice);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);
					}
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							LoadingAdditionalService(chkaddservice);

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");
		}

	}

	private void LoadingAdditionalService(SoapObject objInsert) {
		// TODO Auto-generated method stub
		n = objInsert.getPropertyCount();
		String id="",Name="";
		if (n > 0) {
			rladditionalservice.setVisibility(View.GONE);
			lladditionalservice.setVisibility(View.VISIBLE);
			lladditionalservice.removeAllViews();
			// findViewById(R.id.orderinspection_tvadditionalinspection).setVisibility(View.VISIBLE);
			ScrollView sv = new ScrollView(this);
			lladditionalservice.addView(sv);
			LinearLayout l1 = new LinearLayout(this);
			l1.setOrientation(LinearLayout.VERTICAL);
			sv.addView(l1);
			l1.setMinimumWidth(925);

			cb = new CheckBox[n];
			img = new ImageView[n];

			for (i = 0; i < n; i++) {
				SoapObject obj = (SoapObject) objInsert.getProperty(i);
				try {
					id = String.valueOf(obj.getProperty("ID"));
					Name = String.valueOf(obj.getProperty("Name"));
					String ImageName = String.valueOf(obj
							.getProperty("ImageName"));
					String flatfee = String.valueOf(obj.getProperty("FlatFee"));

					final LinearLayout l2 = new LinearLayout(this);
					LinearLayout.LayoutParams llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					l2.setTag("ins" + i);
					l2.setOrientation(LinearLayout.HORIZONTAL);
					l1.addView(l2, llparams);

					LinearLayout lchkbox = new LinearLayout(this);
					l2.addView(lchkbox);

					LinearLayout.LayoutParams paramschk, paramseditbtn;

					if (width > 1023 || height > 1023) {
						paramschk = new LinearLayout.LayoutParams(800,
								ViewGroup.LayoutParams.WRAP_CONTENT);
						paramschk.topMargin = 5;
						paramschk.bottomMargin = 15;
						paramschk.leftMargin = 15;

						paramseditbtn = new LinearLayout.LayoutParams(93, 37);
						paramseditbtn.topMargin = 10;
						paramseditbtn.leftMargin = 15;
						paramseditbtn.rightMargin = 15;
					} else if (width > 500 && width < 900) {
						paramschk = new LinearLayout.LayoutParams(400,
								ViewGroup.LayoutParams.WRAP_CONTENT);
						paramschk.topMargin = 5;
						paramschk.bottomMargin = 15;
						paramschk.leftMargin = 5;
						paramschk.rightMargin = 5;

						paramseditbtn = new LinearLayout.LayoutParams(100, 70);
						paramseditbtn.topMargin = 5;
						paramseditbtn.leftMargin = 20;
						paramseditbtn.rightMargin = 5;
					} else {
						paramschk = new LinearLayout.LayoutParams(300,
								ViewGroup.LayoutParams.WRAP_CONTENT);
						paramschk.topMargin = 5;
						paramschk.bottomMargin = 15;
						paramschk.leftMargin = 5;
						paramschk.rightMargin = 5;

						paramseditbtn = new LinearLayout.LayoutParams(50, 50);
						paramseditbtn.topMargin = 5;
						paramseditbtn.leftMargin = 5;
						paramseditbtn.rightMargin = 5;
					}

					cb[i] = new CheckBox(this);
					cb[i].setMinimumWidth(400);
					cb[i].setMaxWidth(400);
					cb[i].setTag(flatfee + ":" + id);
					cb[i].setText(Name);
					lchkbox.addView(cb[i], paramschk);

					LinearLayout limage = new LinearLayout(this);
					l2.addView(limage);

					img[i] = new ImageView(this);
					// Drawable drawable =
					// LoadImageFromWebOperations(ImageName);
					// img[i].setImageDrawable(drawable);
					new DownloadImageTask(img[i]).execute(ImageName);
					limage.addView(img[i], paramseditbtn);

					cb[i].setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							try {
								String fees = v.getTag().toString();
								String[] allfeevalue = fees.split(":");
								String fee = allfeevalue[0];
								String repidofselbtn = l2.getTag().toString()
										.replace("ins", "");
								final int cvrtstr = Integer
										.parseInt(repidofselbtn) + 1;

								String s = ((EditText) findViewById(R.id.orderinsp_etadditionalservicefees))
										.getText().toString().trim();
								if (cb[cvrtstr - 1].isChecked() == true) {
									if (s.equals("")) {
//										float value=Float.parseFloat(fee)*Float.parseFloat(etnoofbuildings.getText().toString());
										value += Float.parseFloat(fee);
//										((EditText) findViewById(R.id.orderinsp_etadditionalservicefees))
//												.setText(fee);

										
//										((EditText) findViewById(R.id.orderinsp_etadditionalservicefees))
//										.setText(String.valueOf(value));
									} else {
										value += Float.parseFloat(fee);
//												+ Float.parseFloat(s);
//										value *=Float.parseFloat(etnoofbuildings.getText().toString());
//										((EditText) findViewById(R.id.orderinsp_etadditionalservicefees))
//												.setText(String.valueOf(value));
									}
								} else {
									if (!s.equals("")) {
										value -= /*Float.parseFloat(s) - */ Float.parseFloat(fee);
//										value *=Float.parseFloat(etnoofbuildings.getText().toString());
//										((EditText) findViewById(R.id.orderinsp_etadditionalservicefees))
//												.setText(String.valueOf(value));
									}
								}
							} catch (Exception e) {
								System.out.println("EEEE" + e.getMessage());
							}
						}
					});

				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("catch" + e.getMessage());
				}
			}
		} else {
			rladditionalservice.setVisibility(View.VISIBLE);
			lladditionalservice.setVisibility(View.GONE);
			/*
			 * lladditionalservice.removeAllViews();
			 * lladditionalservice.setVisibility(View.VISIBLE);
			 * 
			 * LinearLayout.LayoutParams llparams1 = new
			 * LinearLayout.LayoutParams( ViewGroup.LayoutParams.FILL_PARENT,
			 * 50);
			 * 
			 * LinearLayout ll = new LinearLayout(this);
			 * ll.setOrientation(LinearLayout.HORIZONTAL);
			 * lladditionalservice.addView(ll,llparams1);
			 * 
			 * LinearLayout.LayoutParams llparams = new
			 * LinearLayout.LayoutParams( ViewGroup.LayoutParams.FILL_PARENT,
			 * 50);
			 * 
			 * TextView tv = new TextView(this); tv.setLayoutParams(llparams);
			 * tv.setText("N/A"); tv.setGravity(Gravity.CENTER); ll.addView(tv);
			 */

		}
	}

	public void LoadInsuranceCarrier() {
		if (cf.isInternetOn() == true) {

			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = cf
								.Calling_WS_LoadInspectionCategory("FillCarrierDetails");
						SoapObject obj = (SoapObject) chklogin.getProperty(0);
						// InsertData(obj);
						LoadInsuranceCarrier(chklogin);
						System.out.println("response FillCarrierDetails"
								+ chklogin);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							ArrayAdapter<String> insurancecarrieradapter = new ArrayAdapter<String>(
									OrderInspection.this,
									android.R.layout.simple_spinner_item,
									array_carrier_name);
							insurancecarrieradapter
									.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
							spinnerinsurancecarrier
									.setAdapter(insurancecarrieradapter);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}

	public void LoadInsuranceCarrier(SoapObject objInsert) {
		cf.CreateTable(19);
		cf.db.execSQL("delete from " + cf.LoadInsuranceCarrier);
		int n = objInsert.getPropertyCount();
		System.out.println("LoadInsuranceCarrier property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String ID = String.valueOf(obj.getProperty("id"));
				String Category = String.valueOf(obj.getProperty("Name"));
				cf.db.execSQL("insert into " + cf.LoadInsuranceCarrier
						+ " (id,Name) values('" + cf.encode(ID) + "','"
						+ cf.encode(Category) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		LoadInsuranceCarrierData();

	}

	public void LoadInsuranceCarrierData() {
		cf.CreateTable(4);
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoadInsuranceCarrier,
				null);
		int rows = cur.getCount();
		array_carrier_id = new String[rows];
		array_carrier_name = new String[rows];
		// arrayid[0]="--Select--";
		// arraycategory[0]="--Select--";
		System.out.println("LoadInsuranceCarrier count is " + rows);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			int i = 0;
			do {
				String id = cf.decode(cur.getString(cur.getColumnIndex("id")));
				String Name = cf.decode(cur.getString(cur
						.getColumnIndex("Name")));
				array_carrier_id[i] = id;
				array_carrier_name[i] = Name;
				i++;
			} while (cur.moveToNext());

		}
	}
	
	private void Check_Couponcode()
	{
		if (cf.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				String chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = cf
								.Calling_WS_CheckCouponcode(etcouponcode.getText().toString().trim(),"CHECKCOUPONID");
						System.out.println("response CHECKCOUPONID" + chklogin1);

						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							if(chklogin1.toLowerCase().equals("false"))
							{
								etcouponcode.setText("");
								etcouponcode.requestFocus();
								toast = new ShowToast(
										OrderInspection.this,
										"Coupon code is not valid");
							}
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	}
	
	private void Load_State_County_City(final EditText et)
	{

		if (cf.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OrderInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = cf
								.Calling_WS_GETADDRESSDETAILS(et.getText().toString(),"GETADDRESSDETAILS");
						System.out.println("response GETADDRESSDETAILS" + chklogin1);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									OrderInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							if (chklogin1.toString().equals("anyType{}"))
							{
								et.setText("");
								toast = new ShowToast(OrderInspection.this,
										"Please enter a valid Zip");
							}
							else
							{
								Load_State_County_City(chklogin1);
							}
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(OrderInspection.this,
					"Internet connection not available");

		}
	
	}
	
	private void Load_State_County_City(SoapObject objInsert)
	{
			SoapObject obj = (SoapObject) objInsert.getProperty(0);
			state=String.valueOf(obj.getProperty("s_state"));
			stateid=String.valueOf(obj.getProperty("i_state"));
			county=String.valueOf(obj.getProperty("A_County"));
			countyid=String.valueOf(obj.getProperty("i_County"));
			city=String.valueOf(obj.getProperty("city"));
			
			System.out.println("State :"+state);
			System.out.println("County :"+county);
			System.out.println("City :"+city);
			
			if(zipidentifier.equals("zip1"))
			{
				spinnerstate.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity.setText(city);
			}
			else if(zipidentifier.equals("zip2"))
			{
				spinnerstate2.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity2.setText(city);
			}
			
	}

	class check implements OnCheckedChangeListener {
		int i;

		public check(int i) {
			// TODO Auto-generated constructor stub
			this.i = i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			RadioButton checkedRadioButton = (RadioButton) group
					.findViewById(checkedId);
			boolean isChecked = checkedRadioButton.isChecked();
			if (isChecked) {
				switch (i) {
				case 1:
					strinsurancecarrier = checkedRadioButton.getText()
							.toString().trim();
					if (strinsurancecarrier.equals("Yes")) {
						spinnercarrier.setVisibility(View.VISIBLE);
						LoadInsuranceCarrier();
					} else {
						spinnercarrier.setVisibility(View.GONE);
						strcarrierid = "0";
					}
					break;

				case 2:
					stradditionalinspection = checkedRadioButton.getText()
							.toString().trim();
					if (stradditionalinspection.equals("Yes")) {
						lladditionalservice.setVisibility(View.VISIBLE);
						lladditionalservice.removeAllViews();
						value=0;
						LoadingAdditionalService();

					} else {
						System.out.println("else");
						rladditionalservice.setVisibility(View.GONE);
						lladditionalservice.setVisibility(View.GONE);
						lladditionalservice.removeAllViews();
						etadditionalservicefees.setText("");
						value=0;
					}
					break;

				case 3:
					stradditionalbuilding = checkedRadioButton.getText()
							.toString().trim();
					if (stradditionalbuilding.equals("Yes")) {
						lladditionalinspecton.setVisibility(View.VISIBLE);
						lladditionalinspecton.removeAllViews();
						String value = spinnerinspectiontype.getSelectedItem()
								.toString();
						int position = spinnerinspectiontype
								.getSelectedItemPosition();
						String id = array_Insp_ID[position];
						System.out.println("id is " + id);
						LoadAdditionalBuilding(id);

					} else {
						rladditionalinspecton.setVisibility(View.GONE);
						lladditionalinspecton.setVisibility(View.GONE);
						lladditionalinspecton.removeAllViews();
						strbuildingbasesq_feet = null;
						strbuildingbase_fee = null;
						strbuildingaddsq_feet = null;
						strbuildingadd_fee = null;
						etadditionalbuildingfees.setText("");
					}
					break;

				case 4:
					strtime = checkedRadioButton.getText().toString().trim();
					break;

				}
			}
		}
	}

	private Drawable LoadImageFromWebOperations(String url) {
		// TODO Auto-generated method stub
		try {
			InputStream is = (InputStream) new URL(url).getContent();
			Drawable d = Drawable.createFromStream(is, "src name");
			return d;
		} catch (Exception e) {
			System.out.println("Exc=" + e);
			return null;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent inthome = new Intent(OrderInspection.this, HomeScreen.class);
		startActivity(inthome);
		finish();
	}
}
