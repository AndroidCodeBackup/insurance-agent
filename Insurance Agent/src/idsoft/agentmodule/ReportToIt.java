package idsoft.agentmodule;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ReportToIt extends Activity{
	TextView tvagentname;
	EditText etcomments,etmailid,etcontactno;
	LinearLayout lldynamic;
	CommonFunctions cf;
	ShowToast toast;
	int show_handler;
	String agentname, imagepath = "", version, agentid, image1 = "",
			image2 = "", image3 = "", imagename1 = "", imagename2 = "",
			imagename3 = "",agentphone,agentemail,agency_name;
	int count,keyDel,id_len;
	TextView tvpercentage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reporttoit);
		cf=new CommonFunctions(this);
		
		tvagentname=(TextView)findViewById(R.id.reporttoit_agentname);
		lldynamic=(LinearLayout)findViewById(R.id.reporttoit_dynamiclayout);
		etmailid=(EditText)findViewById(R.id.reporttoit_contactmailid);
		etcontactno=(EditText)findViewById(R.id.reporttoit_contactno);
		etcomments=(EditText)findViewById(R.id.reporttoit_comments);
		tvpercentage=(TextView)findViewById(R.id.reporttoit_tvpercentage);
		
		cf.CreateTable(1);
		Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			agentname = cf
					.decode(cur.getString(cur.getColumnIndex("FirstName")));
			agentname += " "
					+ cf.decode(cur.getString(cur.getColumnIndex("LastName")));
			agentid=cf.decode(cur.getString(cur.getColumnIndex("Agentid")));
			agentphone=cf.decode(cur.getString(cur.getColumnIndex("AgencyWorkPhone")));
			agentemail=cf.decode(cur.getString(cur.getColumnIndex("Email")));
			agency_name=cf.decode(cur.getString(cur
					.getColumnIndex("AgencyName")));
		}
		
		tvagentname.setText(agentname);
//		etcontactno.setText(agentphone);
		etmailid.setText(agentemail);
		
		if(agentphone.equals("N/A")||
				agentphone.equals("()-")||
				agentphone.toLowerCase().equals("null")||
				agentphone.equals(null)||
				agentphone.equals(""))
		{
			etcontactno.setText("");
		}
		else
		{
			etcontactno.setText(agentphone);
		}
		
		// get application version
		 try {
				
			 version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 //get application version
		cf.Device_Information();
		
		etmailid.addTextChangedListener(new CustomTextWatcher(etmailid));
//		etcomments.addTextChangedListener(new CustomTextWatcher(etcomments));
		
		etcomments.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (etcomments.getText().toString().startsWith(" "))
		        {
		            // Not allowed
					etcomments.setText("");
		        }
				int length=etcomments.getText().toString().length();
				if(length==0)
				{
					tvpercentage.setText("0%");
				}
				else
				{
					length=length/2;
					tvpercentage.setText(String.valueOf(length)+"%");
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		etcontactno.setLongClickable(false);
		
		etcontactno.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (etcontactno.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etcontactno.setText("");
		        }
				if (etcontactno.getText().toString().trim().matches("^0") )
	            {
	                // Not allowed
					etcontactno.setText("");
	            }

				etcontactno.setOnKeyListener(new OnKeyListener() {

					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						// TODO Auto-generated method stub
						if (keyCode == KeyEvent.KEYCODE_DEL)
						{
							keyDel = 1;
							System.out.println("Inside delete");
							
							String str = etcontactno.getText().toString();
							int len=str.length();
							
							if(len==9)
							{
								str = str.substring(0, str.length()-1);
								etcontactno.setText(str);
								etcontactno.setSelection(str.length());
							}
							if(len==5)
							{
//								str = str.substring(0, str.length()-2);
								str = str.substring(0, str.length()-1);
								etcontactno.setText(str);
								etcontactno.setSelection(str.length());
							}
							if(len==1)
							{
								str = str.substring(0, str.length()-1);
								etcontactno.setText(str);
								etcontactno.setSelection(str.length());
							}
							
							return false;
						}
						else
						{
							keyDel = 0;
							return false;
						}
					}
				});

				if (keyDel == 0) {
					String a = "";
					String str = etcontactno.getText().toString();
					String replaced = str.replaceAll(Pattern.quote("("), "");
					replaced = replaced.replaceAll(Pattern.quote("-"), "");
					replaced = replaced.replaceAll(Pattern.quote(")"), "");
//					replaced = replaced.replaceAll(Pattern.quote(" "), "");
					char[] id_char = replaced.toCharArray();
					id_len = replaced.length();
					System.out.println("The length is "+id_len);
					for (int i = 0; i < id_len; i++) {
						if (i == 0) {
							a = "(" + id_char[i];
						} else if (i == 2) {
//							a += id_char[i] + ") ";
							a += id_char[i] + ")";
						} else if (i == 5) {
							a += id_char[i] + "-";
						} else
							a += id_char[i];
							keyDel = 0;
					}
					etcontactno.removeTextChangedListener(this);
					etcontactno.setText(a);
					if (before > 0)
						etcontactno.setSelection(start);
					else
						etcontactno.setSelection(a.length());
					etcontactno.addTextChangedListener(this);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		
	}
	
	public void clicker(View v)
	{
		switch(v.getId())
		{
			case R.id.reporttoit_home:
				Intent home=new Intent(ReportToIt.this,HomeScreen.class);
				startActivity(home);
				finish();
				break;
				
			case R.id.reporttoit_submit:
				check_validation();
				break;
				
			case R.id.reporttoit_cancel:
				Intent intent = getIntent();
			    overridePendingTransition(0, 0);
			    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			    finish();

			    overridePendingTransition(0, 0);
			    startActivity(intent);
				break;
				
			case R.id.reporttoit_browse:
				count=lldynamic.getChildCount();
				if(count<3)
				{
					Intent i = new Intent(Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
					i.setType("image/*");
					startActivityForResult(i, 0);
				}
				else
				{
					toast=new ShowToast(ReportToIt.this, "Maximum image can be uploaded is 3");
				}
				break;
				
		}
	}
	
	private void check_validation()
	{
		System.out.println("The image path is "+imagepath);
		/*boolean email,phone;
		if(etmailid.getText().toString().trim().equals(""))
		{
			email=true;
		}
		else
		{
			if(cf.eMailValidation(etmailid.getText().toString().trim()))
			{
				email=true;
			}
			else
			{
				email=false;
			}
		}
		if(etcontactno.getText().toString().trim().equals(""))
		{
			phone=true;
		}
		else
		{
			if(etcontactno.getText().toString().length()<14)
			{
				phone=false;
			}
			else
			{
				phone=true;
			}
		}*/
		
		if(!etmailid.getText().toString().trim().equals(""))
		{
			if(cf.eMailValidation(etmailid.getText().toString()))
			{
				if(!etcontactno.getText().toString().trim().equals(""))
				{
					if(etcontactno.getText().toString().length()==13)
					{
						if(!etcomments.getText().toString().trim().equals(""))
						{
							send_report();
						}
						else
						{
							toast = new ShowToast(ReportToIt.this,
									"Please enter Comments");
							etcomments.setText("");
							etcomments.requestFocus();
						}
					}
					else
					{
						toast = new ShowToast(ReportToIt.this,
								"Please enter a valid Contact No");
						etcontactno.setText("");
						etcontactno.requestFocus();
					}
				}
				else
				{
					toast = new ShowToast(ReportToIt.this,
							"Please enter Contact No");
					etcontactno.setText("");
					etcontactno.requestFocus();
				}
			}
			else
			{
				toast = new ShowToast(ReportToIt.this,
						"Please enter a valid Contact Mail Id");
				etmailid.setText("");
				etmailid.requestFocus();
			}
		}
		else
		{
			toast = new ShowToast(ReportToIt.this,
					"Please enter a Contact Mail Id");
			etmailid.setText("");
			etmailid.requestFocus();
		}
		
		/*if(email)
		{
			if(phone)
			{
				send_report();
//				toast = new ShowToast(ReportToIt.this,
//						"Success");
			}
			else
			{
				toast = new ShowToast(ReportToIt.this,
						"Please enter a valid phone number");
				etcontactno.setText("");
				etcontactno.requestFocus();
			}
		}
		else
		{
			toast = new ShowToast(ReportToIt.this,
					"Please enter a valid email id");
			etmailid.setText("");
			etmailid.requestFocus();
		}*/
		
	}
	
	private void send_report()
	{
			
			if (cf.isInternetOn() == true) {
				String source = "<b><font color=#00FF33>" + "Reporting to IT... Please wait."
						+ "</font></b>";
				final ProgressDialog pd = ProgressDialog.show(ReportToIt.this,
						"", Html.fromHtml(source), true);
				new Thread() {
					String result;
					public void run() {
						Looper.prepare();
						try {
							SoapObject request = new SoapObject(cf.NAMESPACE, "SendReportToIT");
							SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
									SoapEnvelope.VER11);
							envelope.dotNet = true;
							request.addProperty("AgentID",Integer.parseInt(agentid));
							request.addProperty("ApplicationVersion",version);
							request.addProperty("DeviceName",cf.model);
							request.addProperty("APILevel",cf.apiLevel);
							request.addProperty("AgentName",agentname);
							request.addProperty("AgencyName",agency_name);
							request.addProperty("Comments",etcomments.getText().toString());
							request.addProperty("ContactNumber",etcontactno.getText().toString());
							request.addProperty("ContactEmail",etmailid.getText().toString());
							request.addProperty("ReportDate",cf.datewithtime);
							
							if (imagepath.equals("")) {
								image1 = "";
								image2 = "";
								image3 = "";
								imagename1 = "";
								imagename2 = "";
								imagename3 = "";
								
								request.addProperty("imgByte1",image1);
								request.addProperty("ImageNameWithExtension1",imagename1);
								request.addProperty("imgByte2",image2);
								request.addProperty("ImageNameWithExtension2",imagename2);
								request.addProperty("imgByte3",image3);
								request.addProperty("ImageNameWithExtension3",imagename3);
							}
							else
							{
								String[] imagepathsplit=imagepath.split(":");
								System.out.println("the length is "+imagepathsplit.length);
								if(imagepathsplit.length==1)
								{
									image1 = imagepathsplit[0];
//									image1=image1.substring(0, image1.length()-1);
									String[] filenamesplit = image1.split("/");
									imagename1 = filenamesplit[filenamesplit.length - 1];
									
									Bitmap bitmap = cf.ShrinkBitmap(image1, 400, 400);
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 0, out);
									byte[] raw = out.toByteArray();
									
									image2 = "";
									image3 = "";
									imagename2 = "";
									imagename3 = "";
									
									request.addProperty("imgByte1",raw);
									request.addProperty("ImageNameWithExtension1",imagename1);
									request.addProperty("imgByte2",image2);
									request.addProperty("ImageNameWithExtension2",imagename2);
									request.addProperty("imgByte3",image3);
									request.addProperty("ImageNameWithExtension3",imagename3);
									
								}
								else if(imagepathsplit.length==2)
								{
									image1 = imagepathsplit[0];
//									image1=image1.substring(0, image1.length()-1);
									String[] filenamesplit = image1.split("/");
									imagename1 = filenamesplit[filenamesplit.length - 1];
									
									Bitmap bitmap = cf.ShrinkBitmap(image1, 400, 400);
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 0, out);
									byte[] raw = out.toByteArray();
									
									image2 = imagepathsplit[1];
//									image2=image2.substring(0, image2.length()-1);
									String[] filenamesplit1 = image2.split("/");
									imagename2 = filenamesplit1[filenamesplit1.length - 1];
									
									Bitmap bitmap1 = cf.ShrinkBitmap(image2, 400, 400);
									ByteArrayOutputStream out1 = new ByteArrayOutputStream();
									bitmap1.compress(CompressFormat.PNG, 0, out1);
									byte[] raw1 = out1.toByteArray();
									
									image3 = "";
									imagename3 = "";
									
									request.addProperty("imgByte1",raw);
									request.addProperty("ImageNameWithExtension1",imagename1);
									request.addProperty("imgByte2",raw1);
									request.addProperty("ImageNameWithExtension2",imagename2);
									request.addProperty("imgByte3",image3);
									request.addProperty("ImageNameWithExtension3",imagename3);
								}
								
								else if(imagepathsplit.length==3)
								{
									image1 = imagepathsplit[0];
//									image1=image1.substring(0, image1.length()-1);
									String[] filenamesplit = image1.split("/");
									imagename1 = filenamesplit[filenamesplit.length - 1];
									
									Bitmap bitmap = cf.ShrinkBitmap(image1, 400, 400);
									ByteArrayOutputStream out = new ByteArrayOutputStream();
									bitmap.compress(CompressFormat.PNG, 0, out);
									byte[] raw = out.toByteArray();
									
									image2 = imagepathsplit[1];
//									image2=image2.substring(0, image2.length()-1);
									String[] filenamesplit1 = image2.split("/");
									imagename2 = filenamesplit1[filenamesplit1.length - 1];
									
									Bitmap bitmap1 = cf.ShrinkBitmap(image2, 400, 400);
									ByteArrayOutputStream out1 = new ByteArrayOutputStream();
									bitmap1.compress(CompressFormat.PNG, 0, out1);
									byte[] raw1 = out1.toByteArray();
									
									image3 = imagepathsplit[2];
//									image3=image3.substring(0, image3.length()-1);
									String[] filenamesplit2 = image3.split("/");
									imagename3 = filenamesplit2[filenamesplit2.length - 1];
									
									Bitmap bitmap2 = cf.ShrinkBitmap(image3, 400, 400);
									ByteArrayOutputStream out2 = new ByteArrayOutputStream();
									bitmap2.compress(CompressFormat.PNG, 0, out2);
									byte[] raw2 = out2.toByteArray();
									
									System.out.println("Image1 "+raw);
									System.out.println("Image name1 "+imagename1);
									System.out.println("Image2 "+raw1);
									System.out.println("Image name2 "+imagename2);
									System.out.println("Image3 "+raw2);
									System.out.println("Image name3 "+imagename3);
									
									request.addProperty("imgByte1",raw);
									request.addProperty("ImageNameWithExtension1",imagename1);
									request.addProperty("imgByte2",raw1);
									request.addProperty("ImageNameWithExtension2",imagename2);
									request.addProperty("imgByte3",raw2);
									request.addProperty("ImageNameWithExtension3",imagename3);
									
								}
							}
							MarshalBase64 marshal = new MarshalBase64();
							envelope.setOutputSoapObject(request);
							marshal.register(envelope);
							System.out.println("SendReportToIT request is" + request);
							HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
							androidHttpTransport.call(cf.NAMESPACE + "SendReportToIT", envelope);
							result = envelope.getResponse().toString();
							/*SoapObject chklogin = cf
									.Calling_WS_ReportToIt(agentid,version,cf.model,cf.apiLevel,agentname,
											etcomments.getText().toString(),etcontactno.getText().toString(),
											etmailid.getText().toString(),cf.datewithtime,image1,imagename1,
											image2,imagename2,image3,imagename3,"SendReportToIT");*/
							System.out.println("response SendReportToIT "
									+ result);
							show_handler = 5;
							handler.sendEmptyMessage(0);
						} catch (SocketException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (XmlPullParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 4;
							handler.sendEmptyMessage(0);

						}

					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							// pd.dismiss();
							pd.dismiss();
							if (show_handler == 3) {
								show_handler = 0;
								toast = new ShowToast(
										ReportToIt.this,
										"There is a problem on your Network. Please try again later with better Network.");

							} else if (show_handler == 4) {
								show_handler = 0;
								toast = new ShowToast(
										ReportToIt.this,
										"There is a problem on your application. Please contact Paperless administrator.");

							} else if (show_handler == 5) {
								show_handler = 0;
								
								if(result.toLowerCase().equals("true"))
								{
									toast = new ShowToast(
											ReportToIt.this,
											"Thank you for submitting your IT issue. We will review this and get back to you as soon as possible.");
									
									Intent home=new Intent(ReportToIt.this,HomeScreen.class);
									startActivity(home);
									finish();
									
								}
								else
								{
									toast = new ShowToast(
											ReportToIt.this,
											"There is a problem on your application. Please contact Paperless administrator.");
								}
								
							}
						}
					};
				}.start();

			} else {
				toast = new ShowToast(ReportToIt.this,
						"Internet connection not available");

			}
//		}
		
	}
	
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
//		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==0&&resultCode==RESULT_OK)
		{
			Bitmap bitmapdb=null;
			Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String filePath = cursor.getString(columnIndex);
				
				if(imagepath.contains(filePath))
				{
					toast=new ShowToast(ReportToIt.this, "Your image has been selected already");
				}
				else
				{

				File f = new File(filePath);
				double length = f.length();
				double kb = length / 1024;
				double mb = kb / 1024;
				System.out.println("The file length is" + length);
				System.out.println("The file length in kb is" + kb);
				System.out.println("The file length in mb is" + mb);

				if (mb >= 2) {
					toast = new ShowToast(ReportToIt.this,
							"File too large to attach");
				} else {
					try
					{
					bitmapdb = BitmapFactory.decodeFile(filePath);
					System.out.println("The bitmap is "+bitmapdb);
					if(bitmapdb==null)
					{
						toast = new ShowToast(ReportToIt.this,
								"File corrupted cant able to attach");
					}
					else
					{
							Call_Dynamic_Layout(filePath);
					}
				}
				catch (OutOfMemoryError e) {
					// TODO: handle exception
					toast=new ShowToast(ReportToIt.this, "You cannot upload this image");
				}
				}
			}
			
		}
	}
	
	private void Call_Dynamic_Layout(final String filePath)
	{
		imagepath +=filePath+":";
		System.out.println("The image path is "+imagepath);
		Bitmap bitmapdb = cf.ShrinkBitmap(filePath, 100, 100);
		
		LinearLayout.LayoutParams llparams;
		
		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getMetrics(displayMetrics);

		int width = displayMetrics.widthPixels;
		int height = displayMetrics.heightPixels;
		
		if (width > 1023 || height > 1023) {
			llparams=new LinearLayout.LayoutParams(100,100);
			llparams.setMargins(10, 10, 10, 10);
		}
		else
		{
			llparams=new LinearLayout.LayoutParams(100,100);
			llparams.setMargins(10, 10, 10, 10);
		}
		final RelativeLayout rl=new RelativeLayout(this);
		rl.setLayoutParams(llparams);
		lldynamic.addView(rl);
		
		RelativeLayout.LayoutParams rlparams = new RelativeLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);
		
		ImageView iv=new ImageView(this);
		iv.setLayoutParams(rlparams);
		iv.setImageBitmap(bitmapdb);
		rl.addView(iv);
		
		RelativeLayout.LayoutParams rlparams1 = new RelativeLayout.LayoutParams(
				20,20);
		rlparams1.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		rlparams1.leftMargin=80;
		
		Button btn=new Button(this);
		btn.setLayoutParams(rlparams1);
		btn.setBackgroundResource(R.drawable.iconcross);
		rl.addView(btn);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				lldynamic.removeView(rl);
				imagepath=imagepath.replace(filePath+":", "");
				System.out.println("The image path is "+imagepath);
			}
		});
		
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
		Intent home=new Intent(ReportToIt.this,HomeScreen.class);
		startActivity(home);
		finish();
	}
	
	
	
}
