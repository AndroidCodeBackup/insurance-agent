package idsoft.agentmodule;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Calendar;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HomeScreen extends Activity {
	Dialog dialog, dialog1;
	String sel_inspection, username, password, agentid, agentname, agency_name,
			strheadshot, strlogo, stremail = "", strcomments, emailchecker,
			agentemail = "", agencyemail = "", customermailid = "", srid,
			filename, emailaddress = "", emailstatus;
	CommonFunctions cf;
	ShowToast toast;
	int show_handler;
	TextView name, agencyname, selectedtext,tvversinname,tvdateinformation;
	EditText emailid, comments;
	ImageView headshot, agencylogo;
	public ProgressThread progThread;
	ProgressDialog progDialog;
	int vcode;
	static LinearLayout llemailid;
	AlarmManager am;
	CheckBox ckagency, ckagent, ckcustomer, ckattachreport;
	LinearLayout llcustomer, llpolicyno, llmailid, lldownloadreport;
	TextView tvclickhere; // tvmailid,
	AutoCompleteTextView etpolicyno;
	EditText etcomments, etagencymail, etagentmail, etcustomermail;
	Button btngetmailid, btnsendemail, btncancel;
	SoapObject chklogin;
	boolean report = false;
	String versionname, newversionname, newversioncode;
	AlertDialog alertDialog;
	ProgressDialog progressDialog1;
	Animation animation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getMetrics(displayMetrics);

		int width = displayMetrics.widthPixels;
		int height = displayMetrics.heightPixels;

		System.out.println("The width is " + width);
		System.out.println("The height is " + height);

		// if(width > 480 && width < 801)
		// {
		// setContentView(R.layout.homescreen_large);
		// }
		// else
		// {
		setContentView(R.layout.homescreen);
		// }

		cf = new CommonFunctions(this);
		
		ImageView iv = (ImageView) findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);
		am = (AlarmManager) getSystemService(HomeScreen.this.ALARM_SERVICE);

		progDialog = new ProgressDialog(this);
		vcode = getcurrentversioncode();
		cf.versionname = cf.getdeviceversionname();
		
		//Animation code
		
		animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
	    animation.setDuration(500); // duration - half a second
	    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
	    animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
	    animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in
	    
	    //End of Animation code
		
		name = (TextView) findViewById(R.id.homescreen_name);
		tvdateinformation = (TextView) findViewById(R.id.homescreen_tvdateinformation);
		tvversinname = (TextView) findViewById(R.id.homescreen_tvversioninformation);
		agencyname = (TextView) findViewById(R.id.homescreen_agency);
		headshot = (ImageView) findViewById(R.id.homescreen_headshot);
		agencylogo = (ImageView) findViewById(R.id.homescreen_agencylogo);
		cf.CreateTable(1);
		Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			agentname = cf
					.decode(cur.getString(cur.getColumnIndex("FirstName")));
			agentname += " "
					+ cf.decode(cur.getString(cur.getColumnIndex("LastName")));
			agency_name = cf.decode(cur.getString(cur
					.getColumnIndex("AgencyName")));
			agency_name=cf.decode(cur.getString(cur
					.getColumnIndex("AgencyName")));
			String agentid = cf.decode(cur.getString(cur.getColumnIndex("Agentid")));
			agentemail = cf.decode(cur.getString(cur.getColumnIndex("Email")));
			System.out.println("Agent Email " + agentemail);
			System.out.println("Agency Email " + agencyemail);
			agencyemail = cf.decode(cur.getString(cur
					.getColumnIndex("AgencyEmail")));
			try
			{
				File outputFile = new File(this.getFilesDir()+"/"+agentid+".png");
				
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile),
						null, o);
				final int REQUIRED_SIZE = 200;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						outputFile), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				
				headshot.setImageDrawable(bmd);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			
			try
			{
				File outputFile = new File(this.getFilesDir()+"/"+agentid+"Agency"+".png");
				
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inJustDecodeBounds = true;
				BitmapFactory.decodeStream(new FileInputStream(outputFile),
						null, o);
				final int REQUIRED_SIZE = 200;
				int width_tmp = o.outWidth, height_tmp = o.outHeight;
				int scale = 1;
				while (true) {
					if (width_tmp / 2 < REQUIRED_SIZE
							|| height_tmp / 2 < REQUIRED_SIZE)
						break;
					width_tmp /= 2;
					height_tmp /= 2;
					scale *= 2;
				}
				BitmapFactory.Options o2 = new BitmapFactory.Options();
				o2.inSampleSize = scale;
				Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(
						outputFile), null, o2);
				BitmapDrawable bmd = new BitmapDrawable(bitmap);
				
				agencylogo.setImageDrawable(bmd);
			}
			catch (Exception e) {
				// TODO: handle exception
			}
			

//			headshot.setImageBitmap(head_shot);
//			agencylogo.setImageBitmap(agency_logo);

//			new DownloadImageTask(headshot).execute(strheadshot);
//			new DownloadImageTask(agencylogo).execute(strlogo);

		}
		cur.close();
		name.setText(agentname);
		agencyname.setText(agency_name);

		cf.CreateTable(1);
		Cursor cur1 = cf.db.rawQuery("select * from " + cf.LoginPage, null);
		cur1.moveToFirst();
		if (cur1.getCount() >= 1) {
			cf.agentid = cf.decode(cur1.getString(cur1
					.getColumnIndex("agentid")));
		}
		cur1.close();
		
		tvversinname.setText("Version "+cf.versionname);
//		tvdateinformation.setText(cf.datewithtime);
		Thread myThread = null;

	    Runnable myRunnableThread = new CountDownRunner();
	    myThread= new Thread(myRunnableThread);   
	    myThread.start();
//		Display_Date_Information();
	    
	    cf.CreateTable(25);
	    Cursor versioncur=cf.db.rawQuery("select * from "+cf.Version, null);
	    
	    if (versioncur.getCount()>=1) {//cf.value == true
	    	versioncur.moveToFirst();
	    	String versioncode=versioncur.getString(versioncur.getColumnIndex("VersionCode"));
	    	if(Integer.parseInt(versioncode)>getcurrentversioncode())
	    	{
	    		System.out.println("Update available");
				// update available
				//Animation code
				
				final ImageView btn = (ImageView) findViewById(R.id.homescreen_checkforupdates);
			    btn.startAnimation(animation);
			    
			    //End of Animation code
	    	}
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.homescreen_completed:
			Intent in = new Intent(HomeScreen.this,CompletedInspection.class);
			startActivity(in);
			finish();
			break;
		case R.id.homescreen_orderinspection:
			final Dialog dialog = new Dialog(HomeScreen.this,
					android.R.style.Theme_Translucent_NoTitleBar);
			// dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.getWindow().setContentView(R.layout.placeorder_confirmation);
			Button yes=(Button)dialog.findViewById(R.id.placeorder_confirmation_yes);
			Button no=(Button)dialog.findViewById(R.id.placeorder_confirmation_no);
			
			yes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
					Intent orderinsp = new Intent(HomeScreen.this,
							OrderInspection.class);
					startActivity(orderinsp);
					finish();
				}
			});
			
			no.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog.dismiss();
				}
			});
			
			dialog.setCancelable(false);
			dialog.show();
			
			break;

		case R.id.homescreen_searchorder:
//			cf.CreateTable(2);
//			Cursor cur1 = cf.db.rawQuery("select * from " + cf.Customer_Details, null);
//			int count1 = cur1.getCount();
//			if (count1 == 0) {
//				toast = new ShowToast(HomeScreen.this,
//						"No records available");
//			} else {
			
			cf.CreateTable(2);
			Cursor cur1 = cf.db.rawQuery("select * from " + cf.DashBoard, null);
			int count1 = cur1.getCount();
			if (count1 == 0) {
				toast = new ShowToast(HomeScreen.this,
						"No records available , Please click SEND/RECEIVE to import data");
			} else {
				cur1.moveToFirst();
				String inspname=cf.decode(cur1.getString(cur1.getColumnIndex("inspname")));
				if(inspname.toUpperCase().equals("N/A"))
				{
					toast = new ShowToast(HomeScreen.this,
							"No records available");
				}
				else
				{
				Intent searchord = new Intent(HomeScreen.this,
						SearchOrder.class);
				startActivity(searchord);
				finish();
			}
			}
			cur1.close();
			break;

		case R.id.homescreen_agentinspection:
			Intent agentinsp = new Intent(HomeScreen.this,
					AgentInspection2.class);
			startActivity(agentinsp);
			finish();
			break;

		case R.id.homescreen_dashboard:
			cf.CreateTable(2);
			Cursor cur = cf.db.rawQuery("select * from " + cf.DashBoard, null);
			int count = cur.getCount();
			if (count == 0) {
				toast = new ShowToast(HomeScreen.this,
						"No records available , Please click SEND/RECEIVE to import data");
			} else {
				cur.moveToFirst();
				String inspname=cf.decode(cur.getString(cur.getColumnIndex("inspname")));
				if(inspname.toUpperCase().equals("N/A"))
				{
					toast = new ShowToast(HomeScreen.this,
							"No records available");
				}
				else
				{
					DashBoard.spinner_selection = 0;
					Intent dashboar = new Intent(HomeScreen.this, DashBoard.class);
					startActivity(dashboar);
					finish();
				}
				
			}
			cur.close();
			break;

		case R.id.homescreen_logout:
			
			Delete_All_Tables();

//			Canncel_Push_Notification();

			Intent logo = new Intent(HomeScreen.this, LoginPage.class);
			startActivity(logo);
			finish();
			break;

		case R.id.homescreen_pushtoemailreport:
			// push_to_email_report();
			// push_to_email_report1();
			
			cf.CreateTable(2);
			Cursor cur2 = cf.db.rawQuery("select * from " + cf.DashBoard, null);
			int count2 = cur2.getCount();
			if (count2 == 0) {
				toast = new ShowToast(HomeScreen.this,
						"No records available , Please click SEND/RECEIVE to import data");
			} else {
				cur2.moveToFirst();
				String inspname=cf.decode(cur2.getString(cur2.getColumnIndex("inspname")));
				if(inspname.toUpperCase().equals("N/A"))
				{
					toast = new ShowToast(HomeScreen.this,
							"No records available");
				}
				else
				{
					Intent intent_emailreport = new Intent(HomeScreen.this,
							EmailReport.class);
					startActivity(intent_emailreport);
					finish();
				}
				
			}
			cur2.close();
			break;

		// case R.id.homescreen_openreport:
		// toast = new ShowToast(HomeScreen.this,
		// "This Tab Is Under Construction");
		// break;
		//
		// case R.id.homescreen_closedreport:
		// toast = new ShowToast(HomeScreen.this,
		// "This Tab Is Under Construction");
		// break;

		case R.id.homescreen_resetpassword:
			password_reset();
			break;

		case R.id.homescreen_import:
			// Intent importingdata = new Intent(HomeScreen.this, Import.class);
			// startActivity(importingdata);
			// finish();
			import_data();
			break;

		case R.id.homescreen_checkforupdates:
			if(animation.hasStarted())
			{
				final ImageView btn = (ImageView) findViewById(R.id.homescreen_checkforupdates);
			    btn.clearAnimation();
//				Update_Alert();
				Update_Alert_Market();
			}
			else
			{
				check_for_updates();
			}
			
			break;

		case R.id.homescreen_reporttoit:
			report_to_it();
			break;
			
		case R.id.homescreen_vehicleinspection:
			Intent intentvehicleinspection=new Intent(HomeScreen.this,VehicleInspection.class);
			startActivity(intentvehicleinspection);
			finish();
			break;
			
		case R.id.homescreen_headshot:
			Bitmap bitmap = ((BitmapDrawable)headshot.getDrawable()).getBitmap();
			Show_Image(bitmap);
			break;
			
		case R.id.homescreen_agencylogo:
			Bitmap bitmap1 = ((BitmapDrawable)agencylogo.getDrawable()).getBitmap();
			Show_Image(bitmap1);
			break;
			
		}
	}
	
	private void Show_Image(final Bitmap bitmap)
	{
		final Dialog dialog = new Dialog(HomeScreen.this,
				android.R.style.Theme_Translucent_NoTitleBar);
		// dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setContentView(R.layout.headshot_logo_show);
		ImageView close=(ImageView)dialog.findViewById(R.id.headshot_logo_show_ivclose);
		final ImageView image=(ImageView)dialog.findViewById(R.id.headshot_logo_show_ivimage);
		
		image.setImageBitmap(bitmap);
		
		close.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.setCancelable(false);
		dialog.show();
	}

	private void check_for_updates() {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Checking for Updates..."
					+ " Please wait.</font></b>";
			final ProgressDialog pd = ProgressDialog.show(HomeScreen.this, "",
					Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			new Thread() {
//				boolean value;

				public void run() {
					Looper.prepare();
					try {
						cf.value = getversioncodefromweb();
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketTimeoutException s) {
						System.out.println("s " + s.getMessage());
						cf.usercheck = 2;
						cf.total_bar = 100;

					} catch (NetworkErrorException n) {
						System.out.println("n " + n.getMessage());
						cf.usercheck = 2;
						cf.total_bar = 100;

					} catch (IOException io) {
						System.out.println("io " + io.getMessage());
						cf.usercheck = 2;
						cf.total_bar = 100;

					} catch (XmlPullParserException x) {
						System.out.println("x " + x.getMessage());
						cf.usercheck = 2;
						cf.total_bar = 100;

					} catch (Exception e) {
						System.out.println("e " + e.getMessage());
						cf.usercheck = 3;
						cf.total_bar = 100;

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									HomeScreen.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									HomeScreen.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;

							System.out.println("value is " + cf.value);
							if (cf.value == true) {
								// No update available
								toast = new ShowToast(HomeScreen.this,
										"No update available");
							} else {
								System.out.println("Inside else");
								// update available
//								Update_Alert();
								Update_Alert_Market();
							}

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(HomeScreen.this,
					"Internet connection not available");

		}
	}

	public boolean getversioncodefromweb() throws NetworkErrorException,
			SocketTimeoutException, IOException, XmlPullParserException,
			Exception {
		SoapObject request = new SoapObject(cf.NAMESPACE,
				"GetVersionInformation");
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		System.out.println("The request for version information is " + request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
		androidHttpTransport.call(cf.NAMESPACE + "GetVersionInformation",
				envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		System.out.println("The result for version information is " + result);
		SoapObject obj = (SoapObject) result.getProperty(0);
		newversioncode = String.valueOf(obj.getProperty("VersionCode"));
		newversionname = String.valueOf(obj.getProperty("VersionName"));
		if (!"null".equals(newversioncode) && null != newversioncode
				&& !newversioncode.equals("")) {
			if (Integer.toString(vcode).equals(newversioncode)) {
								return true;
			} else {
				try {
					cf.CreateTable(25);
					Cursor c12 = cf.db.rawQuery("select * from " + cf.Version,
							null);

					if (c12.getCount() < 1) {
						try {
							cf.db.execSQL("INSERT INTO " + cf.Version
									+ "(VersionCode,VersionName)"
									+ " VALUES ('" + newversioncode + "','"
									+ newversionname + "')");

						} catch (Exception e) {

						}

					} else {
						try {
							cf.db.execSQL("UPDATE " + cf.Version
									+ " set VersionCode='" + newversioncode
									+ "',VersionName='" + newversionname
									+ "'");
						} catch (Exception e) {

						}

					}
					return false;
				} catch (Exception e) {
					return true;

				}

			}
		} else {
			return true;
		}

	}

	private void Update_Alert_Market() {
		
		cf.CreateTable(25);
	    Cursor versioncur=cf.db.rawQuery("select * from "+cf.Version, null);
	    if(versioncur.getCount()>=1)
	    {
	    	versioncur.moveToFirst();
	    	newversionname=versioncur.getString(versioncur.getColumnIndex("VersionName"));
	    }
		
		alertDialog = new AlertDialog.Builder(HomeScreen.this).create();
		alertDialog
				.setMessage("There is a New Version ( "
						+ newversionname
						+ " ) of Agent available. Update now to get the Latest Features and Improvements. ");
		alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(
								Intent.ACTION_VIEW,
								Uri.parse("https://play.google.com/store/apps/details?id=idsoft.agentmodule")));
					}
				});
		alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});

		alertDialog.show();
	}

	private void Update_Alert() {
		alertDialog = new AlertDialog.Builder(HomeScreen.this).create();
		alertDialog
				.setMessage("There is a New Version ( "
						+ newversionname
						+ " ) of Agent available. Update now to get the Latest Features and Improvements. ");
		alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						try {
							if (alertDialog.isShowing()) {

								alertDialog.cancel();
								
								alertDialog.dismiss();
								
								cf.value=true;

							}
							progressDialog1 = ProgressDialog
									.show(HomeScreen.this,
											"",
											"Please wait  Upgrading Version  "
													+ newversionname
													+ " of Agent will take few minutes ...");

							new Thread() {

								public void run() {
									Looper.prepare();
									try {
										Update();
//										show_handler = 5;
//										handler.sendEmptyMessage(0);
									} catch (SocketException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler = 3;
										handler.sendEmptyMessage(0);

									} catch (NetworkErrorException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler = 3;
										handler.sendEmptyMessage(0);

									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler = 3;
										handler.sendEmptyMessage(0);

									} catch (TimeoutException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler = 3;
										handler.sendEmptyMessage(0);

									} catch (XmlPullParserException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler = 3;
										handler.sendEmptyMessage(0);

									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										show_handler = 4;
										handler.sendEmptyMessage(0);

									}

								}

								private Handler handler = new Handler() {
									@Override
									public void handleMessage(Message msg) {
										progressDialog1.dismiss();
										if (show_handler == 3) {
											show_handler = 0;
											toast = new ShowToast(
													HomeScreen.this,
													"There is a problem on your Network. Please try again later with better Network.");

										} else if (show_handler == 4) {
											show_handler = 0;
											toast = new ShowToast(
													HomeScreen.this,
													"There is a problem on your application. Please contact Paperless administrator.");

										} else if (show_handler == 5) {
											show_handler = 0;
										}
									}
								};
							}.start();

						} catch (Exception e) {

						}
					}
				});
		alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});

		alertDialog.show();
	}
	
	private void Update()throws IOException,NetworkErrorException,SocketTimeoutException, XmlPullParserException, Exception {
		// TODO Auto-generated method stub
		
			SoapObject webresult;
			SoapObject request = new SoapObject(cf.NAMESPACE, "GETCURRENTAPKFILE");
			
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
		
			HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);

			androidHttpTransport.call(cf.NAMESPACE+"GETCURRENTAPKFILE", envelope);

			Object response = envelope.getResponse();

			byte[] b;
			if (cf.checkresponce(response)) // check the response is valid or
											// not
			{
				b = Base64.decode(response.toString());

				try {
					String PATH = Environment.getExternalStorageDirectory()
							+ "/Download";
					File file = new File(PATH);
					file.mkdirs();

					File outputFile = new File(PATH + "/InsuranceAgent.apk");
					FileOutputStream fileOuputStream = new FileOutputStream(
							outputFile);
					fileOuputStream.write(b);
					fileOuputStream.close();

					Cursor c12 = cf.db.rawQuery("select * from " + cf.Version,
							null);

					if (c12.getCount() < 1) {
						try {
							cf.db.execSQL("INSERT INTO " + cf.Version
									+ "(VersionCode,VersionName)"
									+ " VALUES ('" + newversioncode + "','"
									+ newversionname + "')");

						} catch (Exception e) {

						}

					} else {
						try {
							cf.db.execSQL("UPDATE " + cf.Version
									+ " set VersionCode='" + newversioncode
									+ "',VersionName='" + newversionname
									+ "'");
						} catch (Exception e) {

						}

					}
					progressDialog1.dismiss();
//					Logout();
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.fromFile(new File(Environment
							.getExternalStorageDirectory()
							+ "/Download/"
							+ "InsuranceAgent.apk")),
							"application/vnd.android.package-archive");
					startActivity(intent);
				} catch (IOException e) {
					toast = new ShowToast(
							HomeScreen.this,"Update error!");
				}
			} else {
				progressDialog1.dismiss();
				toast = new ShowToast(
						HomeScreen.this,"There is a problem on your Network.\n Please try again later with better Network.");
			}

		
	}

	private void report_to_it() {
		Intent reporttoit = new Intent(HomeScreen.this, ReportToIt.class);
		startActivity(reporttoit);
		finish();
	}

	private void import_data() {
		if (cf.isInternetOn() == true)/* CHECK FOR INTERNET CONNECTION */
		{
			cf.CreateTable(2);
			cf.db.execSQL("delete from " + cf.DashBoard);
			showDialog(1);
			importing_data();
			// import_customerdetails();
		} else {
			cf.usercheck = 1;
			handler.sendEmptyMessage(0);

		}
	}

	private void import_customerdetails() {
		new Thread() {
			public void run() {
				Looper.prepare();
				try {
					SoapObject request = new SoapObject(cf.NAMESPACE,
							"SearchHomeowner_Agent");
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
							SoapEnvelope.VER11);
					envelope.dotNet = true;
					request.addProperty("AgentID", Integer.parseInt(cf.agentid));
					request.addProperty("OwnerName", "");
					request.addProperty("City", "");
					request.addProperty("State", 0);
					request.addProperty("County", "");
					request.addProperty("Zip", "");
					request.addProperty("StatusID", 0);
					request.addProperty("SubStatusID", 0);
					request.addProperty("FromDate", "");
					request.addProperty("ToDate", "");
					request.addProperty("InspType", 0);
					request.addProperty("PolicyNumber", "");

					envelope.setOutputSoapObject(request);
					//System.out.println("REQUEt " + request);
					HttpTransportSE androidHttpTransport = new HttpTransportSE(
							cf.URL);
					androidHttpTransport.call(cf.NAMESPACE
							+ "SearchHomeowner_Agent", envelope);
					SoapObject result = (SoapObject) envelope.getResponse();
				//	System.out.println("SearchHomeowner_Agent result is"+ result);
					import_customerdetails(result);
					show_handler = 5;
					handler.sendEmptyMessage(0);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
					cf.usercheck = 2;
					cf.total_bar = 100;
					
					show_handler = 3;
					handler.sendEmptyMessage(0);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					
					cf.usercheck = 2;
					cf.total_bar = 100;

					show_handler = 3;
					handler.sendEmptyMessage(0);
				}
			}

			private Handler handler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					if (show_handler == 3) {
						show_handler = 0;
						toast = new ShowToast(
								HomeScreen.this,
								"There is a problem on your Network. Please try again later with better Network.");

					} else if (show_handler == 4) {
						show_handler = 0;
						toast = new ShowToast(
								HomeScreen.this,
								"There is a problem on your application. Please contact Paperless administrator.");

					} else if (show_handler == 5) {
						show_handler = 0;
						// import_customerdetails(result);
						// display();

						// try {
						// cf.total_bar = 75;
						// Insert_into_Dashboardtable(chklogin);
						// cf.total_bar = 100;
						// cf.usercheck = 4;
						// } catch (SocketTimeoutException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// } catch (NetworkErrorException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// } catch (IOException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// } catch (XmlPullParserException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }

					}
				}
			};
		}.start();
	}

	private void import_customerdetails(SoapObject objInsert) {

		cf.CreateTable(22);
		cf.db.execSQL("delete from " + cf.Customer_Details);
		int n = objInsert.getPropertyCount();
		System.out.println("Customer Details property count" + n);
		for (int i = 0; i < n; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String s_SRID = String.valueOf(obj.getProperty("s_SRID"));
				String ownersName = String.valueOf(obj
						.getProperty("ownersName"));
				String Address = String.valueOf(obj.getProperty("Address"));
				String Address2 = String.valueOf(obj.getProperty("Address2"));
				String City = String.valueOf(obj.getProperty("City"));
				String County = String.valueOf(obj.getProperty("County"));
				String State = String.valueOf(obj.getProperty("State"));
				String Zipcode = String.valueOf(obj.getProperty("Zipcode"));
				String PolicyNumber = String.valueOf(obj
						.getProperty("PolicyNumber"));
				String Email = String.valueOf(obj.getProperty("Email"));
				String Inspection_Type = String.valueOf(obj
						.getProperty("Inspection_Type"));
				String RegisterDate = String.valueOf(obj
						.getProperty("RegisterDate"));
				String AssgDate = String.valueOf(obj.getProperty("AssgDate"));
				String InspDate = String.valueOf(obj.getProperty("InspDate"));
				String ScheduledDate = String.valueOf(obj
						.getProperty("ScheduledDate"));
				String AcceptedDate = String.valueOf(obj
						.getProperty("AcceptedDate"));
				String InspectionTypeid = String.valueOf(obj
						.getProperty("InspectionTypeid"));
				String Status = String.valueOf(obj.getProperty("Status"));
				String COMMpdf = String.valueOf(obj.getProperty("COMMpdf"));
				String Phone = String.valueOf(obj.getProperty("Phone"));
				String IMC = String.valueOf(obj.getProperty("IMC"));
				String InspectorName = String.valueOf(obj
						.getProperty("InspectorName"));

				if (Status.equals("ADMIN NEW")
						|| Status.equals("ASSIGNED TO IMC")
						|| Status.equals("ASSIGNED TO INSPECTOR")
						|| Status.equals("PRE INSPECTED")
						|| Status.equals("CARRIER REORDER")
						|| Status.equals("RESEND")) {
					Status = "Awaiting Scheduling";
				} else if (Status.equals("SCHEDULED")) {
					Status = "Scheduled";
				} else if (Status.equals("INSPECTED")
						|| Status.equals("RECEIVED BY IMC")
						|| Status.equals("RECEIVED BY IPA")
						|| Status.equals("IMC REJECTED")
						|| Status.equals("IMC RESUBMITTED")
						|| Status.equals("IPA REJECTED")
						|| Status.equals("IPA RESUBMITTED")
						|| Status.equals("CARRIER REJECTED TPQA")
						|| Status.equals("CARRIER REJECTED NON-TPQA")
						|| Status.equals("CARRIER RESUBMITTED TPQA")
						|| Status.equals("CITIZEN REJECTED")
						|| Status.equals("CITIZEN RESUBMITTED")
						|| Status.equals("CARRIER RESUBMITTED NON-TPQA")) {
					Status = "Inspected (In QA Process)";
				} else if (Status.equals("IMC ACCEPTED")
						|| Status.equals("IPA ACCEPTED")
						|| Status.equals("CARRIER ACCEPTED TPQA")
						|| Status.equals("CARRIER ACCEPTED NON-TPQA")) {
					Status = "Reports Ready";
				} else if (Status.equals("SUSPENDED")
						|| Status
								.equals("CANCELLED-UNABLE TO SCHEDULE BY INSPECTOR")
						|| Status.equals("CANCELLED-UNABLE TO SCHEDULE BY IMC")
						|| Status.equals("CANCELLED-UNABLE TO SCHEDULE BY IPA")
						|| Status.equals("CANCELLED-CLOSED ON CARRIER")
						|| Status.equals("CANCELLED INSPECTION")
						|| Status.equals("DEFERRED") || Status.equals("HOLD")) {
					Status = "Suspended";
				}

				cf.db.execSQL("insert into "
						+ cf.Customer_Details
						+ " (s_SRID,ownersName,Address,Address2,"
						+ "City,County,State,Zipcode,PolicyNumber,Email,Inspection_Type,RegisterDate,AssgDate,"
						+ "InspDate,ScheduledDate,AcceptedDate,InspectionTypeid,Status,COMMpdf,Phone,IMC,InspectorName) values"
						+ "('" + cf.encode(s_SRID) + "','"
						+ cf.encode(ownersName) + "','" + cf.encode(Address)
						+ "','" + cf.encode(Address2) + "','" + cf.encode(City)
						+ "','" + cf.encode(County) + "','" + cf.encode(State)
						+ "','" + cf.encode(Zipcode) + "','"
						+ cf.encode(PolicyNumber) + "','" + cf.encode(Email)
						+ "','" + Inspection_Type.trim() + "','"
						+ cf.encode(RegisterDate) + "','" + cf.encode(AssgDate)
						+ "','" + cf.encode(InspDate) + "','"
						+ cf.encode(ScheduledDate) + "','"
						+ cf.encode(AcceptedDate) + "','"
						+ cf.encode(InspectionTypeid) + "','"
						+ cf.encode(Status) + "','" + cf.encode(COMMpdf)
						+ "','" + cf.encode(Phone) + "','" + cf.encode(IMC)
						+ "','" + cf.encode(InspectorName) + "');");

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		try {
			cf.total_bar = 50;
			Insert_into_Dashboardtable(chklogin);
			// cf.total_bar = 100;
			// cf.usercheck = 4;
		} catch (SocketTimeoutException s) {
			System.out.println("s " + s.getMessage());
			cf.usercheck = 2;
			cf.total_bar = 100;
		} catch (NetworkErrorException n1) {
			System.out.println("n1 " + n1.getMessage());
			cf.usercheck = 2;
			cf.total_bar = 100;
		} catch (IOException io) {
			System.out.println("io " + io.getMessage());
			cf.usercheck = 2;
			cf.total_bar = 100;
		} catch (XmlPullParserException x) {
			System.out.println("x " + x.getMessage());
			cf.usercheck = 2;
			cf.total_bar = 100;
		} catch (Exception e) {
			System.out.println("e " + e.getMessage());
			cf.usercheck = 3;
			cf.total_bar = 100;
		}
	}

	private void importing_data() {

		new Thread() {
			public void run() {
				Looper.prepare();
				try {
					chklogin = cf.Calling_WS_DashBoard(cf.agentid, "All",
							"AgentDashboardDetails");
					//System.out.println("chl " + chklogin);
					cf.total_bar = 25;
					import_customerdetails();
					// cf.total_bar = 75;
					// Insert_into_Dashboardtable(chklogin);
					// cf.total_bar = 100;
					// cf.usercheck = 4;
				} catch (SocketTimeoutException s) {
					System.out.println("s " + s.getMessage());
					cf.usercheck = 2;
					cf.total_bar = 100;

				} catch (NetworkErrorException n) {
					System.out.println("n " + n.getMessage());
					cf.usercheck = 2;
					cf.total_bar = 100;

				} catch (IOException io) {
					System.out.println("io " + io.getMessage());
					cf.usercheck = 2;
					cf.total_bar = 100;

				} catch (XmlPullParserException x) {
					System.out.println("x " + x.getMessage());
					cf.usercheck = 2;
					cf.total_bar = 100;

				} catch (Exception e) {
					System.out.println("e " + e.getMessage());
					cf.usercheck = 3;
					cf.total_bar = 100;

				}

			}
		}.start();

	}

	private void Insert_into_Dashboardtable(SoapObject objInsert)
			throws NetworkErrorException, IOException, XmlPullParserException,
			SocketTimeoutException {
		int Cnt = objInsert.getPropertyCount();
		System.out.println("The Cnt is " + Cnt);
		cf.CreateTable(21);
		cf.db.execSQL("delete from " + cf.Property_Count);
		cf.db.execSQL("insert into " + cf.Property_Count
				+ "(propertycount) values('" + cf.encode(String.valueOf(Cnt))
				+ "')");
		if (Cnt >= 1) {
			for (int i = 0; i < Cnt; i++) {
				SoapObject obj = (SoapObject) objInsert.getProperty(i);
				try {
					cf.inspection_id = String.valueOf(obj
							.getProperty("InspectionID"));
					cf.inspection_name = String.valueOf(obj
							.getProperty("InspName"));
					cf.inspection_type = String.valueOf(obj
							.getProperty("InspType"));
					cf.awaiting = String.valueOf(obj.getProperty("Awaiting"));
					cf.scheduled = String.valueOf(obj.getProperty("Scheduled"));
					cf.inspected = String.valueOf(obj.getProperty("Inspected"));
					cf.reports_ready = String.valueOf(obj
							.getProperty("ReportsReady"));
					cf.suspended = String.valueOf(obj.getProperty("Suspended"));
					cf.unsuspended = String.valueOf(obj
							.getProperty("UnSuspended"));
					cf.other = String.valueOf(obj.getProperty("Other"));
					cf.total = String.valueOf(obj.getProperty("Total"));

					cf.db.execSQL("insert into "
							+ cf.DashBoard
							+ " (inspid,inspname,insptype,awaiting,scheduled,inspected,reportsready,suspended,unsuspended,other,total) values('"
							+ cf.encode(cf.inspection_id) + "','"
							+ cf.encode(cf.inspection_name) + "','"
							+ cf.encode(cf.inspection_type) + "','"
							+ cf.encode(cf.awaiting) + "','"
							+ cf.encode(cf.scheduled) + "','"
							+ cf.encode(cf.inspected) + "','"
							+ cf.encode(cf.reports_ready) + "','"
							+ cf.encode(cf.suspended) + "','"
							+ cf.encode(cf.unsuspended) + "','"
							+ cf.encode(cf.other) + "','" + cf.encode(cf.total)
							+ "');");

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		try {
			cf.total_bar = 75;
			Insert_Into_OnlineList();
			// cf.total_bar = 100;
			// cf.usercheck = 4;
		} catch (Exception e) {
			System.out.println("Insert_Into_OnlineList exception is "
					+ e.getMessage());
			cf.usercheck = 3;
			cf.total_bar = 100;
		}

		// Call_Push_Notification();

	}

	private void Insert_Into_OnlineList() {
		if (cf.isInternetOn() == true) {
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						System.out
								.println("Calling Webservice OnlineSyncPolicyInfo_AgencyID");

						SoapObject chklogin = cf.Calling_WS_OnlineList(cf.agentid, "OnlineSyncPolicyInfo_AgencyID");
						OnlineList(chklogin);
//						cf.total_bar = 100;
//						cf.usercheck = 4;
						System.out.println("response OnlineSyncPolicyInfo_AgencyID"+ chklogin);

					} catch (SocketTimeoutException s) {
						System.out.println("s " + s.getMessage());
						cf.usercheck = 2;
						cf.total_bar = 100;
					} catch (NetworkErrorException n) {
						System.out.println("n " + n.getMessage());
						cf.usercheck = 2;
						cf.total_bar = 100;
					} catch (IOException io) {
						System.out.println("io " + io.getMessage());
						cf.usercheck = 2;
						cf.total_bar = 100;
					} catch (XmlPullParserException x) {
						System.out.println("x " + x.getMessage());
						cf.usercheck = 2;
						cf.total_bar = 100;
					} catch (Exception e) {
						System.out.println("e " + e.getMessage());
						cf.usercheck = 3;
						cf.total_bar = 100;
					}
				}
			}.start();
		} else {
			toast = new ShowToast(HomeScreen.this,
					"Internet connection not available");

		}

	}

	private void OnlineList(SoapObject objInsert) {
		System.out.println("Inside OnlineSyncPolicyInfo "+objInsert);

		cf.CreateTable(3);
		cf.db.execSQL("delete from " + cf.OnlineSyncPolicyInfo_Agency);

		int noofdatas = objInsert.getPropertyCount();
		System.out.println("Property count is" + noofdatas);
		for (int i = 0; i < noofdatas; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			System.out.println("obj-"+obj.toString());
			try {
				String SRID = String.valueOf(obj.getProperty("SRID"));
				/*if(SRID.equals("RI276925"))
						{*/
				String FirstName = String.valueOf(obj.getProperty("FirstName"));
				String LastName = String.valueOf(obj.getProperty("LastName"));
				String MiddleName = String.valueOf(obj
						.getProperty("MiddleName"));
				System.out.println("comesin=i"+i);
				String Address1 = String.valueOf(obj.getProperty("Address1"));
				String Address2 = String.valueOf(obj.getProperty("Address2"));
				String City = String.valueOf(obj.getProperty("City"));
				String StateId = String.valueOf(obj.getProperty("StateId"));
				String State = String.valueOf(obj.getProperty("State"));
				String Country = String.valueOf(obj.getProperty("Country"));
				String Zip = String.valueOf(obj.getProperty("Zip"));System.out.println("zipcomesin=i"+i);
				String HomePhone = String.valueOf(obj.getProperty("HomePhone"));
				String CellPhone = String.valueOf(obj.getProperty("CellPhone"));
				String WorkPhone = String.valueOf(obj.getProperty("WorkPhone"));
				String Email = String.valueOf(obj.getProperty("Email"));
				String ContactPerson = String.valueOf(obj
						.getProperty("ContactPerson"));
				String OwnerPolicyNo = String.valueOf(obj
						.getProperty("OwnerPolicyNo"));
				String Status = String.valueOf(obj.getProperty("Status"));
				String SubStatusID = String.valueOf(obj
						.getProperty("SubStatusID"));
				String statefarmcompanyid = String.valueOf(obj
						.getProperty("statefarmcompanyid"));
				String InspectorId = String.valueOf(obj
						.getProperty("InspectorId"));
				String wId = String.valueOf(obj.getProperty("wId"));
				String CompanyId = String.valueOf(obj.getProperty("CompanyId"));
				String InspectorFirstName = String.valueOf(obj
						.getProperty("InspectorFirstName"));
				String InspectorLastName = String.valueOf(obj
						.getProperty("InspectorLastName"));
				String ScheduledDate = String.valueOf(obj
						.getProperty("ScheduledDate"));
				String YearBuilt = String.valueOf(obj.getProperty("YearBuilt"));
				String Nstories = String.valueOf(obj.getProperty("Nstories"));
				String InspectionTypeId = String.valueOf(obj
						.getProperty("InspectionTypeId"));
				String ScheduleCreatedDate = String.valueOf(obj
						.getProperty("ScheduleCreatedDate"));
				String AssignedDate = String.valueOf(obj
						.getProperty("AssignedDate"));
				String InspectionStartTime = String.valueOf(obj
						.getProperty("InspectionStartTime"));
				String InspectionEndTime = String.valueOf(obj
						.getProperty("InspectionEndTime"));
				String InspectionComment = String.valueOf(obj
						.getProperty("InspectionComment"));
				String IsInspected = String.valueOf(obj
						.getProperty("IsInspected"));System.out.println("uisins=i"+i);
				String InsuranceCompany = String.valueOf(obj
						.getProperty("InsuranceCompany"));System.out.println("copany=i"+i);
				String s_InspFees = String.valueOf(obj
						.getProperty("s_InspFees"));
				String InsuranceAgentName = String.valueOf(obj
						.getProperty("InsuranceAgentName"));
				String AgentEmail = String.valueOf(obj
						.getProperty("AgentEmail"));
				String InsuranceAgencyName = String.valueOf(obj
						.getProperty("InsuranceAgencyName"));
				String AgencyEmail = String.valueOf(obj.getProperty("AgencyEmail"));
				if(AgencyEmail.equals("null"))
				{
					AgencyEmail="";
				}
				String COMMpdf = String.valueOf(obj.getProperty("COMMpdf"));
				String COMMMergedpdf = String.valueOf(obj
						.getProperty("COMMMergedpdf"));
				String RoofPdfPath = String.valueOf(obj
						.getProperty("RoofPdfPath"));
				System.out.println("SRID" + SRID+"i="+i);
						
				cf.db.execSQL("insert into "
						+ cf.OnlineSyncPolicyInfo_Agency
						+ " (SRID,FirstName,LastName,MiddleName,"
						+ "Address1,Address2,"
						+ "City,StateId,State,Country,Zip,HomePhone,CellPhone,WorkPhone,"
						+ "Email,ContactPerson,OwnerPolicyNo,Status,SubStatusID,statefarmcompanyid,"
						+ "InspectorId,wId,CompanyId,InspectorFirstName,InspectorLastName,"
						+ "ScheduledDate,YearBuilt,Nstories,InspectionTypeId,ScheduleCreatedDate,"
						+ "AssignedDate,InspectionStartTime,InspectionEndTime,InspectionComment,"
						+ "IsInspected,InsuranceCompany,s_InspFees,InsuranceAgentName,AgentEmail,"
						+ "InsuranceAgencyName,AgencyEmail,COMMpdf,COMMMergedpdf,RoofPdfPath)values('"
						+ cf.encode(SRID) + "','" + cf.encode(FirstName)
						+ "','" + cf.encode(LastName) + "','"
						+ cf.encode(MiddleName) + "','" + cf.encode(Address1)
						+ "','" + cf.encode(Address2) + "','" + cf.encode(City)
						+ "','" + cf.encode(StateId) + "','" + cf.encode(State)
						+ "','" + cf.encode(Country) + "','" + cf.encode(Zip)
						+ "','" + cf.encode(HomePhone) + "','"
						+ cf.encode(CellPhone) + "','" + cf.encode(WorkPhone)
						+ "','" + cf.encode(Email) + "','"
						+ cf.encode(ContactPerson) + "','"
						+ cf.encode(OwnerPolicyNo) + "','" + cf.encode(Status)
						+ "','" + cf.encode(SubStatusID) + "','"
						+ cf.encode(statefarmcompanyid) + "','"
						+ cf.encode(InspectorId) + "','" + cf.encode(wId)
						+ "','" + cf.encode(CompanyId) + "','"
						+ cf.encode(InspectorFirstName) + "','"
						+ cf.encode(InspectorLastName) + "','" + ScheduledDate
						+ "','" + cf.encode(YearBuilt) + "','"
						+ cf.encode(Nstories) + "','"
						+ cf.encode(InspectionTypeId) + "','"
						+ ScheduleCreatedDate + "','" + AssignedDate + "','"
						+ cf.encode(InspectionStartTime) + "','"
						+ cf.encode(InspectionEndTime) + "','"
						+ cf.encode(InspectionComment) + "','"
						+ cf.encode(IsInspected) + "','"
						+ cf.encode(InsuranceCompany) + "','"
						+ cf.encode(s_InspFees) + "','"
						+ cf.encode(InsuranceAgentName) + "','"
						+ cf.encode(AgentEmail) + "','"
						+ cf.encode(InsuranceAgencyName) + "','"
						+ cf.encode(AgencyEmail) + "','" + cf.encode(COMMpdf)
						+ "','" + cf.encode(COMMMergedpdf) + "','"
						+ cf.encode(RoofPdfPath) + "');");
				/*System.out.println("insert into "
						+ cf.OnlineSyncPolicyInfo_Agency
						+ " (SRID,FirstName,LastName,MiddleName,"
						+ "Address1,Address2,"
						+ "City,StateId,State,Country,Zip,HomePhone,CellPhone,WorkPhone,"
						+ "Email,ContactPerson,OwnerPolicyNo,Status,SubStatusID,statefarmcompanyid,"
						+ "InspectorId,wId,CompanyId,InspectorFirstName,InspectorLastName,"
						+ "ScheduledDate,YearBuilt,Nstories,InspectionTypeId,ScheduleCreatedDate,"
						+ "AssignedDate,InspectionStartTime,InspectionEndTime,InspectionComment,"
						+ "IsInspected,InsuranceCompany,s_InspFees,InsuranceAgentName,AgentEmail,"
						+ "InsuranceAgencyName,AgencyEmail,COMMpdf,COMMMergedpdf,RoofPdfPath)values('"
						+ cf.encode(SRID) + "','" + cf.encode(FirstName)
						+ "','" + cf.encode(LastName) + "','"
						+ cf.encode(MiddleName) + "','" + cf.encode(Address1)
						+ "','" + cf.encode(Address2) + "','" + cf.encode(City)
						+ "','" + cf.encode(StateId) + "','" + cf.encode(State)
						+ "','" + cf.encode(Country) + "','" + cf.encode(Zip)
						+ "','" + cf.encode(HomePhone) + "','"
						+ cf.encode(CellPhone) + "','" + cf.encode(WorkPhone)
						+ "','" + cf.encode(Email) + "','"
						+ cf.encode(ContactPerson) + "','"
						+ cf.encode(OwnerPolicyNo) + "','" + cf.encode(Status)
						+ "','" + cf.encode(SubStatusID) + "','"
						+ cf.encode(statefarmcompanyid) + "','"
						+ cf.encode(InspectorId) + "','" + cf.encode(wId)
						+ "','" + cf.encode(CompanyId) + "','"
						+ cf.encode(InspectorFirstName) + "','"
						+ cf.encode(InspectorLastName) + "','" + ScheduledDate
						+ "','" + cf.encode(YearBuilt) + "','"
						+ cf.encode(Nstories) + "','"
						+ cf.encode(InspectionTypeId) + "','"
						+ ScheduleCreatedDate + "','" + AssignedDate + "','"
						+ cf.encode(InspectionStartTime) + "','"
						+ cf.encode(InspectionEndTime) + "','"
						+ cf.encode(InspectionComment) + "','"
						+ cf.encode(IsInspected) + "','"
						+ cf.encode(InsuranceCompany) + "','"
						+ cf.encode(s_InspFees) + "','"
						+ cf.encode(InsuranceAgentName) + "','"
						+ cf.encode(AgentEmail) + "','"
						+ cf.encode(InsuranceAgencyName) + "','"
						+ cf.encode(AgencyEmail) + "','" + cf.encode(COMMpdf)
						+ "','" + cf.encode(COMMMergedpdf) + "','"
						+ cf.encode(RoofPdfPath) + "');");*/
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.out.println("error="+e.getMessage());
			}
		}
		Cursor cur = cf.db.rawQuery("select * from "
				+ cf.OnlineSyncPolicyInfo_Agency, null);
		int rows = cur.getCount();
		System.out.println("afterins="+rows);
		
		
		try {
			cf.total_bar = 95;
			check_for_updates();
			 cf.total_bar = 100;
			 cf.usercheck = 4;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("e " + e.getMessage());
			cf.usercheck = 3;
			cf.total_bar = 100;
		}

		System.out.println("OnlineList count is " + rows);
	}

	private int getcurrentversioncode() {
		// TODO Auto-generated method stub

		try {
			vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vcode;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case 1:
			progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progDialog.setMax(cf.maxBarValue);
			progDialog.setMessage("Importing please wait :");
			progDialog.setCancelable(false);
			progThread = new ProgressThread(handler1);
			progThread.start();
			return progDialog;
		default:
			return null;
		}
	}

	final Handler handler1 = new Handler() {
		public void handleMessage(Message msg) {
			// Get the current value of the variable total from the message data
			// and update the progress bar.
			int total = msg.getData().getInt("total");
			progDialog.setProgress(total);
			if (total == 100) {
				progDialog.setCancelable(true);
				dismissDialog(cf.typeBar);
				handler.sendEmptyMessage(0);
				progThread.setState(ProgressThread.DONE);
			}

		}
	};
	final Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			if (cf.usercheck == 1) {
				cf.usercheck = 0;
				toast = new ShowToast(HomeScreen.this,
						"Internet connection not available");
				Intent intent = getIntent();
				overridePendingTransition(0, 0);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				finish();

				overridePendingTransition(0, 0);
				startActivity(intent);
			} else if (cf.usercheck == 2) {
				cf.usercheck = 0;
				toast = new ShowToast(
						HomeScreen.this,
						"There is a problem on your network,so please try again later with better network.");
				Intent intent = getIntent();
				overridePendingTransition(0, 0);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				finish();

				overridePendingTransition(0, 0);
				startActivity(intent);
			} else if (cf.usercheck == 3) {
				cf.usercheck = 0;
				toast = new ShowToast(
						HomeScreen.this,
						"There is a problem on your application. Please contact Paperless administrator.");
				Intent intent = getIntent();
				overridePendingTransition(0, 0);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				finish();

				overridePendingTransition(0, 0);
				startActivity(intent);
			} else if (cf.usercheck == 4) {
				cf.usercheck = 0;
				cf.alerttitle = "Import";
				cf.alertcontent = "Data successfully received";
				showalert(cf.alerttitle, cf.alertcontent);
			}

		}

	};

	private class ProgressThread extends Thread {

		// Class constants defining state of the thread
		final static int DONE = 0;

		Handler mHandler;

		ProgressThread(Handler h) {
			mHandler = h;
		}

		@Override
		public void run() {
			cf.mState = cf.RUNNING;
			while (cf.mState == cf.RUNNING) {
				try {
					Thread.sleep(cf.delay);
				} catch (InterruptedException e) {
					Log.e("ERROR", "Thread was Interrupted");
				}

				Message msg = mHandler.obtainMessage();
				Bundle b = new Bundle();
				b.putInt("total", cf.total_bar);
				msg.setData(b);
				mHandler.sendMessage(msg);

			}
		}

		public void setState(int state) {
			cf.mState = state;
		}

	}

	public void showalert(String alerttitle2, String alertcontent2) {
		// TODO Auto-generated method stub
		final Dialog dialog1 = new Dialog(HomeScreen.this,
				android.R.style.Theme_Translucent_NoTitleBar);
		// dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog1.getWindow().setContentView(R.layout.send_receive_alert);
		TextView txt = (TextView) dialog1.findViewById(R.id.alert_txtid);
		txt.setText(Html.fromHtml(alertcontent2));
		Button btn_ok = (Button) dialog1.findViewById(R.id.alert_ok);
		btn_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// finish();
				dialog1.dismiss();
				// HomeScreen.this.startActivity(new Intent(HomeScreen.this,
				// HomeScreen.class));
				// finish();

				Intent intent = getIntent();
				overridePendingTransition(0, 0);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				finish();

				overridePendingTransition(0, 0);
				startActivity(intent);
			}

		});
		dialog1.setCancelable(false);
		dialog1.show();
	}

	private void password_reset() {
		dialog1 = new Dialog(HomeScreen.this,
				android.R.style.Theme_Translucent_NoTitleBar);
		// dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog1.setContentView(R.layout.resetpassword);
		dialog1.setCancelable(true);
		dialog1.setCanceledOnTouchOutside(true);
		dialog1.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		final EditText oldpassword;
		final EditText newpassword;
		final EditText confirmpassword;
		Button submit, cancel;
		oldpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_oldpassword);
		newpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_newpassword);
		confirmpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_confirmpassword);

		oldpassword.addTextChangedListener(new CustomTextWatcher(oldpassword));
		newpassword.addTextChangedListener(new CustomTextWatcher(newpassword));
		confirmpassword.addTextChangedListener(new CustomTextWatcher(
				confirmpassword));

		// cf.CreateTable(0);
		// try {
		// Cursor cur = cf.db.rawQuery("select * from " + cf.LoginPage, null);
		// if (cur.getCount() >= 1) {
		// cur.moveToFirst();
		// oldpassword.setText(cur.getString(cur
		// .getColumnIndex("password")));
		// }
		// cur.close();
		// } catch (Exception e) {
		// // TODO: handle exception
		// }
		ImageView clo = (ImageView) dialog1
				.findViewById(R.id.resetpassword_close);
		submit = (Button) dialog1.findViewById(R.id.resetpassword_submit);
		cancel = (Button) dialog1.findViewById(R.id.resetpassword_cancel);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (oldpassword.getText().toString().equals("")) {
					toast = new ShowToast(HomeScreen.this,
							"Please enter Old Password");
					oldpassword.requestFocus();
				} else {

					if (newpassword.getText().toString().equals("")) {
						toast = new ShowToast(HomeScreen.this,
								"Please enter New Password.");
						newpassword.requestFocus();
					} else {
						
						boolean upperFound = false;
						boolean specialcharacterFound = false;
						boolean numberFound = false;
						
						//Checks for edittext contains a Digit/Number
						String numberchecker=newpassword.getText().toString();
						if(numberchecker.matches(".*\\d.*")){
						    System.out.println("Contains Number");
						    numberFound = true;
						} else{
						    System.out.println("Does not contain a Number");
						    numberFound = false;
						}
						
						//Checks for edittext contains a Special Character
						Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
						Matcher m = p.matcher(newpassword.getText().toString());
						boolean b = m.find();

						if (b)
						{
							System.out.println("There is a special character in my string");
							specialcharacterFound=true;
						}
						else
						{
							System.out.println("There is no special character in my string");
							specialcharacterFound=false;
						}
						
						//Checks for edittext contains a Upper Case Character
						String aString = newpassword.getText().toString();
						for (char c : aString.toCharArray()) {
						    if (Character.isUpperCase(c)) {
						        upperFound = true;
						        System.out.println("contains upper case");
						        break;
						    }
						}
						
						if(numberFound&&specialcharacterFound&&upperFound)
						{
							if (confirmpassword.getText().toString().equals("")) {
								toast = new ShowToast(HomeScreen.this,
										"Please enter Confirm Password.");
								confirmpassword.requestFocus();
							} else {
								if (newpassword
										.getText()
										.toString()
										.equals(confirmpassword.getText()
												.toString())) {
									cf.CreateTable(0);
									Cursor cur = cf.db.rawQuery("select * from "
											+ cf.LoginPage, null);
									cur.moveToFirst();
									if (cur.getCount() >= 1) {
										username = cf.decode(cur.getString(cur
												.getColumnIndex("username")));
										password = cf.decode(cur.getString(cur
												.getColumnIndex("password")));
										System.out.println("The Pass word is "
												+ password);
										agentid = cf.decode(cur.getString(cur
												.getColumnIndex("agentid")));
									}
									cur.close();

									if (!password.equals(oldpassword.getText()
											.toString())) {
										toast = new ShowToast(HomeScreen.this,
												"You have entered wrong password");
										oldpassword.setText("");
										newpassword.setText("");
										confirmpassword.setText("");
										oldpassword.requestFocus();
									} else {
										call_reset_password_webservice(confirmpassword
												.getText().toString());
									}

								} else {
									toast = new ShowToast(HomeScreen.this,
											"Old Password and New Password do not match.");
									newpassword.setText("");
									confirmpassword.setText("");
									newpassword.requestFocus();
								}

							}
						}
						else
						{
							toast = new ShowToast(HomeScreen.this,
									"please enter a valid password.");
							newpassword.setText("");
							newpassword.requestFocus();
						}
						
						
					}

				}
			}
		});
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
//				cf.hidekeyboard();
			}
		});
		clo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
//				cf.hidekeyboard();
			}
		});

		dialog1.show();
	}

	private void call_reset_password_webservice(final String newpass) {
		if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Resetting Password... Please wait.");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						String chkauth = cf.Calling_WS_ChangePassword(username,
								password, newpass, "ChangePassword_Agent");
						System.out.println("The Result is " + chkauth);
						if (chkauth.equals("true")) /*
													 * USERAUTHENTICATION IS
													 * TRUE
													 */
						{
							show_handler = 5;
							handler.sendEmptyMessage(0);

						} else /* USERAUTHENTICATION IS FALSE */
						{
							show_handler = 1;
							handler.sendEmptyMessage(0);

						}
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("The error is " + e.getMessage());
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						// dialog1.dismiss();
						if (show_handler == 1) {
							show_handler = 0;
							toast = new ShowToast(HomeScreen.this,
									"Invalid Password.");
							// autousername.setText("");
							// etpassword.setText("");
							// autousername.requestFocus();
						} else if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									HomeScreen.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									HomeScreen.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							cf.CreateTable(0);
							cf.db.execSQL("delete from " + cf.LoginPage);
							// cf.db.execSQL("insert into " + cf.LoginPage
							// + " (username,password,agentid)"
							// + " values('" + username + "','" + newpass
							// + "','" + agentid + "');");
							dialog1.dismiss();
							// toast = new ShowToast(HomeScreen.this,
							// "Password Changed Successfully");
							final Dialog dialog2 = new Dialog(
									HomeScreen.this,
									android.R.style.Theme_Translucent_NoTitleBar);
							dialog2.setContentView(R.layout.loginagain);
							dialog2.setCancelable(false);
							TextView ok = (TextView) dialog2
									.findViewById(R.id.loginagain_ok);
							ok.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									dialog2.dismiss();
									
									Delete_All_Tables();
									
									Intent intent = new Intent(HomeScreen.this,
											LoginPage.class);
									startActivity(intent);
									finish();
								}
							});
							dialog2.show();
						}
					}
				};
			}.start();
		} else {
			toast = new ShowToast(HomeScreen.this,
					"Internet connection not available.");

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 0) {
			dialog.dismiss();
			// toast=new ShowToast(HomeScreen.this, "Email has been sent");
		}
		if (requestCode == 1) {
			dialog.dismiss();
			customermailid = "";
			emailaddress = "";
		}
	}

	private Drawable LoadImageFromWebOperations(String url) {
		// TODO Auto-generated method stub
		try {
			InputStream is = (InputStream) new URL(url).getContent();
			Drawable d = Drawable.createFromStream(is, "src name");
			return d;
		} catch (Exception e) {
			System.out.println("Exc=" + e);
			return null;
		}
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

		private ProgressDialog mDialog;
		private ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected void onPreExecute() {

			// mDialog =
			// ProgressDialog.show(ChartActivity.this,"Please wait...",
			// "Retrieving data ...", true);
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", "image download error");
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			// set image of your imageview
			bmImage.setImageBitmap(result);
			// close
			// mDialog.dismiss();
		}
	}
	
	private void Delete_All_Tables()
	{
		cf.CreateTable(0);
		cf.db.execSQL("delete from " + cf.LoginPage);

		cf.CreateTable(1);
		cf.db.execSQL("delete from " + cf.AgentInformation);

		cf.CreateTable(2);
		cf.db.execSQL("delete from " + cf.DashBoard);

		cf.CreateTable(3);
		cf.db.execSQL("delete from " + cf.OnlineSyncPolicyInfo_Agency);

		cf.CreateTable(4);
		cf.db.execSQL("delete from " + cf.LoadInspectionCategory);

		cf.CreateTable(5);
		cf.db.execSQL("delete from " + cf.LoadInspectionType);

		cf.CreateTable(8);
		cf.db.execSQL("delete from " + cf.LoadCompany);

		cf.CreateTable(9);
		cf.db.execSQL("delete from " + cf.LoadInspectors);

		cf.CreateTable(10);
		cf.db.execSQL("delete from " + cf.DashBoardToday);

		cf.CreateTable(11);
		cf.db.execSQL("delete from " + cf.DashBoardWeek);

		cf.CreateTable(12);
		cf.db.execSQL("delete from " + cf.DashBoardMonth);

		cf.CreateTable(13);
		cf.db.execSQL("delete from " + cf.LoadInspectionType2);

		cf.CreateTable(14);
		cf.db.execSQL("delete from " + cf.LoadInspectionStatus);

		cf.CreateTable(15);
		cf.db.execSQL("delete from " + cf.LoadInspectionSubStatus);

		cf.CreateTable(16);
		cf.db.execSQL("delete from " + cf.LoadCaptionValue);

		cf.CreateTable(17);
		cf.db.execSQL("delete from " + cf.AddAImage);

		cf.CreateTable(18);
		cf.db.execSQL("delete from " + cf.LoadbuildingType);

		cf.CreateTable(19);
		cf.db.execSQL("delete from " + cf.LoadInsuranceCarrier);

		cf.CreateTable(20);
		cf.db.execSQL("delete from " + cf.AgentInspection_Pdf);

		cf.CreateTable(21);
		cf.db.execSQL("delete from " + cf.Property_Count);

		cf.CreateTable(22);
		cf.db.execSQL("delete from " + cf.Customer_Details);

		cf.CreateTable(25);
		cf.db.execSQL("delete from " + cf.Version);
		
		cf.CreateTable(29);
		cf.db.execSQL("delete from " + cf.GetAgencyName);
		
		cf.CreateTable(31);
		cf.db.execSQL("Drop table " + cf.AgentInspection);
	}
	
	class CountDownRunner implements Runnable{
	    // @Override
	    public void run() {
	            while(!Thread.currentThread().isInterrupted()){
	                try {
	                doWork();
	                    Thread.sleep(1000); // Pause of 1 Second
	                } catch (InterruptedException e) {
	                        Thread.currentThread().interrupt();
	                }catch(Exception e){
	                }
	            }
	    }
	}
	
	public void doWork() {
	    runOnUiThread(new Runnable() {
	        public void run() {
	            try{
	            	final Calendar c = Calendar.getInstance();
	        		final int year = c.get(Calendar.YEAR);
	        		int month = c.get(Calendar.MONTH);
	        		int day = c.get(Calendar.DAY_OF_MONTH);
	        		int hours=c.get(Calendar.HOUR_OF_DAY);
	        		int minutes=c.get(Calendar.MINUTE);
	        		int seconds=c.get(Calendar.SECOND);
	        		String currentdate = (month + 1) + "/" + day + "/" + year+" "+hours+":"+minutes+":"+seconds;
	                    tvdateinformation.setText(currentdate);
	            }catch (Exception e) {}
	        }
	    });
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		finish();
		// cf.db.close();
	}

}
