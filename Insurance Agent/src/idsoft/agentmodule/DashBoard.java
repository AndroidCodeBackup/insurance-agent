package idsoft.agentmodule;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.serialization.SoapObject;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class DashBoard extends Activity {
	Button home, placeorder;
	TextView briefexplanation,  name;;
	String redcolor = "<font color=red> * </font>";
	Spinner spinnerinspections;
	String inspectionvalue, agentid, strname;
	String[] inspid;
	String[] inspections = { "All", "Today's Inspections",
			"This Week Inspections", "This Month Inspections" };
	TableLayout TableLayout_Dynamic;
	CommonFunctions cf;
	int i, show_handler;
	ShowToast toast;
	public static String status;
	public static int noofdatas;

//	private ScrollView vScroll;
//	private HorizontalScrollView hScroll;
	private float mx, my;
	private float curX, curY;
	static int spinner_selection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		cf = new CommonFunctions(this);
		ImageView iv=(ImageView)findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);

//		vScroll = (ScrollView) findViewById(R.id.vScroll);
//		hScroll = (HorizontalScrollView) findViewById(R.id.hScroll);

		home = (Button) findViewById(R.id.dash_home);
		placeorder = (Button) findViewById(R.id.dash_placeorder);
		briefexplanation = (TextView) findViewById(R.id.txtbriefexplanation);
		//note = (TextView) findViewById(R.id.dashboard_textviewnote);
		name = (TextView) findViewById(R.id.dash_name);
		spinnerinspections = (Spinner) findViewById(R.id.dash_inspections);
		TableLayout_Dynamic = (TableLayout) findViewById(R.id.dash_tablelayoutdynamic);
		cf.CreateTable(1);
		Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			strname = cur.getString(cur.getColumnIndex("FirstName"));
			strname += " " + cur.getString(cur.getColumnIndex("LastName"));
		}
		cur.close();

		name.setText(strname);

		//note.setText(Html.fromHtml(redcolor + "Note"));
		/*String txt  ="<b>Awaiting Scheduling&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>";
		String txt1 ="<b>Scheduled&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>";
		String txt2 ="<b>Inspected (In QA Process)&nbsp;&nbsp;:</b>";
		String txt3 ="<b>Suspended&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>";
		String txt4 ="<b>Reports Ready&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b>";
		briefexplanation.setText(Html.fromHtml(txt
				+ "  Newly Registered Homeowner(s) awaiting for Scheduling "
				+ "<br/>" + txt1 + "  Scheduled Homeowner(s)"
				+ "<br/>" + txt2 
				+ "  Inspected and waiting for QA Process " + "<br/>"
				+ txt3 + "  Suspended Homeowner(s)" + "<br/>"
				+ txt4 + "  Reports Ready to View " + "<br/>"));*/
		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inthome = new Intent(DashBoard.this, HomeScreen.class);
				startActivity(inthome);
				finish();
			}
		});
		placeorder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent inthome = new Intent(DashBoard.this,
						OrderInspection.class);
				startActivity(inthome);
				finish();
			}
		});
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(DashBoard.this,
				android.R.layout.simple_spinner_item, inspections);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerinspections.setAdapter(adapter);
		
		spinnerinspections.setSelection(spinner_selection);
		
		spinnerinspections
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						inspectionvalue = spinnerinspections.getSelectedItem()
								.toString();
						System.out.println("Inspection Value:"
								+ inspectionvalue);
						if (inspectionvalue.equals("All")) {
							cf.CreateTable(2);
							Cursor cur = cf.db.rawQuery("select * from "
									+ cf.DashBoard, null);
							DynamicList("All", cur);

						} else if (inspectionvalue
								.equals("Today's Inspections")) {
							cf.CreateTable(10);
							cf.db.execSQL("delete from " + cf.DashBoardToday);
							Import_Data("Today", cf.DashBoardToday);
						} else if (inspectionvalue
								.equals("This Week Inspections")) {
							cf.CreateTable(11);
							cf.db.execSQL("delete from " + cf.DashBoardWeek);
							Import_Data("Week", cf.DashBoardWeek);
						} else if (inspectionvalue
								.equals("This Month Inspections")) {
							cf.CreateTable(12);
							cf.db.execSQL("delete from " + cf.DashBoardMonth);
							Import_Data("Month", cf.DashBoardMonth);

						}

					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

	}

	private void DynamicList(final String inspection, Cursor cur) {
		TableLayout_Dynamic.removeAllViews();

		TableLayout.LayoutParams trparams = new TableLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		trparams.setMargins(1, 1, 1, 1);

		TableRow trh = new TableRow(this);
		trh.setLayoutParams(trparams);
		//trh.setBackgroundColor(0x5b5d64);
		trh.setBackgroundResource(R.drawable.status);
		TableLayout_Dynamic.addView(trh);

		TableRow.LayoutParams textviewparamshead = new TableRow.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		textviewparamshead.setMargins(35, 0, 0, 2);

		TableRow.LayoutParams textviewparamshead1 = new TableRow.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		textviewparamshead1.setMargins(13, 0, 0, 2);

		TextView tv1 = new TextView(this);
		tv1.setLayoutParams(textviewparamshead1);
		tv1.setGravity(Gravity.LEFT);
		tv1.setText("Status");
		tv1.setTextColor(Color.parseColor("#073D66"));
		tv1.setTypeface(null, Typeface.BOLD);
		tv1.setTextSize(14);
		trh.addView(tv1);

		TextView tv2 = new TextView(this);
		tv2.setLayoutParams(textviewparamshead);
		tv2.setGravity(Gravity.CENTER);
		tv2.setText("Awaiting" + "\n" + "Scheduling");
		tv2.setTextColor(Color.parseColor("#073D66"));
		tv2.setTypeface(null, Typeface.BOLD);
		tv2.setTextSize(14);
		trh.addView(tv2);

		TextView tv3 = new TextView(this);
		tv3.setLayoutParams(textviewparamshead);
		tv3.setGravity(Gravity.CENTER);
		tv3.setText("Scheduled");
		tv3.setTextColor(Color.parseColor("#073D66"));
		tv3.setTypeface(null, Typeface.BOLD);
		tv3.setTextSize(14);
		trh.addView(tv3);

		TextView tv4 = new TextView(this);
		tv4.setLayoutParams(textviewparamshead);
		tv4.setGravity(Gravity.CENTER);
		tv4.setText("Inspected" + "\n" + "(In QA Process)");
		tv4.setTextColor(Color.parseColor("#073D66"));
		tv4.setTypeface(null, Typeface.BOLD);
		tv4.setTextSize(14);
		trh.addView(tv4);

		TextView tv6 = new TextView(this);
		tv6.setLayoutParams(textviewparamshead);
		tv6.setGravity(Gravity.CENTER);
		tv6.setText("Suspended");
		tv6.setTextColor(Color.parseColor("#073D66"));
		tv6.setTypeface(null, Typeface.BOLD);
		tv6.setTextSize(14);
		trh.addView(tv6);
		
		TextView tv5 = new TextView(this);
		tv5.setLayoutParams(textviewparamshead);
		tv5.setGravity(Gravity.CENTER);
		tv5.setText("Reports" + "\n" + "Ready");
		tv5.setTextColor(Color.parseColor("#073D66"));
		tv5.setTypeface(null, Typeface.BOLD);
		tv5.setTextSize(14);
		trh.addView(tv5);

		// TextView tv7 = new TextView(this);
		// tv7.setLayoutParams(textviewparamshead);
		// tv7.setGravity(Gravity.CENTER);
		// tv7.setText("Unsuspended");
		// tv7.setTextColor(Color.WHITE);
		// tv7.setTypeface(null, Typeface.BOLD);
		// tv7.setTextSize(14);
		// trh.addView(tv7);
		//
		// TextView tv8 = new TextView(this);
		// tv8.setLayoutParams(textviewparamshead);
		// tv8.setGravity(Gravity.CENTER);
		// tv8.setText("Other");
		// tv8.setTextColor(Color.WHITE);
		// tv8.setTypeface(null, Typeface.BOLD);
		// tv8.setTextSize(14);
		// trh.addView(tv8);

		TableRow.LayoutParams textviewparamshead3 = new TableRow.LayoutParams(
				ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		textviewparamshead3.setMargins(35, 0, 20, 2);

		TextView tv9 = new TextView(this);
		tv9.setLayoutParams(textviewparamshead3);
		tv9.setGravity(Gravity.CENTER);
		tv9.setText("Total");
		tv9.setTextColor(Color.parseColor("#073D66"));
		tv9.setTypeface(null, Typeface.BOLD);
		tv9.setTextSize(14);
		trh.addView(tv9);

		int count = cur.getCount();
		cur.moveToFirst();
		TableRow[] tr = new TableRow[count];
		TextView[] tvinspectionname = new TextView[count];
		final Button[] btawaiting = new Button[count];
		Button[] btscheduled = new Button[count];
		Button[] btinspected = new Button[count];
		Button[] btreportsready = new Button[count];
		Button[] suspended = new Button[count];
		Button[] btunsuspended = new Button[count];
		Button[] btother = new Button[count];
		Button[] bttotal = new Button[count];
		inspid = new String[count];

		if (cur.getCount() >= 1) {
			i = 0;
			do {
				String InspectionID = cf.decode(cur.getString(cur
						.getColumnIndex("inspid")));
				String InspName = cf.decode(cur.getString(cur
						.getColumnIndex("inspname")));
				String InspType = cf.decode(cur.getString(cur
						.getColumnIndex("insptype")));
				String Awaiting = cf.decode(cur.getString(cur
						.getColumnIndex("awaiting")));
				String Scheduled = cf.decode(cur.getString(cur
						.getColumnIndex("scheduled")));
				String Inspected = cf.decode(cur.getString(cur
						.getColumnIndex("inspected")));
				String ReportsReady = cf.decode(cur.getString(cur
						.getColumnIndex("reportsready")));
				String Suspended = cf.decode(cur.getString(cur
						.getColumnIndex("suspended")));
				String UnSuspended = cf.decode(cur.getString(cur
						.getColumnIndex("unsuspended")));
				String Other = cf.decode(cur.getString(cur
						.getColumnIndex("other")));
				String Total = cf.decode(cur.getString(cur
						.getColumnIndex("total")));

				inspid[i] = InspectionID;

				TableLayout.LayoutParams tlparams = new TableLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				tlparams.setMargins(1, 1, 1, 5);

				tr[i] = new TableRow(this);
				tr[i].setLayoutParams(tlparams);
				//tr[i].setBackgroundColor(0xabFFFFFF);
				tr[i].setBackgroundColor(Color.parseColor("#073861"));
				TableLayout_Dynamic.addView(tr[i]);

				TableRow.LayoutParams textviewparams = new TableRow.LayoutParams(
						300, ViewGroup.LayoutParams.WRAP_CONTENT);
				textviewparams.setMargins(10, 0, 0, 10);

				tvinspectionname[i] = new TextView(this);
				tvinspectionname[i].setText(InspName);
				tvinspectionname[i].setLayoutParams(textviewparams);
				tvinspectionname[i].setTextColor(Color.WHITE);
				tvinspectionname[i].setTextSize(14);
				tr[i].addView(tvinspectionname[i]);

				TableRow.LayoutParams buttonparams = new TableRow.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				buttonparams.setMargins(30, 0, 0, 0);

				btawaiting[i] = new Button(this);
				btawaiting[i].setLayoutParams(buttonparams);
				btawaiting[i].setBackgroundColor(0x00FFFFFF);
				btawaiting[i].setTextColor(Color.WHITE);
				btawaiting[i].setTextSize(16);
				btawaiting[i].setText(Awaiting);
				btawaiting[i].setTag("Awaiting:" + i + ":" + InspName);
				tr[i].addView(btawaiting[i]);

				btscheduled[i] = new Button(this);
				btscheduled[i].setLayoutParams(buttonparams);
				btscheduled[i].setBackgroundColor(0x00FFFFFF);
				btscheduled[i].setTextColor(Color.WHITE);
				btscheduled[i].setTextSize(16);
				btscheduled[i].setText(Scheduled);
				btscheduled[i].setTag("Scheduled:" + i + ":" + InspName);
				tr[i].addView(btscheduled[i]);

				btinspected[i] = new Button(this);
				btinspected[i].setLayoutParams(buttonparams);
				btinspected[i].setBackgroundColor(0x00FFFFFF);
				btinspected[i].setTextColor(Color.WHITE);
				btinspected[i].setTextSize(16);
				btinspected[i].setText(Inspected);
				btinspected[i].setTag("Inspected:" + i + ":" + InspName);
				tr[i].addView(btinspected[i]);

				suspended[i] = new Button(this);
				suspended[i].setLayoutParams(buttonparams);
				suspended[i].setBackgroundColor(0x00FFFFFF);
				suspended[i].setTextColor(Color.WHITE);
				suspended[i].setTextSize(16);
				suspended[i].setText(Suspended);
				suspended[i].setTag("Suspended:" + i + ":" + InspName);
				tr[i].addView(suspended[i]);
				
				btreportsready[i] = new Button(this);
				btreportsready[i].setLayoutParams(buttonparams);
				btreportsready[i].setBackgroundColor(0x00FFFFFF);
				btreportsready[i].setTextColor(Color.WHITE);
				btreportsready[i].setTextSize(16);
				btreportsready[i].setText(ReportsReady);
				btreportsready[i].setTag("ReportsReady:" + i + ":" + InspName);
				tr[i].addView(btreportsready[i]);

				// btunsuspended[i] = new Button(this);
				// btunsuspended[i].setLayoutParams(buttonparams);
				// btunsuspended[i].setBackgroundColor(0x00FFFFFF);
				// btunsuspended[i].setTextColor(Color.BLACK);
				// btunsuspended[i].setTextSize(16);
				// btunsuspended[i].setText(UnSuspended);
				// btunsuspended[i].setTag("UnSuspended:" + i + ":" + InspName);
				// tr[i].addView(btunsuspended[i]);
				//
				// btother[i] = new Button(this);
				// btother[i].setLayoutParams(buttonparams);
				// btother[i].setBackgroundColor(0x00FFFFFF);
				// btother[i].setTextColor(Color.BLACK);
				// btother[i].setTextSize(16);
				// btother[i].setText(Other);
				// btother[i].setTag("Other:" + i + ":" + InspName);
				// tr[i].addView(btother[i]);

				bttotal[i] = new Button(this);
				bttotal[i].setLayoutParams(buttonparams);
				bttotal[i].setBackgroundColor(0x00FFFFFF);
				bttotal[i].setTextColor(Color.WHITE);
				bttotal[i].setTextSize(16);
				bttotal[i].setText(Total);
				bttotal[i].setTag("Total:" + i + ":" + InspName);
				tr[i].addView(bttotal[i]);

				btawaiting[i].setOnClickListener(new ClickListener());
				btscheduled[i].setOnClickListener(new ClickListener());
				btinspected[i].setOnClickListener(new ClickListener());
				btreportsready[i].setOnClickListener(new ClickListener());
				suspended[i].setOnClickListener(new ClickListener());
				// btunsuspended[i].setOnClickListener(new ClickListener());
				// btother[i].setOnClickListener(new ClickListener());

				i++;
			} while (cur.moveToNext());

			//note.setVisibility(View.VISIBLE);
			briefexplanation.setVisibility(View.VISIBLE);

		} else {
			// TableLayout_Dynamic.removeAllViews();
			//
			// TableLayout.LayoutParams trparams1 = new
			// TableLayout.LayoutParams(
			// ViewGroup.LayoutParams.MATCH_PARENT,
			// ViewGroup.LayoutParams.WRAP_CONTENT);
			// trparams1.setMargins(20, 50, 20, 50);
			//
			// TableRow trh1 = new TableRow(this);
			// trh1.setLayoutParams(trparams1);
			// trh1.setBackgroundColor(0x5b5d64);
			// TableLayout_Dynamic.addView(trh1);
			//
			// TableRow.LayoutParams textviewparamshead11 = new
			// TableRow.LayoutParams(
			// ViewGroup.LayoutParams.WRAP_CONTENT,
			// ViewGroup.LayoutParams.WRAP_CONTENT);
			// textviewparamshead11.setMargins(20, 25, 20, 25);
			// if (inspection.equals("Today")) {
			// TextView t = new TextView(this);
			// t.setLayoutParams(textviewparamshead);
			// t.setGravity(Gravity.CENTER);
			// t.setText("No Inspection Available for " + inspection);
			// t.setTextColor(Color.WHITE);
			// t.setTypeface(null, Typeface.BOLD);
			// t.setTextSize(20);
			// trh1.addView(t);
			// } else {
			// TextView t = new TextView(this);
			// t.setLayoutParams(textviewparamshead);
			// t.setGravity(Gravity.CENTER);
			// t.setText("No Inspection Available for this " + inspection);
			// t.setTextColor(Color.WHITE);
			// t.setTypeface(null, Typeface.BOLD);
			// t.setTextSize(20);
			// trh1.addView(t);
			// }
			//
			// note.setVisibility(View.INVISIBLE);
			// briefexplanation.setVisibility(View.INVISIBLE);

			if (inspection.equals("Today")) {
				toast = new ShowToast(DashBoard.this, "No Inspection for "
						+ inspection);
			} else {
				toast = new ShowToast(DashBoard.this, "No Inspection for this"
						+ inspection);
			}

		}

	}

	private void Import_Data(final String inspection, final String table) {
		cf.CreateTable(1);
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoginPage, null);
		cur.moveToFirst();
		cf.agentid = cf.decode(cur.getString(cur.getColumnIndex("agentid")));

		if (cf.isInternetOn() == true) {

			cf.show_ProgressDialog("Processing... Please wait.");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject chklogin = cf
								.Calling_WS_DashBoard(cf.agentid, inspection,
										"AgentDashboardDetails");
						System.out.println("chl " + chklogin);
						SoapObject chk = (SoapObject) chklogin.getProperty(0);
						String inspectionid = chk.getProperty("InspectionID")
								.toString();
						if (inspectionid.equals("0")) {
							show_handler = 6;
							handler.sendEmptyMessage(0);
						} else {
							System.out.println("Inside elseeeeeeeeeeee");
							Insert_into_Dashboardtable(chklogin, table);
							show_handler = 5;
							handler.sendEmptyMessage(0);
						}

					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									DashBoard.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									DashBoard.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;

							Cursor cur = cf.db.rawQuery("select * from "
									+ table, null);
							DynamicList(inspection, cur);

						} else if (show_handler == 6) {
							show_handler = 0;

							if (inspection.equals("Today")) {
								toast = new ShowToast(DashBoard.this,
										"No Inspection available for "
												+ inspection);
							} else {
								toast = new ShowToast(DashBoard.this,
										"No Inspection available for this"
												+ inspection);
							}

							spinnerinspections.setSelection(0);

						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(DashBoard.this,
					"Internet connection not available");

		}
	}

	private void Insert_into_Dashboardtable(SoapObject objInsert,
			final String table) throws NetworkErrorException, IOException,
			XmlPullParserException, SocketTimeoutException {
		int Cnt = objInsert.getPropertyCount();
		System.out.println("check property count is" + Cnt);
		if (Cnt >= 1) {
			for (int i = 0; i < Cnt; i++) {
				SoapObject obj = (SoapObject) objInsert.getProperty(i);
				try {
					cf.inspection_id = String.valueOf(obj
							.getProperty("InspectionID"));
					cf.inspection_name = String.valueOf(obj
							.getProperty("InspName"));
					cf.inspection_type = String.valueOf(obj
							.getProperty("InspType"));
					cf.awaiting = String.valueOf(obj.getProperty("Awaiting"));
					cf.scheduled = String.valueOf(obj.getProperty("Scheduled"));
					cf.inspected = String.valueOf(obj.getProperty("Inspected"));
					cf.reports_ready = String.valueOf(obj
							.getProperty("ReportsReady"));
					cf.suspended = String.valueOf(obj.getProperty("Suspended"));
					cf.unsuspended = String.valueOf(obj
							.getProperty("UnSuspended"));
					cf.other = String.valueOf(obj.getProperty("Other"));
					cf.total = String.valueOf(obj.getProperty("Total"));

					cf.db.execSQL("insert into "
							+ table
							+ " (inspid,inspname,insptype,awaiting,scheduled,inspected,reportsready,suspended,unsuspended,other,total) values('"
							+ cf.encode(cf.inspection_id) + "','"
							+ cf.encode(cf.inspection_name) + "','"
							+ cf.encode(cf.inspection_type) + "','"
							+ cf.encode(cf.awaiting) + "','"
							+ cf.encode(cf.scheduled) + "','"
							+ cf.encode(cf.inspected) + "','"
							+ cf.encode(cf.reports_ready) + "','"
							+ cf.encode(cf.suspended) + "','"
							+ cf.encode(cf.unsuspended) + "','"
							+ cf.encode(cf.other) + "','" + cf.encode(cf.total)
							+ "');");

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
	}

	private void call_online_list(final String status, final String inspid,
			final String inspectionname) {

		cf.CreateTable(0);
		Cursor cur = cf.db.rawQuery("select * from " + cf.LoginPage, null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			agentid = cf.decode(cur.getString(cur.getColumnIndex("agentid")));
		}

		if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Processing... Please wait.");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {

						System.out
								.println("Calling Webservice OnlineSyncPolicyInfo_Agency");

						SoapObject chklogin = cf
								.Calling_WS_OnlineSyncPolicyInfo_Agency(status,
										inspid, agentid,
										"OnlineSyncPolicyInfo_Agency");
						OnlineSyncPolicyInfo(chklogin);
						cf.pd.dismiss();

						Intent intent = new Intent(DashBoard.this,
								OnlineList.class);
						intent.putExtra("status", status);
						intent.putExtra("noofdatas",
								Integer.toString(noofdatas));
						intent.putExtra("inspectionname", inspectionname);
						startActivity(intent);
						finish();
						spinner_selection=spinnerinspections.getSelectedItemPosition();

						System.out
								.println("response OnlineSyncPolicyInfo_Agency"
										+ chklogin);

					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									DashBoard.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									DashBoard.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						}
					}
				};
			}.start();
		} else {
			toast = new ShowToast(DashBoard.this,
					"Internet connection not available");

		}

	}

	private void OnlineSyncPolicyInfo(SoapObject objInsert) {
		System.out.println("Inside OnlineSyncPolicyInfo");

		cf.CreateTable(3);
		cf.db.execSQL("delete from " + cf.OnlineSyncPolicyInfo_Agency);

		noofdatas = objInsert.getPropertyCount();
		System.out.println("Property count is" + noofdatas);
		for (int i = 0; i < noofdatas; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String SRID = String.valueOf(obj.getProperty("SRID"));
				String FirstName = String.valueOf(obj.getProperty("FirstName"));
				String LastName = String.valueOf(obj.getProperty("LastName"));
				String MiddleName = String.valueOf(obj
						.getProperty("MiddleName"));
				String Address1 = String.valueOf(obj.getProperty("Address1"));
				String Address2 = String.valueOf(obj.getProperty("Address2"));
				String City = String.valueOf(obj.getProperty("City"));
				String StateId = String.valueOf(obj.getProperty("StateId"));
				String State = String.valueOf(obj.getProperty("State"));
				String Country = String.valueOf(obj.getProperty("Country"));
				String Zip = String.valueOf(obj.getProperty("Zip"));
				String HomePhone = String.valueOf(obj.getProperty("HomePhone"));
				String CellPhone = String.valueOf(obj.getProperty("CellPhone"));
				String WorkPhone = String.valueOf(obj.getProperty("WorkPhone"));
				String Email = String.valueOf(obj.getProperty("Email"));
				String ContactPerson = String.valueOf(obj
						.getProperty("ContactPerson"));
				String OwnerPolicyNo = String.valueOf(obj
						.getProperty("OwnerPolicyNo"));
				String Status = String.valueOf(obj.getProperty("Status"));
				String SubStatusID = String.valueOf(obj
						.getProperty("SubStatusID"));
				String statefarmcompanyid = String.valueOf(obj
						.getProperty("statefarmcompanyid"));
				String InspectorId = String.valueOf(obj
						.getProperty("InspectorId"));
				String wId = String.valueOf(obj.getProperty("wId"));
				String CompanyId = String.valueOf(obj.getProperty("CompanyId"));
				String InspectorFirstName = String.valueOf(obj
						.getProperty("InspectorFirstName"));
				String InspectorLastName = String.valueOf(obj
						.getProperty("InspectorLastName"));
				String ScheduledDate = String.valueOf(obj
						.getProperty("ScheduledDate"));
				String YearBuilt = String.valueOf(obj.getProperty("YearBuilt"));
				String Nstories = String.valueOf(obj.getProperty("Nstories"));
				String InspectionTypeId = String.valueOf(obj
						.getProperty("InspectionTypeId"));
				String ScheduleCreatedDate = String.valueOf(obj
						.getProperty("ScheduleCreatedDate"));
				String AssignedDate = String.valueOf(obj
						.getProperty("AssignedDate"));
				String InspectionStartTime = String.valueOf(obj
						.getProperty("InspectionStartTime"));
				String InspectionEndTime = String.valueOf(obj
						.getProperty("InspectionEndTime"));
				String InspectionComment = String.valueOf(obj
						.getProperty("InspectionComment"));
				String IsInspected = String.valueOf(obj
						.getProperty("IsInspected"));
				String InsuranceCompany = String.valueOf(obj
						.getProperty("InsuranceCompany"));
				String s_InspFees = String.valueOf(obj
						.getProperty("s_InspFees"));
				String InsuranceAgentName = String.valueOf(obj
						.getProperty("InsuranceAgentName"));
				String AgentEmail = String.valueOf(obj
						.getProperty("AgentEmail"));
				String InsuranceAgencyName = String.valueOf(obj
						.getProperty("InsuranceAgencyName"));
				String AgencyEmail = String.valueOf(obj
						.getProperty("AgencyEmail"));
				String COMMpdf = String.valueOf(obj.getProperty("COMMpdf"));
				String COMMMergedpdf = String.valueOf(obj
						.getProperty("COMMMergedpdf"));
				String RoofPdfPath = String.valueOf(obj
						.getProperty("RoofPdfPath"));
				System.out.println("Roof Pdf Path" + RoofPdfPath);

				cf.db.execSQL("insert into "
						+ cf.OnlineSyncPolicyInfo_Agency
						+ " (SRID,FirstName,LastName,MiddleName,"
						+ "Address1,Address2,"
						+ "City,StateId,State,Country,Zip,HomePhone,CellPhone,WorkPhone,"
						+ "Email,ContactPerson,OwnerPolicyNo,Status,SubStatusID,statefarmcompanyid,"
						+ "InspectorId,wId,CompanyId,InspectorFirstName,InspectorLastName,"
						+ "ScheduledDate,YearBuilt,Nstories,InspectionTypeId,ScheduleCreatedDate,"
						+ "AssignedDate,InspectionStartTime,InspectionEndTime,InspectionComment,"
						+ "IsInspected,InsuranceCompany,s_InspFees,InsuranceAgentName,AgentEmail,"
						+ "InsuranceAgencyName,AgencyEmail,COMMpdf,COMMMergedpdf,RoofPdfPath)values('"
						+ cf.encode(SRID) + "','" + cf.encode(FirstName)
						+ "','" + cf.encode(LastName) + "','"
						+ cf.encode(MiddleName) + "','" + cf.encode(Address1)
						+ "','" + cf.encode(Address2) + "','" + cf.encode(City)
						+ "','" + cf.encode(StateId) + "','" + cf.encode(State)
						+ "','" + cf.encode(Country) + "','" + cf.encode(Zip)
						+ "','" + cf.encode(HomePhone) + "','"
						+ cf.encode(CellPhone) + "','" + cf.encode(WorkPhone)
						+ "','" + cf.encode(Email) + "','"
						+ cf.encode(ContactPerson) + "','"
						+ cf.encode(OwnerPolicyNo) + "','" + cf.encode(Status)
						+ "','" + cf.encode(SubStatusID) + "','"
						+ cf.encode(statefarmcompanyid) + "','"
						+ cf.encode(InspectorId) + "','" + cf.encode(wId)
						+ "','" + cf.encode(CompanyId) + "','"
						+ cf.encode(InspectorFirstName) + "','"
						+ cf.encode(InspectorLastName) + "','"
						+ ScheduledDate + "','"
						+ cf.encode(YearBuilt) + "','" + cf.encode(Nstories)
						+ "','" + cf.encode(InspectionTypeId) + "','"
						+ ScheduleCreatedDate + "','"
						+ AssignedDate + "','"
						+ cf.encode(InspectionStartTime) + "','"
						+ cf.encode(InspectionEndTime) + "','"
						+ cf.encode(InspectionComment) + "','"
						+ cf.encode(IsInspected) + "','"
						+ cf.encode(InsuranceCompany) + "','"
						+ cf.encode(s_InspFees) + "','"
						+ cf.encode(InsuranceAgentName) + "','"
						+ cf.encode(AgentEmail) + "','"
						+ cf.encode(InsuranceAgencyName) + "','"
						+ cf.encode(AgencyEmail) + "','" + cf.encode(COMMpdf)
						+ "','" + cf.encode(COMMMergedpdf) + "','"
						+ cf.encode(RoofPdfPath) + "');");

				Cursor cur = cf.db.rawQuery("select * from "
						+ cf.OnlineSyncPolicyInfo_Agency, null);
				int rows = cur.getCount();

				System.out.println("OnlineSyncPolicyInfo_Agency count is "
						+ rows);

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public class ClickListener implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Button b = (Button) v;
			String buttonvalue = b.getText().toString();
			if (!buttonvalue.equals("0")) {
				String tagname = v.getTag().toString();
				String[] tagnamesplit = tagname.split(":");
				status = tagnamesplit[0];
				String value = tagnamesplit[1];
				String inspectionname = tagnamesplit[2];
				int s = Integer.parseInt(value);
				String id = inspid[s];
				
				System.out.println("Status :"+status);
				System.out.println("id :"+id);
				System.out.println("Inspection name :"+inspectionname);
				
				Intent intent = new Intent(DashBoard.this,
						OnlineList.class);
				intent.putExtra("status", status);
				intent.putExtra("inspid",inspid[s]);
				intent.putExtra("inspectionname", inspectionname);
				startActivity(intent);
				finish();
				spinner_selection=spinnerinspections.getSelectedItemPosition();
				
//				call_online_list(status, id, inspectionname);
			} else {
				toast = new ShowToast(DashBoard.this, "No records available");
			}
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		Intent inthome = new Intent(DashBoard.this, HomeScreen.class);
		startActivity(inthome);
		finish();
	}

}
