package idsoft.agentmodule;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.serialization.SoapObject;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class SyncInspection extends Activity{
	CommonFunctions cf;
	ShowToast toast;
	int i=0,show_handler=0,propertycount=0;
	String AgentId="",path="";
	LinearLayout lldisplaypdf;
	RelativeLayout rlnote;
	String[] elevation_name,caption_name,file_name,pdfpath,data,datasend;
	AlertDialog alertDialog;
	static String reportpath;
	TextView tvrws;
	static SyncInspection ai;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sync);
		cf = new CommonFunctions(this);
		
		ImageView iv=(ImageView)findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);
		lldisplaypdf = (LinearLayout) findViewById(R.id.agentinsplist);
		rlnote = (RelativeLayout) findViewById(R.id.agentinsp_tableLayout2);
		tvrws = (TextView)findViewById(R.id.rows);
		try
		{
			cf.CreateTable(1);
			Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,null);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				AgentId = cur.getString(cur.getColumnIndex("Agentid"));
			}
			cur.close();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		Call_AgentInformationList();
	}
	public void Call_AgentInformationList() {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Loading Agent Inspection... Please wait."
					+ "</font></b>";
			final ProgressDialog pd = ProgressDialog.show(SyncInspection.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			
			new Thread() {
				SoapObject chklogin;
				public void run() {
					Looper.prepare();
					try {
						chklogin = cf
								.Calling_WS_AGENTINFORMATIONLIST(AgentId,"AGENTINFORMATIONLIST");
						System.out.println("response AGENTINFORMATIONLIST"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							toast = new ShowToast(
									SyncInspection.this,
									"There is a problem on your Network. Please try again later with better Network.");

						} else if (show_handler == 4) {
							show_handler = 0;
							toast = new ShowToast(
									SyncInspection.this,
									"There is a problem on your application. Please contact Paperless administrator.");

						} else if (show_handler == 5) {
							show_handler = 0;
							Call_AgentInformationList(chklogin);
						}
					}
				};
			}.start();

		} else {
			toast = new ShowToast(SyncInspection.this,
					"Internet connection not available");

		}
//	}
	}

	public void Call_AgentInformationList(SoapObject objInsert) {
		cf.CreateTable(20);
		cf.db.execSQL("delete from " + cf.AgentInspection_Pdf);
		if(!objInsert.equals(null))
		{
			propertycount = objInsert.getPropertyCount();
			tvrws.setText("No. of surveys : "+propertycount);
			System.out.println("AgentInspection_Pdf property count" + propertycount);
		if(propertycount>=1)
		{
		for (int i = 0; i < propertycount; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String filename = String.valueOf(obj.getProperty("PDFPath"));
				String FirstName = String.valueOf(obj.getProperty("Firstname"));
				String LastName = String.valueOf(obj.getProperty("Lastname"));
				String InspectionAddress1 = String.valueOf(obj.getProperty("InspectionAddress1"));
				String InspectionAddress2 = String.valueOf(obj.getProperty("InspectionAddress2"));
				String InspectionCity = String.valueOf(obj.getProperty("InspectionCity"));
				String InspectionState = String.valueOf(obj.getProperty("InspectionState"));
				String InspectionCounty = String.valueOf(obj.getProperty("InspectionCounty"));
				String PolicyNumber = String.valueOf(obj.getProperty("PolicyNumber"));
				String InspectionZip = String.valueOf(obj.getProperty("InspectionZip"));
				
				cf.db.execSQL("insert into " + cf.AgentInspection_Pdf
//						+ " (filename) values('"+ cf.encode(filename)
				+ " (filename,FirstName,LastName,InspectionAddress1,InspectionAddress2,InspectionCity," +
				"InspectionState,InspectionCounty,PolicyNumber,InspectionZip) values('"+ cf.encode(filename) + "','"+ 
				cf.encode(FirstName) + "','"+ cf.encode(LastName) + "','"+ cf.encode(InspectionAddress1)
				+ "','"+ cf.encode(InspectionAddress2) + "','"+ cf.encode(InspectionCity) + "','"+ 
				cf.encode(InspectionState) + "','"+ cf.encode(InspectionCounty)
				+ "','"+ cf.encode(PolicyNumber) + "','"+cf.encode(InspectionZip)+"');");
				

			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		Call_Dynamic_Pdf_Display();
		}
		else
		{
			lldisplaypdf.removeAllViews();
			rlnote.setVisibility(View.GONE);
		}
		}
		
	}
	
	private void Call_Dynamic_Pdf_Display()
	{
		lldisplaypdf.removeAllViews();
		ScrollView sv = new ScrollView(this);
		lldisplaypdf.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		LinearLayout.LayoutParams viewparams1 = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, 1);
		viewparams1.setMargins(20, 0, 0, 0);

		/*View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v, viewparams1);*/

		Cursor cur=cf.db.rawQuery("select * from "+cf.AgentInspection_Pdf, null);
		
		int rows = cur.getCount();


		TextView[] tvstatus = new TextView[rows];
		Button[] view = new Button[rows];
		final Button[] download = new Button[rows];
		final Button[] pter = new Button[rows];
		// LinearLayout[] l2 = new LinearLayout[rows];
		RelativeLayout[] rl = new RelativeLayout[rows];
		data = new String[rows];
		datasend = new String[rows];
		pdfpath = new String[rows];
		final String[] ownersname=new String[rows];
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			i = 0;
			do {
				String file_name = cf.decode(cur.getString(cur
						.getColumnIndex("filename")));
				String FirstName = cf.decode(cur.getString(cur
						.getColumnIndex("FirstName")));
				ownersname[i] =FirstName+" ";
				data[i] = " " + FirstName + " | ";
				String LastName = cf.decode(cur.getString(cur
						.getColumnIndex("LastName")));
				ownersname[i] +=LastName;
				data[i] += LastName + " | ";
				String Address1 = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionAddress1")));
				data[i] += Address1 + " | ";
				String Address2 = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionAddress2")));
				data[i] += Address2 + " | ";
				String City = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionCity")));
				data[i] += City + " | ";
				String State = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionState")));
				data[i] += State + " | ";
				String County = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionCounty")));
				data[i] += County + " | ";
				String InspectionZip = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionZip")));
				data[i] += InspectionZip + " | ";
				String PolicyNumber = cf.decode(cur.getString(cur
						.getColumnIndex("PolicyNumber")));
				data[i] += PolicyNumber + " | ";
				datasend[i] = PolicyNumber;
				
				
				pdfpath[i] = file_name;

				
				LinearLayout.LayoutParams llparams;
				RelativeLayout.LayoutParams tvparams;
				RelativeLayout.LayoutParams downloadparams,pterparams,viewparams;
				LinearLayout.LayoutParams lltxtparams;
				
				rlnote.setVisibility(View.VISIBLE);
				
				
				Display display = getWindowManager().getDefaultDisplay();
			    DisplayMetrics displayMetrics = new DisplayMetrics();
			    display.getMetrics(displayMetrics);

			    int width = displayMetrics.widthPixels;
			    int height = displayMetrics.heightPixels;
			    
			    System.out.println("The width is "+width);
			    System.out.println("The height is "+height);
				
				if(width > 1023 || height > 1023)
				{
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					llparams.setMargins(0, 0, 0, 0);
					
					tvparams = new RelativeLayout.LayoutParams(
							680,
							ViewGroup.LayoutParams.MATCH_PARENT);
					tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
					tvparams.setMargins(0, 0, 0, 0);
					

					downloadparams = new RelativeLayout.LayoutParams(
							110, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams.addRule(RelativeLayout.CENTER_VERTICAL);
					downloadparams.setMargins(830, 0, 0, 0);
					
					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					viewparams.addRule(RelativeLayout.CENTER_VERTICAL);
					viewparams.setMargins(700, 0, 0, 0);
					
					rl[i] = new RelativeLayout(this);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					lltxt.setPadding(20, 0, 20, 0);
					rl[i].addView(lltxt);
					
					lltxtparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					lltxtparams.setMargins(0, 30, 0, 30);
	
					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
//					tvstatus[i].setText("The Record is ");
					tvstatus[i].setText(data[i]);
//					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextColor(Color.WHITE);
					tvstatus[i].setTextSize(14);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setLayoutParams(downloadparams);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							110, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.gravity=Gravity.CENTER_VERTICAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
					LinearLayout llpter=new LinearLayout(this);
					llpter.setLayoutParams(downloadparams);
					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i);
					pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);
//					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					llpter.addView(pter[i]);
					
					LinearLayout llview=new LinearLayout(this);
					llview.setLayoutParams(viewparams);
					rl[i].addView(llview);
	
					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					llview.addView(view[i]);
				}
				
				else if((width > 500 && width<1000) || (height > 500 && height<1000))
				{

					
					System.out.println("Phoneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llparams.setMargins(0, 0, 0, 0);
					
					
					
					tvparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
					tvparams.setMargins(10, 0, 170, 0);
						
					lltxtparams = new LinearLayout.LayoutParams(
							320, ViewGroup.LayoutParams.MATCH_PARENT);
					lltxtparams.setMargins(0, 0, 0, 0);
										
					downloadparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
					downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					downloadparams.setMargins(0, 0, 20, 0);

					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					viewparams.setMargins(20, 0, 150, 0);
					
					rl[i] = new RelativeLayout(this);
					rl[i].setPadding(0, 20, 0, 20);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					rl[i].addView(lltxt);
					
					

					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
//					tvstatus[i].setText("The Redord is ");
					tvstatus[i].setText(data[i]);
					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextSize(14);
//					tvstatus[i].setGravity(Gravity.CENTER_VERTICAL);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setOrientation(LinearLayout.VERTICAL);
					lldownload.setLayoutParams(downloadparams);
					lldownload.setGravity(Gravity.CENTER_HORIZONTAL);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.topMargin=10;
					downloadparams1.gravity=Gravity.CENTER_HORIZONTAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
//					LinearLayout llpter=new LinearLayout(this);
//					llpter.setLayoutParams(downloadparams);
//					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);
					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(pter[i]);
					
//					LinearLayout llview=new LinearLayout(this);
//					llview.setLayoutParams(viewparams);
//					rl[i].addView(llview);

					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(view[i]);
				}
				
				else
				{
					
					System.out.println("Phoneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					llparams.setMargins(0, 0, 0, 0);
					
					
					
					tvparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
					tvparams.setMargins(10, 0, 0, 0);
						
					lltxtparams = new LinearLayout.LayoutParams(
							320, ViewGroup.LayoutParams.MATCH_PARENT);
					lltxtparams.setMargins(0, 0, 0, 0);
										
					downloadparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
					downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					downloadparams.setMargins(0, 0, 20, 0);

					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					viewparams.setMargins(20, 0, 150, 0);
					
					rl[i] = new RelativeLayout(this);
					rl[i].setPadding(0, 20, 0, 20);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					rl[i].addView(lltxt);
					
					

					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
//					tvstatus[i].setText("The Redord is ");
					tvstatus[i].setText(data[i]);
					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextSize(14);
//					tvstatus[i].setGravity(Gravity.CENTER_VERTICAL);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setOrientation(LinearLayout.VERTICAL);
					lldownload.setLayoutParams(downloadparams);
					lldownload.setGravity(Gravity.CENTER_HORIZONTAL);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.topMargin=10;
					downloadparams1.gravity=Gravity.CENTER_HORIZONTAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
//					LinearLayout llpter=new LinearLayout(this);
//					llpter.setLayoutParams(downloadparams);
//					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i+"&#40"+ownersname[i]+"&#40"+i);
					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(pter[i]);
					
//					LinearLayout llview=new LinearLayout(this);
//					llview.setLayoutParams(viewparams);
//					rl[i].addView(llview);

					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(view[i]);
					
				}

				path = pdfpath[i];
				String[] filenamesplit = path.split("/");
				final String filename = filenamesplit[filenamesplit.length - 1];
				System.out.println("the file name is" + filename);
				File sdDir = new File(Environment.getExternalStorageDirectory()
						.getPath());
				File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
						+ filename);

				if (file.exists()) {
					pter[i].setVisibility(View.VISIBLE);
					download[i].setVisibility(View.GONE);
				} else {
					pter[i].setVisibility(View.VISIBLE);
					download[i].setVisibility(View.GONE);
				}

				view[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Button b = (Button) v;
						String buttonvalue = v.getTag().toString();
						System.out.println("buttonvalue is" + buttonvalue);
						int s = Integer.parseInt(buttonvalue);
						path = pdfpath[s];
						String[] filenamesplit = path
								.split("/");
						final String filename = filenamesplit[filenamesplit.length - 1];
						System.out
								.println("The File Name is "
										+ filename);
						File sdDir = new File(Environment.getExternalStorageDirectory()
								.getPath());
						File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
								+ filename);
						
						if(file.exists())
						{
							View_Pdf_File(filename,path);
						}
						else
						{
							if (cf.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading... Please wait.");
								new Thread() {
									public void run() {
										Looper.prepare();
										try {
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {
										@Override
										public void handleMessage(Message msg) {
											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												toast = new ShowToast(
														SyncInspection.this,
														"There is a problem on your application. Please contact Paperless administrator.");

											} else if (show_handler == 2) {
												show_handler = 0;
//												toast = new ShowToast(
//														VehicleInspection.this,
//														"Report downloaded successfully");
												
//												alertDialog = new AlertDialog.Builder(VehicleInspection.this).create();
//												alertDialog
//														.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
//												alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
//														new DialogInterface.OnClickListener() {
//															public void onClick(DialogInterface dialog, int which) {
//																Call_Dynamic_Pdf_Display();
//															}
//														});
//												alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
//														new DialogInterface.OnClickListener() {
//
//															@Override
//															public void onClick(DialogInterface dialog, int which) {
//																// TODO Auto-generated method stub
//																Call_Dynamic_Pdf_Display();
//															}
//														});
//
//												alertDialog.show();
												
//												Call_Dynamic_Pdf_Display();
												
												View_Pdf_File(filename,path);

											}
										}
									};
								}.start();
							} else {
								toast = new ShowToast(SyncInspection.this,
										"Internet connection not available");

							}
						}
					}
				});

				download[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(final View v) {
						// TODO Auto-generated method stub
						Button b = (Button) v;
						if (b.getText().toString().equals("Download")) {
							if (cf.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading");
								new Thread() {
									public void run() {
										Looper.prepare();
										try {
											Button b = (Button) v;
											String buttonvalue = v.getTag()
													.toString();
											int s = Integer
													.parseInt(buttonvalue);
											path = pdfpath[s];
											String[] filenamesplit = path
													.split("/");
											String filename = filenamesplit[filenamesplit.length - 1];
											System.out
													.println("The File Name is "
															+ filename);
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {
										@Override
										public void handleMessage(Message msg) {
											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												toast = new ShowToast(
														SyncInspection.this,
														"There is a problem on your application. Please contact Paperless administrator.");

											} else if (show_handler == 2) {
												show_handler = 0;
//												toast = new ShowToast(
//														AgentInspection2.this,
//														"Report downloaded successfully");
//												Call_Dynamic_Pdf_Display();
												
												alertDialog = new AlertDialog.Builder(SyncInspection.this).create();
												alertDialog
														.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
												alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
														new DialogInterface.OnClickListener() {
															public void onClick(DialogInterface dialog, int which) {
																Call_Dynamic_Pdf_Display();
															}
														});
												alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
														new DialogInterface.OnClickListener() {

															@Override
															public void onClick(DialogInterface dialog, int which) {
																// TODO Auto-generated method stub
																Call_Dynamic_Pdf_Display();
															}
														});

												alertDialog.show();
												

											}
										}
									};
								}.start();
							} else {
								toast = new ShowToast(SyncInspection.this,
										"Internet connection not available");

							}
						} else {
							toast = new ShowToast(SyncInspection.this,
									"This TAB is under Construction");
						}

					}
				});

				pter[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						String buttonvalue = v.getTag().toString();
						String[] splitvalue=buttonvalue.split("&#40");
						int s = Integer.parseInt(splitvalue[0]);
						String name=splitvalue[1];
						path = pdfpath[s];
						reportpath=path;
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];
						
						
						String ivalue=splitvalue[2];
						System.out.println("ivalue ="+ivalue);
						int ival=Integer.parseInt(ivalue);
						String ivalsplit=datasend[ival];
						System.out.println("ivaluesplit ="+ivalsplit);
						String pn=datasend[ival];
						String status="";

//						Intent intent=new Intent(AgentInspection2.this,EmailReport3.class);
//						intent.putExtra("emailaddress", "");
//						intent.putExtra("pdfpath", path);
//						intent.putExtra("name", name);
//						intent.putExtra("classidentifier", "AgentInspection2");
//						intent.putExtra("policynumber", pn);
//						intent.putExtra("status", status);
//						startActivity(intent);
////						finish();
						
						Intent intent=new Intent(SyncInspection.this,EmailReport2.class);
						intent.putExtra("policynumber", "Sync");
						intent.putExtra("status", status);
						intent.putExtra("mailid", "");
						intent.putExtra("classidentifier", "Sync");
						intent.putExtra("ownersname", ownersname[s]);
						startActivity(intent);
						finish();

					}
				});

				if (i % 2 == 0) {
					rl[i].setBackgroundColor(Color.parseColor("#13456d"));
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#386588"));
				}
				/*if (data.length != (i + 1)) {
					View v1 = new View(this);
					v1.setBackgroundResource(R.color.white);
					l1.addView(v1, LayoutParams.FILL_PARENT, 1);
				}*/

				i++;
			} while (cur.moveToNext());

			/*LinearLayout.LayoutParams viewparams2 = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT, 1);
			viewparams2.setMargins(20, 0, 20, 0);

			View v2 = new View(this);
			v2.setBackgroundResource(R.color.black);
			dynamic.addView(v2, viewparams2);*/

		}
		else
		{
			rlnote.setVisibility(View.GONE);
		}
		cur.close();
	}
	private void View_Pdf_File(String filename,String filepath)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(SyncInspection.this,
					ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			// finish();
        }
	}
	
	public void clicker(View v)
	{
		switch (v.getId()) {
		 case R.id.agentinsp_home:
			Intent intenthome = new Intent(SyncInspection.this,HomeScreen.class);
			intenthome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intenthome);
			finish();
			break;
		 case R.id.back:
			 onBackPressed();
			 break;
		default:
			break;
		}
	}
	
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(SyncInspection.this, CompletedInspection.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
		
	}
}
