package idsoft.agentmodule;

import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.ksoap2.serialization.SoapObject;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class Check_Dashboard_Count extends BroadcastReceiver {

	NotificationManager nm;
	CommonFunctions cf;
	String oldpropertycount;
	int show_handler;
	ShowToast toast;

	@Override
	public void onReceive(final Context context, Intent intent) {
		
		cf = new CommonFunctions(context);
		cf.CreateTable(21);
		Cursor cur = cf.db.rawQuery("select * from " + cf.Property_Count, null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			do {
				oldpropertycount = cf.decode(cur.getString(cur
						.getColumnIndex("propertycount")));
			} while (cur.moveToNext());
		}
		cur.close();

		cf.CreateTable(1);
		Cursor cur1 = cf.db.rawQuery("select * from " + cf.LoginPage, null);
		cur1.moveToFirst();
		if (cur1.getCount() >= 1) {
			cf.agentid = cf
					.decode(cur1.getString(cur1.getColumnIndex("agentid")));
		}

		if (cf.isInternetOn() == true) {
			new Thread() {
				SoapObject chklogin;

				public void run() {
					Looper.prepare();
					try {
						chklogin = cf.Calling_WS_DashBoard(cf.agentid, "All",
								"AgentDashboardDetails");
						System.out.println("Calling AgentDashboardDetails webservice");
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						if (show_handler == 3) {
							show_handler = 0;

						} else if (show_handler == 4) {
							show_handler = 0;

						} else if (show_handler == 5) {
							show_handler = 0;
							int newcount = chklogin.getPropertyCount();
							System.out.println("The old count is "+Integer.parseInt(oldpropertycount));
							System.out.println("The new count is "+newcount);
							if (newcount > Integer.parseInt(oldpropertycount)) {
								cf.CreateTable(21);
								cf.db.execSQL("delete from "
										+ cf.Property_Count);
								cf.db.execSQL("insert into "
										+ cf.Property_Count
										+ "(propertycount) values('"
										+ cf.encode(String.valueOf(newcount)) + "')");
								Send_Notification(context);
							}
							else
							{
								Send_Notification1(context);
							}
						}
					}
				};
			}.start();

		} else {
			
		}

		

	}

	private void Send_Notification(Context context) {
		nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		CharSequence from = "Inspection Depot";
		CharSequence message = "New data is available in dashboard , Please open the Agent Inspection and click send or receive";
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				new Intent(), 0);
		Notification notif = new Notification(R.drawable.ic_launcher,
				"New data is available in dashboard , Please open the Agent Inspection and click send or receive", System.currentTimeMillis());
		notif.setLatestEventInfo(context, from, message, contentIntent);
		nm.notify(1, notif);
	}
	
	private void Send_Notification1(Context context) {
		nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		CharSequence from = "Inspection Depot";
		CharSequence message = "No Data Available";
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				new Intent(), 0);
		Notification notif = new Notification(R.drawable.ic_launcher,
				"No Data Available", System.currentTimeMillis());
		notif.setLatestEventInfo(context, from, message, contentIntent);
		nm.notify(1, notif);
	}
	
}
