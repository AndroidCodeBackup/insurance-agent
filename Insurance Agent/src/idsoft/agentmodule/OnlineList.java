package idsoft.agentmodule;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class OnlineList extends Activity {
	TextView status, noofdatas, inspectionname;
	AutoCompleteTextView etsearch;
	String strsearch, downloadidentifier = "0",status_check;
	Button search, cancel, home, placeorder;
	LinearLayout dynamic;
	CommonFunctions cf;
	ShowToast toast;
	int i;
	String[] data, pdfpath, datasend;
	String stragentname, path, agentemail = "", agencyemail = "", filename;
	int show_handler;
	static OnlineList ol;
	static String strstatus,inspid,strinspectionname;
	AlertDialog alertDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.onlinelist);
		cf = new CommonFunctions(this);
		ol=this;
		ImageView iv=(ImageView)findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);
		//name = (TextView) findViewById(R.id.onlinelist_name);
		status = (TextView) findViewById(R.id.onlinelist_status);
		noofdatas = (TextView) findViewById(R.id.onlinelist_noofdatas);
		inspectionname = (TextView) findViewById(R.id.onlinelist_inspectionname);
		dynamic = (LinearLayout) findViewById(R.id.onlinelist_tablelayoutdynamic);
		etsearch = (AutoCompleteTextView) findViewById(R.id.onlinelist_etsearch);
		
		Intent getintent = getIntent();
		Bundle bundle = getintent.getExtras();
		strstatus = bundle.getString("status");
		inspid = bundle.getString("inspid");
		strinspectionname = bundle.getString("inspectionname");
		
		if(strstatus.equals("Awaiting"))
		{
			status_check = "10,20,30";
		}
		else if(strstatus.equals("Scheduled"))
		{
			status_check = "40";
		}
		else if(strstatus.equals("Suspended"))
		{
			status_check = "120";
		}
		else if(strstatus.equals("Inspected"))
		{
			status_check = "1";
		}
		else if(strstatus.equals("Other"))
		{
			status_check = "2,50,60,70,80,90,110";
		}
		else if(strstatus.equals("ReportsReady"))
		{
			status_check = "2,5";
		}
		
		cf.CreateTable(1);
		Cursor cur = cf.db.rawQuery("select * from " + cf.AgentInformation,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			stragentname = cur.getString(cur.getColumnIndex("FirstName"));
			stragentname += " " + cur.getString(cur.getColumnIndex("LastName"));
		}
		cur.close();
		
		cf.CreateTable(3);
		Cursor cur1 = cf.db.rawQuery("select * from "
				+ cf.OnlineSyncPolicyInfo_Agency+" where Status in("+status_check+") and InspectionTypeId='"+inspid+"' order by AssignedDate desc", null);
		System.out.println("select * from "
				+ cf.OnlineSyncPolicyInfo_Agency+" where Status in("+status_check+") and InspectionTypeId='"+inspid+"' order by AssignedDate desc");
		int strnoofdatas=cur1.getCount();
		DynamicList(cur1);
		cur1.close();

		
		//name.setText(stragentname);
		status.setText(strstatus);
		noofdatas.setText(Integer.toString(strnoofdatas));
		inspectionname.setText(strinspectionname);
		etsearch.addTextChangedListener(new Text_Watcher());
	}
	
	class Text_Watcher implements TextWatcher
	{

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			try {
        		cf.CreateTable(3);
  				Cursor cur1 = cf.db.rawQuery("select * from "
  						+ cf.OnlineSyncPolicyInfo_Agency
  						+ " where OwnerPolicyNo like '%" + etsearch.getText().toString() + "%'", null);
  				String[] autopolicyno = new String[cur1.getCount()];
  				cur1.moveToFirst();
  				if(cur1.getCount()!=0)
  				{
  					if (cur1 != null) {
  						int i = 0;
  						do {
  							autopolicyno[i] = cf.decode(cur1.getString(cur1.getColumnIndex("OwnerPolicyNo")));
  							
  							if (autopolicyno[i].contains("null")) {
  								autopolicyno[i] = autopolicyno[i].replace("null", "");
  							}
  							 
  							i++;
  						} while (cur1.moveToNext());
  					}
  					cur1.close();
  				}
//  				ArrayAdapter<String> adapter = new ArrayAdapter<String>(OnlineList.this,R.layout.autocompletelist,autopolicyno);
  				
  				Cursor cur2 = cf.db.rawQuery("select * from "
  						+ cf.OnlineSyncPolicyInfo_Agency
  						+ " where FirstName like '%" + etsearch.getText().toString() + "%'", null);
  				String[] autofirstname = new String[cur2.getCount()];
  				cur2.moveToFirst();
  				if(cur2.getCount()!=0)
  				{
  					if (cur2 != null) {
  						int i = 0;
  						do {
  							autofirstname[i] = cf.decode(cur2.getString(cur2.getColumnIndex("FirstName")));
  							
  							if (autofirstname[i].contains("null")) {
  								autofirstname[i] = autofirstname[i].replace("null", "");
  							}
  							 
  							i++;
  						} while (cur2.moveToNext());
  					}
  					cur2.close();
  				}
//  				adapter = new ArrayAdapter<String>(OnlineList.this,R.layout.autocompletelist,autofirstname);
  				
  				Cursor cur3 = cf.db.rawQuery("select * from "
  						+ cf.OnlineSyncPolicyInfo_Agency
  						+ " where LastName like '%" + etsearch.getText().toString() + "%'", null);
  				String[] autolastname = new String[cur3.getCount()];
  				cur3.moveToFirst();
  				if(cur3.getCount()!=0)
  				{
  					if (cur3 != null) {
  						int i = 0;
  						do {
  							autolastname[i] = cf.decode(cur3.getString(cur3.getColumnIndex("LastName")));
  							
  							if (autolastname[i].contains("null")) {
  								autolastname[i] = autolastname[i].replace("null", "");
  							}
  							 
  							i++;
  						} while (cur3.moveToNext());
  					}
  					cur3.close();
  				}
//  				adapter = new ArrayAdapter<String>(OnlineList.this,R.layout.autocompletelist,autolastname);
  				
  				List<String> images = new ArrayList<String>();
  				images.addAll(Arrays.asList(autopolicyno));
  				images.addAll(Arrays.asList(autofirstname));
  				images.addAll(Arrays.asList(autolastname));
  				
  				ArrayAdapter<String> adapter = new ArrayAdapter<String>(OnlineList.this,R.layout.autocompletelist,images);
  				
  				etsearch.setThreshold(1);
  				etsearch.setAdapter(adapter);
  				
  			}
  			catch(Exception e)
  			{
  				
  			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			if (etsearch.getText().toString().startsWith(" "))
	        {
	            // Not allowed
	        	etsearch.setText("");
	        }
		}
		
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.onlinelist_search:
			SearchList();
			break;

		case R.id.onlinelist_cancel:
			etsearch.setText("");
			cf.CreateTable(3);
			Cursor cur1 = cf.db.rawQuery("select * from "
					+ cf.OnlineSyncPolicyInfo_Agency+" order by AssignedDate desc", null);
			DynamicList(cur1);
			cur1.close();
			break;

		case R.id.onlinelist_home:
			Intent intent = new Intent(OnlineList.this, HomeScreen.class);
			startActivity(intent);
			finish();
			break;

		case R.id.onlinelist_placeorder:
			Intent intent1 = new Intent(OnlineList.this, OrderInspection.class);
			startActivity(intent1);
			finish();
			break;
		}

	}

	private void SearchList() {
		strsearch = cf.encode(etsearch.getText().toString());
		if (strsearch.equals("")) {
			toast = new ShowToast(OnlineList.this,
					"Please enter the Name or Policy Number to search.");
			etsearch.requestFocus();
		} else {
			dbquery();
		}
	}

	private void dbquery() {
		Cursor cur = null;
		if (!strsearch.trim().equals("")) {
			cf.CreateTable(3);
			cur = cf.db.rawQuery("select * from "
					+ cf.OnlineSyncPolicyInfo_Agency
					+ " where FirstName like '%" + strsearch
					+ "%' or LastName like '%" + strsearch
					+ "%' or OwnerPolicyNo like '%" + strsearch + "%'"+" order by AssignedDate desc", null);
		}
		cur.moveToFirst();
		int rows = cur.getCount();
		if (cur.getCount() >= 1) {
			DynamicList(cur);
		} else {
			toast = new ShowToast(OnlineList.this,
					"Sorry, No Results Available");
			cf.hidekeyboard((EditText)etsearch);
		}

	}

	private void DynamicList(Cursor cur) {
		dynamic.removeAllViews();
		ScrollView sv = new ScrollView(this);
		dynamic.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		LinearLayout.LayoutParams viewparams1 = new LinearLayout.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, 1);
		viewparams1.setMargins(20, 0, 0, 0);

		/*View v = new View(this);
		v.setBackgroundResource(R.color.black);
		l1.addView(v, viewparams1);*/

		int rows = cur.getCount();
		noofdatas.setText(String.valueOf(rows));
		TextView[] tvstatus = new TextView[rows];
		Button[] view = new Button[rows];
		final Button[] download = new Button[rows];
		final Button[] pter = new Button[rows];
		final Button[] sendmail = new Button[rows];
		LinearLayout llsendmail = null;
		// LinearLayout[] l2 = new LinearLayout[rows];
		RelativeLayout[] rl = new RelativeLayout[rows];
		final String[] ownersname=new String[rows];
		data = new String[rows];
		datasend = new String[rows];
		pdfpath = new String[rows];
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			i = 0;
			do {
				String SRID = cf.decode(cur.getString(cur
						.getColumnIndex("SRID")));
				// data[i] = " " + SRID + " ";
				String FirstName = cf.decode(cur.getString(cur
						.getColumnIndex("FirstName")));
				data[i] = " " + FirstName + " | ";
				ownersname[i]=FirstName+" ";
				String LastName = cf.decode(cur.getString(cur
						.getColumnIndex("LastName")));
				data[i] += LastName + " | ";
				ownersname[i] +=" "+FirstName;
				String MiddleName = cf.decode(cur.getString(cur
						.getColumnIndex("MiddleName")));
				// data[i] += MiddleName + " | ";
				String Address1 = cf.decode(cur.getString(cur
						.getColumnIndex("Address1")));
				data[i] += Address1 + " | ";
				String Address2 = cf.decode(cur.getString(cur
						.getColumnIndex("Address2")));
				data[i] += Address2 + " | ";
				String City = cf.decode(cur.getString(cur
						.getColumnIndex("City")));
				data[i] += City + " | ";
				String StateId = cf.decode(cur.getString(cur
						.getColumnIndex("StateId")));
				// data[i] += StateId + " | ";
				String State = cf.decode(cur.getString(cur
						.getColumnIndex("State")));
				data[i] += State + " | ";
				String Country = cf.decode(cur.getString(cur
						.getColumnIndex("Country")));
				data[i] += Country + " | ";
				String Zip = cf
						.decode(cur.getString(cur.getColumnIndex("Zip")));
				data[i] += Zip + " | ";
				String HomePhone = cf.decode(cur.getString(cur
						.getColumnIndex("HomePhone")));
				// data[i] += HomePhone + " | ";
				String CellPhone = cf.decode(cur.getString(cur
						.getColumnIndex("CellPhone")));
				// data[i] += CellPhone + " | ";
				String WorkPhone = cf.decode(cur.getString(cur
						.getColumnIndex("WorkPhone")));
				// data[i] += WorkPhone + " | ";
				String Email = cf.decode(cur.getString(cur
						.getColumnIndex("Email")));
				data[i] += Email + " | ";
				String ContactPerson = cf.decode(cur.getString(cur
						.getColumnIndex("ContactPerson")));
				// data[i] += ContactPerson + " | ";
				String OwnerPolicyNo = cf.decode(cur.getString(cur
						.getColumnIndex("OwnerPolicyNo")));
				data[i] += OwnerPolicyNo + " | ";
				datasend[i] = OwnerPolicyNo + "&#40";
				String Status = cf.decode(cur.getString(cur
						.getColumnIndex("Status")));
				// data[i] += Status + " | ";
				datasend[i] += Status;
				String SubStatusID = cf.decode(cur.getString(cur
						.getColumnIndex("SubStatusID")));
				// data[i] += SubStatusID + " | ";
				String statefarmcompanyid = cf.decode(cur.getString(cur
						.getColumnIndex("statefarmcompanyid")));
				// data[i] += statefarmcompanyid + " | ";
				String InspectorId = cf.decode(cur.getString(cur
						.getColumnIndex("InspectorId")));
				// data[i] += InspectorId + " | ";
				String wId = cf
						.decode(cur.getString(cur.getColumnIndex("wId")));
				// data[i] += wId + " | ";
				String CompanyId = cf.decode(cur.getString(cur
						.getColumnIndex("CompanyId")));
				// data[i] += CompanyId + " | ";
				String InspectorFirstName = cf.decode(cur.getString(cur
						.getColumnIndex("InspectorFirstName")));
				// data[i] += InspectorFirstName + " | ";
				String InspectorLastName = cf.decode(cur.getString(cur
						.getColumnIndex("InspectorLastName")));
				// data[i] += InspectorLastName + " | ";
				String ScheduledDate = cf.decode(cur.getString(cur
						.getColumnIndex("ScheduledDate")));
				// data[i] += ScheduledDate + " | ";
				String YearBuilt = cf.decode(cur.getString(cur
						.getColumnIndex("YearBuilt")));
				// data[i] += YearBuilt + " | ";
				String Nstories = cf.decode(cur.getString(cur
						.getColumnIndex("Nstories")));
				// data[i] += Nstories + " | ";
				String InspectionTypeId = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionTypeId")));
				// data[i] += InspectionTypeId + " | ";
				String ScheduleCreatedDate = cf.decode(cur.getString(cur
						.getColumnIndex("ScheduleCreatedDate")));
				// data[i] += ScheduleCreatedDate + " | ";
				String AssignedDate = cf.decode(cur.getString(cur
						.getColumnIndex("AssignedDate")));
				// data[i] += AssignedDate + " | ";
				String InspectionStartTime = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionStartTime")));
				// data[i] += InspectionStartTime + " | ";
				String InspectionEndTime = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionEndTime")));
				// data[i] += InspectionEndTime + " | ";
				String InspectionComment = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionComment")));
				// data[i] += InspectionComment + " | ";
				String IsInspected = cf.decode(cur.getString(cur
						.getColumnIndex("IsInspected")));
				// data[i] += IsInspected + " | ";
				String InsuranceCompany = cf.decode(cur.getString(cur
						.getColumnIndex("InsuranceCompany")));
				// data[i] += InsuranceCompany + " | ";
				String s_InspFees = cf.decode(cur.getString(cur
						.getColumnIndex("s_InspFees")));
				// data[i] += s_InspFees + " | ";
				String InsuranceAgentName = cf.decode(cur.getString(cur
						.getColumnIndex("InsuranceAgentName")));
				// data[i] += InsuranceAgentName + " | ";
				String AgentEmail = cf.decode(cur.getString(cur
						.getColumnIndex("AgentEmail")));
				// data[i] += AgentEmail + " | ";
				String InsuranceAgencyName = cf.decode(cur.getString(cur
						.getColumnIndex("InsuranceAgencyName")));
				// data[i] += InsuranceAgencyName + " | ";
				String AgencyEmail = cf.decode(cur.getString(cur
						.getColumnIndex("AgencyEmail")));
				// data[i] += AgencyEmail + " | ";
				String COMMpdf = cf.decode(cur.getString(cur
						.getColumnIndex("COMMpdf")));
				String COMMMergedpdf = cf.decode(cur.getString(cur
						.getColumnIndex("COMMMergedpdf")));
				String RoofPdfPath = cf.decode(cur.getString(cur
						.getColumnIndex("RoofPdfPath")));
				pdfpath[i] = COMMpdf;
				System.out.println("The roodpdf path is "+RoofPdfPath);
				System.out.println("The COMMpdf path is "+COMMpdf);
				System.out.println("The COMMMergedpdf path is "+COMMMergedpdf);

				System.out.println("The Datas is " + data[i]);
				
				LinearLayout.LayoutParams llparams;
				RelativeLayout.LayoutParams tvparams;
				RelativeLayout.LayoutParams downloadparams,pterparams,viewparams;
				LinearLayout.LayoutParams lltxtparams;
				
				
				Display display = getWindowManager().getDefaultDisplay();
			    DisplayMetrics displayMetrics = new DisplayMetrics();
			    display.getMetrics(displayMetrics);

			    int width = displayMetrics.widthPixels;
			    int height = displayMetrics.heightPixels;
			    
			    System.out.println("The width is "+width);
			    System.out.println("The height is "+height);
				
				if(width > 1023 || height > 1023)
				{
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					llparams.setMargins(20, 0, 0, 0);
					
					if (RoofPdfPath.equals("N/A")) {
						tvparams = new RelativeLayout.LayoutParams(
								ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.MATCH_PARENT);
						tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
						tvparams.setMargins(20, 0, 275, 0);
					}
					else
					{
						tvparams = new RelativeLayout.LayoutParams(
								ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.MATCH_PARENT);
						tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
						tvparams.setMargins(20, 0, 275, 0);
					}
					

					downloadparams = new RelativeLayout.LayoutParams(
							110, ViewGroup.LayoutParams.MATCH_PARENT);
					downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					downloadparams.setMargins(0, 0, 20, 0);
					
					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					viewparams.setMargins(20, 0, 150, 0);
					
					rl[i] = new RelativeLayout(this);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					rl[i].addView(lltxt);
					
					lltxtparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					lltxtparams.setMargins(0, 30, 0, 30);
	
					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
					tvstatus[i].setText(data[i]);
//					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextColor(Color.WHITE);
					tvstatus[i].setTextSize(14);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setLayoutParams(downloadparams);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							110, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.gravity=Gravity.CENTER_VERTICAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
					LinearLayout llpter=new LinearLayout(this);
					llpter.setLayoutParams(downloadparams);
					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i+"&#40"+Email+"&#40"+ownersname[i]+"&#40"+i);
//					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					llpter.addView(pter[i]);
					
					llsendmail=new LinearLayout(this);
					llsendmail.setLayoutParams(downloadparams);
					rl[i].addView(llsendmail);
					
					sendmail[i] = new Button(this);
					sendmail[i].setLayoutParams(downloadparams1);
					sendmail[i].setId(2);
					sendmail[i].setText("Send Mail");
					sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
					sendmail[i].setTextColor(0xffffffff);
					sendmail[i].setTextSize(14);
					sendmail[i].setTypeface(null, Typeface.BOLD);
					sendmail[i].setTag(Email+"&#40"+ownersname[i]+"&#40"+i);
//					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					llsendmail.addView(sendmail[i]);
					
					llsendmail.setVisibility(View.GONE);
					
					LinearLayout llview=new LinearLayout(this);
					llview.setLayoutParams(viewparams);
					rl[i].addView(llview);
	
					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					llview.addView(view[i]);
					
//					if (RoofPdfPath.equals("N/A")) {
//						llsendmail.setVisibility(View.VISIBLE);
//						sendmail[i].setVisibility(View.VISIBLE);
//						download[i].setVisibility(View.GONE);
//						view[i].setVisibility(View.GONE);
//					}
					
					if (COMMpdf.equals("N/A")) {
						llsendmail.setVisibility(View.VISIBLE);
						sendmail[i].setVisibility(View.VISIBLE);
						download[i].setVisibility(View.GONE);
						view[i].setVisibility(View.GONE);
						pter[i].setVisibility(View.GONE);
					}
					else
					{
						llsendmail.setVisibility(View.GONE);
						sendmail[i].setVisibility(View.GONE);
						download[i].setVisibility(View.VISIBLE);
						view[i].setVisibility(View.VISIBLE);
						pter[i].setVisibility(View.VISIBLE);
						
						path = pdfpath[i];
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];
						System.out.println("the file name is" + filename);
						File sdDir = new File(Environment.getExternalStorageDirectory()
								.getPath());
						File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
								+ filename);

						if (file.exists()) {
							pter[i].setVisibility(View.VISIBLE);
							download[i].setVisibility(View.GONE);
						} else {
							pter[i].setVisibility(View.VISIBLE);
							download[i].setVisibility(View.GONE);
						}
						
					}

				}
				
				else if((width > 500 && width<1000) || (height > 500 && height<1000))
				{

					
					System.out.println("800x600");
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					llparams.setMargins(20, 0, 0, 0);
					
					
					
					if (RoofPdfPath.equals("N/A")) {
						tvparams = new RelativeLayout.LayoutParams(
								ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.MATCH_PARENT);
						tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
						tvparams.setMargins(10, 0, 170, 0);
						
						lltxtparams = new LinearLayout.LayoutParams(
								ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						lltxtparams.setMargins(0, 20, 0, 20);
						
					} else {
						tvparams = new RelativeLayout.LayoutParams(
								ViewGroup.LayoutParams.WRAP_CONTENT,
								ViewGroup.LayoutParams.MATCH_PARENT);
						tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
						tvparams.setMargins(10, 0, 170, 0);
						
						lltxtparams = new LinearLayout.LayoutParams(
								320, ViewGroup.LayoutParams.WRAP_CONTENT);
						lltxtparams.setMargins(0, 20, 0, 20);
						
					}

					downloadparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					downloadparams.setMargins(0, 0, 20, 0);

					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					viewparams.setMargins(20, 0, 150, 0);
					
					rl[i] = new RelativeLayout(this);
					rl[i].setPadding(0, 20, 0, 20);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					rl[i].addView(lltxt);
					
					

					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
					tvstatus[i].setText(data[i]);
//					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextColor(Color.WHITE);
					tvstatus[i].setTextSize(14);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setOrientation(LinearLayout.VERTICAL);
					lldownload.setLayoutParams(downloadparams);
					lldownload.setGravity(Gravity.CENTER_HORIZONTAL);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.topMargin=10;
					downloadparams1.gravity=Gravity.CENTER_HORIZONTAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
//					LinearLayout llpter=new LinearLayout(this);
//					llpter.setLayoutParams(downloadparams);
//					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i+"&#40"+Email+"&#40"+ownersname[i]+"&#40"+i);
					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(pter[i]);
					
					
					sendmail[i] = new Button(this);
					sendmail[i].setLayoutParams(downloadparams1);
					sendmail[i].setId(2);
					sendmail[i].setText("Send Mail");
					sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
					sendmail[i].setTextColor(0xffffffff);
					sendmail[i].setTextSize(14);
					sendmail[i].setTypeface(null, Typeface.BOLD);
					sendmail[i].setTag(Email+"&#40"+ownersname[i]+"&#40"+i);
					sendmail[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(sendmail[i]);
					
//					LinearLayout llview=new LinearLayout(this);
//					llview.setLayoutParams(viewparams);
//					rl[i].addView(llview);

					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(view[i]);
					
					if (RoofPdfPath.equals("N/A")) {
						sendmail[i].setVisibility(View.VISIBLE);
						download[i].setVisibility(View.GONE);
						view[i].setVisibility(View.GONE);
					}
					
					if (COMMpdf.equals("N/A")) {
						sendmail[i].setVisibility(View.VISIBLE);
						download[i].setVisibility(View.GONE);
						view[i].setVisibility(View.GONE);
						pter[i].setVisibility(View.GONE);
					}
					else
					{
						sendmail[i].setVisibility(View.GONE);
						download[i].setVisibility(View.VISIBLE);
						view[i].setVisibility(View.VISIBLE);
						pter[i].setVisibility(View.VISIBLE);
						
						path = pdfpath[i];
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];
						System.out.println("the file name is" + filename);
						File sdDir = new File(Environment.getExternalStorageDirectory()
								.getPath());
						File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
								+ filename);

						if (file.exists()) {
							pter[i].setVisibility(View.VISIBLE);
							download[i].setVisibility(View.GONE);
						} else {
							pter[i].setVisibility(View.VISIBLE);
							download[i].setVisibility(View.GONE);
						}
						
					}
					
				
				}
				
				else
				{
					
					System.out.println("Phoneeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
					llparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					llparams.setMargins(20, 0, 0, 0);
					
					
					
					if (RoofPdfPath.equals("N/A")) {
						tvparams = new RelativeLayout.LayoutParams(
								ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.MATCH_PARENT);
						tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
						tvparams.setMargins(10, 0, 170, 0);
						
						lltxtparams = new LinearLayout.LayoutParams(
								ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						lltxtparams.setMargins(0, 20, 0, 20);
						
					} else {
						tvparams = new RelativeLayout.LayoutParams(
								ViewGroup.LayoutParams.WRAP_CONTENT,
								ViewGroup.LayoutParams.MATCH_PARENT);
						tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
						tvparams.setMargins(10, 0, 170, 0);
						
						lltxtparams = new LinearLayout.LayoutParams(
								320, ViewGroup.LayoutParams.WRAP_CONTENT);
						lltxtparams.setMargins(0, 20, 0, 20);
						
					}

					downloadparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
					downloadparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					downloadparams.setMargins(0, 0, 20, 0);

					viewparams = new RelativeLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.MATCH_PARENT);
					viewparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					viewparams.setMargins(20, 0, 150, 0);
					
					rl[i] = new RelativeLayout(this);
					l1.addView(rl[i], llparams);
					
					LinearLayout lltxt=new LinearLayout(this);
					lltxt.setLayoutParams(tvparams);
					rl[i].addView(lltxt);
					
					

					tvstatus[i] = new TextView(this);
					tvstatus[i].setLayoutParams(lltxtparams);
					tvstatus[i].setId(1);
					tvstatus[i].setText(data[i]);
//					tvstatus[i].setTextColor(Color.parseColor("#76A4C8"));
					tvstatus[i].setTextColor(Color.WHITE);
					tvstatus[i].setTextSize(14);
					lltxt.addView(tvstatus[i]);
					
					LinearLayout lldownload=new LinearLayout(this);
					lldownload.setOrientation(LinearLayout.VERTICAL);
					lldownload.setLayoutParams(downloadparams);
					lldownload.setGravity(Gravity.CENTER_HORIZONTAL);
					rl[i].addView(lldownload);
					
					LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
					downloadparams1.topMargin=10;
					downloadparams1.gravity=Gravity.CENTER_HORIZONTAL;
					
					download[i] = new Button(this);
					download[i].setLayoutParams(downloadparams1);
					download[i].setId(2);
					download[i].setText("Download");
					download[i].setBackgroundResource(R.drawable.buttonrepeat);
					download[i].setTextColor(0xffffffff);
					download[i].setTextSize(14);
					download[i].setTypeface(null, Typeface.BOLD);
					download[i].setTag(i);
//					download[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(download[i]);
					
//					LinearLayout llpter=new LinearLayout(this);
//					llpter.setLayoutParams(downloadparams);
//					rl[i].addView(llpter);
					
					pter[i] = new Button(this);
					pter[i].setLayoutParams(downloadparams1);
					pter[i].setId(2);
					pter[i].setText("Email Report");
					pter[i].setBackgroundResource(R.drawable.buttonrepeat);
					pter[i].setTextColor(0xffffffff);
					pter[i].setTextSize(14);
					pter[i].setTypeface(null, Typeface.BOLD);
					pter[i].setTag(i+"&#40"+Email+"&#40"+ownersname[i]+"&#40"+i);
					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(pter[i]);
					
					
					sendmail[i] = new Button(this);
					sendmail[i].setLayoutParams(downloadparams1);
					sendmail[i].setId(2);
					sendmail[i].setText("Send Mail");
					sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
					sendmail[i].setTextColor(0xffffffff);
					sendmail[i].setTextSize(14);
					sendmail[i].setTypeface(null, Typeface.BOLD);
					sendmail[i].setTag(Email+"&#40"+ownersname[i]+"&#40"+i);
					sendmail[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(sendmail[i]);
					
//					LinearLayout llview=new LinearLayout(this);
//					llview.setLayoutParams(viewparams);
//					rl[i].addView(llview);

					view[i] = new Button(this);
					view[i].setLayoutParams(downloadparams1);
					view[i].setId(2);
					view[i].setText("View PDF");
					view[i].setBackgroundResource(R.drawable.buttonrepeat);
					view[i].setTextColor(0xffffffff);
					view[i].setTextSize(14);
					view[i].setTypeface(null, Typeface.BOLD);
					view[i].setTag(i);
//					view[i].setGravity(Gravity.CENTER_VERTICAL);
					lldownload.addView(view[i]);
					
//					if (RoofPdfPath.equals("N/A")) {
//						sendmail[i].setVisibility(View.VISIBLE);
//						download[i].setVisibility(View.GONE);
//						view[i].setVisibility(View.GONE);
//					}
					
					if (COMMpdf.equals("N/A")) {
						sendmail[i].setVisibility(View.VISIBLE);
						download[i].setVisibility(View.GONE);
						view[i].setVisibility(View.GONE);
						pter[i].setVisibility(View.GONE);
					}
					else
					{
						sendmail[i].setVisibility(View.GONE);
						download[i].setVisibility(View.VISIBLE);
						view[i].setVisibility(View.VISIBLE);
						pter[i].setVisibility(View.VISIBLE);
						
						path = pdfpath[i];
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];
						System.out.println("the file name is" + filename);
						File sdDir = new File(Environment.getExternalStorageDirectory()
								.getPath());
						File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
								+ filename);

						if (file.exists()) {
							pter[i].setVisibility(View.VISIBLE);
							download[i].setVisibility(View.GONE);
						} else {
							pter[i].setVisibility(View.VISIBLE);
							download[i].setVisibility(View.GONE);
						}
						
					}
					
				}

//				rl[i] = new RelativeLayout(this);
//				l1.addView(rl[i], llparams);
//				
//				LinearLayout lltxt=new LinearLayout(this);
//				lltxt.setLayoutParams(tvparams);
//				rl[i].addView(lltxt);
//				
//				LinearLayout.LayoutParams lltxtparams = new LinearLayout.LayoutParams(
//						ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//				lltxtparams.setMargins(0, 20, 0, 20);
//
//				tvstatus[i] = new TextView(this);
//				tvstatus[i].setLayoutParams(lltxtparams);
//				tvstatus[i].setId(1);
//				tvstatus[i].setText(data[i]);
//				tvstatus[i].setTextColor(Color.BLACK);
//				tvstatus[i].setTextSize(14);
//				lltxt.addView(tvstatus[i]);
//				
//				LinearLayout lldownload=new LinearLayout(this);
//				lldownload.setLayoutParams(downloadparams);
//				rl[i].addView(lldownload);
//				
//				LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
//						110, ViewGroup.LayoutParams.WRAP_CONTENT);
//				downloadparams1.gravity=Gravity.CENTER_VERTICAL;
//				
//				download[i] = new Button(this);
//				download[i].setLayoutParams(downloadparams1);
//				download[i].setId(2);
//				download[i].setText("Download");
//				download[i].setBackgroundResource(R.drawable.buttonrepeat);
//				download[i].setTextColor(0xffffffff);
//				download[i].setTextSize(14);
//				download[i].setTypeface(null, Typeface.BOLD);
//				download[i].setTag(i);
////				download[i].setGravity(Gravity.CENTER_VERTICAL);
//				lldownload.addView(download[i]);
//				
//				LinearLayout llpter=new LinearLayout(this);
//				llpter.setLayoutParams(downloadparams);
//				rl[i].addView(llpter);
//				
//				pter[i] = new Button(this);
//				pter[i].setLayoutParams(downloadparams1);
//				pter[i].setId(2);
//				pter[i].setText("Email Report");
//				pter[i].setBackgroundResource(R.drawable.buttonrepeat);
//				pter[i].setTextColor(0xffffffff);
//				pter[i].setTextSize(14);
//				pter[i].setTypeface(null, Typeface.BOLD);
//				pter[i].setTag(i);
//				pter[i].setGravity(Gravity.CENTER_VERTICAL);
//				llpter.addView(pter[i]);
//				
//				LinearLayout llview=new LinearLayout(this);
//				llview.setLayoutParams(viewparams);
//				rl[i].addView(llview);
//
//				view[i] = new Button(this);
//				view[i].setLayoutParams(downloadparams1);
//				view[i].setId(2);
//				view[i].setText("View PDF");
//				view[i].setBackgroundResource(R.drawable.buttonrepeat);
//				view[i].setTextColor(0xffffffff);
//				view[i].setTextSize(14);
//				view[i].setTypeface(null, Typeface.BOLD);
//				view[i].setTag(i);
////				view[i].setGravity(Gravity.CENTER_VERTICAL);
//				llview.addView(view[i]);

//				path = pdfpath[i];
//				String[] filenamesplit = path.split("/");
//				String filename = filenamesplit[filenamesplit.length - 1];
//				System.out.println("the file name is" + filename);
//				File sdDir = new File(Environment.getExternalStorageDirectory()
//						.getPath());
//				File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
//						+ filename);
//
//				if (file.exists()) {
//					pter[i].setVisibility(View.VISIBLE);
//					download[i].setVisibility(View.GONE);
//				} else {
//					pter[i].setVisibility(View.GONE);
//					download[i].setVisibility(View.VISIBLE);
//				}

				view[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Button b = (Button) v;
						String buttonvalue = v.getTag().toString();
						System.out.println("buttonvalue is" + buttonvalue);
						int s = Integer.parseInt(buttonvalue);
						path = pdfpath[s];
						String[] filenamesplit = path
								.split("/");
						final String filename = filenamesplit[filenamesplit.length - 1];
						System.out
								.println("The File Name is "
										+ filename);
						File sdDir = new File(Environment.getExternalStorageDirectory()
								.getPath());
						File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
								+ filename);
						
						if(file.exists())
						{
							View_Pdf_File(filename,path);
						}
						else
						{
							if (cf.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading... Please wait.");
								new Thread() {
									public void run() {
										Looper.prepare();
										try {
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {
										@Override
										public void handleMessage(Message msg) {
											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												toast = new ShowToast(
														OnlineList.this,
														"There is a problem on your application. Please contact Paperless administrator.");

											} else if (show_handler == 2) {
												show_handler = 0;
//												toast = new ShowToast(
//														VehicleInspection.this,
//														"Report downloaded successfully");
												
//												alertDialog = new AlertDialog.Builder(VehicleInspection.this).create();
//												alertDialog
//														.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
//												alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
//														new DialogInterface.OnClickListener() {
//															public void onClick(DialogInterface dialog, int which) {
//																Call_Dynamic_Pdf_Display();
//															}
//														});
//												alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
//														new DialogInterface.OnClickListener() {
//
//															@Override
//															public void onClick(DialogInterface dialog, int which) {
//																// TODO Auto-generated method stub
//																Call_Dynamic_Pdf_Display();
//															}
//														});
//
//												alertDialog.show();
												
//												Call_Dynamic_Pdf_Display();
												
												View_Pdf_File(filename,path);

											}
										}
									};
								}.start();
							} else {
								toast = new ShowToast(OnlineList.this,
										"Internet connection not available");

							}
						}
					}
				});
				

				download[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(final View v) {
						// TODO Auto-generated method stub
						Button b = (Button) v;
						if (b.getText().toString().equals("Download")) {
							if (cf.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading... Please wait.");
								new Thread() {
									public void run() {
										Looper.prepare();
										try {
											Button b = (Button) v;
											String buttonvalue = v.getTag()
													.toString();
											int s = Integer
													.parseInt(buttonvalue);
											path = pdfpath[s];
											String[] filenamesplit = path
													.split("/");
											filename = filenamesplit[filenamesplit.length - 1];
											System.out
													.println("The File Name is "
															+ filename);
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {
										@Override
										public void handleMessage(Message msg) {
											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												toast = new ShowToast(
														OnlineList.this,
														"There is a problem on your application. Please contact Paperless administrator.");

											} else if (show_handler == 2) {
												show_handler = 0;
//												toast = new ShowToast(
//														OnlineList.this,
//														"Downloaded Successfully");
												
												alertDialog = new AlertDialog.Builder(OnlineList.this).create();
												alertDialog
														.setMessage("Report downloaded successfully"+"\n"+"The location of pdf file is Myfiles/DownloadedPdfFile/"+filename);
												alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
														new DialogInterface.OnClickListener() {
															public void onClick(DialogInterface dialog, int which) {
																cf.CreateTable(3);
																Cursor cur1 = cf.db
																		.rawQuery(
																				"select * from "
																						+ cf.OnlineSyncPolicyInfo_Agency,
																				null);
																DynamicList(cur1);
															}
														});
												alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
														new DialogInterface.OnClickListener() {

															@Override
															public void onClick(DialogInterface dialog, int which) {
																// TODO Auto-generated method stub
																cf.CreateTable(3);
																Cursor cur1 = cf.db
																		.rawQuery(
																				"select * from "
																						+ cf.OnlineSyncPolicyInfo_Agency,
																				null);
																DynamicList(cur1);
															}
														});

												alertDialog.show();

											}
										}
									};
								}.start();
							} else {
								toast = new ShowToast(OnlineList.this,
										"Internet Connection is not Available");

							}
						} else {
							toast = new ShowToast(OnlineList.this,
									"This TAB is under Construction");
						}

					}
				});

				pter[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// toast = new ShowToast(OnlineList.this,
						// "This TAB is under Construction");
						String buttonvalue = v.getTag().toString();
						String[] arrayvalue=buttonvalue.split("&#40");
						String value=arrayvalue[0];
						String emailaddress=arrayvalue[1];
						String name=arrayvalue[2];
						
						String ivalue=arrayvalue[3];
						int ival=Integer.parseInt(ivalue);
						String ivalsplit=datasend[ival];
						String[] arrayivalsplit=ivalsplit.split("&#40");
						String pn=arrayivalsplit[0];
						String status=arrayivalsplit[1];
						
						int s = Integer.parseInt(value);
						path = pdfpath[s];
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];
						
//						Intent intent=new Intent(OnlineList.this,EmailReport3.class);
//						intent.putExtra("emailaddress", emailaddress);
//						intent.putExtra("pdfpath", path);
//						intent.putExtra("name", name);
//						intent.putExtra("classidentifier", "OnlineList");
//						intent.putExtra("policynumber", pn);
//						intent.putExtra("status", strstatus);
//						startActivity(intent);
////						finish();
						
						Intent intent=new Intent(OnlineList.this,EmailReport2.class);
						intent.putExtra("policynumber", pn);
						intent.putExtra("status", strstatus);
						intent.putExtra("mailid", emailaddress);
						intent.putExtra("classidentifier", "OnlineList");
						intent.putExtra("ownersname", ownersname[ival]);
						startActivity(intent);
						finish();
						

					}
				});
				
				sendmail[i].setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						String buttonvalue = v.getTag().toString();
						String[] arrayvalue=buttonvalue.split("&#40");
						String emailaddress=arrayvalue[0];
						String name=arrayvalue[1];
						
						String ivalue=arrayvalue[2];
						int ival=Integer.parseInt(ivalue);
						String ivalsplit=datasend[ival];
						String[] arrayivalsplit=ivalsplit.split("&#40");
						String pn=arrayivalsplit[0];
						String status=arrayivalsplit[1];
						
//						Intent intent=new Intent(OnlineList.this,EmailReport3.class);
//						intent.putExtra("emailaddress", emailaddress);
//						intent.putExtra("pdfpath", "N/A");
//						intent.putExtra("name", name);
//						intent.putExtra("classidentifier", "OnlineList");
//						intent.putExtra("policynumber", pn);
//						intent.putExtra("status", strstatus);
//						startActivity(intent);
//						finish();
						
						Intent intent=new Intent(OnlineList.this,EmailReport2.class);
						intent.putExtra("policynumber", pn);
						intent.putExtra("status", strstatus);
						intent.putExtra("mailid", emailaddress);
						intent.putExtra("classidentifier", "OnlineList");
						intent.putExtra("ownersname", ownersname[ival]);
						startActivity(intent);
						finish();
						
					}
				});

//				if (RoofPdfPath.equals("N/A")) {
//					llsendmail.setVisibility(View.VISIBLE);
//					sendmail[i].setVisibility(View.VISIBLE);
//					download[i].setVisibility(View.GONE);
//					view[i].setVisibility(View.GONE);
//				}

				if (i % 2 == 0) {
					rl[i].setBackgroundColor(Color.parseColor("#13456d"));
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#386588"));
				}
				/*if (data.length != (i + 1)) {
					View v1 = new View(this);
					v1.setBackgroundResource(R.color.white);
					l1.addView(v1, LayoutParams.FILL_PARENT, 1);
				}*/

				i++;
			} while (cur.moveToNext());

			/*LinearLayout.LayoutParams viewparams2 = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT, 1);
			viewparams2.setMargins(20, 0, 20, 0);

			View v2 = new View(this);
			v2.setBackgroundResource(R.color.black);
			dynamic.addView(v2, viewparams2);*/

		}

	}
	
	private void View_Pdf_File(String filename,String filepath)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(OnlineList.this,
					ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			// finish();
        }
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		Intent inthome = new Intent(OnlineList.this, DashBoard.class);
		startActivity(inthome);
		finish();
	}

}
