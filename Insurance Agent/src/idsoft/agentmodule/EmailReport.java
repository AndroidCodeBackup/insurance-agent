package idsoft.agentmodule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class EmailReport extends Activity {
	AutoCompleteTextView actsearch;
	Button home, search, clear;
	CommonFunctions cf;
	ShowToast toast;
	LinearLayout lldynamic;
	RelativeLayout rlheader;
	String[] data,datasend;
	static String mailid,policyno,name;
	static EmailReport er;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email_report);
		cf = new CommonFunctions(this);
		er=this;
		ImageView iv=(ImageView)findViewById(R.id.ImageView02);
		cf.Dynamic_Image_Changing(iv);
		actsearch = (AutoCompleteTextView) findViewById(R.id.emailreport_actsearch);
		home = (Button) findViewById(R.id.emailreport_home);
		home = (Button) findViewById(R.id.emailreport_home);
		search = (Button) findViewById(R.id.emailreport_search);
		clear = (Button) findViewById(R.id.emailreport_cancel);
		lldynamic = (LinearLayout) findViewById(R.id.emailreport_lldynamic);
		rlheader = (RelativeLayout) findViewById(R.id.emailreport_rlheader);
		
		cf.CreateTable(22);
		Cursor cur = cf.db.rawQuery("select * from " + cf.Customer_Details, null);
		display_list(cur);

		TextWatcher textWatcher = new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence charSequence, int i,
					int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1,
					int i2) {
				if (actsearch.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	actsearch.setText("");
		        }
			}

			@Override
			public void afterTextChanged(Editable editable) {
				// here, after we introduced something in the EditText we get
				// the string from it
				try {
					cf.CreateTable(22);
					Cursor cur1 = cf.db.rawQuery(
							"select * from " + cf.Customer_Details
									+ " where PolicyNumber like '"
									+ cf.encode(actsearch.getText().toString())
									+ "%'", null);
					String[] autopolicyno = new String[cur1.getCount()];
					cur1.moveToFirst();
					if (cur1.getCount() != 0) {
						if (cur1 != null) {
							int i = 0;
							do {
								autopolicyno[i] = cf.decode(cur1.getString(cur1
										.getColumnIndex("PolicyNumber")));

								if (autopolicyno[i].contains("null")) {
									autopolicyno[i] = autopolicyno[i].replace(
											"null", "");
								}

								i++;
							} while (cur1.moveToNext());
						}
						cur1.close();
					}

					Cursor cur2 = cf.db.rawQuery("select * from "
							+ cf.Customer_Details + " where ownersName like '"
							+ cf.encode(actsearch.getText().toString()) + "%'",
							null);
					String[] autoownersname = new String[cur2.getCount()];
					cur2.moveToFirst();
					if (cur2.getCount() != 0) {
						if (cur2 != null) {
							int i = 0;
							do {
								autoownersname[i] = cf.decode(cur2
										.getString(cur2
												.getColumnIndex("ownersName")));

								if (autoownersname[i].contains("null")) {
									autoownersname[i] = autoownersname[i]
											.replace("null", "");
								}

								i++;
							} while (cur2.moveToNext());
						}
						cur2.close();
					}

					List<String> images = new ArrayList<String>();
					images.addAll(Arrays.asList(autopolicyno));
					images.addAll(Arrays.asList(autoownersname));

					ArrayAdapter<String> adapter = new ArrayAdapter<String>(
							EmailReport.this, R.layout.autocompletelist, images);
					actsearch.setThreshold(1);
					actsearch.setAdapter(adapter);

				} catch (Exception e) {

				}

			}
		};

		// third, we must add the textWatcher to our EditText
		actsearch.addTextChangedListener(textWatcher);

	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.emailreport_placeorder:
			Intent placeintent=new Intent(EmailReport.this,OrderInspection.class);
			startActivity(placeintent);
			finish();
			break;
		
		case R.id.emailreport_home:
			Intent intent_emailreport = new Intent(EmailReport.this,
					HomeScreen.class);
			startActivity(intent_emailreport);
			finish();
			break;

		case R.id.emailreport_search:
			Search();
			cf.hidekeyboard((EditText)actsearch);
			break;

		case R.id.emailreport_cancel:
			actsearch.setText("");
			cf.CreateTable(22);
			Cursor cur = cf.db.rawQuery("select * from " + cf.Customer_Details, null);
			display_list(cur);
			cur.close();
			cf.hidekeyboard((EditText)actsearch);
			break;
		}
	}

	private void Search() {
		if (actsearch.getText().toString().trim().equals("")) {
			toast = new ShowToast(EmailReport.this,
					"Please enter the Name or Policy Number to search.");
			actsearch.requestFocus();
			actsearch.setText("");
		} else {
			cf.CreateTable(22);
			Cursor cur = cf.db.rawQuery("select * from " + cf.Customer_Details
					+ " where ownersName like '"
					+ cf.encode(actsearch.getText().toString().trim())
					+ "%' or PolicyNumber like '"
					+ cf.encode(actsearch.getText().toString().trim()) + "%'", null);
			
			System.out.println("select * from " + cf.Customer_Details
					+ " where ownersName like '"
					+ cf.encode(actsearch.getText().toString().trim())
					+ "%' or PolicyNumber like '"
					+ cf.encode(actsearch.getText().toString().trim()) + "%'");
			System.out.println("Customer_Details search count is "
					+ cur.getCount());
			
			display_list(cur);
		}
	}
	
	private void display_list(Cursor cur)
	{
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			lldynamic.removeAllViews();
			rlheader.setVisibility(View.VISIBLE);
			int cnt = cur.getCount();
			data=new String[cnt];
			datasend=new String[cnt];
			String[] ownersName=new String[cnt];

			TextView[] tv = new TextView[cnt];
			Button[] sendmail = new Button[cnt];
			LinearLayout.LayoutParams textparams,btnparams;

			int i = 0;
			do {
				String ownersname=cf.decode(cur.getString(cur.getColumnIndex("ownersName")));
				data[i]=ownersname+" | ";
				ownersName[i]=ownersname;
				String policynumber=cf.decode(cur.getString(cur.getColumnIndex("PolicyNumber")));
				data[i] +=policynumber+" | ";
				String email=cf.decode(cur.getString(cur.getColumnIndex("Email")));
				data[i] +=email;
				
//				For sending policy number and status to email report3
				String pn=cf.decode(cur.getString(cur.getColumnIndex("PolicyNumber")));
				datasend[i] =pn+":";
				String status=cf.decode(cur.getString(cur.getColumnIndex("Status")));
				datasend[i] +=status+":"+ownersname;
//				End of For sending policy number and status to email report3
				
				
				LinearLayout.LayoutParams llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);

				LinearLayout ll = new LinearLayout(this);
				ll.setOrientation(LinearLayout.HORIZONTAL);
				ll.setLayoutParams(llparams);
				lldynamic.addView(ll);
				
				Display display = getWindowManager().getDefaultDisplay();
			    DisplayMetrics displayMetrics = new DisplayMetrics();
			    display.getMetrics(displayMetrics);

			    int width = displayMetrics.widthPixels;
			    int height = displayMetrics.heightPixels;
			    
			    System.out.println("The width is "+width);
			    System.out.println("The height is "+height);

				if (width > 1000 || height > 1000) {
					textparams = new LinearLayout.LayoutParams(
							775, ViewGroup.LayoutParams.WRAP_CONTENT);
					textparams.setMargins(20, 20, 20, 20);
//					textparams.gravity = Gravity.CENTER_VERTICAL;

					btnparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					btnparams.setMargins(20, 0, 20, 0);
					btnparams.gravity = Gravity.CENTER_VERTICAL;
					
//					LinearLayout lltext = new LinearLayout(this);
//					ll.addView(lltext);
					
					tv[i] = new TextView(this);
					tv[i].setLayoutParams(textparams);
					tv[i].setText(data[i]);
//					tv[i].setTextColor(Color.parseColor("#76A4C8"));
					tv[i].setTextColor(Color.WHITE);
					tv[i].setTextSize(14);
					ll.addView(tv[i]);
					
//					LinearLayout llbtn = new LinearLayout(this);
//					llbtn.setGravity(Gravity.CENTER_VERTICAL);
//					ll.addView(llbtn);
					
					sendmail[i] = new Button(this);
					sendmail[i].setLayoutParams(btnparams);
					sendmail[i].setText("Send Mail");
					sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
					sendmail[i].setTextColor(0xffffffff);
					sendmail[i].setTextSize(14);
					sendmail[i].setTypeface(null, Typeface.BOLD);
					sendmail[i].setTag(email+"&#40"+policynumber+"&#40"+ownersName[i]+"&#40"+i);
					ll.addView(sendmail[i]);
					
				}
				
				else if((width > 500 && width<1000) || (height > 500 && height<1000))
				{

					textparams = new LinearLayout.LayoutParams(
							500, ViewGroup.LayoutParams.WRAP_CONTENT);
					textparams.setMargins(10, 20, 10, 20);

					btnparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					btnparams.setMargins(10, 0, 10, 0);
					btnparams.gravity = Gravity.CENTER_VERTICAL;
					
					tv[i] = new TextView(this);
					tv[i].setLayoutParams(textparams);
					tv[i].setText(data[i]);
//					tv[i].setTextColor(Color.parseColor("#76A4C8"));
					tv[i].setTextColor(Color.WHITE);
					tv[i].setTextSize(14);
					ll.addView(tv[i]);
					
					sendmail[i] = new Button(this);
					sendmail[i].setLayoutParams(btnparams);
					sendmail[i].setText("Send Mail");
					sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
					sendmail[i].setTextColor(0xffffffff);
					sendmail[i].setTextSize(14);
					sendmail[i].setTypeface(null, Typeface.BOLD);
					sendmail[i].setTag(email+"&#40"+policynumber+"&#40"+ownersName[i]+"&#40"+i);
//					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					ll.addView(sendmail[i]);
				
				}
				
				else
				{
					textparams = new LinearLayout.LayoutParams(
							280, ViewGroup.LayoutParams.WRAP_CONTENT);
					textparams.setMargins(10, 20, 10, 20);

					btnparams = new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT);
					btnparams.setMargins(10, 0, 10, 0);
					btnparams.gravity = Gravity.CENTER_VERTICAL;
					
					tv[i] = new TextView(this);
					tv[i].setLayoutParams(textparams);
					tv[i].setText(data[i]);
//					tv[i].setTextColor(Color.parseColor("#76A4C8"));
					tv[i].setTextColor(Color.WHITE);
					tv[i].setTextSize(14);
					ll.addView(tv[i]);
					
					sendmail[i] = new Button(this);
					sendmail[i].setLayoutParams(btnparams);
					sendmail[i].setText("Send Mail");
					sendmail[i].setBackgroundResource(R.drawable.buttonrepeat);
					sendmail[i].setTextColor(0xffffffff);
					sendmail[i].setTextSize(14);
					sendmail[i].setTypeface(null, Typeface.BOLD);
					sendmail[i].setTag(email+"&#40"+policynumber+"&#40"+ownersName[i]+"&#40"+i);
//					pter[i].setGravity(Gravity.CENTER_VERTICAL);
					ll.addView(sendmail[i]);
				}
				
				sendmail[i].setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String value=v.getTag().toString();
						String[] splitvalue=value.split("&#40");
						mailid=splitvalue[0];
						policyno=splitvalue[1];
						name=splitvalue[2];
						
						String ivalue=splitvalue[3];
						int ival=Integer.parseInt(ivalue);
						String ivalsplit=datasend[ival];
						String[] arrayivalsplit=ivalsplit.split(":");
						String pn=arrayivalsplit[0];
						String status=arrayivalsplit[1];
						String ownersname=arrayivalsplit[2];
						
						System.out.println("Mail id is :"+mailid);
						System.out.println("Policy no is :"+policyno);
						
						Intent intent=new Intent(EmailReport.this,EmailReport2.class);
						intent.putExtra("policynumber", pn);
						intent.putExtra("status", status);
						intent.putExtra("mailid", mailid);
						intent.putExtra("classidentifier", "EmailReport");
						intent.putExtra("ownersname", ownersname);
						startActivity(intent);
						finish();
						
					}
				});
				
				if (i % 2 == 0) {
					ll.setBackgroundColor(Color.parseColor("#13456d"));
				} else {
					ll.setBackgroundColor(Color.parseColor("#386588"));
				}
				i++;
			} while (cur.moveToNext());
		} else {
			rlheader.setVisibility(View.GONE);
			lldynamic.removeAllViews();
			toast = new ShowToast(EmailReport.this,
					"Sorry, No results available");
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent_emailreport = new Intent(EmailReport.this,
				HomeScreen.class);
		startActivity(intent_emailreport);
		finish();
	}

}
